require "csv"
require "json"

def load_touindex(filename,ratio)
  json = File.read(filename)
  hash = JSON.parse(json)
  index = {}
  ret = {}

  hash.each do |key,value|
    index[ key.split("-",3)[0] + "-" + key.split("-",3)[2].delete("p") ] = value
  end

  index.each do |k1,v1|
    index.each do |k2,v2|
      d1 = (v1["COOH"] + v1["OH"] + v1["NH2"] + v1["NOC"]) - ( v2["COOH"] + v2["OH"] + v2["NH2"] + v2["NOC"] ).abs
      d2 = (v1["COOH"].to_i - v2["COOH"].to_i ).abs
      d3 = (v1["NH2"] - v2["NH2"] ).abs
      d4 = (v1["NOC"] - v2["NOC"] ).abs
      distance = d1 + d2 + d3 + d4
      ret[k1 + "," + k2] = distance * ratio.to_f
    end
  end
  return ret
end

def load_3distaces(filename)
  data = CSV.read(filename, headers: false)
  distancesrr = []
  score = {}

  data.each do |d|
    distance = d[2].to_f
    distancesrr.push( distance)
  end

  distancesrr.each_with_index do |v, id|
    anomer1 = data[id][0].split("-", 3)[0]
    node1 = data[id][0].split("-", 3)[2]
    anomer2 = data[id][1].split("-", 3)[0]
    node2 = data[id][1].split("-", 3)[2]
    key = anomer1 + "-" + node1.delete("p") + "," + anomer2 + "-" + node2.delete("p")
    score[key] = v
    key = anomer2 + "-" + node2.delete("p") + "," + anomer1 + "-" + node1.delete("p")
    score[key] = v
  end
  return score
end

ratio = ARGV[0]
p ratio

three_distance = "data/distance.csv"
tou_index = "data/touindex.json"

$distances = load_3distaces(three_distance)
$index = load_touindex(tou_index, ratio)

ret_str = ""
$distances.each do |key,value|
  ret_str = ret_str + key.downcase + "," + (value.to_f + $index[key].to_f).to_s + "\n"
end

File.open("data/mix_distance.csv", mode = "w"){|f|
    f.write(ret_str)
}

sim = ""
data = ret_str.split("\n")
data.each do |d|
  v = d.split(",")
  distance = v[2].to_f
  sim = sim + v[0] + "," + v[1] + "," + (1 / (1 + distance)).to_s + "\n"
end

File.open("data/similarity.csv", mode = "w"){|f|
    f.write(sim)
}

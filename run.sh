#!/bin/bash

#ARRAY=(0 0.2 0.4 0.6 0.8 1 1.2 1.4 1.6 1.8 2)
ARRAY=( 2 )

for i in ${ARRAY[@]}; do
  ruby ./replaceSM/main.rb $i
  ruby ./script/clustering.rb
  Rscript ./script/clustering.R $i
  mv *.svg /mnt/c/Users/skit/Desktop/
  rm *.pdf
done

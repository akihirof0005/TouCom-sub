ENTRY       G05242                      Glycan
NODE        8
            1   Xyl         0     0
            2   LRha      -10     0
            3   Xyl       -20     0
            4   LRha      -30     0
            5   Xyl       -40     0
            6   Glc       -50     0
            7   Glc       -60     5
            8   Xyl       -60    -5
EDGE        7
            1     2:a1    1:2  
            2     3:b1    2:3  
            3     4:a1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:6  
            7     8:b1    6:4  
///

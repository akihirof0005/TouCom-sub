ENTRY       G05215                      Glycan
NODE        8
            1   GlcNAc      0     0
            2   Gal       -10     5
            3   LFuc      -10    -5
            4   GlcNAc    -20     5
            5   Gal       -30    10
            6   LFuc      -30     0
            7   GlcNAc    -40    10
            8   Gal       -50    10
EDGE        7
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    2:3  
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     7:b1    5:3  
            7     8:b1    7:4  
///

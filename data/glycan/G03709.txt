ENTRY       G03709                      Glycan
NODE        17
            1   GlcNAc     25     0
            2   GlcNAc     15     0
            3   Man         6     0
            4   Man        -2     6
            5   Man        -2    -6
            6   GlcNAc    -10    10
            7   GlcNAc    -10     2
            8   GlcNAc    -10    -2
            9   GlcNAc    -10   -10
            10  Gal       -18    10
            11  Gal       -18     2
            12  Gal       -18    -2
            13  Gal       -18   -10
            14  Neu5Ac    -26    10
            15  Neu5Ac    -26     2
            16  Neu5Ac    -26    -2
            17  Neu5Ac    -26   -10
EDGE        16
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:6  
            6     7:b1    4:2  
            7     8:b1    5:4  
            8     9:b1    5:2  
            9    10:b1    6:4  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:a2   10:6  
            14   15:a2   11:6  
            15   16:a2   12:6  
            16   17:a2   13:6  
///

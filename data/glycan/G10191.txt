ENTRY       G10191                      Glycan
NODE        3
            1   Ino       7.2   0.5
            2   Gal       0.2   0.5
            3   Gal      -6.8   0.5
EDGE        2
            1     2:1     1    
            2     3:1     2    
///

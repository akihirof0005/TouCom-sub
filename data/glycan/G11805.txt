ENTRY       G11805                      Glycan
NODE        6
            1   *          18     0
            2   LAltA       9     0
            3   GlcA        0     0
            4   Glc        -8     4
            5   QuiNAc     -9    -4
            6   *         -19    -4
EDGE        5
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    3:3  
            5     6       5:3  
BRACKET     1 -14.0   7.0 -14.0  -9.0
            1  14.0  -9.0  14.0   7.0
            1 n
///

ENTRY       G02314                      Glycan
NODE        5
            1   Man        15     2
            2   Gal         5     2
            3   P          -1    -2
            4   Gal        -5     2
            5   Gal       -15     2
EDGE        4
            1     2:b1    1:4  
            2     3       2:6  
            3     4:b1    2:3  
            4     5:b1    4:3  
///

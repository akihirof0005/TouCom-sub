ENTRY       G05166                      Glycan
NODE        8
            1   Cer        30     0
            2   Glc        22     0
            3   Gal        14     0
            4   GlcNAc      5     0
            5   Gal        -4     0
            6   GlcNAc    -13     0
            7   Gal       -22     0
            8   LFuc      -30     0
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:a1    7:2  
///

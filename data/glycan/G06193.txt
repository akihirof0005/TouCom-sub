ENTRY       G06193                      Glycan
NODE        6
            1   LAra        0     0
            2   LRha      -10     0
            3   Xyl       -20     0
            4   LRha      -30     0
            5   Xyl       -40     0
            6   Xyl       -50     0
EDGE        5
            1     2:a1    1:2  
            2     3:b1    2:3  
            3     4:a1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
///

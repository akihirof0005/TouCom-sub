ENTRY       G12603                      Glycan
NODE        9
            1   *          23     2
            2   Ribf       13     2
            3   Glc         4     6
            4   LRha        4    -2
            5   GlcNAc     -5     2
            6   LRha       -5    -6
            7   GalA      -14     2
            8   Galf      -14    -6
            9   *         -24     2
EDGE        8
            1     2:b1    1    
            2     3:a1    2:5  
            3     4:a1    2:2  
            4     5:b1    4:3  
            5     6:a1    4:2  
            6     7:a1    5:3  
            7     8:a1    6:2  
            8     9       7:4  
BRACKET     1 -18.0  10.0 -18.0  -9.0
            1  18.0  -9.0  18.0  10.0
            1 n
///

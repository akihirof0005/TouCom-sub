ENTRY       G05032                      Glycan
NODE        8
            1   Asn        26     0
            2   GlcNAc     18     0
            3   GlcNAc      8     0
            4   Man        -1     0
            5   Man        -9     4
            6   Man        -9    -4
            7   GlcNAc    -18    -4
            8   Glc       -26    -4
EDGE        7
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    6:2  
            7     8:b1    7:4  
///

ENTRY       G01297                      Glycan
NODE        3
            1   GalNAc      5     0
            2   Gal        -5     5
            3   Gal        -5    -5
EDGE        2
            1     2:b1    1:4  
            2     3:b1    1:3  
///

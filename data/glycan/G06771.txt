ENTRY       G06771                      Glycan
NODE        5
            1   GlcNAc     15    -2
            2   S          10     2
            3   GlcNAc      5    -2
            4   GlcNAc     -5    -2
            5   GlcNX     -15    -2
EDGE        4
            1     2       1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
///

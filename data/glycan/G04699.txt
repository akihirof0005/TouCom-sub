ENTRY       G04699                      Glycan
NODE        10
            1   GlcNAc      0     0
            2   LFuc      -10     5
            3   GlcNAc    -10    -5
            4   Man       -20    -5
            5   Man       -30     0
            6   Man       -30   -10
            7   Man       -40     5
            8   Man       -40    -5
            9   GlcNAc    -40   -10
            10  Gal       -50   -10
EDGE        9
            1     2:a1    1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:6  
            7     8:a1    5:2  
            8     9:b1    6:2  
            9    10:b1    9:4  
///

ENTRY       G04871                      Glycan
NODE        10
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20    10
            4   GlcNAc    -20   -10
            5   Gal       -30    15
            6   LFuc      -30     5
            7   Gal       -30    -5
            8   LFuc      -30   -15
            9   GlcNAc    -40    15
            10  Gal       -50    15
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:4  
            5     6:a1    3:3  
            6     7:b1    4:4  
            7     8:a1    4:3  
            8     9:b1    5:3  
            9    10:b1    9:3  
///

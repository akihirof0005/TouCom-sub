ENTRY       G04268                      Glycan
NODE        11
            1   Kdo        32     0
            2   Lgro-manHep    22     0
            3   P          15     4
            4   P          11     4
            5   Lgro-manHep     8     0
            6   Lgro-manHep    -2     5
            7   Glc        -2    -5
            8   GalA      -12     5
            9   Glc       -12    -5
            10  Gal       -22    -5
            11  Glc       -32    -5
EDGE        10
            1     2:a1    1:5  
            2     3       2:4  
            3     5:a1    2:3  
            4     4       3    
            5     6:a1    5:7  
            6     7:a1    5:3  
            7     8:b1    6:7  
            8     9:a1    7:4  
            9    10:a1    9:2  
            10   11:a1   10:2  
///

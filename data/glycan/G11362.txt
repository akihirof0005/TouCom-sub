ENTRY       G11362                      Glycan
NODE        8
            1   Glc        25     0
            2   Glc        15     0
            3   Glc         5     0
            4   Xyl        -5     5
            5   Glc        -5    -5
            6   LAraf     -15     5
            7   Xyl       -15    -5
            8   Gal       -25    -5
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    3:4  
            5     6:a1    4:2  
            6     7:a1    5:6  
            7     8:b1    7:2  
///

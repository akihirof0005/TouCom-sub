ENTRY       G05609                      Glycan
NODE        7
            1   Cer      19.2   0.5
            2   Glc      12.2   0.5
            3   Gal       5.2   0.5
            4   GalNAc   -2.8   5.5
            5   Neu5Ac   -2.8  -4.5
            6   Gal     -11.8   5.5
            7   LFuc    -19.8   5.5
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:2     3:3  
            5     6:b1    4:3  
            6     7:a1    6:3  
///

ENTRY       G01356                      Glycan
NODE        5
            1   Man        20     0
            2   Man        10     0
            3   Man         0     0
            4   Man       -10     0
            5   Man       -20     0
EDGE        4
            1     2:a1    1:2  
            2     3:a1    2:2  
            3     4:a1    3:3  
            4     5:a1    4:3  
///

ENTRY       G11460                      Glycan
NODE        9
            1   Lgro-manHep    37     0
            2   Lgro-manHep    23     0
            3   Glc4Ac     13     4
            4   Lgro-manHep    10    -4
            5   Lgro-manHep3Ac    -4    -4
            6   Glc       -15    -4
            7   Gal       -22    -4
            8   Gal       -29    -4
            9   GalNAc    -38    -4
EDGE        8
            1     2:a1    1:5  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:a1    4:2  
            5     6:b1    5:2  
            6     7:b1    6:4  
            7     8:a1    7:4  
            8     9:b1    8:3  
///

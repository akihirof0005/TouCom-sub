ENTRY       G10375                      Glycan
NODE        4
            1   LCym     17.8   0.1
            2   LCym      8.8   0.1
            3   2,6daraHex  -3.2   0.1
            4   2,6daraHex3Me -18.2   0.1
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
///

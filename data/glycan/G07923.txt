ENTRY       G07923                      Glycan
NODE        5
            1   Glc        18     0
            2   Glc        11     0
            3   3,6-Anhydro-Glc     0     0
            4   Glc       -11     0
            5   Glc       -18     0
EDGE        4
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
///

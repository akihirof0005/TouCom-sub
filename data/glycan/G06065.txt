ENTRY       G06065                      Glycan
NODE        5
            1   Lgro-manHep    18     0
            2   Lgro-manHep     4     0
            3   Glc        -8    -5
            4   Lgro-manHep   -11     5
            5   Gal       -18    -5
EDGE        4
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:a1    2:7  
            4     5:a1    3:3  
///

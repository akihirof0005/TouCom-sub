ENTRY       G11505                      Glycan
NODE        4
            1   GalNAc     10     0
            2   Gal         0     5
            3   Gal         0    -5
            4   GlcNAc    -10     5
EDGE        3
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:3  
///

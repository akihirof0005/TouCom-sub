ENTRY       G11583                      Glycan
NODE        6
            1   *          18     1
            2   Gal         8     1
            3   GalNAc     -1     5
            4   Neu5Ac     -1    -4
            5   Galf      -11     5
            6   *         -19     5
EDGE        5
            1     2:b1    1    
            2     3:b1    2:3  
            3     4:a2    2:6  
            4     5:b1    3:3  
            5     6       5:6  
BRACKET     1 -16.0   9.0 -16.0  -6.0
            1  15.0  -6.0  15.0   9.0
            1 n
///

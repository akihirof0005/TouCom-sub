ENTRY       G08680                      Glycan
NODE        4
            1   Man        10     0
            2   Man         0     0
            3   P          -5     0
            4   Me        -10     0
EDGE        3
            1     2:a1    1:2  
            2     3       2    
            3     4       3    
///

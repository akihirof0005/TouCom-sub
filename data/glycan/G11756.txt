ENTRY       G11756                      Glycan
NODE        6
            1   *          22     0
            2   GalA       13     0
            3   GlcNAc      3     0
            4   Gal        -6     0
            5   Ribf      -14     0
            6   *         -23     0
EDGE        5
            1     2:a1    1    
            2     3:a1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6       5:2  
BRACKET     1 -19.0   8.0 -19.0  -8.0
            1  18.0  -8.0  18.0   8.0
            1 n
///

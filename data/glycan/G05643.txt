ENTRY       G05643                      Glycan
NODE        5
            1   Ser/Thr  12.2   0.5
            2   Gal       4.2   0.5
            3   Gal      -3.8   5.5
            4   Neu5Ac   -4.8  -4.5
            5   Neu5Ac  -11.8   5.5
EDGE        4
            1     2:1     1    
            2     3:b1    2:3  
            3     4:a2    2    
            4     5:a2    3    
///

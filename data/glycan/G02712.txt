ENTRY       G02712                      Glycan
NODE        8
            1   Cer        22     0
            2   Glc        13     0
            3   Gal         4     0
            4   GalNAc     -5     5
            5   Neu5Ac     -5    -5
            6   Gal       -14     5
            7   Neu5Ac    -15    -5
            8   Gal       -22     5
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
            6     7:a2    5:8  
            7     8:a1    6:3  
///

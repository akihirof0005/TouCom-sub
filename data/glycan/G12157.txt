ENTRY       G12157                      Glycan
NODE        6
            1   Asn        18     0
            2   GlcNAc      9     0
            3   GlcNAc     -1     0
            4   Man       -11     0
            5   Man       -19     3
            6   Man       -19    -3
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
///

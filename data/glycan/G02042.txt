ENTRY       G02042                      Glycan
NODE        4
            1   Glc        15     0
            2   Gal         5     0
            3   Glc        -5     0
            4   Gal       -15     0
EDGE        3
            1     2:a1    1:6  
            2     3:b1    2:6  
            3     4:a1    3:6  
///

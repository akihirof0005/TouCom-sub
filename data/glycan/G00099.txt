ENTRY       G00099                      Glycan
NODE        7
            1   Cer      19.2  -0.2
            2   Glc      13.2  -0.2
            3   Gal       7.2  -0.2
            4   Gal       1.2  -0.2
            5   GalNAc   -5.8  -0.2
            6   Gal     -12.8  -0.2
            7   LFuc    -18.8  -0.2
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:3  
            6     7:a1    6:2  
///

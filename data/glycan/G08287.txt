ENTRY       G08287                      Glycan
NODE        4
            1   Lgro-manHep    13     1
            2   Lgro-manHep     0     1
            3   Glc       -10    -4
            4   Lgro-manHep   -13     5
EDGE        3
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:1     2:7  
///

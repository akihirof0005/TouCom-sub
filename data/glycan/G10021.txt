ENTRY       G10021                      Glycan
NODE        4
            1   Cer      15.6  -0.1
            2   Glc       5.6  -0.1
            3   Gal      -4.4  -0.1
            4   Neu5Ac4Gc -15.4  -0.1
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
///

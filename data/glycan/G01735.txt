ENTRY       G01735                      Glycan
NODE        4
            1   GlcNAc     15     0
            2   Man         5     0
            3   Man        -5     0
            4   Man       -15     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    3:6  
///

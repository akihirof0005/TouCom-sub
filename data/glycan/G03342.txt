ENTRY       G03342                      Glycan
NODE        3
            1   Man       4.6  -0.4
            2   Abe      -4.4   4.6
            3   Glc      -4.4  -5.4
EDGE        2
            1     2:a1    1:3  
            2     3:b1    1:2  
///

ENTRY       G01031                      Glycan
NODE        3
            1   Glc         5     0
            2   Glc        -5     5
            3   Glc        -5    -5
EDGE        2
            1     2:a1    1:6  
            2     3:a1    1:4  
///

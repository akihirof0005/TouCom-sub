ENTRY       G02955                      Glycan
NODE        5
            1   Cer      14.6  -0.1
            2   Glc       4.6  -0.1
            3   Gal      -5.4  -0.1
            4   GalNAc  -15.4   4.9
            5   Neu5Ac1N -15.4  -5.1
EDGE        4
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
///

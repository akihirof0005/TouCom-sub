ENTRY       G05460                      Glycan
NODE        8
            1   GalNAc     20     0
            2   GlcNAc     10     5
            3   GlcNAc     10    -5
            4   Gal         0     5
            5   Gal         0    -5
            6   S          -6    -5
            7   GlcNAc    -10     5
            8   Gal       -20     5
EDGE        7
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:b1    3:3  
            5     7:b1    4:3  
            6     6       5:6  
            7     8:b1    7:4  
///

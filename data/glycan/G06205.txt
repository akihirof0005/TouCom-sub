ENTRY       G06205                      Glycan
NODE        7
            1   GlcNAc   17.2  -2.5
            2   LFuc      8.2  -7.5
            3   GlcNAc    7.2   2.5
            4   Man      -1.8   2.5
            5   Man      -9.8   7.5
            6   Man      -9.8  -2.5
            7   GlcNAc  -17.8   7.5
EDGE        6
            1     2:a1    1:3  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5    
///

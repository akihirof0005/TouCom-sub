ENTRY       G04480                      Glycan
NODE        11
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   GlcNAc    -30     0
            5   Gal       -40     0
            6   GlcNAc    -50     0
            7   Gal       -60     5
            8   LFuc      -60    -5
            9   GlcNAc    -70     5
            10  Gal       -80    10
            11  LFuc      -80     0
EDGE        10
            1     2:b1    1:1  
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:a1    6:3  
            8     9:b1    7:3  
            9    10:b1    9:4  
            10   11:a1    9:3  
///

ENTRY       G04170                      Glycan
NODE        12
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     5
            4   Man       -20    -5
            5   GlcNAc    -30     5
            6   GlcNAc    -30     0
            7   GlcNAc    -30   -10
            8   Gal       -40     5
            9   Gal       -40     0
            10  Gal       -40   -10
            11  GlcNAc    -50     0
            12  Gal       -60     0
EDGE        11
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:b1    3:2  
            5     6:b1    4:4  
            6     7:b1    4:2  
            7     8:b1    5:4  
            8     9:b1    6:4  
            9    10:b1    7:4  
            10   11:b1    9:3  
            11   12:b1   11:4  
///

ENTRY       G07898                      Glycan
NODE        4
            1   Gal         0     0
            2   GlcNAc    -10     0
            3   Gal       -20     0
            4   LFuc      -30     0
EDGE        3
            1     2:b1    1:6  
            2     3:b1    2:3  
            3     4:a1    3:2  
///

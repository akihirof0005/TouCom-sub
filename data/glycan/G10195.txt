ENTRY       G10195                      Glycan
NODE        3
            1   Glc         0     0
            2   GalA      -10     0
            3   GlcA      -20     0
EDGE        2
            1     2:1     1:4  
            2     3:1     2:3  
///

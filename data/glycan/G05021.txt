ENTRY       G05021                      Glycan
NODE        9
            1   GlcNAc     21     3
            2   GlcNAc     11     3
            3   Man         2     3
            4   Man        -6     8
            5   Man        -6    -2
            6   Man       -14     3
            7   Man       -14    -7
            8   Man       -22     3
            9   Man       -22    -7
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    5:6  
            6     7:a1    5:2  
            7     8:a1    6:2  
            8     9:a1    7:2  
///

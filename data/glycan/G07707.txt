ENTRY       G07707                      Glycan
NODE        5
            1   LFucNAc     0     0
            2   LRha      -10     0
            3   Glc       -20     5
            4   GlcNAc    -20    -5
            5   Glc       -30     5
EDGE        4
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:b1    2:2  
            4     5:a1    3:6  
///

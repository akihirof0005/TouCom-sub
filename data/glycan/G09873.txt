ENTRY       G09873                      Glycan
NODE        4
            1   Glc      15.4     0
            2   Glc       6.4     0
            3   3dribHex  -3.6     0
            4   3dribHex -15.6     0
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    3:6  
///

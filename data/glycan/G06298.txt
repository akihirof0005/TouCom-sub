ENTRY       G06298                      Glycan
NODE        4
            1   Gal         0     0
            2   Neu5Ac    -10     5
            3   Gal       -10    -5
            4   Neu5Ac    -20    -5
EDGE        3
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:a2    3:3  
///

ENTRY       G04468                      Glycan
NODE        10
            1   GalNAc     26   0.5
            2   Gal        17   0.5
            3   GlcNAc      8   0.5
            4   Gal        -1   0.5
            5   GlcNAc    -10   5.5
            6   GlcNAc    -10  -4.5
            7   Gal       -18   5.5
            8   Gal       -18  -4.5
            9   Gal       -25   5.5
            10  Neu5Gc    -27  -4.5
EDGE        9
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:6  
            5     6:b1    4:3  
            6     7:1     5:4  
            7     8:1     6:4  
            8     9:1     7:3  
            9    10:a2    8:3  
///

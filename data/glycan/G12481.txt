ENTRY       G12481                      Glycan
NODE        3
            1   GalNAc      4     0
            2   S          -4     4
            3   Gal        -5    -4
EDGE        2
            1     2       1:6  
            2     3:b1    1:3  
///

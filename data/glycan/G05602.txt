ENTRY       G05602                      Glycan
NODE        6
            1   GlcNAc      0     0
            2   GlcNAc    -10     5
            3   LFuc      -10    -5
            4   Gal       -20    10
            5   LFuc      -20     0
            6   Neu5Ac    -30    10
EDGE        5
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    2:4  
            4     5:a1    2:3  
            5     6:a2    4:3  
///

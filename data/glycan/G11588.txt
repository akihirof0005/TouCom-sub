ENTRY       G11588                      Glycan
NODE        5
            1   GalNAc     15     0
            2   Gal         4     0
            3   GlcNAc     -6     5
            4   Gal        -6    -5
            5   LFuc      -16     5
EDGE        4
            1     2:b1    1:3  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:a1    3:3  
///

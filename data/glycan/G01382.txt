ENTRY       G01382                      Glycan
NODE        4
            1   Glc        10     0
            2   Glc         0     0
            3   Glc       -10     5
            4   Glc       -10    -5
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:4  
///

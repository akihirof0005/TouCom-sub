ENTRY       G07396                      Glycan
NODE        4
            1   Gal         0     0
            2   GlcNAc    -10     0
            3   Gal       -20     5
            4   LFuc      -20    -5
EDGE        3
            1     2:a1    1:3  
            2     3:b1    2:4  
            3     4:a1    2:3  
///

ENTRY       G08369                      Glycan
NODE        4
            1   D/L3-en-4dHex-ol  12.2   0.5
            2   GlcNAc   -1.8   0.5
            3   Gal     -11.8   5.5
            4   LFuc    -11.8  -4.5
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    2:3  
///

ENTRY       G02788                      Glycan
NODE        7
            1   Cer        19     0
            2   Glc        12     0
            3   Gal         5     0
            4   GalNAc     -3     5
            5   Neu5Ac9Ac    -4    -5
            6   Gal       -11     5
            7   Neu5Ac9Ac   -20     5
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
            6     7:a2    6:3  
///

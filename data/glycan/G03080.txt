ENTRY       G03080                      Glycan
NODE        6
            1   Cer         0     0
            2   Glc       -10     0
            3   GlcNAc    -20     0
            4   GalNAc    -30     5
            5   LFuc      -30    -5
            6   LFuc      -40     5
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6:a1    4:3  
///

ENTRY       G02131                      Glycan
NODE        4
            1   LRha       10     4
            2   Glc        10    -4
            3   LRha        0     4
            4   LRha      -10     4
EDGE        3
            1     2:1     1:a1 
            2     3:a1    1:2  
            3     4:a1    3:2  
///

ENTRY       G05934                      Glycan
NODE        7
            1   Kdo        21     3
            2   Lgro-manHep    11     3
            3   GlcN       -1     8
            4   GlcA       -1    -2
            5   Glc       -11    -2
            6   GalAN     -21     3
            7   GlcN      -21    -7
EDGE        6
            1     2:a1    1:5  
            2     3:a1    2:7  
            3     4:b1    2:2  
            4     5:b1    4:3  
            5     6:a1    5:6  
            6     7:a1    5:4  
///

ENTRY       G07769                      Glycan
NODE        4
            1   Glc         0     0
            2   LAra      -10     0
            3   LAraf     -20     5
            4   Glc       -20    -5
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:b1    2:2  
///

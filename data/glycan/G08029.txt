ENTRY       G08029                      Glycan
NODE        3
            1   Gal       4.6  -0.4
            2   Gal      -4.4   4.6
            3   Col      -4.4  -5.4
EDGE        2
            1     2:a1    1:3  
            2     3:a1    1:2  
///

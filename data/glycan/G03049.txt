ENTRY       G03049                      Glycan
NODE        5
            1   Gal         0     0
            2   Glc       -10     0
            3   Glc       -20     5
            4   Xyl       -20    -5
            5   Xyl       -30    -5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    2:2  
            4     5:b1    4:3  
///

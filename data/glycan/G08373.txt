ENTRY       G08373                      Glycan
NODE        3
            1   GalA6Me     0     0
            2   GalA      -10     0
            3   GalA      -20     0
EDGE        2
            1     2:a1    1:4  
            2     3:a1    2:4  
///

ENTRY       G12714                      Glycan
NODE        11
            1   Cer        34     0
            2   Glc        27     0
            3   Gal        21     0
            4   GlcNAc     14     0
            5   Gal         5     0
            6   GlcNAc     -3     0
            7   Gal       -11     0
            8   GalNAc    -19     0
            9   Gal       -27     0
            10  LFuc      -34    -4
            11  GalNAc    -35     4
EDGE        10
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:b1    7:3  
            8     9:b1    8:4  
            9    10:a1    9:2  
            10   11:a1    9:3  
///

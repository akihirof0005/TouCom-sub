ENTRY       G07166                      Glycan
NODE        14
            1   Asn        29    -2
            2   GlcNAc     21    -2
            3   GlcNAc     11    -2
            4   Man         2    -2
            5   Man        -6     3
            6   Man        -6    -7
            7   GlcNAc    -14     7
            8   GlcNAc    -14    -1
            9   GlcNAc    -14    -7
            10  Gal       -22     7
            11  Gal       -22    -1
            12  Gal       -22    -7
            13  Neu5Ac    -30    -1
            14  Neu5Ac    -30    -7
EDGE        13
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   13:a2   11:6  
            13   14:a2   12:6  
///

ENTRY       G00149                      Glycan
NODE        7
            1   Ino(acyl)-P    15    -2
            2   GlcN        5    -2
            3   EtN         0     2
            4   P          -2     2
            5   Man        -2    -2
            6   Man        -9    -2
            7   Man       -16    -2
EDGE        6
            1     2:a1    1:6  
            2     5:a1    2:4  
            3     4       3    
            4     5:2     4    
            5     6:a1    5:6  
            6     7:a1    6:2  
///

ENTRY       G07270                      Glycan
NODE        4
            1   Man        10     2
            2   GlcNAc      0     2
            3   S          -5    -1
            4   Gal       -10     2
EDGE        3
            1     2:b1    1:6  
            2     3       2:6  
            3     4:b1    2:4  
///

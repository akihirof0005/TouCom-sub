ENTRY       G11950                      Glycan
NODE        12
            1   GlcNAc     27     2
            2   LFuc       17     5
            3   GlcNAc     17    -1
            4   Man         8    -1
            5   Man        -1     3
            6   Man        -1    -5
            7   GlcNAc     -2    -1
            8   Man        -9     6
            9   Man        -9     0
            10  GlcNAc    -10    -5
            11  Gal       -19    -5
            12  Neu5Ac    -28    -5
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:4  
            7     8:a1    5:6  
            8     9:a1    5:3  
            9    10:b1    6:2  
            10   11:b1   10:4  
            11   12:a2   11:6  
///

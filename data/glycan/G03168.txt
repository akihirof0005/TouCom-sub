ENTRY       G03168                      Glycan
NODE        4
            1   GlcNAc    9.2   0.5
            2   Gal       0.2  -4.5
            3   L4,6dxylHex  -2.8   5.5
            4   LFuc     -8.8  -4.5
EDGE        3
            1     2:b1    1:3  
            2     3:a1    1:4  
            3     4:a1    2:2  
///

ENTRY       G05229                      Glycan
NODE        8
            1   Cer        25     0
            2   Glc        17     0
            3   Gal         9     0
            4   GlcNAc      0     0
            5   LFuc       -8     5
            6   Gal        -8    -5
            7   GlcNAc    -17    -5
            8   Gal       -26    -5
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:a1    4:4  
            5     6:b1    4:3  
            6     7:b1    6:3  
            7     8:b1    7:3  
///

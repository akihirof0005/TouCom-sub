ENTRY       G06317                      Glycan
NODE        18
            1   GlcNAc      0     0
            2   LFuc      -10     5
            3   GlcNAc    -10    -5
            4   Man       -20    -5
            5   Man       -30     5
            6   Man       -30   -15
            7   GlcNAc    -40    10
            8   GlcNAc    -40     0
            9   GlcNAc    -40   -10
            10  GlcNAc    -40   -20
            11  Gal       -50    10
            12  GalNAc    -50     0
            13  Gal       -50   -10
            14  Gal       -50   -20
            15  Neu5Ac    -60    10
            16  Neu5Ac    -60     0
            17  Neu5Ac    -60   -10
            18  Neu5Ac    -60   -20
EDGE        17
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:4  
            14   15:a2   11:3  
            15   16:a2   12:6  
            16   17:a2   13:3  
            17   18:a2   14:3  
///

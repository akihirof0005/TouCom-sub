ENTRY       G11270                      Glycan
NODE        7
            1   GalNAc     15     0
            2   Gal         5     0
            3   Gal        -5     5
            4   Gal        -5     0
            5   LFuc       -5    -5
            6   GalNAc    -15     5
            7   GlcA      -15     0
EDGE        6
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    2:3  
            4     5:a1    2:2  
            5     6:a1    3:4  
            6     7:b1    4:3  
///

ENTRY       G00015                      Glycan
NODE        8
            1   Asn        20     0
            2   GlcNAc     12     0
            3   GlcNAc      3     0
            4   Man        -5     0
            5   Man       -12     5
            6   Man       -12    -5
            7   GlcNAc    -20     5
            8   GlcNAc    -20    -5
EDGE        7
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
///

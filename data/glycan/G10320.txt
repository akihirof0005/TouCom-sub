ENTRY       G10320                      Glycan
NODE        6
            1   Cer        23     0
            2   Glc        15     0
            3   Neu5Ac      6     0
            4   Neu5Ac     -4     0
            5   Neu5Ac    -14     0
            6   Neu5Ac    -24     0
EDGE        5
            1     2:b1    1:1  
            2     3:a2    2:3  
            3     4:a2    3:8  
            4     5:a2    4:8  
            5     6:a2    5:8  
///

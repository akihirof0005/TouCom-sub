ENTRY       G04566                      Glycan
NODE        8
            1   GalNAc     13    -2
            2   Gal         4    -7
            3   GlcNAc      3     3
            4   S          -3     7
            5   Neu5Ac     -5    -7
            6   Gal        -6     3
            7   LFuc       -6    -2
            8   LFuc      -14     3
EDGE        7
            1     2:b1    1:3  
            2     3:b1    1:6  
            3     5:a2    2:3  
            4     4       3:6  
            5     6:b1    3:4  
            6     7:a1    3:3  
            7     8:a1    6:2  
///

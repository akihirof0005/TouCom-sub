ENTRY       G00732                      Glycan
NODE        10
            1   Glc        15     2
            2   Glc         7     2
            3   Xyl         0     6
            4   Glc         0    -2
            5   Xyl        -7     2
            6   Glc        -7    -6
            7   Gal        -8     6
            8   Gal       -15     2
            9   Xyl       -15    -6
            10  LFuc      -16     6
EDGE        9
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     7:b1    3:2  
            5     5:a1    4:6  
            6     6:b1    4:4  
            7     8:b1    5:2  
            8     9:a1    6:6  
            9    10:a1    7:2  
///

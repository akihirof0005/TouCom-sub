ENTRY       G07232                      Glycan
NODE        5
            1   GalNAc     11    -2
            2   GlcNAc      1     3
            3   Gal         1    -7
            4   Gal        -9     3
            5   S         -12     8
EDGE        4
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5       4:6  
///

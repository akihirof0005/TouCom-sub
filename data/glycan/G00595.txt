ENTRY       G00595                      Glycan
NODE        3
            1   GlcNAc    8.5  -0.2
            2   Man      -0.5  -0.2
            3   Man      -8.5  -0.2
EDGE        2
            1     2:b1    1:4  
            2     3:a1    2:6  
///

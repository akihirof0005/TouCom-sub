ENTRY       G06169                      Glycan
NODE        6
            1   Cer      19.2   0.5
            2   Glc      11.2   0.5
            3   Gal       4.2   0.5
            4   GlcNAc   -3.8   0.5
            5   Gal     -11.8   0.5
            6   LFuc    -18.8   0.5
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2    
            3     4:b1    3    
            4     5:b1    4    
            5     6:a1    5    
///

ENTRY       G00352                      Glycan
NODE        11
            1   GlcNAc     22     1
            2   GlcNAc     12     1
            3   Man         3     1
            4   Man        -6     6
            5   GlcNAc     -6     1
            6   Man        -6    -5
            7   Man       -13    10
            8   Man       -13     2
            9   GlcNAc    -14    -1
            10  GlcNAc    -14    -9
            11  Gal       -23    -1
EDGE        10
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    3:4  
            5     6:a1    3:3  
            6     7:a1    4:6  
            7     8:a1    4:3  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    9:4  
///

ENTRY       G11376                      Glycan
NODE        9
            1   GlcNAc     22     2
            2   LFuc       14     6
            3   GlcNAc     14    -2
            4   Man         6    -2
            5   Man        -1     2
            6   Man        -1    -6
            7   Man        -8    -6
            8   Man       -15    -6
            9   LFuc      -22    -6
EDGE        8
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    6:2  
            7     8:a1    7:2  
            8     9:a1    8:2  
///

ENTRY       G06930                      Glycan
NODE        5
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     0
            4   GlcNAc    -30     5
            5   GlcNAc    -30    -5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    3:6  
            4     5:b1    3:2  
///

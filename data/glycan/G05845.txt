ENTRY       G05845                      Glycan
NODE        8
            1   GlcNAc   18.2  -2.5
            2   LFuc      9.2  -7.5
            3   GlcNAc    8.2   2.5
            4   Man      -0.8   2.5
            5   Man      -8.8   7.5
            6   Xyl      -8.8  -2.5
            7   Man      -9.8   2.5
            8   GlcNAc  -17.8   7.5
EDGE        7
            1     2:a1    1:3  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:b1    4:2  
            6     7:a1    4:3  
            7     8:b1    5    
///

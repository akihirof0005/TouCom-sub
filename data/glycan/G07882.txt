ENTRY       G07882                      Glycan
NODE        4
            1   Cer      14.2   0.5
            2   Glc       8.2   0.5
            3   Gal       2.2   0.5
            4   L3daraHep-2-ulop5NGc-onic -13.8   0.5
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b2    3:3  
///

ENTRY       G11295                      Glycan
NODE        4
            1   Ino-P      15     0
            2   Man         5     0
            3   Gal        -5     0
            4   Glc       -15     0
EDGE        3
            1     2:a1    1:2  
            2     3:b1    2:6  
            3     4:a1    3:6  
///

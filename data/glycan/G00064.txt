ENTRY       G00064                      Glycan
NODE        7
            1   Cer        20     0
            2   Glc        14     0
            3   Gal         8     0
            4   GlcNAc      1     0
            5   Gal        -6     0
            6   Neu5Ac    -13     0
            7   Neu5Ac    -21     0
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a2    5:3  
            6     7:a2    6:8  
///

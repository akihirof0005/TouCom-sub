ENTRY       G02878                      Glycan
NODE        8
            1   GalNAc     13     3
            2   GlcNAc      4     8
            3   Gal         4    -2
            4   Gal        -5     8
            5   GlcNAc     -5     3
            6   GlcNAc     -5    -7
            7   Gal       -14     3
            8   Gal       -14    -7
EDGE        7
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:b1    3:6  
            5     6:b1    3:3  
            6     7:b1    5:4  
            7     8:b1    6:4  
///

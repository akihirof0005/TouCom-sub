ENTRY       G01280                      Glycan
NODE        15
            1   GlcNAc     30     3
            2   LFuc       20     8
            3   GlcNAc     20    -2
            4   Man        10    -2
            5   Man         0     3
            6   Man         0    -7
            7   GlcNAc    -10     8
            8   GlcNAc    -10    -2
            9   GlcNAc    -10    -7
            10  Gal       -20     8
            11  Gal       -20    -2
            12  Gal       -20    -7
            13  Neu5Ac    -30     8
            14  Neu5Ac    -30    -2
            15  Neu5Ac    -30    -7
EDGE        14
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   13:a2   10:3  
            13   14:a2   11:3  
            14   15:a2   12:3  
///

ENTRY       G10826                      Glycan
NODE        10
            1   Asn      24.3   0.5
            2   GlcNAc   17.3   0.5
            3   GlcNAc    8.3   0.5
            4   Man       0.3   0.5
            5   Man      -6.7   5.5
            6   Man      -6.7  -4.5
            7   GlcNAc  -14.7   5.5
            8   GlcNAc  -14.7  -4.5
            9   GalNAc  -23.7   5.5
            10  GalNAc  -23.7  -4.5
EDGE        9
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
///

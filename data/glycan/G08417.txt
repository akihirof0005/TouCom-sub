ENTRY       G08417                      Glycan
NODE        3
            1   Glc         0     0
            2   Glc       -10     5
            3   Xyl       -10    -5
EDGE        2
            1     2:b1    1:6  
            2     3:b1    1:4  
///

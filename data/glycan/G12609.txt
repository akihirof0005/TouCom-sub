ENTRY       G12609                      Glycan
NODE        5
            1   *          16     0
            2   Xyl         8     0
            3   Gal         1     0
            4   6LFuc      -7     0
            5   *         -16     0
EDGE        4
            1     2:a1    1    
            2     3:b1    2:2  
            3     4:a1    3:2  
            4     5       4    
BRACKET     1 -11.0   3.0 -11.0  -4.0
            1  12.0  -4.0  12.0   3.0
            1 n
///

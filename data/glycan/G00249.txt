ENTRY       G00249                      Glycan
NODE        3
            1   Fruf        4     4
            2   Glc         4    -3
            3   Gal        -5    -3
EDGE        2
            1     1:b2    2:a1 
            2     3:a1    2:6  
///

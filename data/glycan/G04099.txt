ENTRY       G04099                      Glycan
NODE        13
            1   LRha        0     0
            2   Gal       -10     5
            3   GalA      -10    -5
            4   Gal       -20     5
            5   LRha      -20    -5
            6   GalA      -30    -5
            7   LRha      -40    -5
            8   Gal       -50     0
            9   GalA      -50   -10
            10  Gal       -60     0
            11  LRha      -60   -10
            12  Gal       -70    -5
            13  GalA      -70   -15
EDGE        12
            1     2:b1    1:4  
            2     3:a1    1:2  
            3     4:b1    2:6  
            4     5:b1    3:4  
            5     6:a1    5:2  
            6     7:b1    6:4  
            7     8:b1    7:4  
            8     9:a1    7:2  
            9    10:b1    8:6  
            10   11:b1    9:4  
            11   12:b1   11:4  
            12   13:a1   11:2  
///

ENTRY       G02607                      Glycan
NODE        3
            1   Kdo       8.2   0.5
            2   Kdo       0.2   0.5
            3   Kdo      -7.8   0.5
EDGE        2
            1     2:2     1:4  
            2     3:2     2:8  
///

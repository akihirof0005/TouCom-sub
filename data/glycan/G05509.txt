ENTRY       G05509                      Glycan
NODE        7
            1   Cer        25     0
            2   Glc        17     0
            3   Glc         9     0
            4   Gal         1     0
            5   Gal        -7     0
            6   GalNAc    -16     0
            7   GalNAc    -26     0
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:4  
            5     6:b1    5:3  
            6     7:a1    6:3  
///

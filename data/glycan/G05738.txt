ENTRY       G05738                      Glycan
NODE        6
            1   P        23.1  -0.1
            2   4-C-methyl-GlcA6N  12.1  -0.1
            3   GlcNAc   -1.9  -0.1
            4   Glc     -10.9   4.9
            5   QuiNAc  -11.9  -5.1
            6   GalA6N  -23.9  -5.1
EDGE        5
            1     2:a1    1    
            2     3:b1    2:2  
            3     4:b1    3:6  
            4     5:b1    3:4  
            5     6:b1    5:4  
///

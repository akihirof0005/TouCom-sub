ENTRY       G07485                      Glycan
NODE        5
            1   Gal        21     2
            2   L3,6-Anhydro-Gal2Me     7     2
            3   Gal        -7     2
            4   S         -11    -2
            5   L3,6-Anhydro-Gal2Me   -21     2
EDGE        4
            1     2:a1    1:3  
            2     3:b1    2:4  
            3     4       3:4  
            4     5:a1    3:3  
///

ENTRY       G08818                      Glycan
NODE        3
            1   FucNAc     14     0
            2   ManANAc3NAc     1     0
            3   ManANAc3NAc   -14     0
EDGE        2
            1     2:b1    1:3  
            2     3:b1    2:4  
///

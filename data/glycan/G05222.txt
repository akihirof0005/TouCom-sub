ENTRY       G05222                      Glycan
NODE        7
            1   Cer        21     0
            2   Glc        13     0
            3   Gal         5     0
            4   Neu5Ac     -4     0
            5   Gal       -13     0
            6   LAraf     -22     5
            7   LAraf     -22    -5
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
            4     5:a1    4:4  
            5     6:a1    5:4  
            6     7:a1    5:3  
///

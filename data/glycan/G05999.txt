ENTRY       G05999                      Glycan
NODE        6
            1   Glc         0     0
            2   Gal       -10     0
            3   Gal       -20     0
            4   Gal       -30     0
            5   Gal       -40     0
            6   GalNAc    -50     0
EDGE        5
            1     2:b1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:3  
            4     5:a1    4:3  
            5     6:b1    5:3  
///

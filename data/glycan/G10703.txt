ENTRY       G10703                      Glycan
NODE        12
            1   Asn        30     3
            2   GlcNAc     20     3
            3   GlcNAc     10     3
            4   Man         0     3
            5   Man        -9    -4
            6   Man       -10    10
            7   GlcNAc    -20    10
            8   GlcNAc    -20     1
            9   GlcNAc    -20    -9
            10  Gal       -30    10
            11  Gal       -30     1
            12  Gal       -30    -9
EDGE        11
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:3  
            5     6:a1    4:6  
            6     8:b1    5:4  
            7     9:b1    5:2  
            8     7:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:b1    9:4  
///

ENTRY       G10651                      Glycan
NODE        6
            1   Asn        22     0
            2   GlcNAc     14     0
            3   GlcNAc      4     0
            4   Man        -5     0
            5   Man       -13     0
            6   GlcNAc    -22     0
EDGE        5
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:3  
            5     6:b1    5:2  
///

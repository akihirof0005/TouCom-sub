ENTRY       G07255                      Glycan
NODE        4
            1   Glc2Me3Me4Me    11     0
            2   Man        -1     0
            3   GlcNAc    -11     5
            4   GlcNAc    -11    -5
EDGE        3
            1     2:a1    1:6  
            2     3:b1    2:6  
            3     4:b1    2:2  
///

ENTRY       G06211                      Glycan
NODE        5
            1   LRha        0     0
            2   LRha      -10     0
            3   LRha      -20     0
            4   Glc       -30     5
            5   Fuc       -30    -5
EDGE        4
            1     2:a1    1:2  
            2     3:a1    2:4  
            3     4:b1    3:4  
            4     5:b1    3:3  
///

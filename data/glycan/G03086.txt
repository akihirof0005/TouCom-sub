ENTRY       G03086                      Glycan
NODE        5
            1   GalNAc     10     3
            2   Gal         0    -2
            3   Neu5Gc     -1     8
            4   GalNAc    -10     3
            5   LFuc      -10    -7
EDGE        4
            1     2:b1    1:3  
            2     3:a2    1:6  
            3     4:a1    2:3  
            4     5:a1    2:2  
///

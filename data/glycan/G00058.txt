ENTRY       G00058                      Glycan
NODE        9
            1   Cer        23     0
            2   Glc        17     0
            3   Gal        11     0
            4   GlcNAc      4     0
            5   Gal        -3     0
            6   LFuc      -10    -4
            7   GalNAc    -11     4
            8   Gal       -18     4
            9   LFuc      -24     4
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    5:2  
            6     7:a1    5:3  
            7     8:b1    7:3  
            8     9:a1    8:2  
///

ENTRY       G10907                      Glycan
NODE        6
            1   Ser        25     0
            2   Xyl        15     0
            3   Gal         5     0
            4   Gal        -5     0
            5   GlcA      -15     0
            6   GalNAc    -25     0
EDGE        5
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:a1    5:4  
///

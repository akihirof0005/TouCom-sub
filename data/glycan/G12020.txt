ENTRY       G12020                      Glycan
NODE        11
            1   2,5-Anhydro-Man    32     1
            2   GlcA       21     1
            3   GlcN       14     1
            4   S           9    -1
            5   GlcA        6     1
            6   GlcN       -1     1
            7   S          -6    -1
            8   GlcA       -9     1
            9   GlcNAc    -17     1
            10  GlcA      -25     1
            11  GlcNAc    -33     1
EDGE        10
            1     2:b1    1:3  
            2     3:a1    2:4  
            3     4       3:2  
            4     5:b1    3:4  
            5     6:a1    5:4  
            6     7       6:2  
            7     8:b1    6:4  
            8     9:a1    8:4  
            9    10:b1    9:4  
            10   11:a1   10:4  
///

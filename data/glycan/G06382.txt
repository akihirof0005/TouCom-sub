ENTRY       G06382                      Glycan
NODE        7
            1   Xyl         0     0
            2   Xyl       -10     0
            3   Xyl       -20     0
            4   Xyl       -30     0
            5   Xyl       -40     5
            6   LAraf     -40     0
            7   LAraf     -40    -5
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     7:a1    4:2  
///

ENTRY       G05558                      Glycan
NODE        8
            1   1,5-Anhydro-Glc     0     0
            2   Glc       -15     5
            3   Gal       -15    -5
            4   Glc       -25     5
            5   Glc       -25    -5
            6   Glc       -35     5
            7   Glc       -35    -5
            8   Glc4,6Py   -45     5
EDGE        7
            1     2:b1    1:6  
            2     3:b1    1:4  
            3     4:b1    2:6  
            4     5:b1    3:3  
            5     6:b1    4:3  
            6     7:a1    5:4  
            7     8:b1    6:3  
///

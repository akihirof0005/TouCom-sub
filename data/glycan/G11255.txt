ENTRY       G11255                      Glycan
NODE        8
            1   Kdo        24     2
            2   Lgro-manHep    14     2
            3   Glc         4     7
            4   Lgro-manHep     1    -2
            5   Glc        -9     3
            6   Lgro-manHep   -13    -6
            7   Glc       -17     3
            8   Glc       -24    -6
EDGE        7
            1     2:a1    1:5  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:a1    4:3  
            5     6:a1    4:2  
            6     7:b1    5:4  
            7     8:b1    6:3  
///

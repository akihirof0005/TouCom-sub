ENTRY       G11907                      Glycan
NODE        14
            1   GlcNAc     28     2
            2   LFuc       19     5
            3   GlcNAc     18    -1
            4   Man         9    -1
            5   Man         1     2
            6   Man         1    -4
            7   GlcNAc     -7     2
            8   GlcNAc     -7    -4
            9   Gal       -15     2
            10  Gal       -15    -4
            11  Gal       -22     2
            12  Gal       -22    -4
            13  Gal       -29     2
            14  Gal       -29    -4
EDGE        13
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:b1    9:4  
            11   12:b1   10:4  
            12   13:a1   11:3  
            13   14:a1   12:3  
///

ENTRY       G02443                      Glycan
NODE        3
            1   Gal         0     0
            2   Gal       -10     5
            3   Gal       -10    -5
EDGE        2
            1     2:b1    1:3  
            2     3:b1    1:2  
///

ENTRY       G08789                      Glycan
NODE        3
            1   Glc3Ac4Ac     0     0
            2   LRha2Ac3Ac4Ac   -10     5
            3   LRha2Ac3Ac4Ac   -10    -5
EDGE        2
            1     2:a1    1:6  
            2     3:a1    1:2  
///

ENTRY       G11969                      Glycan
NODE        12
            1   GlcNAc     27     2
            2   LFuc       18     5
            3   GlcNAc     17    -1
            4   Man         8    -1
            5   Man         0     2
            6   Man         0    -4
            7   GlcNAc     -9     2
            8   GlcNAc     -9    -4
            9   Gal       -18     2
            10  Gal       -18    -4
            11  LFuc      -26    -1
            12  GalNAc    -27     5
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:a1    9:2  
            11   12:a1    9:3  
///

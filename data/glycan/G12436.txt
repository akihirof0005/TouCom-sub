ENTRY       G12436                      Glycan
NODE        3
            1   Glc         4     0
            2   LAra       -5     4
            3   LRha       -5    -4
EDGE        2
            1     2:a1    1:6  
            2     3:a1    1:3  
///

ENTRY       G09040                      Glycan
NODE        4
            1   Ara         0     0
            2   Araf      -10     0
            3   Araf      -20     0
            4   Araf      -30     0
EDGE        3
            1     2:a1    1:5  
            2     3:a1    2:5  
            3     4:b1    3:2  
///

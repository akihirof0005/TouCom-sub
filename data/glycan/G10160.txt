ENTRY       G10160                      Glycan
NODE        4
            1   Cer      10.2   0.5
            2   Glc       3.2   0.5
            3   Gal      -3.8   0.5
            4   Gal     -10.8   0.5
EDGE        3
            1     2:1     1:1  
            2     3:1     2    
            3     4:1     3    
///

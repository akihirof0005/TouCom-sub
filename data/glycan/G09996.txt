ENTRY       G09996                      Glycan
NODE        3
            1   GalNAc    9.2   0.5
            2   Gal       0.2   0.5
            3   GlcNAc   -8.8   0.5
EDGE        2
            1     2:1     1:3  
            2     3:1     2:3  
///

ENTRY       G06955                      Glycan
NODE        4
            1   GlcNAc    9.6  -0.4
            2   Gal      -0.4   4.6
            3   Col      -0.4  -5.4
            4   Neu5Ac  -10.4   4.6
EDGE        3
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:a2    2:3  
///

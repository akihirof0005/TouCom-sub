ENTRY       G04911                      Glycan
NODE        10
            1   Man         0     0
            2   Man       -10     5
            3   GlcA      -10    -5
            4   Man       -20    10
            5   GlcA      -20     0
            6   Man       -30    10
            7   Man       -40    10
            8   Man       -50    10
            9   Man       -60    10
            10  Xyl       -70    10
EDGE        9
            1     2:b1    1:3  
            2     3:b1    1:2  
            3     4:a1    2:3  
            4     5:b1    2:2  
            5     6:a1    4:3  
            6     7:a1    6:3  
            7     8:a1    7:3  
            8     9:a1    8:3  
            9    10:b1    9:4  
///

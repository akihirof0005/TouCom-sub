ENTRY       G07110                      Glycan
NODE        5
            1   GalNAc    9.2   0.5
            2   Gal       0.2  -4.5
            3   GlcNAc   -0.8   5.5
            4   LFuc     -7.8  -4.5
            5   Gal      -8.8   5.5
EDGE        4
            1     2:b1    1:3  
            2     3:b1    1:6  
            3     4:a1    2:2  
            4     5:b1    3    
///

ENTRY       G03387                      Glycan
NODE        10
            1   GalNAc     30     2
            2   S          25    -2
            3   GlcA       20     2
            4   GalNAc     10     2
            5   S           5    -2
            6   GlcA        0     2
            7   GalNAc    -10     2
            8   S         -15    -2
            9   L4-en-thrHexA   -23     2
            10  S         -30    -2
EDGE        9
            1     2       1:4  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5       4:4  
            5     6:b1    4:3  
            6     7:b1    6:4  
            7     8       7:6  
            8     9:a1    7:3  
            9    10       9:2  
///

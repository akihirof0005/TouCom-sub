ENTRY       G01535                      Glycan
NODE        3
            1   Glc       8.2   0.5
            2   Gal       0.2   0.5
            3   Neu5Ac   -8.8   0.5
EDGE        2
            1     2:1     1:4  
            2     3:2     2:3  
///

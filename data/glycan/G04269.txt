ENTRY       G04269                      Glycan
NODE        12
            1   GlcNAc      0     0
            2   LFuc      -10     5
            3   GlcNAc    -10    -5
            4   Man       -20    -5
            5   Man       -30     0
            6   GlcNAc    -30    -5
            7   Man       -30   -10
            8   GlcNAc    -40     0
            9   GlcNAc    -40    -5
            10  GlcNAc    -40   -15
            11  Gal       -50    -5
            12  Gal       -50   -15
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:b1    4:4  
            6     7:a1    4:3  
            7     8:b1    5:2  
            8     9:b1    7:4  
            9    10:b1    7:2  
            10   11:b1    9:4  
            11   12:b1   10:4  
///

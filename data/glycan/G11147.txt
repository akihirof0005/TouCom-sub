ENTRY       G11147                      Glycan
NODE        5
            1   Glc        15     0
            2   Gal         8     0
            3   GlcNAc      0     0
            4   Gal        -8     0
            5   Neu5Ac    -16     0
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a2    4:6  
///

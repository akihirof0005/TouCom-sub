ENTRY       G07407                      Glycan
NODE        6
            1   GalNAc     16     0
            2   S          11     4
            3   S          11    -4
            4   GlcA        6     0
            5   GalNAc     -4     0
            6   L4-en-thrHexA   -17     0
EDGE        5
            1     2       1:6  
            2     3       1:4  
            3     4:b1    1:3  
            4     5:b1    4:4  
            5     6:a1    5:3  
///

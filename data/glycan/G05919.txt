ENTRY       G05919                      Glycan
NODE        6
            1   Man         0     0
            2   Man       -10     0
            3   Man       -20     5
            4   Man       -20    -5
            5   Man       -30     5
            6   Man       -30    -5
EDGE        5
            1     2:a1    1:6  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:2  
            5     6:a1    4:2  
///

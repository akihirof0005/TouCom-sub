ENTRY       G09298                      Glycan
NODE        4
            1   GlcN       10     3
            2   S           5    -3
            3   L4-en-thrHexA    -3     3
            4   S         -11    -3
EDGE        3
            1     2       1:2  
            2     3:b1    1:4  
            3     4       3:2  
///

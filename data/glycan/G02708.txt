ENTRY       G02708                      Glycan
NODE        11
            1   GlcNAc     24     3
            2   LFuc       15     8
            3   GlcNAc     15    -2
            4   Man         7    -2
            5   Man         0     3
            6   Man         0    -7
            7   GlcNAc     -8     3
            8   GlcNAc     -8    -7
            9   Gal       -16     3
            10  Gal       -16    -7
            11  Neu5Ac    -24     3
EDGE        10
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:a2    9:6  
///

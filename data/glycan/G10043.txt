ENTRY       G10043                      Glycan
NODE        3
            1   GlcN        9     2
            2   L4-en-thrHexA    -3     2
            3   S         -10    -2
EDGE        2
            1     2:a1    1:4  
            2     3       2:2  
///

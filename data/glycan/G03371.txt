ENTRY       G03371                      Glycan
NODE        4
            1   Fruf       10     4
            2   Glc        10    -4
            3   Glc         0    -4
            4   Glc       -10    -4
EDGE        3
            1     2:a1    1:b2 
            2     3:a1    2:4  
            3     4:a1    3:3  
///

ENTRY       G06106                      Glycan
NODE        7
            1   GalNAc     11    -2
            2   GlcNAc      1     3
            3   Gal         1    -7
            4   Gal        -9     8
            5   LFuc       -9    -2
            6   LFuc       -9    -7
            7   S         -12     4
EDGE        6
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a1    2:3  
            5     6:a1    3:2  
            6     7       4:3  
///

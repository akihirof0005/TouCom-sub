ENTRY       G12657                      Glycan
NODE        7
            1   *          24     0
            2   LRha       14     0
            3   GalNAc      5     0
            4   GalNAc     -5     0
            5   GalNAc    -14     4
            6   GlcNAc    -14    -4
            7   *         -24     4
EDGE        6
            1     2:a1    1    
            2     3:a1    2:2  
            3     4:a1    3:3  
            4     5:a1    4:4  
            5     6:b1    4:3  
            6     7       5:6  
BRACKET     1 -18.0   6.0 -18.0  -5.0
            1  18.0  -5.0  18.0   6.0
            1 n
///

ENTRY       G09477                      Glycan
NODE        5
            1   GlcN        5     0
            2   S           5    -5
            3   S           0     5
            4   S           0    -5
            5   LIdoA      -5     0
EDGE        4
            1     2       1:2  
            2     3       1:6  
            3     4       1:3  
            4     5:a1    1:4  
///

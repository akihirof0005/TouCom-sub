ENTRY       G07631                      Glycan
NODE        4
            1   Glc         0     0
            2   LRha      -10     5
            3   LRha      -10    -5
            4   Xyl       -20    -5
EDGE        3
            1     2:a1    1:6  
            2     3:a1    1:2  
            3     4:b1    3:4  
///

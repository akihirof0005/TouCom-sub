ENTRY       G03242                      Glycan
NODE        4
            1   Glc         6     0
            2   Man        -3     0
            3   S          -7     4
            4   S          -7    -4
EDGE        3
            1     2:a1    1:2  
            2     3       2:6  
            3     4       2:2  
///

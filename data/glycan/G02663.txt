ENTRY       G02663                      Glycan
NODE        9
            1   Cer        23     0
            2   Glc        17     0
            3   Gal        11     0
            4   GalNAc      3     5
            5   Neu5Ac      3    -5
            6   Gal        -5     5
            7   Neu5Ac     -6    -5
            8   Neu5Ac9Ac   -14     5
            9   Neu5Ac    -24     5
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
            6     7:a2    5:8  
            7     8:a2    6:3  
            8     9:a2    8:8  
///

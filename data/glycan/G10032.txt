ENTRY       G10032                      Glycan
NODE        3
            1   Man       4.6  -0.4
            2   Abe      -4.4   4.6
            3   Gal      -4.4  -5.4
EDGE        2
            1     2:1     1:3  
            2     3:1     1:2  
///

ENTRY       G02208                      Glycan
NODE        11
            1   GlcNAc     20     5
            2   LFuc       11    10
            3   GlcNAc     11     0
            4   Man         3     0
            5   Man        -4     5
            6   Man        -4    -5
            7   GlcNAc    -12     5
            8   GlcNAc    -12    -5
            9   Gal       -20     5
            10  Gal       -20     0
            11  LFuc      -20   -10
EDGE        10
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:a1    8:3  
///

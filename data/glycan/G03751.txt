ENTRY       G03751                      Glycan
NODE        13
            1   GlcNAc     30     5
            2   LFuc       20    10
            3   GlcNAc     20     0
            4   Man        10     0
            5   Man         0     5
            6   Man         0    -5
            7   GlcNAc    -10     5
            8   GlcNAc    -10    -5
            9   S         -15    -9
            10  Gal       -20     5
            11  Gal       -20    -5
            12  Neu5Ac    -30     5
            13  Neu5Ac    -30    -5
EDGE        12
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8    10:b1    7:4  
            9     9       8:6  
            10   11:b1    8:4  
            11   12:a2   10:3  
            12   13:a2   11:6  
///

ENTRY       G12321                      Glycan
NODE        5
            1   Ser      11.2   0.5
            2   GalNAc    2.2   0.5
            3   Gal      -5.8  -4.5
            4   Neu5Ac   -7.8   4.5
            5   S       -11.8  -4.5
EDGE        4
            1     2:1     1:3  
            2     3:b1    2:3  
            3     4:a2    2:6  
            4     5       3:6  
///

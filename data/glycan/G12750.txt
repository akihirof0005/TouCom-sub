ENTRY       G12750                      Glycan
NODE        8
            1   *          24     0
            2   Gal        16     0
            3   Gal         9     0
            4   GalNAc      2     3
            5   GalNAc      2    -3
            6   GalNAc     -6    -3
            7   Fuc3NBuOH   -15    -3
            8   *         -24     3
EDGE        7
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    3:4  
            4     5:b1    3:3  
            5     8       4:3  
            6     6:a1    5:3  
            7     7:b1    6:3  
BRACKET     1 -19.0   5.0 -19.0  -5.0
            1  19.0  -5.0  19.0   5.0
            1 n
///

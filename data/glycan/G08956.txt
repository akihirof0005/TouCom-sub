ENTRY       G08956                      Glycan
NODE        3
            1   Fruf6Iodo     5     4
            2   Glc         5    -4
            3   Gal        -5    -4
EDGE        2
            1     2:a1    1:b2 
            2     3:a1    2:6  
///

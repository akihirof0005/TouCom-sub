ENTRY       G10206                      Glycan
NODE        3
            1   Xyl       7.2   0.5
            2   Glc       0.2   0.5
            3   Glc      -6.8   0.5
EDGE        2
            1     2:1     1:4  
            2     3:1     2:4  
///

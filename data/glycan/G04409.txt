ENTRY       G04409                      Glycan
NODE        10
            1   Cer        30   0.5
            2   Glc        22   0.5
            3   Gal        14   0.5
            4   GlcNAc      5   0.5
            5   Gal        -4   5.5
            6   LFuc       -4  -4.5
            7   LFuc      -12  -4.5
            8   GlcNAc    -13   5.5
            9   Gal       -22   5.5
            10  Neu5Ac    -31   5.5
EDGE        9
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     8:b1    5:3  
            7     7:a1    6    
            8     9:b1    8:4  
            9    10:a2    9:3  
///

ENTRY       G03646                      Glycan
NODE        4
            1   LRha        0     0
            2   Glc       -10     0
            3   Glc       -20     5
            4   Glc       -20    -5
EDGE        3
            1     2:a1    1:3  
            2     3:b1    2:6  
            3     4:b1    2:4  
///

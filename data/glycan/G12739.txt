ENTRY       G12739                      Glycan
NODE        8
            1   *          20     2
            2   Gal        12     2
            3   Gal         5     2
            4   Gal        -2     2
            5   P          -2    -2
            6   GalNAc    -10     2
            7   sn-glycerol   -10    -2
            8   *         -20     2
EDGE        7
            1     2:b1    1    
            2     3:b1    2:6  
            3     4:b1    3:6  
            4     6:b1    4:4  
            5     4:3     5    
            6     7:3     5    
            7     8       6:3  
BRACKET     1 -16.0   6.0 -16.0  -4.0
            1  16.0  -4.0  16.0   6.0
            1 n
///

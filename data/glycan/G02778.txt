ENTRY       G02778                      Glycan
NODE        10
            1   Glc        27     0
            2   Gal        21     0
            3   GlcNAc     14     0
            4   Gal         7     0
            5   GlcNAc      0     0
            6   Gal        -7     0
            7   GlcNAc    -14     0
            8   Gal       -21     0
            9   GalNAc    -28     4
            10  LFuc      -28    -4
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7:b1    6:3  
            7     8:b1    7:4  
            8     9:a1    8:3  
            9    10:a1    8:2  
///

ENTRY       G11812                      Glycan
NODE        13
            1   GlcN       34    -2
            2   GlcN     25.9    -2
            3   Kdo        18    -2
            4   Kdo        11    -6
            5   Lgro-manHep     8     3
            6   Lgro-manHep    -5     3
            7   Glc       -14    -2
            8   Lgro-manHep   -17     8
            9   Gal       -21    -2
            10  GlcN      -27     8
            11  GlcN      -27     3
            12  Glc       -27    -7
            13  Glc       -34    -7
EDGE        12
            1     2:b1    1:6  
            2     3:a2    2:6  
            3     4:a2    3:4  
            4     5:a1    3:5  
            5     6:a1    5:3  
            6     7:a1    6:3  
            7     8:a1    6:7  
            8     9:a1    7:3  
            9    10:a1    8:7  
            10   11:a1    9:3  
            11   12:a1    9:2  
            12   13:a1   12:2  
///

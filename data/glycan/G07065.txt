ENTRY       G07065                      Glycan
NODE        4
            1   Man         0     0
            2   Man       -10     5
            3   Man       -10    -5
            4   GlcN      -20    -5
EDGE        3
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:b1    3:2  
///

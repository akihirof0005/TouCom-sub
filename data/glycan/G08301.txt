ENTRY       G08301                      Glycan
NODE        4
            1   Glc         0     0
            2   Glc       -10     5
            3   Glc       -10    -5
            4   Glc       -20    -5
EDGE        3
            1     2:b1    1:6  
            2     3:a1    1:4  
            3     4:b1    3:6  
///

ENTRY       G12268                      Glycan
NODE        6
            1   Glc        18     0
            2   Gal        10     0
            3   GlcNAc      1     0
            4   Gal        -9     0
            5   LFuc      -17    -4
            6   GalNAc    -18     4
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a1    4:2  
            5     6:a1    4:3  
///

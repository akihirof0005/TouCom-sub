ENTRY       G10041                      Glycan
NODE        3
            1   GlcA      9.2   0.5
            2   Glc2N3N   0.2   0.5
            3   Glc2N3N  -9.8   0.5
EDGE        2
            1     2:1     1    
            2     3:1     2:6  
///

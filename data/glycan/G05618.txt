ENTRY       G05618                      Glycan
NODE        6
            1   GalNAc      0     0
            2   Neu5Ac    -10     5
            3   GlcNAc    -10    -5
            4   Gal       -20     0
            5   LFuc      -20   -10
            6   Neu5Ac    -30     0
EDGE        5
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6:a2    4:3  
///

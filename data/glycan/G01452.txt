ENTRY       G01452                      Glycan
NODE        6
            1   GalNAc     20     0
            2   Neu5Gc     10     5
            3   Gal        10    -5
            4   Gal         0    -5
            5   GalNAc    -10    -5
            6   LFuc      -20    -5
EDGE        5
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:a1    5:3  
///

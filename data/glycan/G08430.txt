ENTRY       G08430                      Glycan
NODE        3
            1   4dxylHexNAc   9.2   0.5
            2   Gal      -1.8   0.5
            3   S        -8.8   0.5
EDGE        2
            1     2:b1    1:3  
            2     3       2:3  
///

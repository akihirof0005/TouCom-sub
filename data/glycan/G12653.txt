ENTRY       G12653                      Glycan
NODE        6
            1   *          19     0
            2   GlcNAc      9     0
            3   GalNAc      0     0
            4   Glc        -8    -4
            5   GlcNAc     -9     4
            6   *         -19     4
EDGE        5
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:b1    3:3  
            4     5:a1    3:4  
            5     6       5:6  
BRACKET     1 -13.0   6.0 -13.0  -6.0
            1  14.0  -6.0  14.0   6.0
            1 n
///

ENTRY       G11974                      Glycan
NODE        12
            1   GlcNAc     24    -2
            2   GlcNAc     15    -2
            3   Man         7    -2
            4   Man         0     2
            5   Man         0    -6
            6   GlcNAc     -8     2
            7   GlcNAc     -8    -6
            8   Gal       -16     2
            9   Gal       -16    -6
            10  Gal       -23     6
            11  Neu5Ac    -24    -2
            12  Neu5Ac    -24    -6
EDGE        11
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:2  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:a2    8:3  
            11   12:a2    9:3  
///

ENTRY       G01453                      Glycan
NODE        8
            1   Glc        20     7
            2   LRha       20    -1
            3   LRha       10     7
            4   LRha       10    -1
            5   GlcNAc      0     4
            6   LRha        0    -6
            7   Gal       -10     4
            8   LRha      -20     4
EDGE        7
            1     3:a1    1:3  
            2     1:1     2:a1 
            3     4:a1    2:2  
            4     5:b1    4:4  
            5     6:a1    4:2  
            6     7:a1    5:3  
            7     8:a1    7:3  
///

ENTRY       G07382                      Glycan
NODE        3
            1   Glc      12.2   0.5
            2   Gal       3.2   0.5
            3   ery-3dgalNon-2-ulop-onic -12.8   0.5
EDGE        2
            1     2:b1    1:4  
            2     3:a2    2:3  
///

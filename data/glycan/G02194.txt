ENTRY       G02194                      Glycan
NODE        12
            1   GlcNAc     25    -2
            2   GlcNAc     17    -2
            3   Man        10    -2
            4   Man         4     3
            5   Man         4    -7
            6   Man        -2     8
            7   Man        -2    -2
            8   Man        -2    -7
            9   Man        -8    -7
            10  Glc       -14    -7
            11  Glc       -20    -7
            12  Glc       -26    -7
EDGE        11
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    4:3  
            7     8:a1    5:2  
            8     9:a1    8:2  
            9    10:a1    9:3  
            10   11:a1   10:3  
            11   12:a1   11:2  
///

ENTRY       G03752                      Glycan
NODE        18
            1   Man         0     0
            2   Man       -10    20
            3   GlcNAc    -10     0
            4   Man       -10   -20
            5   GlcNAc    -20    30
            6   GlcNAc    -20    10
            7   GlcNAc    -20     0
            8   GlcNAc    -20   -10
            9   GlcNAc    -20   -30
            10  Gal       -30    35
            11  LFuc      -30    25
            12  Gal       -30    15
            13  LFuc      -30     5
            14  LFuc      -30     0
            15  Gal       -30    -5
            16  LFuc      -30   -15
            17  Gal       -30   -25
            18  LFuc      -30   -35
EDGE        17
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:a1    1:3  
            4     5:a1    2:6  
            5     6:a1    2:2  
            6     7:b1    3:4  
            7     8:a1    4:4  
            8     9:a1    4:2  
            9    10:b1    5:4  
            10   11:a1    5:3  
            11   12:b1    6:4  
            12   13:a1    6:3  
            13   14:a1    7:6  
            14   15:b1    8:4  
            15   16:a1    8:3  
            16   17:b1    9:4  
            17   18:a1    9:3  
///

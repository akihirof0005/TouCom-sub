ENTRY       G11485                      Glycan
NODE        8
            1   Ino-P      24     0
            2   Man        16     0
            3   Gal         8     0
            4   Man         0     0
            5   Man        -8     5
            6   Xyl        -8    -5
            7   Man       -16     5
            8   Man       -24     5
EDGE        7
            1     2:a1    1:2  
            2     3:b1    2:6  
            3     4:a1    3:4  
            4     5:a1    4:3  
            5     6:b1    4:2  
            6     7:a1    5:6  
            7     8:a1    7:6  
///

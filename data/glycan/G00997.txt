ENTRY       G00997                      Glycan
NODE        6
            1   GlcNAc     12     0
            2   Man         3     0
            3   Man        -4     4
            4   Man        -4    -4
            5   Man       -12     4
            6   Man       -12    -4
EDGE        5
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:3  
            5     6:a1    4:2  
///

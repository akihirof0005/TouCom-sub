ENTRY       G06216                      Glycan
NODE        5
            1   Glc         0     0
            2   Glc       -10     0
            3   Xyl       -20     5
            4   Gal       -20    -5
            5   Xyl       -30    -5
EDGE        4
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    2:2  
            4     5:b1    4:2  
///

ENTRY       G05865                      Glycan
NODE        8
            1   Xyl        26     2
            2   P          22    -2
            3   Gal        16     2
            4   Gal         6     2
            5   GlcA       -4     2
            6   GalNAc    -14     2
            7   S         -18    -2
            8   L4-en-thrHexA   -27     2
EDGE        7
            1     2       1:2  
            2     3:b1    1:4  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7       6:6  
            7     8:a1    6:3  
///

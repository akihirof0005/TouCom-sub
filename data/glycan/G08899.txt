ENTRY       G08899                      Glycan
NODE        3
            1   Gal         0     0
            2   Gal       -10     0
            3   GlcA      -20     0
EDGE        2
            1     2:b1    1:6  
            2     3:b1    2:6  
///

ENTRY       G12412                      Glycan
NODE        4
            1   GlcNAc     10     0
            2   LFuc        0     4
            3   Gal         0    -4
            4   Neu5Ac    -11    -4
EDGE        3
            1     2:a1    1:4  
            2     3:b1    1:3  
            3     4:a2    3:6  
///

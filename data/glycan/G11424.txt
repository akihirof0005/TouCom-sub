ENTRY       G11424                      Glycan
NODE        7
            1   *          23     0
            2   Galf       16     0
            3   Glc         8     0
            4   Gal         0     0
            5   Galf       -8     0
            6   Glc       -16     0
            7   *         -23     0
EDGE        6
            1     2:b1    1    
            2     3:a1    2:3  
            3     4:a1    3:6  
            4     5:b1    4:6  
            5     6:a1    5:2  
            6     7       6:3  
BRACKET     1 -19.0   3.0 -19.0  -3.0
            1  20.0  -3.0  20.0   3.0
            1 n
///

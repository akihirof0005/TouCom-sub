ENTRY       G04642                      Glycan
NODE        7
            1   Cer      22.1  -0.5
            2   Glc      14.1  -0.5
            3   Gal       6.1  -0.5
            4   Kdn      -1.5  -5.2
            5   GalNAc   -2.9   4.5
            6   Gal     -11.9   4.5
            7   Kdn9Ac  -21.4   4.5
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
            4     5:b1    3:4  
            5     6:b1    5:3  
            6     7:a2    6:3  
///

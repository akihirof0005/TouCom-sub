ENTRY       G06324                      Glycan
NODE        6
            1   GalNAc      0     0
            2   Man       -10     0
            3   Man       -20     0
            4   Man       -30     0
            5   Man3Me    -40     0
            6   Man3Me    -50     0
EDGE        5
            1     2:a1    1:4  
            2     3:a1    2:2  
            3     4:a1    3:2  
            4     5:a1    4:2  
            5     6:a1    5:6  
///

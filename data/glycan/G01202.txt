ENTRY       G01202                      Glycan
NODE        4
            1   Cer        15     0
            2   Glc         5     0
            3   Man        -5     0
            4   Man       -15     0
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:3  
///

ENTRY       G07425                      Glycan
NODE        5
            1   Glc         7     4
            2   Glc         7    -3
            3   Glc         0     4
            4   Glc         0    -3
            5   Glc        -7     4
EDGE        4
            1     3:a1    1:4  
            2     1:a1    2:b1 
            3     4:b1    2:4  
            4     5:a1    3:4  
///

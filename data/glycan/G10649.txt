ENTRY       G10649                      Glycan
NODE        6
            1   Asn        25     0
            2   GlcNAc     15     0
            3   Gal         5     0
            4   GlcNAc     -5     0
            5   Gal       -15     0
            6   Neu5Ac    -25     0
EDGE        5
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a2    5:3  
///

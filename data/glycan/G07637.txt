ENTRY       G07637                      Glycan
NODE        8
            1   GlcN       14     2
            2   S          10     5
            3   S          10    -1
            4   S          10    -4
            5   GlcA        5     2
            6   GlcNAc     -5     2
            7   S         -10    -1
            8   LIdoA     -15     2
EDGE        7
            1     2       1:6  
            2     3       1:3  
            3     4       1:2  
            4     5:b1    1:4  
            5     6:a1    5:4  
            6     7       6:6  
            7     8:a1    6:4  
///

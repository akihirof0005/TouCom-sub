ENTRY       G04411                      Glycan
NODE        9
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   Gal       -20     5
            5   Neu5Ac    -20    -5
            6   GlcNAc    -30    10
            7   GlcNAc    -30     0
            8   Gal       -40    10
            9   Gal       -40     0
EDGE        8
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a2    3:3  
            5     6:b1    4:6  
            6     7:b1    4:3  
            7     8:b1    6:4  
            8     9:b1    7:3  
///

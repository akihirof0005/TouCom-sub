ENTRY       G11183                      Glycan
NODE        12
            1   GlcN     28.2   2.5
            2   GlcN     20.2   2.5
            3   Kdo      12.2   2.5
            4   Kdo       5.2   6.5
            5   Lgro-manHep   2.2  -1.5
            6   Kdo      -1.8  10.5
            7   LRha     -1.8   2.5
            8   Lgro-manHep -10.8  -1.5
            9   Glc     -20.8  -6.5
            10  Lgro-manHep -24.8   3.5
            11  Gal     -27.8  -2.5
            12  Glc     -27.8 -10.5
EDGE        11
            1     2:b1    1:6  
            2     3:a2    2:6  
            3     4:a2    3:4  
            4     5:a1    3:5  
            5     6:a2    4    
            6     7:a1    4:5  
            7     8:a1    5:3  
            8     9:a1    8:3  
            9    10:a1    8:7  
            10   11:a1    9:6  
            11   12:a1    9:3  
///

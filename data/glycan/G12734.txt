ENTRY       G12734                      Glycan
NODE        6
            1   *          18     0
            2   LRha       11     0
            3   LRha        4     0
            4   LRha       -3     0
            5   GlcNAc    -11     0
            6   *         -19     0
EDGE        5
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:a1    3:2  
            4     5:b1    4:2  
            5     6       5:3  
BRACKET     1 -15.0   3.0 -15.0  -3.0
            1  15.0  -3.0  15.0   3.0
            1 n
///

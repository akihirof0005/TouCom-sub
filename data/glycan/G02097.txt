ENTRY       G02097                      Glycan
NODE        3
            1   Glc         5     0
            2   Man        -5     5
            3   LRha       -5    -5
EDGE        2
            1     2:a1    1:3  
            2     3:a1    1:2  
///

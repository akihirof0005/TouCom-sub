ENTRY       G07682                      Glycan
NODE        4
            1   Glc         0     0
            2   Xyl       -10     0
            3   LAra      -20     0
            4   LRha      -30     0
EDGE        3
            1     2:b1    1:3  
            2     3:a1    2:3  
            3     4:a1    3:3  
///

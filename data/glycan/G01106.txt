ENTRY       G01106                      Glycan
NODE        6
            1   Glc        12    -2
            2   Glc         4     3
            3   Glc         4    -7
            4   Glc        -4     3
            5   GlcN      -12     8
            6   Glc       -12    -2
EDGE        5
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:6  
            4     5:b1    4:6  
            5     6:b1    4:3  
///

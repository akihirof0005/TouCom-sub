ENTRY       G10824                      Glycan
NODE        11
            1   Asn        26     2
            2   GlcNAc     18     2
            3   GlcNAc      8     2
            4   Man        -1     2
            5   Man       -10     7
            6   GlcNAc    -10     2
            7   Man       -10    -3
            8   GlcNAc    -18     7
            9   GlcNAc    -18     1
            10  GlcNAc    -18    -7
            11  Gal       -26     1
EDGE        10
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:b1    4:4  
            6     7:a1    4:3  
            7     8:b1    5:2  
            8     9:b1    7:4  
            9    10:b1    7:2  
            10   11:b1    9:4  
///

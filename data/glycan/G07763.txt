ENTRY       G07763                      Glycan
NODE        4
            1   LKdo        0     0
            2   Man       -10     0
            3   Gal       -20     5
            4   GalA      -20    -5
EDGE        3
            1     2:a1    1:5  
            2     3:a1    2:6  
            3     4:a1    2:4  
///

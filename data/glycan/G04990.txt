ENTRY       G04990                      Glycan
NODE        11
            1   2,5-Anhydro-Tal    28     0
            2   LIdoA      16     0
            3   S          10    -4
            4   GalNAc      6     0
            5   S           0     4
            6   LIdoA      -4     0
            7   S         -10    -4
            8   GalNAc    -14     0
            9   S         -20     4
            10  LIdoA     -24     0
            11  S         -29    -4
EDGE        10
            1     2:a1    1:3  
            2     3       2:2  
            3     4:b1    2:4  
            4     5       4:4  
            5     6:a1    4:3  
            6     7       6:2  
            7     8:b1    6:4  
            8     9       8:4  
            9    10:a1    8:3  
            10   11      10:2  
///

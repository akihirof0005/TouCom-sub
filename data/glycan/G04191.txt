ENTRY       G04191                      Glycan
NODE        18
            1   GlcNAc     32     5
            2   LFuc       22    10
            3   GlcNAc     22     0
            4   Man        13     0
            5   Man         4     9
            6   Man         4    -9
            7   GlcNAc     -5     9
            8   GlcNAc     -5    -4
            9   GlcNAc     -5   -14
            10  Gal       -14    14
            11  LFuc      -14     5
            12  Gal       -14     1
            13  LFuc      -14    -9
            14  Gal       -14   -14
            15  GlcNAc    -23     1
            16  Neu5Ac    -23   -14
            17  Gal       -32     6
            18  LFuc      -32    -4
EDGE        17
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:4  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:a1    7:3  
            11   12:b1    8:4  
            12   13:a1    8:3  
            13   14:b1    9:4  
            14   15:b1   12:6  
            15   16:a2   14:6  
            16   17:b1   15:4  
            17   18:a1   15:3  
///

ENTRY       G12719                      Glycan
NODE        7
            1   *          20     0
            2   LMan       12     0
            3   Glc         4     0
            4   GlcA       -4     4
            5   LRha       -4    -4
            6   Glc       -12     4
            7   *         -21     4
EDGE        6
            1     2:a1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6:b1    4:4  
            6     7       6:3  
BRACKET     1 -15.0   8.0 -15.0  -7.0
            1  16.0  -7.0  16.0   8.0
            1 n
///

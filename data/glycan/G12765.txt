ENTRY       G12765                      Glycan
NODE        10
            1   Man        23     2
            2   Man        16     2
            3   Man        10     2
            4   Man         4     5
            5   Man         4    -1
            6   Man        -3    -1
            7   Man        -9     2
            8   Man        -9    -4
            9   Man       -16    -4
            10  Man       -23    -4
EDGE        9
            1     2:a1    1:2  
            2     3:a1    2:2  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    5:2  
            6     7:a1    6:6  
            7     8:a1    6:3  
            8     9:b1    8:2  
            9    10:b1    9:2  
///

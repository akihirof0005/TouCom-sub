ENTRY       G03218                      Glycan
NODE        4
            1   Kdo      16.2   0.5
            2   Lgro-manHep   6.2   0.5
            3   Lgro-manHep  -7.8   0.5
            4   Glc     -16.8   0.5
EDGE        3
            1     2:a1    1:5  
            2     3:a1    2:3  
            3     4:1     3:3  
///

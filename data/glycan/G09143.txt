ENTRY       G09143                      Glycan
NODE        3
            1   LIdoA     9.2   0.5
            2   GalNAc    0.2   0.5
            3   LIdoA    -8.8   0.5
EDGE        2
            1     2:b1    1    
            2     3:a1    2    
///

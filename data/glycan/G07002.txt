ENTRY       G07002                      Glycan
NODE        10
            1   GlcN       20     0
            2   S          16    -3
            3   LIdoA      11     0
            4   S           7    -3
            5   GlcN        2     0
            6   S          -2     3
            7   S          -2    -3
            8   GlcA       -7     0
            9   GlcNAc    -16     0
            10  S         -21    -3
EDGE        9
            1     2       1:2  
            2     3:a1    1:4  
            3     4       3:2  
            4     5:a1    3:4  
            5     6       5:3  
            6     7       5:2  
            7     8:b1    5:4  
            8     9:a1    8:4  
            9    10       9:6  
///

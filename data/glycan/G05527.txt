ENTRY       G05527                      Glycan
NODE        7
            1   GalNAc     16     2
            2   Rib        16    -5
            3   P          11     6
            4   GalNAc      6     2
            5   P           0     6
            6   FucNAc4NAc    -6     2
            7   Glc       -16     2
EDGE        6
            1     2:1     1:b1 
            2     3       1:6  
            3     4:a1    1:3  
            4     5       4:6  
            5     6:a1    4:4  
            6     7:b1    6:3  
///

ENTRY       G08972                      Glycan
NODE        3
            1   GalNAc      0     0
            2   GalNAc    -10     0
            3   FucNAc4NAc   -20     0
EDGE        2
            1     2:a1    1:3  
            2     3:a1    2:4  
///

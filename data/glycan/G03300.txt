ENTRY       G03300                      Glycan
NODE        4
            1   Fruf        5     1
            2   Glc         5    -6
            3   Fruf       -5     6
            4   Fruf       -5    -3
EDGE        3
            1     2:a1    1:b2 
            2     3:b2    1:6  
            3     4:b2    1:1  
///

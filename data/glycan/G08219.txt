ENTRY       G08219                      Glycan
NODE        4
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   GlcNAc    -20     0
            4   GlcNAc    -30     0
EDGE        3
            1     2:b1    1:6  
            2     3:b1    2:6  
            3     4:b1    3:6  
///

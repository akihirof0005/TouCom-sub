ENTRY       G02751                      Glycan
NODE        10
            1   Cer        36     0
            2   Glc        29     0
            3   Man        22     0
            4   GlcNAc     14     0
            5   GalNAc      5     0
            6   GalNAc     -4     0
            7   Gal       -12     0
            8   GlcNAc    -20     0
            9   GalNAc    -29     0
            10  Gal       -37     0
EDGE        9
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    5:4  
            6     7:b1    6:3  
            7     8:b1    7:3  
            8     9:b1    8:3  
            9    10:b1    9:3  
///

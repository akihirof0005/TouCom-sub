ENTRY       G12205                      Glycan
NODE        7
            1   S          22    -1
            2   2,5-Anhydro-Man    12    -1
            3   S           2     2
            4   GlcA       -2    -1
            5   GlcNAc    -12    -1
            6   S         -18     2
            7   LIdoA     -23    -1
EDGE        6
            1     2:1     1    
            2     3       2:4  
            3     4:b1    2:3  
            4     5:a1    4:4  
            5     6       5:6  
            6     7:a1    5:4  
///

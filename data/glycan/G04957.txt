ENTRY       G04957                      Glycan
NODE        9
            1   Asn        28     0
            2   GlcNAc     20     0
            3   LFuc       10     5
            4   GlcNAc     10    -5
            5   Man         2    -5
            6   Man        -5    -5
            7   GlcNAc    -13    -5
            8   Gal       -21    -5
            9   LFuc      -28    -5
EDGE        8
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:b1    6:6  
            7     8:b1    7:4  
            8     9:a1    8:2  
///

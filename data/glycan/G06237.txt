ENTRY       G06237                      Glycan
NODE        5
            1   GlcN        0     0
            2   Man       -10     0
            3   Man       -20     0
            4   Galf      -30     0
            5   Gal       -40     0
EDGE        4
            1     2:a1    1:4  
            2     3:a1    2:3  
            3     4:a1    3:3  
            4     5:a1    4:3  
///

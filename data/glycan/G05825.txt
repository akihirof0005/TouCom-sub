ENTRY       G05825                      Glycan
NODE        5
            1   GlcNAc      0     0
            2   Gal       -10     0
            3   GlcNAc    -20     0
            4   Gal       -30     0
            5   Neu5Gc    -40     0
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a2    4:3  
///

ENTRY       G08535                      Glycan
NODE        3
            1   Cym      10.6  -0.4
            2   Cym       0.6  -0.4
            3   Fuc2Ac3Me -11.4  -0.4
EDGE        2
            1     2:b1    1:4  
            2     3:b1    2:4  
///

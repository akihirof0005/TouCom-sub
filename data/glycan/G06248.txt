ENTRY       G06248                      Glycan
NODE        5
            1   GalNAc      0     0
            2   Neu5Ac    -10     5
            3   Gal       -10    -5
            4   Gal       -20    -5
            5   GalNAc    -30    -5
EDGE        4
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
///

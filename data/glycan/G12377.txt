ENTRY       G12377                      Glycan
NODE        4
            1   Ser      13.2   0.5
            2   GalNAc    4.2   0.5
            3   Gal      -4.8   0.5
            4   Neu5Ac  -12.8   0.5
EDGE        3
            1     2:1     1:3  
            2     3:b1    2:3  
            3     4:a2    3    
///

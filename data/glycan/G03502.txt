ENTRY       G03502                      Glycan
NODE        3
            1   Neu5Ac      0     0
            2   GlcNAc    -10     0
            3   GlcNAc    -20     0
EDGE        2
            1     2:b1    1:4  
            2     3:b1    2:4  
///

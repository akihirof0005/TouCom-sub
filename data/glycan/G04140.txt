ENTRY       G04140                      Glycan
NODE        10
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   Gal       -20    10
            5   LFuc      -20     0
            6   Neu5Ac    -20    -5
            7   GlcNAc    -30    10
            8   LFuc      -40    15
            9   Gal       -40     5
            10  Neu5Ac    -50     5
EDGE        9
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a1    2:3  
            5     6:a2    3:3  
            6     7:b1    4:3  
            7     8:a1    7:4  
            8     9:b1    7:3  
            9    10:a2    9:3  
///

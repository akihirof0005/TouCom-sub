ENTRY       G11298                      Glycan
NODE        6
            1   Ino-P      15     0
            2   Man         5     0
            3   Gal        -5     0
            4   Gal       -15     5
            5   Gal       -15     0
            6   LFuc      -15    -5
EDGE        5
            1     2:a1    1:4  
            2     3:b1    2:6  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    3:2  
///

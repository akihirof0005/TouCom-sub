ENTRY       G06000                      Glycan
NODE        7
            1   Man         0     0
            2   Man       -10     0
            3   Man       -20     5
            4   GlcA      -20    -5
            5   Xyl       -30    10
            6   Man       -30     0
            7   GlcA      -40     0
EDGE        6
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:b1    2:2  
            4     5:b1    3:4  
            5     6:a1    3:3  
            6     7:b1    6:2  
///

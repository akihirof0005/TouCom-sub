ENTRY       G11457                      Glycan
NODE        5
            1   *        32.2   0.5
            2   GlcNAc   21.2   0.5
            3   LFucNAc  10.2   0.5
            4   Lgro-L3,9dmanNon-2-ulop5NAc7NAc-onic -11.8   0.5
            5   *       -32.8   0.5
EDGE        4
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:-1    3:3  
            4     5       4:8  
BRACKET     1 -28.8   2.5 -28.8  -2.5
            1  28.2  -2.5  28.2   2.5
            1 n
///

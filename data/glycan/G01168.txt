ENTRY       G01168                      Glycan
NODE        7
            1   GlcNAc     20     0
            2   Man        10     0
            3   Man         0     5
            4   Man         0    -5
            5   Man       -10     5
            6   Man       -10    -5
            7   Man       -20    -5
EDGE        6
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:3  
            5     6:a1    4:2  
            6     7:a1    6:2  
///

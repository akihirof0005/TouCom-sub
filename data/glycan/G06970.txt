ENTRY       G06970                      Glycan
NODE        5
            1   Gal         0     0
            2   GlcNAc    -10     5
            3   GlcNAc    -10    -5
            4   Gal       -20    -5
            5   GlcNAc    -30    -5
EDGE        4
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
///

ENTRY       G04683                      Glycan
NODE        9
            1   Kdo      29.2   0.5
            2   Lgro-Hep  20.2   5.5
            3   Lgro-manHep  19.2  -4.5
            4   Glc       9.2  -4.5
            5   Lgro-manHep   8.2   5.5
            6   Lgro-manHep  -0.8  -4.5
            7   Gal     -11.8  -4.5
            8   GlcNAc  -20.8  -4.5
            9   Gal     -29.8  -4.5
EDGE        8
            1     2:a1    1    
            2     3:a1    1    
            3     5:a1    2:2  
            4     4:b1    3:4  
            5     6:a1    4:6  
            6     7:b1    6:4  
            7     8:b1    7:3  
            8     9:b1    8:4  
///

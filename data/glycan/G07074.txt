ENTRY       G07074                      Glycan
NODE        5
            1   Glc         0     0
            2   Glc       -10     0
            3   Glc       -20     0
            4   Glc       -30     0
            5   Glc       -40     0
EDGE        4
            1     2:b1    1:3  
            2     3:b1    2:6  
            3     4:b1    3:3  
            4     5:b1    4:3  
///

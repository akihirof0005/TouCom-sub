ENTRY       G00126                      Glycan
NODE        7
            1   Cer        21     0
            2   Glc        15     0
            3   Gal         9     0
            4   GalNAc      2     0
            5   Gal        -5     0
            6   Neu5Ac    -12     0
            7   Neu5Ac    -21     0
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:a2    5:3  
            6     7:a2    6:8  
///

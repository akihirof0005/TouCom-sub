ENTRY       G04634                      Glycan
NODE        11
            1   GlcNAc   25.2  -2.5
            2   GlcNAc   15.2  -2.5
            3   Man       6.2  -2.5
            4   Man      -1.8   2.5
            5   Man      -1.8  -7.5
            6   Man      -9.8   7.5
            7   Man      -9.8  -2.5
            8   Man      -9.8  -7.5
            9   Man     -17.8   7.5
            10  Man     -17.8  -7.5
            11  Glc     -24.8  -7.5
EDGE        10
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    4:3  
            7     8:a1    5:2  
            8     9:a1    6:2  
            9    10:a1    8:2  
            10   11:1    10:3  
///

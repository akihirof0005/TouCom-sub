ENTRY       G11885                      Glycan
NODE        15
            1   Asn        28     4
            2   GlcNAc     20     4
            3   LFuc       11     7
            4   GlcNAc     11     1
            5   Man         3     1
            6   Man        -5     5
            7   Man        -5    -4
            8   GlcNAc    -13     5
            9   GlcNAc    -13    -4
            10  Gal       -21     8
            11  LFuc      -21     2
            12  Gal       -21    -1
            13  LFuc      -21    -7
            14  Neu5Ac    -29     8
            15  Neu5Ac    -29    -1
EDGE        14
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:2  
            9    10:b1    8:4  
            10   11:a1    8:3  
            11   12:b1    9:4  
            12   13:a1    9:3  
            13   14:a2   10:3  
            14   15:a2   12:3  
///

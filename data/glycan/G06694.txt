ENTRY       G06694                      Glycan
NODE        7
            1   2,5-Anhydro-Man    17     1
            2   S           8     5
            3   Gal         2     1
            4   GlcNAc     -8     1
            5   S         -13     5
            6   Gal       -18     1
            7   LFuc      -18    -4
EDGE        6
            1     2       1:1  
            2     3:b1    1:3  
            3     4:b1    3:3  
            4     5       4:6  
            5     6:b1    4:4  
            6     7:a1    4:3  
///

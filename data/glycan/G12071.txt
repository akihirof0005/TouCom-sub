ENTRY       G12071                      Glycan
NODE        9
            1   Asn        28     0
            2   GlcNAc     20     0
            3   GlcNAc     11     0
            4   Man         3     0
            5   Man        -5     4
            6   Man        -5    -4
            7   GlcNAc    -13    -4
            8   GalNAc    -22    -4
            9   S         -28    -4
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    6:2  
            7     8:b1    7:4  
            8     9       8:4  
///

ENTRY       G00227                      Glycan
NODE        5
            1   Ser/Thr    14     0
            2   GalNAc      4     0
            3   Gal        -5    -5
            4   Neu5Gc     -6     5
            5   LFuc      -14    -5
EDGE        4
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:a2    2:6  
            4     5:a1    3:2  
///

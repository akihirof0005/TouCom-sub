ENTRY       G04831                      Glycan
NODE        10
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     5
            4   Man       -20    -5
            5   Man       -30    10
            6   Man       -30     0
            7   Man       -30    -5
            8   Man       -40    10
            9   Man       -40     0
            10  Man       -40    -5
EDGE        9
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
            5     6:a1    3:3  
            6     7:a1    4:2  
            7     8:a1    5:2  
            8     9:a1    6:2  
            9    10:a1    7:2  
///

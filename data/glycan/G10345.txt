ENTRY       G10345                      Glycan
NODE        6
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30     5
            5   Xyl       -30    -5
            6   GlcNAc    -40     5
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5:b1    3:2  
            5     6:b1    4:2  
///

ENTRY       G08852                      Glycan
NODE        3
            1   2,7-Anhydro-Kdof    12     0
            2   Glc        -3     0
            3   Man       -13     0
EDGE        2
            1     2:a1    1:4  
            2     3:a1    2:4  
///

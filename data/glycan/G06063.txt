ENTRY       G06063                      Glycan
NODE        7
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20     5
            4   LFuc      -20    -5
            5   LFuc      -30    10
            6   Gal       -30     0
            7   LFuc      -40     0
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:a1    2:2  
            4     5:a1    3:4  
            5     6:b1    3:3  
            6     7:a1    6:2  
///

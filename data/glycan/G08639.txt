ENTRY       G08639                      Glycan
NODE        4
            1   Xyl         0     0
            2   Xyl       -10     0
            3   LAraf     -20     0
            4   Xyl       -30     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:b1    3:2  
///

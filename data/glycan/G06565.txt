ENTRY       G06565                      Glycan
NODE        4
            1   Man         0     0
            2   GlcNAc    -10     0
            3   Gal       -20     0
            4   Neu5Ac    -30     0
EDGE        3
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:a2    3:3  
///

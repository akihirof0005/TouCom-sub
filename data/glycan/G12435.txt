ENTRY       G12435                      Glycan
NODE        3
            1   Gal         5     0
            2   GlcNAc     -5     4
            3   GlcNAc     -5    -4
EDGE        2
            1     2:b1    1:4  
            2     3:b1    1:2  
///

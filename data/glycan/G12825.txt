ENTRY       G12825                      Glycan
NODE        4
            1   LGal        7     0
            2   Xyl        -1     4
            3   Xyl        -1    -3
            4   Xyl        -8     4
EDGE        3
            1     2:b1    1:4  
            2     3:b1    1:2  
            3     4:b1    2:4  
///

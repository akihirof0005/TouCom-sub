ENTRY       G07423                      Glycan
NODE        3
            1   Lgro-manHep    14     0
            2   Lgro-manHep    -1     0
            3   GlcNAc    -14     0
EDGE        2
            1     2:a1    1:3  
            2     3:a1    2:2  
///

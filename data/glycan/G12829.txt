ENTRY       G12829                      Glycan
NODE        6
            1   *          15     0
            2   Man         7     0
            3   Man         0     0
            4   Rha        -7    -4
            5   Ribf       -8     4
            6   *         -16    -4
EDGE        5
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5:a1    3:4  
            5     6       4:4  
BRACKET     1 -10.0  -1.1 -10.0  -6.8
            1  11.0  -2.9  11.0   2.7
            1 n
///

ENTRY       G12154                      Glycan
NODE        8
            1   Asn        22     2
            2   GlcNAc     13     2
            3   LFuc        4     5
            4   GlcNAc      3    -1
            5   Man        -6    -1
            6   Man       -14     2
            7   Man       -14    -4
            8   Man       -23     2
EDGE        7
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:a1    6:3  
///

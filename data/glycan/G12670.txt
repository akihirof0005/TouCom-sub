ENTRY       G12670                      Glycan
NODE        6
            1   *          16     0
            2   LRha        7     0
            3   Gal         0    -4
            4   GalNAc     -1     4
            5   LRha       -9     4
            6   *         -16    -4
EDGE        5
            1     2:a1    1    
            2     3:a1    2:2  
            3     4:b1    2:4  
            4     6       3:3  
            5     5:a1    4:3  
BRACKET     1 -12.0   7.0 -12.0  -8.0
            1  12.0  -8.0  12.0   7.0
            1 n
///

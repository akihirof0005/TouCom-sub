ENTRY       G12177                      Glycan
NODE        7
            1   GlcN       18    -1
            2   P          12     2
            3   Man         8    -1
            4   Man         0    -1
            5   Man        -9    -1
            6   P         -15     2
            7   Man       -18    -1
EDGE        6
            1     2       1:6  
            2     3:a1    1:4  
            3     4:a1    3:6  
            4     5:a1    4:2  
            5     6       5:6  
            6     7:a1    5:2  
///

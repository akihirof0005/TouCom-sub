ENTRY       G07204                      Glycan
NODE        4
            1   Dig      13.5     0
            2   Cym       4.5     0
            3   LCym     -4.5     0
            4   Glc     -13.5     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:4  
            3     4:b1    3:4  
///

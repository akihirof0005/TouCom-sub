ENTRY       G03262                      Glycan
NODE        4
            1   Fruf       10     4
            2   Glc        10    -3
            3   Fruf        0    -3
            4   Fruf      -10    -3
EDGE        3
            1     2:a1    1:b2 
            2     3:b2    2:6  
            3     4:b2    3:6  
///

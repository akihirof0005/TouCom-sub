ENTRY       G01251                      Glycan
NODE        5
            1   Xyl        15     0
            2   GalNAc      5     5
            3   GlcNAc      5    -5
            4   Xyl        -5    -5
            5   Glc       -15    -5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    1:2  
            3     4:b1    3:6  
            4     5:b1    4:2  
///

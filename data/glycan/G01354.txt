ENTRY       G01354                      Glycan
NODE        5
            1   Gal        16     3
            2   S           9    -2
            3   3,6-Anhydro-Gal     6     3
            4   Gal        -7     3
            5   3,6-Anhydro-Gal   -17     3
EDGE        4
            1     2       1:4  
            2     3:a1    1:3  
            3     4:b1    3:4  
            4     5:a1    4:3  
///

ENTRY       G12024                      Glycan
NODE        11
            1   Asn        28     2
            2   GlcNAc     20     2
            3   LFuc       11     5
            4   GlcNAc     10    -1
            5   Man         2    -1
            6   Man        -5     2
            7   Man        -5    -4
            8   GlcNAc    -13     2
            9   GlcNAc    -13    -4
            10  GalNAc    -22    -4
            11  S         -28    -4
EDGE        10
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:2  
            9    10:b1    9:4  
            10   11      10:4  
///

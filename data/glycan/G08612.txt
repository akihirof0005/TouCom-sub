ENTRY       G08612                      Glycan
NODE        3
            1   Cer       7.2   0.5
            2   Gal      -0.8   0.5
            3   Gal      -7.8   0.5
EDGE        2
            1     2:b1    1:1  
            2     3:b1    2    
///

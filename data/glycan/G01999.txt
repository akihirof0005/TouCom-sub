ENTRY       G01999                      Glycan
NODE        4
            1   GlcNAc      0     0
            2   Gal       -10     5
            3   LFuc      -10    -5
            4   Neu5Ac    -20     5
EDGE        3
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:a2    2:6  
///

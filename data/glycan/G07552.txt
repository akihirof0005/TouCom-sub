ENTRY       G07552                      Glycan
NODE        8
            1   GlcN       11     0
            2   S           7    -3
            3   LIdoA       2     0
            4   S          -2    -3
            5   GlcN       -7     0
            6   S         -11     3
            7   S         -11    -3
            8   S         -12     0
EDGE        7
            1     2       1:2  
            2     3:a1    1:4  
            3     4       3:2  
            4     5:a1    3:4  
            5     6       5:6  
            6     7       5:2  
            7     8       5:3  
///

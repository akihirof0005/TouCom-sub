ENTRY       G10707                      Glycan
NODE        12
            1   Asn        25     4
            2   GlcNAc     17     4
            3   LFuc        8     9
            4   GlcNAc      8    -1
            5   Man        -1    -1
            6   Man        -9     3
            7   Man        -9    -5
            8   GlcNAc    -17     3
            9   GlcNAc    -17    -5
            10  Gal       -25     3
            11  Gal       -25    -1
            12  LFuc      -25    -9
EDGE        11
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:2  
            9    10:b1    8:4  
            10   11:b1    9:4  
            11   12:a1    9:3  
///

ENTRY       G06915                      Glycan
NODE        9
            1   GlcN       19     0
            2   S          15     4
            3   S          15    -4
            4   LGalA       9     0
            5   GlcN       -1     0
            6   S          -5     4
            7   S          -5    -4
            8   L4-en-thrHexA   -13     0
            9   S         -20    -4
EDGE        8
            1     2       1:6  
            2     3       1:2  
            3     4:a1    1:4  
            4     5:a1    4:4  
            5     6       5:6  
            6     7       5:2  
            7     8:a1    5:4  
            8     9       8:2  
///

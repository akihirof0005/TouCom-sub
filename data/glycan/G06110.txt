ENTRY       G06110                      Glycan
NODE        8
            1   Glc         0     0
            2   Glc6Me    -10     0
            3   Glc       -20     0
            4   Glc6Me    -30     0
            5   Glc       -40     0
            6   Glc6Me    -50     0
            7   Glc       -60     0
            8   Glc6Me    -70     0
EDGE        7
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
            5     6:a1    5:4  
            6     7:a1    6:4  
            7     8:a1    7:4  
///

ENTRY       G04541                      Glycan
NODE        9
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   GlcNAc    -30     5
            5   Gal       -30    -5
            6   GlcNAc    -40    -5
            7   Gal       -50     0
            8   LFuc      -50   -10
            9   Neu5Ac    -60     0
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:a1    6:3  
            8     9:a2    7:3  
///

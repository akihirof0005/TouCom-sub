ENTRY       G09447                      Glycan
NODE        5
            1   GlcN1Ac    12     0
            2   S           7     4
            3   S           7    -4
            4   L4-en-thrHexA3Ac    -3     0
            5   S         -12    -4
EDGE        4
            1     2       1:6  
            2     3       1:2  
            3     4:a1    1:4  
            4     5       4:2  
///

ENTRY       G10167                      Glycan
NODE        4
            1   LRha        0     0
            2   GalA      -10     0
            3   LMan6Me   -20     0
            4   GalA      -30     0
EDGE        3
            1     2:1     1:2  
            2     3:1     2:4  
            3     4:1     3:2  
///

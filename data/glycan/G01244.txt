ENTRY       G01244                      Glycan
NODE        8
            1   GlcN       20     0
            2   Man        10     0
            3   P           4    -4
            4   Man         0     5
            5   GalNAc      0     0
            6   Man       -10     5
            7   P         -16     1
            8   Man       -20     5
EDGE        7
            1     2:a1    1:4  
            2     3       2:2  
            3     4:a1    2:6  
            4     5:b1    2:4  
            5     6:a1    4:2  
            6     7       6:6  
            7     8:a1    6:2  
///

ENTRY       G02030                      Glycan
NODE        5
            1   GlcNAc     18     2
            2   Man         8     2
            3   Man        -2     2
            4   Man       -12     2
            5   P         -18    -2
EDGE        4
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    3:2  
            4     5       4:6  
///

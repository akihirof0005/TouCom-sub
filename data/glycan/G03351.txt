ENTRY       G03351                      Glycan
NODE        3
            1   Gal         5     0
            2   LFuc       -5     5
            3   LFuc       -5    -5
EDGE        2
            1     2:b1    1:3  
            2     3:b1    1:2  
///

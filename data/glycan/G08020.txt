ENTRY       G08020                      Glycan
NODE        4
            1   Ser/Thr    13     0
            2   Gal         6     0
            3   Galf       -2     0
            4   GlcNAc3Me   -13     0
EDGE        3
            1     2:a1    1    
            2     3:a1    2:2  
            3     4:a1    3:2  
///

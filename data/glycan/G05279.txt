ENTRY       G05279                      Glycan
NODE        6
            1   Cer        21     0
            2   Glc        13     0
            3   Gal         5     0
            4   Neu5Ac     -4     0
            5   Gal       -13     0
            6   Galf      -21     0
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
            4     5:a1    4:4  
            5     6:b1    5:3  
///

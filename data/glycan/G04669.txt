ENTRY       G04669                      Glycan
NODE        10
            1   GlcNAc     26     2
            2   LFuc       17     6
            3   GlcNAc     16    -2
            4   Man         7    -2
            5   Man        -1     2
            6   Man        -1    -6
            7   GlcNAc    -10     2
            8   GlcNAc    -10    -6
            9   Gal       -19     2
            10  Gal       -26     2
EDGE        9
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:a1    9:3  
///

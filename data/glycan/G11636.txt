ENTRY       G11636                      Glycan
NODE        4
            1   *          15     0
            2   Glc         5     0
            3   GlcA       -5     0
            4   *         -15     0
EDGE        3
            1     2:b1    1    
            2     3:b1    2:4  
            3     4       3:3  
BRACKET     1 -11.0   3.0 -11.0  -3.0
            1  11.0  -3.0  11.0   3.0
            1 n
///

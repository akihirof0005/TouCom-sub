ENTRY       G01575                      Glycan
NODE        3
            1   GlcA        5     0
            2   Glc        -5     5
            3   Xyl        -5    -5
EDGE        2
            1     2:b1    1:4  
            2     3:b1    1:2  
///

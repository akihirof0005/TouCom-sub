ENTRY       G01320                      Glycan
NODE        12
            1   GlcNAc     30     3
            2   LFuc       20     8
            3   GlcNAc     20    -2
            4   Man        10    -2
            5   Man         0     3
            6   Man         0    -7
            7   GlcNAc    -10     3
            8   GlcNAc    -10    -7
            9   Gal       -20     3
            10  Gal       -20    -7
            11  Gal       -30     3
            12  Neu5Gc    -30    -7
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:a1    9:3  
            11   12:a2   10:6  
///

ENTRY       G00300                      Glycan
NODE        6
            1   GlcNAc     15    -2
            2   Man         5    -2
            3   Man        -5     3
            4   Man        -5    -7
            5   Man       -15     8
            6   Man       -15    -2
EDGE        5
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
            5     6:a1    3:3  
///

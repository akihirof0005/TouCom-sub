ENTRY       G08824                      Glycan
NODE        3
            1   1,6-Anhydro-Glc     0     0
            2   Gal       -13     0
            3   LFuc      -23     0
EDGE        2
            1     2:b1    1:4  
            2     3:b1    2:6  
///

ENTRY       G02227                      Glycan
NODE        9
            1   GlcNAc     20     0
            2   GlcNAc     11     0
            3   Man         3     0
            4   Man        -4     5
            5   Man        -4    -5
            6   GlcNAc    -12     5
            7   GlcNAc    -12    -5
            8   Glc       -20     5
            9   Glc       -20    -5
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:2  
            7     8:b1    6:4  
            8     9:b1    7:4  
///

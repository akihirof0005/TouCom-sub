ENTRY       G03340                      Glycan
NODE        5
            1   2,5-Anhydro-Man    18     2
            2   GlcA        5     2
            3   GlcNAc     -5     2
            4   LIdoA     -15     2
            5   S         -19    -2
EDGE        4
            1     2:b1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5       4:2  
///

ENTRY       G06772                      Glycan
NODE        4
            1   P          15     0
            2   GalA6N      5     0
            3   GlcNAc     -5     0
            4   GlcNAc    -15     0
EDGE        3
            1     2:a1    1    
            2     3:b1    2:2  
            3     4:b1    3:4  
///

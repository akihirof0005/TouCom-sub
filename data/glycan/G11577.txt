ENTRY       G11577                      Glycan
NODE        10
            1   GalNAc     25     0
            2   GlcNAc     15     5
            3   Gal        15    -5
            4   Gal         5     5
            5   Gal         5    -5
            6   Gal        -5     5
            7   Gal        -5    -5
            8   Gal       -15     5
            9   LFuc      -15    -5
            10  LFuc      -25     5
EDGE        9
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a1    3:4  
            5     6:a1    4:4  
            6     7:a1    5:3  
            7     8:a1    6:3  
            8     9:a1    7:2  
            9    10:a1    8:2  
///

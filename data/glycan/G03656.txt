ENTRY       G03656                      Glycan
NODE        14
            1   GlcNAc     25   2.5
            2   LFuc       16   7.5
            3   GlcNAc     16  -2.5
            4   Man         7  -2.5
            5   Man        -1   2.5
            6   Man        -1  -7.5
            7   GlcNAc     -9   7.5
            8   GlcNAc     -9  -2.5
            9   GlcNAc     -9  -7.5
            10  Gal       -17   7.5
            11  Gal       -17  -2.5
            12  Gal       -17  -7.5
            13  Neu5Ac    -25   7.5
            14  Neu5Ac    -25  -7.5
EDGE        13
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    5    
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   13:a2   10:3  
            13   14:a2   12:3  
///

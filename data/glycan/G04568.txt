ENTRY       G04568                      Glycan
NODE        10
            1   GalNAc      0     0
            2   GlcNAc    -10     0
            3   Gal       -20     0
            4   GlcNAc    -30     5
            5   GlcNAc    -30    -5
            6   Gal       -40     5
            7   Gal       -40    -5
            8   GalNAc    -50     5
            9   GalNAc    -50     0
            10  LFuc      -50   -10
EDGE        9
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:6  
            4     5:b1    3:3  
            5     6:b1    4:4  
            6     7:b1    5:4  
            7     8:a1    6:3  
            8     9:a1    7:3  
            9    10:a1    7:2  
///

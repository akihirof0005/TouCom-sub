ENTRY       G01708                      Glycan
NODE        4
            1   Gal        10     0
            2   Xyl         0     5
            3   Glc         0    -5
            4   Glc       -10    -5
EDGE        3
            1     2:b1    1:4  
            2     3:b1    1:2  
            3     4:b1    3:3  
///

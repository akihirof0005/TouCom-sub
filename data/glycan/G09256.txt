ENTRY       G09256                      Glycan
NODE        4
            1   LIdoA       8     3
            2   S           3    -2
            3   Glc        -4     3
            4   S          -9    -2
EDGE        3
            1     2       1:3  
            2     3:a1    1:4  
            3     4       3:2  
///

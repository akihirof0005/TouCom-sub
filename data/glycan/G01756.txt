ENTRY       G01756                      Glycan
NODE        4
            1   Glc        16     0
            2   Glc         8     0
            3   3,6-Anhydro-Glc    -4     0
            4   Glc       -16     0
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
///

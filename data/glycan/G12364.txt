ENTRY       G12364                      Glycan
NODE        4
            1   Glc        12     0
            2   Gal         3     0
            3   Glc        -6     0
            4   P         -12     0
EDGE        3
            1     2:a1    1:2  
            2     3:b1    2:6  
            3     4       3:6  
///

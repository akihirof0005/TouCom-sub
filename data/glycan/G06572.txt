ENTRY       G06572                      Glycan
NODE        3
            1   GlcNAc    8.2   0.5
            2   Gal       0.2   0.5
            3   Neu5Ac   -8.8   0.5
EDGE        2
            1     2:b1    1    
            2     3:a2    2:3  
///

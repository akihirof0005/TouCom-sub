ENTRY       G11751                      Glycan
NODE        14
            1   GlcNAc     24    -2
            2   GlcNAc     15    -2
            3   Man         7    -2
            4   Man         0     2
            5   Man         0    -6
            6   GlcNAc     -8     6
            7   GlcNAc     -8    -2
            8   GlcNAc     -8    -6
            9   Gal       -16     6
            10  Gal       -16    -2
            11  Gal       -16    -6
            12  Neu5Ac    -24     6
            13  Neu5Ac    -24    -2
            14  Neu5Ac    -24    -6
EDGE        13
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:6  
            6     7:b1    4:2  
            7     8:b1    5:4  
            8     9:b1    6:4  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:a2    9:3  
            12   13:a2   10:3  
            13   14:a2   11:3  
///

ENTRY       G11303                      Glycan
NODE        4
            1   Ino-P      10     0
            2   Man         0     0
            3   Man       -10     5
            4   Man       -10    -5
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
///

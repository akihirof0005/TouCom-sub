ENTRY       G07081                      Glycan
NODE        4
            1   GlcN        0     0
            2   Man       -10     0
            3   Man       -20     0
            4   Galf      -30     0
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:3  
            3     4:a1    3:3  
///

ENTRY       G04755                      Glycan
NODE        9
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   Gal       -30     5
            5   Gal       -30    -5
            6   GalNAc    -40     5
            7   Gal       -40    -5
            8   GalNAc    -50     0
            9   LFuc      -50   -10
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:6  
            4     5:b1    3:3  
            5     6:a1    4:3  
            6     7:b1    5:3  
            7     8:a1    7:3  
            8     9:a1    7:2  
///

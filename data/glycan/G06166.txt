ENTRY       G06166                      Glycan
NODE        7
            1   Glc      12.2  -0.5
            2   D/LAll   12.2  -8.5
            3   Glc       4.2   4.5
            4   Glc       4.2  -5.5
            5   Glc      -3.8   4.5
            6   Glc     -11.8   9.5
            7   Glc     -11.8  -0.5
EDGE        6
            1     2:1     1:b1 
            2     3:b1    1:6  
            3     4:b1    1:3  
            4     5:b1    3:6  
            5     6:b1    5:6  
            6     7:b1    5:3  
///

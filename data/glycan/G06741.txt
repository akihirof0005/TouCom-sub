ENTRY       G06741                      Glycan
NODE        4
            1   Glc        13     0
            2   Gal         4     0
            3   Gal        -5     0
            4   Gal       -14     0
EDGE        3
            1     2:a1    1:6  
            2     3:a1    2:6  
            3     4:b1    3:6  
///

ENTRY       G04630                      Glycan
NODE        10
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   GlcNAc    -30     0
            5   Gal       -40     0
            6   GlcNAc    -50     5
            7   GlcNAc    -50    -5
            8   Gal       -60    10
            9   LFuc      -60     0
            10  Gal       -60    -5
EDGE        9
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:b1    5:6  
            6     7:b1    5:3  
            7     8:b1    6:4  
            8     9:a1    6:3  
            9    10:b1    7:3  
///

ENTRY       G01478                      Glycan
NODE        4
            1   GalA       10     0
            2   Gal         0     5
            3   LRha        0    -5
            4   Glc       -10     5
EDGE        3
            1     2:b1    1:4  
            2     3:a1    1:2  
            3     4:b1    2:6  
///

ENTRY       G10805                      Glycan
NODE        7
            1   Asn        25     0
            2   GlcNAc     15     0
            3   GlcNAc      5     0
            4   Man        -5     0
            5   Man       -15     5
            6   Man       -15    -5
            7   Man       -25    -5
EDGE        6
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    6:2  
///

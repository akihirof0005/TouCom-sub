ENTRY       G00374                      Glycan
NODE        11
            1   GlcNAc     21    -2
            2   GlcNAc     11    -2
            3   Man         2    -2
            4   Man        -6     3
            5   Man        -6    -7
            6   Man       -14     8
            7   Man       -14    -2
            8   Man       -14    -7
            9   Man       -22     8
            10  Man       -22    -2
            11  Man       -22    -7
EDGE        10
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    4:3  
            7     8:a1    5:2  
            8     9:a1    6:2  
            9    10:a1    7:2  
            10   11:a1    8:2  
///

ENTRY       G08252                      Glycan
NODE        3
            1   Man         5     0
            2   Man        -5     5
            3   Lyx        -5    -5
EDGE        2
            1     2:a1    1:6  
            2     3:a1    1:3  
///

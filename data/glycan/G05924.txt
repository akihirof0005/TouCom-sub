ENTRY       G05924                      Glycan
NODE        7
            1   P          24     3
            2   GlcN       16     3
            3   GlcN        6     3
            4   P           0    -2
            5   Kdo        -4     3
            6   Kdo       -14     3
            7   Kdo       -24     3
EDGE        6
            1     2:a1    1    
            2     3:b1    2:6  
            3     4       3:4  
            4     5:a2    3:6  
            5     6:a2    5:4  
            6     7:a2    6:4  
///

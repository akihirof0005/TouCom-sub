ENTRY       G05129                      Glycan
NODE        8
            1   Fruf       24     3
            2   Glc        24    -3
            3   Gal        16    -3
            4   Gal         8    -3
            5   Gal         0    -3
            6   Gal        -8    -3
            7   Gal       -16    -3
            8   Gal       -24    -3
EDGE        7
            1     2:a1    1:b2 
            2     3:a1    2:6  
            3     4:a1    3:6  
            4     5:a1    4:6  
            5     6:a1    5:6  
            6     7:a1    6:6  
            7     8:a1    7:6  
///

ENTRY       G00258                      Glycan
NODE        4
            1   Cer        15     0
            2   Glc         5     0
            3   Gal        -5     0
            4   Neu5Gc    -15     0
EDGE        3
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:a2    3:3  
///

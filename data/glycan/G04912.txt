ENTRY       G04912                      Glycan
NODE        9
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   Gal       -30     5
            5   Gal       -30    -5
            6   GlcNAc    -40     5
            7   GlcNAc    -40    -5
            8   Gal       -50     0
            9   LFuc      -50   -10
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:6  
            4     5:b1    3:3  
            5     6:b1    4:4  
            6     7:b1    5:4  
            7     8:b1    7:4  
            8     9:a1    7:3  
///

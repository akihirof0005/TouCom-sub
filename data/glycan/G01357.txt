ENTRY       G01357                      Glycan
NODE        3
            1   Gal         5     0
            2   GalNAc     -5     5
            3   Neu5Ac     -5    -5
EDGE        2
            1     2:b1    1:4  
            2     3:a2    1:3  
///

ENTRY       G04881                      Glycan
NODE        10
            1   Man         0     0
            2   Xyl       -10    10
            3   Man       -10     0
            4   GlcA      -10   -10
            5   Xyl       -20    10
            6   Xyl       -20     5
            7   Man       -20     0
            8   Xyl       -20    -5
            9   Xyl       -30     5
            10  Xyl       -30    -5
EDGE        9
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    1:2  
            4     5:b1    2:4  
            5     6:b1    3:4  
            6     7:a1    3:3  
            7     8:b1    3:2  
            8     9:b1    7:4  
            9    10:b1    7:2  
///

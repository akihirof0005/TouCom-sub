ENTRY       G05542                      Glycan
NODE        8
            1   GlcN        0     0
            2   GlcN      -10     0
            3   GlcN      -20     0
            4   GlcN      -30     0
            5   GlcN      -40     0
            6   GlcN      -50     0
            7   GlcN      -60     0
            8   GlcN      -70     0
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:b1    5:4  
            6     7:b1    6:4  
            7     8:b1    7:4  
///

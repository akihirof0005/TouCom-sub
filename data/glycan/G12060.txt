ENTRY       G12060                      Glycan
NODE        9
            1   Cer        28     0
            2   Glc        21     0
            3   Gal        14     0
            4   Gal         7     0
            5   Gal         0     0
            6   Gal        -7     0
            7   Gal       -14     0
            8   Gal       -21     0
            9   Gal       -28     0
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:3  
            6     7:b1    6:3  
            7     8:b1    7:3  
            8     9:b1    8:3  
///

ENTRY       G08402                      Glycan
NODE        3
            1   GlcA        0     0
            2   Xyl       -10     5
            3   Glc       -10    -5
EDGE        2
            1     2:b1    1:4  
            2     3:b1    1:2  
///

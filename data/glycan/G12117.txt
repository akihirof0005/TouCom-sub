ENTRY       G12117                      Glycan
NODE        8
            1   Gal        17     0
            2   GalNAc      9     0
            3   LFuc        0    -3
            4   GalNAc     -1     3
            5   LFuc       -9     3
            6   LFuc       -9    -3
            7   LFuc      -17     3
            8   LFuc      -17    -3
EDGE        7
            1     2:b1    1:3  
            2     3:a1    2:3  
            3     4:b1    2:4  
            4     6:a1    3:2  
            5     5:a1    4:3  
            6     7:a1    5:2  
            7     8:a1    6:2  
///

ENTRY       G06381                      Glycan
NODE        5
            1   GlcNAc     15     2
            2   Man         5     2
            3   P           1    -2
            4   Man        -5     2
            5   Man       -15     2
EDGE        4
            1     2:a1    1:4  
            2     3       2:6  
            3     4:a1    2:2  
            4     5:a1    4:2  
///

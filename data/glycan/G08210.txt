ENTRY       G08210                      Glycan
NODE        4
            1   LFuc       14     0
            2   GlcA        4     0
            3   Glc        -6     0
            4   Glc       -15     0
EDGE        3
            1     2:a1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:4  
///

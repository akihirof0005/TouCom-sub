ENTRY       G01154                      Glycan
NODE        9
            1   Cer        30    -2
            2   Glc        20    -2
            3   Gal        10    -2
            4   GlcNAc      0    -2
            5   Gal       -10     3
            6   LFuc      -10    -7
            7   GlcNAc    -20     3
            8   LFuc      -30     8
            9   Gal       -30    -2
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     7:b1    5:3  
            7     8:a1    7:4  
            8     9:b1    7:3  
///

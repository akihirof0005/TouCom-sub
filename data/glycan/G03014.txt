ENTRY       G03014                      Glycan
NODE        5
            1   Qui      11.1  -0.1
            2   Xyl       3.1  -0.1
            3   Fuc      -3.9   4.9
            4   Qui      -3.9  -5.1
            5   Fuc     -11.9   4.9
EDGE        4
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    2:2  
            4     5:b1    3:2  
///

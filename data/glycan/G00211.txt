ENTRY       G00211                      Glycan
NODE        6
            1   GalNAc      8     2
            2   Gal         0    -2
            3   GlcNAc     -1     6
            4   GlcNAc     -8     2
            5   GlcNAc     -8    -6
            6   Gal        -9     6
EDGE        5
            1     2:b1    1:3  
            2     3:b1    1:6  
            3     4:b1    2:6  
            4     5:b1    2:3  
            5     6:b1    3:4  
///

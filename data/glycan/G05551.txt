ENTRY       G05551                      Glycan
NODE        8
            1   Lgro-manHep    28     3
            2   Lgro-manHep    13     3
            3   Glc         1    -2
            4   Lgro-manHep    -1     8
            5   Gal        -9    -2
            6   GlcNAc    -19     3
            7   Glc       -19    -7
            8   Glc       -29    -7
EDGE        7
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:a1    2:7  
            4     5:a1    3:3  
            5     6:a1    5:3  
            6     7:a1    5:2  
            7     8:a1    7:2  
///

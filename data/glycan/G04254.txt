ENTRY       G04254                      Glycan
NODE        11
            1   Cer        31   0.5
            2   D/LGlc     22   0.5
            3   Gal        13   0.5
            4   GlcNAc      4   0.5
            5   Gal        -5   0.5
            6   GlcNAc    -14   5.5
            7   GlcNAc    -14  -4.5
            8   Gal       -23   5.5
            9   Gal       -23  -4.5
            10  Gal       -31   5.5
            11  Gal       -31  -4.5
EDGE        10
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:6  
            6     7:b1    5:3  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10:a1    8:3  
            10   11:a1    9:3  
///

ENTRY       G11947                      Glycan
NODE        12
            1   Asn        25    -2
            2   GlcNAc     16    -2
            3   GlcNAc      6    -2
            4   Man        -3    -2
            5   Man       -10     2
            6   Man       -10    -6
            7   Man       -17     6
            8   Man       -17    -2
            9   Man       -17    -6
            10  P         -22    -2
            11  Man       -25     6
            12  Man       -25    -6
EDGE        11
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:6  
            7     8:a1    5:3  
            8     9:a1    6:2  
            9    11:a1    7:2  
            10   10       8:6  
            11   12:a1    9:2  
///

ENTRY       G01936                      Glycan
NODE        7
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20     0
            4   Gal       -30     5
            5   LFuc      -30    -5
            6   GalNAc    -40    10
            7   LFuc      -40     0
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6:a1    4:3  
            6     7:a1    4:2  
///

ENTRY       G12606                      Glycan
NODE        5
            1   *          20     0
            2   Glc6Ac     10     0
            3   LRha        0     0
            4   GalA      -10     0
            5   *         -20     0
EDGE        4
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:a1    3:3  
            4     5       4:3  
BRACKET     1 -14.0   4.0 -14.0  -5.0
            1  15.0  -5.0  15.0   4.0
            1 n
///

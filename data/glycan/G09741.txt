ENTRY       G09741                      Glycan
NODE        3
            1   Glc4,6X     2     7
            2   Glc4,6X     2    -1
            3   S          -3    -6
EDGE        2
            1     2:1     1:a1 
            2     3       2:2  
///

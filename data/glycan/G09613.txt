ENTRY       G09613                      Glycan
NODE        3
            1   Glc         0     0
            2   Glc       -10     5
            3   Man       -10    -5
EDGE        2
            1     2:b1    1:4  
            2     3:a1    1:3  
///

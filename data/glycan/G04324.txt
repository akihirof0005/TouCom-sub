ENTRY       G04324                      Glycan
NODE        8
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   Gal       -30     0
            5   GalNAc    -40     0
            6   Neu5Ac    -50     5
            7   Gal       -50    -5
            8   Neu5Ac    -60    -5
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:b1    4:3  
            5     6:a2    5:6  
            6     7:b1    5:3  
            7     8:a2    7:6  
///

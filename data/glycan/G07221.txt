ENTRY       G07221                      Glycan
NODE        5
            1   Glc        13     3
            2   Glc        13    -3
            3   Glc         4     3
            4   Glc        -4     3
            5   Glc3Me    -13     3
EDGE        4
            1     3:b1    1:6  
            2     1:a1    2:a1 
            3     4:b1    3:4  
            4     5:b1    4:3  
///

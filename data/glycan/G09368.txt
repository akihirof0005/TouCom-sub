ENTRY       G09368                      Glycan
NODE        3
            1   2,6dxylHex3Me  12.8   0.1
            2   Cym      -0.2   0.1
            3   2,6daraHex3Me -13.2   0.1
EDGE        2
            1     2:b1    1:4  
            2     3:b1    2:4  
///

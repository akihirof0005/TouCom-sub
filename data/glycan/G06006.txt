ENTRY       G06006                      Glycan
NODE        7
            1   Glc         0     0
            2   Glc       -10     0
            3   Glc       -20     0
            4   Xyl       -30     5
            5   Glc       -30    -5
            6   Xyl       -40    -5
            7   Gal       -50    -5
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    3:4  
            5     6:a1    5:6  
            6     7:b1    6:2  
///

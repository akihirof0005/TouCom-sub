ENTRY       G04251                      Glycan
NODE        12
            1   GlcNAc      0     0
            2   LFucNAc   -10     0
            3   GalNAc    -20     0
            4   Gal       -30     0
            5   GlcNAc    -40     0
            6   LFucNAc   -50     0
            7   GalNAc    -60     0
            8   Gal       -70     0
            9   GlcNAc    -80     0
            10  LFucNAc   -90     0
            11  GalNAc   -100     0
            12  Gal      -110     0
EDGE        11
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:a1    3:4  
            4     5:b1    4:2  
            5     6:a1    5:3  
            6     7:a1    6:3  
            7     8:a1    7:4  
            8     9:b1    8:4  
            9    10:a1    9:3  
            10   11:a1   10:3  
            11   12:a1   11:4  
///

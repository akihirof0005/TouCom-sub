ENTRY       G08019                      Glycan
NODE        4
            1   Gal         9     1
            2   Gal         0     1
            3   GalNAc    -10     6
            4   LFuc      -10    -5
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:a1    2:2  
///

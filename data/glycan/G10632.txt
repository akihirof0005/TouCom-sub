ENTRY       G10632                      Glycan
NODE        4
            1   Ser        12     0
            2   Xyl         5     0
            3   Gal        -3     0
            4   Neu5Ac    -12     0
EDGE        3
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:a2    3:3  
///

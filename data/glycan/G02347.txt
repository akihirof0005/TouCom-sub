ENTRY       G02347                      Glycan
NODE        4
            1   Gal        10     0
            2   Glc         0     0
            3   Xyl3Ac    -10     5
            4   Glc       -10    -5
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    2:2  
///

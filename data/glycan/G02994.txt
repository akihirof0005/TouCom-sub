ENTRY       G02994                      Glycan
NODE        6
            1   Cer      18.2   0.5
            2   Glc      11.2   0.5
            3   Gal       4.2   0.5
            4   Gal      -2.8   0.5
            5   GalNAc  -10.8   0.5
            6   LFuc    -18.8   0.5
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:b1    4:3  
            5     6:a1    5    
///

ENTRY       G10454                      Glycan
NODE        5
            1   GalNAc   10.1  -2.5
            2   Gal       0.1  -7.5
            3   Kdn         0   2.4
            4   LFuc     -9.2   7.2
            5   LFuc     -9.2  -2.8
EDGE        4
            1     2:a1    1:3  
            2     3:a2    1:6  
            3     4:a1    3:5  
            4     5:a1    3:4  
///

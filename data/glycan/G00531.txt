ENTRY       G00531                      Glycan
NODE        7
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     5
            4   Man       -20    -5
            5   GlcNAc    -30    -5
            6   Gal       -40    -5
            7   Neu5Ac    -50    -5
EDGE        6
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:b1    4:2  
            5     6:b1    5:4  
            6     7:a2    6:6  
///

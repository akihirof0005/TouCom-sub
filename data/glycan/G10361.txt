ENTRY       G10361                      Glycan
NODE        6
            1   D/LGlc   25.2   0.5
            2   D/LGlc   15.2   0.5
            3   D/LGlc    5.2   0.5
            4   D/LGlc   -4.8   0.5
            5   D/LGlc  -14.8   0.5
            6   D/LGlc  -24.8   0.5
EDGE        5
            1     2:1     1:6  
            2     3:1     2:6  
            3     4:1     3:6  
            4     5:1     4:6  
            5     6:1     5:6  
///

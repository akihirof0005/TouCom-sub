ENTRY       G08257                      Glycan
NODE        4
            1   Ser/Thr  14.2   0.5
            2   GalNAc    6.2   0.5
            3   Gal4Me   -3.8   0.5
            4   LFuc2Me -13.8   0.5
EDGE        3
            1     2:1     1    
            2     3:b1    2:3  
            3     4:a1    3:2  
///

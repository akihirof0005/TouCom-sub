ENTRY       G12233                      Glycan
NODE        5
            1   Ino-P      14     0
            2   GlcN        7     0
            3   Man         0     0
            4   Man        -7     0
            5   Galf      -15     0
EDGE        4
            1     2:1     1:6  
            2     3:a1    2:4  
            3     4:a1    3:3  
            4     5:b1    4:3  
///

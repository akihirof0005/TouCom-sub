ENTRY       G00391                      Glycan
NODE        5
            1   Glc        12     0
            2   Gal         5     0
            3   GlcNAc     -3     0
            4   Gal       -11    -4
            5   Neu5Ac    -12     4
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:a2    3:6  
///

ENTRY       G00695                      Glycan
NODE        12
            1   GlcNAc     21     2
            2   GlcNAc     11     2
            3   Man         2     2
            4   Man        -6     8
            5   Man        -6    -4
            6   GlcNAc    -14     8
            7   GlcNAc    -14     0
            8   GlcNAc    -14    -8
            9   Gal       -22     8
            10  Gal       -22     4
            11  LFuc      -22    -4
            12  Gal       -22    -8
EDGE        11
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    7:4  
            10   11:a1    7:3  
            11   12:b1    8:4  
///

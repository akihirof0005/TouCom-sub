ENTRY       G00853                      Glycan
NODE        4
            1   Man        12     2
            2   GlcNAc      2     2
            3   GalNAc     -8     2
            4   S         -13    -2
EDGE        3
            1     2:b1    1:2  
            2     3:b1    2:4  
            3     4       3:4  
///

ENTRY       G06912                      Glycan
NODE        5
            1   Gal         0     0
            2   GlcNAc    -10     0
            3   Gal       -20     0
            4   GlcNAc    -30     5
            5   GlcNAc    -30    -5
EDGE        4
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:6  
            4     5:b1    3:3  
///

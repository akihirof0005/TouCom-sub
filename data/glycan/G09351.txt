ENTRY       G09351                      Glycan
NODE        3
            1   Glc         5     4
            2   Man-ol      5    -3
            3   Glc        -5     4
EDGE        2
            1     3:b1    1:6  
            2     1:b1    2:1  
///

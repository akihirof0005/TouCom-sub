ENTRY       G09929                      Glycan
NODE        3
            1   D/LGal   10.2   0.5
            2   D/LGalf   0.2   0.5
            3   D/LGalf  -9.8   0.5
EDGE        2
            1     2:1     1:3  
            2     3:1     2:5  
///

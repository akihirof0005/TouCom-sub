ENTRY       G03523                      Glycan
NODE        4
            1   GlcA       10     2
            2   GlcNAc      0     2
            3   S          -5    -2
            4   LIdoA     -10     2
EDGE        3
            1     2:a1    1:4  
            2     3       2:6  
            3     4:a1    2:4  
///

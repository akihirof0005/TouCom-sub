ENTRY       G04700                      Glycan
NODE        8
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   Gal       -20     5
            5   Neu5Ac    -20    -5
            6   GlcNAc    -30     5
            7   Gal       -40     5
            8   LFuc      -50     5
EDGE        7
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
            6     7:b1    6:4  
            7     8:a1    7:2  
///

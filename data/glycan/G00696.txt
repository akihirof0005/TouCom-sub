ENTRY       G00696                      Glycan
NODE        7
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30     5
            5   GlcNAc    -30     0
            6   Man       -30    -5
            7   GlcNAc    -40    -5
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    3:4  
            5     6:a1    3:3  
            6     7:b1    6:2  
///

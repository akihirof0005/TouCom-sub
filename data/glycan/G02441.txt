ENTRY       G02441                      Glycan
NODE        3
            1   L6dGlc2Ac3Me  10.3   0.4
            2   Glc      -1.7   0.4
            3   Glc      -9.7   0.4
EDGE        2
            1     2:b1    1:4  
            2     3:b1    2:6  
///

ENTRY       G12021                      Glycan
NODE        11
            1   Asn        28     2
            2   GlcNAc     20     2
            3   LFuc       11     6
            4   GlcNAc     11    -2
            5   Man         3    -2
            6   Man        -4     2
            7   Man        -4    -6
            8   GlcNAc    -12     2
            9   GlcNAc    -12    -6
            10  Gal       -20     2
            11  Neu5Ac    -28     2
EDGE        10
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:2  
            9    10:b1    8:4  
            10   11:a2   10:6  
///

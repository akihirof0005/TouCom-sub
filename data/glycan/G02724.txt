ENTRY       G02724                      Glycan
NODE        9
            1   Gal        12     0
            2   GlcNAc      3     6
            3   GlcNAc      3    -6
            4   Gal        -5    10
            5   LFuc       -5     2
            6   Gal        -5    -2
            7   LFuc       -5   -10
            8   Neu5Ac    -13    10
            9   Neu5Ac    -13    -2
EDGE        8
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a1    2:3  
            5     6:b1    3:4  
            6     7:a1    3:3  
            7     8:a2    4:3  
            8     9:a2    6:3  
///

ENTRY       G02433                      Glycan
NODE        3
            1   Xyl         0     0
            2   Xyl4Ac    -10     0
            3   Glc4Ac6Ac   -20     0
EDGE        2
            1     2:b1    1:2  
            2     3:b1    2:2  
///

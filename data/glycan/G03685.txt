ENTRY       G03685                      Glycan
NODE        15
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30    10
            5   Man       -30   -10
            6   GlcNAc    -40    15
            7   GlcNAc    -40     5
            8   GlcNAc    -40    -5
            9   GlcNAc    -40   -15
            10  Gal       -50    15
            11  Gal       -50     5
            12  Gal       -50   -15
            13  GlcNAc    -60    15
            14  GlcNAc    -60     5
            15  GlcNAc    -60   -15
EDGE        14
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:6  
            6     7:b1    4:2  
            7     8:b1    5:4  
            8     9:b1    5:2  
            9    10:b1    6:4  
            10   11:b1    7:4  
            11   12:b1    9:4  
            12   13:b1   10:3  
            13   14:b1   11:3  
            14   15:b1   12:3  
///

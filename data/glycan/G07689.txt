ENTRY       G07689                      Glycan
NODE        5
            1   Xyl         0     0
            2   Xyl       -10     0
            3   Xyl       -20     5
            4   LAraf     -20     0
            5   LAraf     -20    -5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:a1    2:2  
///

ENTRY       G07001                      Glycan
NODE        6
            1   Glc         0     0
            2   Glc       -10     0
            3   Xyl       -20     5
            4   Glc       -20    -5
            5   Xyl       -30    -5
            6   LAraf     -40    -5
EDGE        5
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:a1    4:6  
            5     6:a1    5:2  
///

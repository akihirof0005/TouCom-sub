ENTRY       G12188                      Glycan
NODE        6
            1   *        27.9   0.4
            2   Qui4N    17.9   0.4
            3   GlcNAc    7.9   0.4
            4   GalANAc  -3.1   0.4
            5   GalANAc3Ac6N -16.1   0.4
            6   *       -27.1   0.4
EDGE        5
            1     2:b1    1    
            2     3:a1    2:3  
            3     4:a1    3:3  
            4     5:a1    4:4  
            5     6       5:4  
BRACKET     1 -23.1   3.4 -23.1  -2.6
            1  23.9  -2.6  23.9   3.4
            1 n
///

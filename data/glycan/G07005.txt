ENTRY       G07005                      Glycan
NODE        5
            1   3-en-eryHex  14.2   0.5
            2   GlcNAc    3.2   0.5
            3   Gal      -5.8   0.5
            4   Gal     -13.8   5.5
            5   LFuc    -13.8  -4.5
EDGE        4
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5:a1    3:2  
///

ENTRY       G06104                      Glycan
NODE        6
            1   Kdo      14.2   0.5
            2   Lgro-Hep   5.2   5.5
            3   Lgro-manHep   4.2  -4.5
            4   Glc      -6.8  -4.5
            5   Lgro-manHep  -7.8   5.5
            6   Gal     -14.8  -4.5
EDGE        5
            1     2:a1    1    
            2     3:a1    1    
            3     5:a1    2:2  
            4     4:b1    3:4  
            5     6:b1    4:4  
///

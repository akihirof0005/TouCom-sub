ENTRY       G00473                      Glycan
NODE        10
            1   GlcNAc     16    -2
            2   Man         7    -2
            3   Man         0     2
            4   Man         0    -6
            5   GlcNAc     -8     6
            6   GlcNAc     -8    -2
            7   GlcNAc     -8    -6
            8   Gal       -16     6
            9   Gal       -16    -2
            10  Gal       -16    -6
EDGE        9
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:b1    3:6  
            5     6:b1    3:2  
            6     7:b1    4:2  
            7     8:b1    5:4  
            8     9:b1    6:4  
            9    10:b1    7:4  
///

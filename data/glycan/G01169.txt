ENTRY       G01169                      Glycan
NODE        6
            1   Man        10     3
            2   Man         0     8
            3   Man         0    -2
            4   Man       -10     8
            5   Man       -10     3
            6   Man       -10    -7
EDGE        5
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:a1    2:2  
            4     5:a1    3:6  
            5     6:a1    3:3  
///

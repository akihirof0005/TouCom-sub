ENTRY       G04732                      Glycan
NODE        6
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   GalNAc    -30     5
            5   Neu5Gc    -30    -5
            6   Neu5Ac    -40    -5
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:a2    5:8  
///

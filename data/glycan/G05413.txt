ENTRY       G05413                      Glycan
NODE        9
            1   Kdo        24   0.5
            2   D/LHep     15   0.5
            3   Glc         6   5.5
            4   D/LHep      5  -4.5
            5   Gal        -1   5.5
            6   GlcNAc     -5  -4.5
            7   GlcNAc     -9   5.5
            8   Gal       -17   5.5
            9   GalNAc    -25   5.5
EDGE        8
            1     2:a1    1:5  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:b1    3:4  
            5     6:a1    4:2  
            6     7:b1    5:3  
            7     8:b1    7:4  
            8     9:b1    8:3  
///

ENTRY       G04738                      Glycan
NODE        9
            1   Man         0     0
            2   Man       -10     0
            3   Man       -20     0
            4   Gal       -30     5
            5   Man       -30    -5
            6   Gal       -40     0
            7   Man       -40   -10
            8   Gal       -50    -5
            9   Man       -50   -15
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    3:4  
            5     6:a1    5:6  
            6     7:b1    5:4  
            7     8:a1    7:6  
            8     9:b1    7:4  
///

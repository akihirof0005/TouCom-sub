ENTRY       G10738                      Glycan
NODE        4
            1   Asn        10     0
            2   GlcNAc      0     0
            3   LFuc      -10     5
            4   Gal       -10    -5
EDGE        3
            1     2:b1    1    
            2     3:a1    2:4  
            3     4:b1    2:3  
///

ENTRY       G03144                      Glycan
NODE        5
            1   GalA        0     0
            2   LRha      -10     0
            3   GalA      -20     0
            4   LRha      -30     0
            5   Gal       -40     0
EDGE        4
            1     2:a1    1:4  
            2     3:a1    2:2  
            3     4:a1    3:4  
            4     5:b1    4:4  
///

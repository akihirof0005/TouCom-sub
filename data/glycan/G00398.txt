ENTRY       G00398                      Glycan
NODE        10
            1   GlcN       21     0
            2   S          20     5
            3   S          16    -5
            4   LIdoA      11     0
            5   S           5    -5
            6   GlcN        1     0
            7   S           0     5
            8   S          -5    -5
            9   L4-en-thrHexA   -12     0
            10  S         -21    -5
EDGE        9
            1     2       1:6  
            2     3       1:2  
            3     4:a1    1:4  
            4     5       4:2  
            5     6:a1    4:4  
            6     7       6:6  
            7     8       6:2  
            8     9:a1    6:4  
            9    10       9:2  
///

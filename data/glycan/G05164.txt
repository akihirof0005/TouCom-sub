ENTRY       G05164                      Glycan
NODE        7
            1   Gal         0     0
            2   GlcNAc    -10     5
            3   GlcNAc    -10    -5
            4   Gal       -20     5
            5   Gal       -20    -5
            6   GlcNAc    -30     5
            7   Gal       -30    -5
EDGE        6
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:b1    3:3  
            5     6:b1    4:3  
            6     7:a1    5:3  
///

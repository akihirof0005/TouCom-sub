ENTRY       G00682                      Glycan
NODE        4
            1   GalNAc     10     0
            2   Neu5Ac      0     5
            3   GlcNAc      0    -5
            4   Gal       -10    -5
EDGE        3
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
///

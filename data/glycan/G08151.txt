ENTRY       G08151                      Glycan
NODE        5
            1   Glc        13     0
            2   Glc         4     0
            3   Glc        -4     5
            4   Glc        -4    -5
            5   Glc       -14     5
EDGE        4
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
///

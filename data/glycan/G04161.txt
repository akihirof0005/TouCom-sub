ENTRY       G04161                      Glycan
NODE        13
            1   Asn        25    -2
            2   GlcNAc     17    -2
            3   GlcNAc      7    -2
            4   Man        -2    -2
            5   Man       -11     3
            6   GlcNAc    -11    -2
            7   Man       -11    -7
            8   Man       -19     8
            9   Man       -19    -2
            10  Man       -19    -7
            11  Man       -26     8
            12  Man       -26    -2
            13  Man       -26    -7
EDGE        12
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:b1    4:4  
            6     7:a1    4:3  
            7     8:a1    5:6  
            8     9:a1    5:3  
            9    10:a1    7:2  
            10   11:a1    8:2  
            11   12:a1    9:2  
            12   13:a1   10:2  
///

ENTRY       G01697                      Glycan
NODE        5
            1   Xyl      13.1  -2.1
            2   S         6.9   2.5
            3   Qui       5.1  -2.1
            4   Glc      -2.9  -2.1
            5   Glc3Me  -12.9  -2.1
EDGE        4
            1     2       1:4  
            2     3:b1    1:2  
            3     4:b1    3:4  
            4     5:b1    4:3  
///

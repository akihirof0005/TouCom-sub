ENTRY       G09112                      Glycan
NODE        4
            1   L1dAlt   15.4     0
            2   LRha      6.4     0
            3   LFuc2Me  -3.6     0
            4   LFuc2Me3Me -15.6     0
EDGE        3
            1     2:a1    1:5  
            2     3:a1    2:3  
            3     4:a1    3:4  
///

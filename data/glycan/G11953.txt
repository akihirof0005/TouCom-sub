ENTRY       G11953                      Glycan
NODE        12
            1   GlcNAc     24     2
            2   LFuc       16     5
            3   GlcNAc     15    -1
            4   Man         7    -1
            5   Man         0     2
            6   Man         0    -4
            7   GlcNAc     -8     2
            8   GlcNAc     -8    -4
            9   Gal       -16     2
            10  Gal       -16    -4
            11  S         -21    -4
            12  Neu5Ac    -24     2
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   12:a2    9:3  
            11   11      10:3  
///

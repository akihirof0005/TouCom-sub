ENTRY       G10774                      Glycan
NODE        10
            1   Asn      24.9     5
            2   GlcNAc   14.9     5
            3   LFuc      4.9    10
            4   GlcNAc    4.9     0
            5   Man      -5.1     0
            6   Man     -15.1     5
            7   Man     -15.1    -5
            8   GlcNAc  -25.1     5
            9   GlcNAc  -25.1     0
            10  GlcNAc  -25.1   -10
EDGE        9
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    7:2  
///

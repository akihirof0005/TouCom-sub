ENTRY       G07658                      Glycan
NODE        4
            1   GlcA6Me     0     0
            2   Glc       -10     0
            3   LRha      -20     0
            4   LAraf     -30     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:2  
///

ENTRY       G11322                      Glycan
NODE        17
            1   GlcNAc     24     0
            2   GlcNAc     15     0
            3   Man         7     0
            4   Man        -1     6
            5   GlcNAc     -1     0
            6   Man        -1    -6
            7   GlcNAc     -9    10
            8   GlcNAc     -9     2
            9   GlcNAc     -9    -2
            10  GlcNAc     -9   -10
            11  Gal       -17    10
            12  Gal       -17     2
            13  Gal       -17    -2
            14  Gal       -17   -10
            15  Gal       -24     2
            16  Gal       -24    -2
            17  Gal       -24   -10
EDGE        16
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    3:4  
            5     6:a1    3:3  
            6     7:b1    4:6  
            7     8:b1    4:2  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:4  
            14   15:a1   12:4  
            15   16:a1   13:4  
            16   17:a1   14:4  
///

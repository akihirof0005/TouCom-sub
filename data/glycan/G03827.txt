ENTRY       G03827                      Glycan
NODE        16
            1   GlcNAc      0     0
            2   GalNAc    -10     0
            3   Man       -20     0
            4   Man       -30    10
            5   Man       -30   -10
            6   GlcNAc    -40    10
            7   GlcNAc    -40    -5
            8   GlcNAc    -40   -15
            9   Neu5Ac    -50    15
            10  Gal       -50     5
            11  Neu5Ac    -50     0
            12  Gal       -50   -10
            13  Gal       -50   -15
            14  Neu5Ac    -60     5
            15  Neu5Ac    -60   -10
            16  Neu5Ac    -60   -15
EDGE        15
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8     9:a2    6:6  
            9    10:b1    6:3  
            10   11:a2    7:6  
            11   12:b1    7:3  
            12   13:b1    8:4  
            13   14:a2   10:3  
            14   15:a2   12:3  
            15   16:a2   13:6  
///

ENTRY       G07249                      Glycan
NODE        5
            1   P          17     0
            2   P          10     0
            3   Gal         2     0
            4   Man        -8     0
            5   Man       -18     0
EDGE        4
            1     2       1    
            2     3:a1    2    
            3     4:a1    3:3  
            4     5:a1    4:2  
///

ENTRY       G08553                      Glycan
NODE        3
            1   Glc      10.2   0.5
            2   3dxylHex   0.2   0.5
            3   LFuc     -9.8   0.5
EDGE        2
            1     2:b1    1:4  
            2     3:a1    2:2  
///

ENTRY       G04819                      Glycan
NODE        9
            1   Kdo      24.2   2.5
            2   Lgro-manHep  13.2   2.5
            3   Glc       2.2   7.5
            4   Lgro-manHep  -0.8  -2.5
            5   Gal      -5.8   7.5
            6   Glc     -11.8   2.5
            7   GlcNAc  -12.8  -7.5
            8   GlcNAc  -14.8   7.5
            9   Gal     -23.8   7.5
EDGE        8
            1     2:a1    1:5  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:b1    3:4  
            5     6:a1    4:3  
            6     7:a1    4:2  
            7     8:b1    5:3  
            8     9:b1    8:4  
///

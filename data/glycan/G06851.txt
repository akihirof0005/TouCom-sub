ENTRY       G06851                      Glycan
NODE        8
            1   GlcN       20     0
            2   S          16     4
            3   S          16    -4
            4   GlcA       10     0
            5   GlcN        0     0
            6   S          -4    -4
            7   L4-en-thrHexA   -12     0
            8   S         -20    -4
EDGE        7
            1     2       1:6  
            2     3       1:2  
            3     4:a1    1:4  
            4     5:b1    4:4  
            5     6       5:2  
            6     7:a1    5:4  
            7     8       7:2  
///

ENTRY       G00320                      Glycan
NODE        3
            1   GalNAc      8     0
            2   Gal        -1     0
            3   LFuc       -9     0
EDGE        2
            1     2:b1    1:3  
            2     3:a1    2:2  
///

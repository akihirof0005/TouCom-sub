ENTRY       G10463                      Glycan
NODE        9
            1   GalNAc   17.1   0.5
            2   Gal       8.1  -4.5
            3   GlcNAc    7.1   5.5
            4   Kdn       0.5  -4.5
            5   LFuc     -1.9   0.5
            6   GalNAc   -2.9  10.5
            7   LFuc     -7.7  -4.5
            8   LFuc    -16.7   0.5
            9   LFuc    -16.7  -9.5
EDGE        8
            1     2:b1    1:3  
            2     3:b1    1:6  
            3     4:a2    2:3  
            4     5:a1    3:3  
            5     6:b1    3:4  
            6     7:a1    4:4  
            7     8:a1    7:3  
            8     9:a1    7:2  
///

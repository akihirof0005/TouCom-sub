ENTRY       G12360                      Glycan
NODE        4
            1   P        11.2   0.5
            2   GlcN      4.2   0.5
            3   GlcN     -5.8   0.5
            4   P       -11.8   0.5
EDGE        3
            1     2:1     1    
            2     3:b1    2:6  
            3     4       3:4  
///

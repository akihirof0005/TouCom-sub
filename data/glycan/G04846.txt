ENTRY       G04846                      Glycan
NODE        8
            1   GlcNAc     18     1
            2   S          12     5
            3   Gal         9     1
            4   LFuc        9    -4
            5   GlcNAc      0     1
            6   S          -6     5
            7   Gal        -9     1
            8   Neu5Ac    -18     1
EDGE        7
            1     2       1:6  
            2     3:b1    1:4  
            3     4:a1    1:3  
            4     5:b1    3:3  
            5     6       5:6  
            6     7:b1    5:4  
            7     8:a2    7:3  
///

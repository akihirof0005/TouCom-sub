ENTRY       G08132                      Glycan
NODE        4
            1   Glc        13     0
            2   Glc         5     0
            3   Man6Ac     -4     0
            4   GlcA      -14     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:b1    3:2  
///

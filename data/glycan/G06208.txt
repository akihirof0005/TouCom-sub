ENTRY       G06208                      Glycan
NODE        11
            1   2,5-Anhydro-Man    25     0
            2   S          15    -4
            3   LIdoA      10     0
            4   S           5    -4
            5   GlcN        0     0
            6   S          -5     4
            7   S          -5    -4
            8   GlcA      -10     0
            9   GlcN      -20     0
            10  S         -25     4
            11  S         -25    -4
EDGE        10
            1     2       1:1  
            2     3:a1    1:3  
            3     4       3:2  
            4     5:a1    3:4  
            5     6       5:6  
            6     7       5:2  
            7     8:b1    5:4  
            8     9:a1    8:4  
            9    10       9:6  
            10   11       9:2  
///

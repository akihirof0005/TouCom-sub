ENTRY       G00602                      Glycan
NODE        4
            1   GlcA       10     0
            2   Gal         0     5
            3   Glc         0    -5
            4   LRha      -10     5
EDGE        3
            1     2:b1    1:3  
            2     3:b1    1:2  
            3     4:a1    2:2  
///

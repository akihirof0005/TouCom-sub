ENTRY       G01904                      Glycan
NODE        7
            1   Gal        17     0
            2   GlcNAc      8     0
            3   Gal        -1     0
            4   GlcNAc     -9     5
            5   GlcNAc     -9    -5
            6   Gal       -17     5
            7   Gal       -17    -5
EDGE        6
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:6  
            4     5:b1    3:3  
            5     6:b1    4:4  
            6     7:b1    5:4  
///

ENTRY       G03898                      Glycan
NODE        13
            1   Asn        29     3
            2   GlcNAc     21     3
            3   LFuc       11     8
            4   GlcNAc     11    -2
            5   Man         2    -2
            6   Man        -6     3
            7   Man        -6    -7
            8   GlcNAc     -7    -2
            9   GlcNAc    -14     3
            10  GlcNAc    -14    -7
            11  Gal       -22     3
            12  Gal       -22    -7
            13  Neu5Ac    -30     3
EDGE        12
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    5:4  
            8     9:b1    6:2  
            9    10:b1    7:2  
            10   11:b1    9:4  
            11   12:b1   10:4  
            12   13:a2   11:6  
///

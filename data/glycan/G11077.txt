ENTRY       G11077                      Glycan
NODE        5
            1   Asn        18     0
            2   GlcNAc      9     0
            3   Gal         0     0
            4   Neu5Ac     -9     0
            5   Neu5Ac    -19     0
EDGE        4
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:a2    3:3  
            4     5:a2    4:8  
///

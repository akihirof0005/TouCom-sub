ENTRY       G05081                      Glycan
NODE        9
            1   Man         0     0
            2   Man       -10     5
            3   Man       -10    -5
            4   Man       -20    10
            5   Man       -20     0
            6   Man       -20    -5
            7   Man       -30    10
            8   Man       -30     0
            9   Man       -30    -5
EDGE        8
            1     2:a1    1:6  
            2     3:a1    1:2  
            3     4:a1    2:6  
            4     5:a1    2:2  
            5     6:a1    3:2  
            6     7:a1    4:2  
            7     8:a1    5:2  
            8     9:a1    6:2  
///

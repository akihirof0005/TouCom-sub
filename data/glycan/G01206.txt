ENTRY       G01206                      Glycan
NODE        4
            1   Fru        15     0
            2   Fruf        5     0
            3   Fruf       -5     0
            4   Fruf      -15     0
EDGE        3
            1     2:b2    1:1  
            2     3:b2    2:1  
            3     4:b2    3:1  
///

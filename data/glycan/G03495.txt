ENTRY       G03495                      Glycan
NODE        4
            1   GlcN        7     2
            2   S           3    -2
            3   LIdoA      -3     2
            4   S          -8    -2
EDGE        3
            1     2       1:2  
            2     3:a1    1:4  
            3     4       3:2  
///

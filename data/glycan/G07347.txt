ENTRY       G07347                      Glycan
NODE        3
            1   Glc      10.6  -0.1
            2   Gal4Ac    0.6  -0.1
            3   Neu5Ac1Me -11.4  -0.1
EDGE        2
            1     2:b1    1:4  
            2     3:a2    2:3  
///

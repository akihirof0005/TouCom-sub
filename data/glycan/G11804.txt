ENTRY       G11804                      Glycan
NODE        4
            1   Gal         8     0
            2   Glc         1     4
            3   GlcNAc      0    -4
            4   GlcNAc     -9     4
EDGE        3
            1     2:a1    1:4  
            2     3:b1    1:3  
            3     4:b1    2:6  
///

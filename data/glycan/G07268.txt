ENTRY       G07268                      Glycan
NODE        5
            1   Fruf        8     1
            2   Glc         8    -6
            3   Fruf        0     6
            4   Fruf        0    -4
            5   Fruf       -9     6
EDGE        4
            1     2:a1    1:b2 
            2     3:b2    1:6  
            3     4:b2    1:1  
            4     5:b2    3:1  
///

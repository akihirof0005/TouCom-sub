ENTRY       G06900                      Glycan
NODE        5
            1   Fuc         0     0
            2   LRha      -10     0
            3   LRha      -20     0
            4   LRha      -30     5
            5   Glc2Me    -30    -5
EDGE        4
            1     2:a1    1:2  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:b1    3:3  
///

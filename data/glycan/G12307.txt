ENTRY       G12307                      Glycan
NODE        5
            1   Gal        24     0
            2   3,6-Anhydro-LGal    12     0
            3   Gal         0     0
            4   3,6-Anhydro-LGal   -12     0
            5   Gal       -24     0
EDGE        4
            1     2:a1    1:3  
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5:b1    4:4  
///

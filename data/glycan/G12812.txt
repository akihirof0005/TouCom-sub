ENTRY       G12812                      Glycan
NODE        7
            1   *          25    -2
            2   GalA       16    -2
            3   Glc         8    -2
            4   Qui4N(Ac-D-Asp)    -3    -2
            5   AcEtnP    -14     2
            6   GlcNAc    -15    -2
            7   *         -25    -2
EDGE        6
            1     2:a1    1    
            2     3:a1    2:4  
            3     4:b1    3:6  
            4     6:a1    4:3  
            5     6:6     5    
            6     7       6:3  
BRACKET     1 -19.0   4.0 -19.0  -6.0
            1  20.0  -6.0  20.0   4.0
            1 n
///

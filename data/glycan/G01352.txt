ENTRY       G01352                      Glycan
NODE        5
            1   Gal        10    -2
            2   Glc         0     3
            3   LRha        0    -7
            4   Xyl       -10     8
            5   Xyl       -10    -2
EDGE        4
            1     2:b1    1:4  
            2     3:a1    1:2  
            3     4:b1    2:3  
            4     5:b1    2:2  
///

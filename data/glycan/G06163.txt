ENTRY       G06163                      Glycan
NODE        6
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   GlcNAc    -20     0
            4   GlcNFormyl   -30     0
            5   GlcNAc    -40     0
            6   GlcNAc    -50     0
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:b1    5:4  
///

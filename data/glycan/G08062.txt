ENTRY       G08062                      Glycan
NODE        3
            1   GalNAc      5     0
            2   Neu        -5     5
            3   Gal        -5    -5
EDGE        2
            1     2:a2    1:6  
            2     3:b1    1:3  
///

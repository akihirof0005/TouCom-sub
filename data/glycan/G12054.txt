ENTRY       G12054                      Glycan
NODE        10
            1   GlcNAc     25    -1
            2   GlcNAc     16    -1
            3   Man         8    -1
            4   Man         0     2
            5   Man         0    -4
            6   Man        -8     5
            7   Man        -8    -1
            8   GlcNAc     -9    -4
            9   Gal       -17    -4
            10  Neu5Ac    -25    -4
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    4:3  
            7     8:b1    5:2  
            8     9:b1    8:4  
            9    10:a2    9:6  
///

ENTRY       G02259                      Glycan
NODE        7
            1   GlcNAc     20    -2
            2   Gal        10     3
            3   LFuc       10    -7
            4   GlcNAc      0     3
            5   Gal       -10     8
            6   LFuc      -10    -2
            7   LFuc      -20     8
EDGE        6
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    2:3  
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     7:a1    5:2  
///

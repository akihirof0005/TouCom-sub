ENTRY       G11386                      Glycan
NODE        7
            1   *          24     0
            2   Glc        17     0
            3   LRha2Ac     8     0
            4   GlcA       -1     0
            5   LRha       -9     0
            6   Glc       -17     0
            7   *         -24     0
EDGE        6
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5:a1    4:4  
            5     6:a1    5:2  
            6     7       6:4  
BRACKET     1 -21.0   2.0 -21.0  -2.0
            1  21.0  -2.0  21.0   2.0
            1 n
///

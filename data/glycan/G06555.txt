ENTRY       G06555                      Glycan
NODE        4
            1   Cer        14     0
            2   Glc1SH      5     0
            3   Gal6SH     -5     0
            4   Neu5Ac    -15     0
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:6  
///

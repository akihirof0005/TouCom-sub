ENTRY       G09262                      Glycan
NODE        3
            1   Gal        14     0
            2   L3,6-Anhydro-Gal     0     0
            3   Gal       -15     0
EDGE        2
            1     2:a1    1:3  
            2     3:b1    2:4  
///

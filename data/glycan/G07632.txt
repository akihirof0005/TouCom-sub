ENTRY       G07632                      Glycan
NODE        3
            1   LFucNAm  21.1  -0.1
            2   gro-L3,9dgalNon-2-ulop5NAc7NAc-onic   0.1  -0.1
            3   QuiNAc  -20.9  -0.1
EDGE        2
            1     2:a2    1:3  
            2     3:a1    2:8  
///

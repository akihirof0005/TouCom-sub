ENTRY       G07032                      Glycan
NODE        5
            1   Ser/Thr    13     0
            2   Gal         5     0
            3   Galf       -3     5
            4   Galf       -3    -5
            5   GlcNAc    -13    -5
EDGE        4
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:a1    2:2  
            4     5:a1    4:2  
///

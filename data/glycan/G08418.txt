ENTRY       G08418                      Glycan
NODE        10
            1   GalNAc     26     2
            2   S          22    -2
            3   GlcA       16     2
            4   S          13    -2
            5   GalNAc      6     2
            6   S           2    -2
            7   GlcA       -4     2
            8   GalNAc    -14     2
            9   S         -18    -2
            10  L4-en-thrHexA   -27     2
EDGE        9
            1     2       1:6  
            2     3:b1    1:3  
            3     4       3:2  
            4     5:b1    3:4  
            5     6       5:4  
            6     7:b1    5:3  
            7     8:b1    7:4  
            8     9       8:4  
            9    10:a1    8:3  
///

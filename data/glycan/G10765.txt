ENTRY       G10765                      Glycan
NODE        10
            1   Asn        25     3
            2   GlcNAc     15     3
            3   GlcNAc      5     3
            4   Man        -5     3
            5   Man       -15     8
            6   GlcNAc    -15     3
            7   Man       -15    -2
            8   Man       -25     8
            9   GlcNAc    -25     3
            10  GlcNAc    -25    -7
EDGE        9
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:b1    4:4  
            6     7:a1    4:3  
            7     8:a1    5:3  
            8     9:b1    7:4  
            9    10:b1    7:2  
///

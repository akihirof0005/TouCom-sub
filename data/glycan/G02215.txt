ENTRY       G02215                      Glycan
NODE        10
            1   GlcNAc     20     3
            2   LFuc       11     8
            3   GlcNAc     11    -2
            4   Man         3    -2
            5   Man        -5     3
            6   Man        -5    -7
            7   Man       -13     8
            8   Man       -13    -2
            9   GlcNAc    -13    -7
            10  Gal       -21    -7
EDGE        9
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:6  
            7     8:a1    5:3  
            8     9:b1    6:2  
            9    10:b1    9:4  
///

ENTRY       G07795                      Glycan
NODE        7
            1   GlcN       12     0
            2   S           8    -3
            3   LIdoA       2     0
            4   S          -2    -3
            5   GlcN       -8     0
            6   S         -12     3
            7   S         -12    -3
EDGE        6
            1     2       1:2  
            2     3:a1    1:4  
            3     4       3:2  
            4     5:a1    3:4  
            5     6       5:6  
            6     7       5:2  
///

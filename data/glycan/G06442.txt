ENTRY       G06442                      Glycan
NODE        6
            1   GalNAc     12    -2
            2   GlcNAc      2     3
            3   GlcNAc      2    -7
            4   Gal        -8     3
            5   Gal        -8    -7
            6   S         -13     7
EDGE        5
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:b1    3:3  
            5     6       4:6  
///

ENTRY       G06810                      Glycan
NODE        6
            1   Glc      12.4     2
            2   D/LAll   12.4    -6
            3   Glc       4.4     7
            4   Glc       4.4    -3
            5   Glc      -3.6     7
            6   Glc     -11.6     7
EDGE        5
            1     2:1     1:a1 
            2     3:b1    1:6  
            3     4:b1    1:3  
            4     5:b1    3:6  
            5     6:b1    5:3  
///

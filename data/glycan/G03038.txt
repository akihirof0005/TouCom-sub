ENTRY       G03038                      Glycan
NODE        5
            1   Glc         0     0
            2   Gal       -10     0
            3   Gal       -20     0
            4   Gal       -30     0
            5   Gal       -40     0
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:b1    4:3  
///

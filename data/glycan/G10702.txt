ENTRY       G10702                      Glycan
NODE        12
            1   Asn        35     0
            2   GlcNAc     25     0
            3   GlcNAc     15     0
            4   Man         5     0
            5   Man        -5     5
            6   Man        -5    -5
            7   GlcNAc    -15     5
            8   GlcNAc    -15    -5
            9   Gal       -25     5
            10  Gal       -25    -5
            11  Neu5Ac    -35     5
            12  Neu5Ac    -35    -5
EDGE        11
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:a2    9:3  
            11   12:a2   10:3  
///

ENTRY       G04331                      Glycan
NODE        11
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   Gal       -20     5
            5   GlcNAc    -20     0
            6   GlcNAc    -20   -10
            7   LFuc      -30     5
            8   Gal       -30     0
            9   Gal       -30   -10
            10  GlcNAc    -40     0
            11  LFuc      -40   -10
EDGE        10
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:b1    3:6  
            5     6:b1    3:3  
            6     7:a1    4:2  
            7     8:b1    5:4  
            8     9:b1    6:4  
            9    10:a1    8:4  
            10   11:a1    9:2  
///

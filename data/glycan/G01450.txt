ENTRY       G01450                      Glycan
NODE        7
            1   Xyl        26     2
            2   Gal        16     2
            3   Gal         6     2
            4   GlcA       -4     2
            5   GalNAc    -14     2
            6   S         -22    -2
            7   L4-en-thrHexA   -26     2
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6       5:6  
            6     7:a1    5:3  
///

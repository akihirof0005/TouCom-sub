ENTRY       G10436                      Glycan
NODE        6
            1   Cer        22     2
            2   Glc        12     2
            3   Gal         2     2
            4   Gal        -8     2
            5   GalNAc    -18     2
            6   S         -23    -2
EDGE        5
            1     2:1     1:1  
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:b1    4:3  
            5     6       5:3  
///

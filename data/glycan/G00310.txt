ENTRY       G00310                      Glycan
NODE        11
            1   GlcNAc     23     3
            2   LFuc       13     8
            3   GlcNAc     13    -2
            4   Man         4    -2
            5   Man        -5     3
            6   GlcNAc     -5    -2
            7   Man        -5    -7
            8   GlcNAc    -14     3
            9   GlcNAc    -14    -7
            10  Gal       -23     3
            11  Gal       -23    -7
EDGE        10
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:b1    4:4  
            6     7:a1    4:3  
            7     8:b1    5:2  
            8     9:b1    7:2  
            9    10:b1    8:4  
            10   11:b1    9:4  
///

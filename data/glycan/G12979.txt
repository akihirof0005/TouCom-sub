ENTRY       G12979                      Glycan
NODE        6
            1   *        25.4   0.4
            2   GlcNAc   17.4   0.4
            3   GalANAc   7.8   0.4
            4   GalANAc6N  -3.6   0.4
            5   Qui4NAcGly -15.8   0.4
            6   *         -25   0.4
EDGE        5
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:a1    3:4  
            4     5:b1    4:4  
            5     6       5:3  
BRACKET     1 -21.3   2.2 -21.3  -2.1
            1  22.2  -1.7  22.2   2.6
            1 n
///

ENTRY       G06191                      Glycan
NODE        7
            1   Glc         0     0
            2   Glc       -10     0
            3   Glc       -20     5
            4   Glc       -20    -5
            5   Glc       -30    10
            6   Glc       -30     0
            7   Glc       -40    10
EDGE        6
            1     2:b1    1:6  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:6  
            5     6:b1    3:3  
            6     7:b1    5:6  
///

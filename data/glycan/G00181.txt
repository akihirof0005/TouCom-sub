ENTRY       G00181                      Glycan
NODE        8
            1   Cer        22     0
            2   Glc        15     0
            3   Gal         8     0
            4   Gal         1     0
            5   GalNAc     -7     0
            6   Gal       -15    -5
            7   Neu5Ac    -16     5
            8   Neu5Ac    -23    -5
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:3  
            6     7:a2    5:6  
            7     8:a2    6:3  
///

ENTRY       G07559                      Glycan
NODE        3
            1   Fuc         0     0
            2   Fuc       -10     0
            3   Fuc3Me    -20     0
EDGE        2
            1     2:a1    1:2  
            2     3:a1    2:2  
///

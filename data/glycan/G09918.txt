ENTRY       G09918                      Glycan
NODE        3
            1   Glc       8.2   0.5
            2   Glc      -0.8   0.5
            3   Glc      -8.8   0.5
EDGE        2
            1     2:a1    1:6  
            2     3:1     2:6  
///

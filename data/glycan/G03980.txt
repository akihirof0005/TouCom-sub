ENTRY       G03980                      Glycan
NODE        14
            1   GlcNAc     22     3
            2   GlcNAc     12     3
            3   Man         3     3
            4   Man        -5    11
            5   Man        -5    -5
            6   GlcNAc    -14    11
            7   GlcNAc    -14     0
            8   GlcNAc    -14   -10
            9   Gal       -22    15
            10  LFuc      -22     7
            11  Gal       -22     4
            12  LFuc      -22    -4
            13  Gal       -22    -6
            14  LFuc      -22   -14
EDGE        13
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:a1    6:3  
            10   11:b1    7:4  
            11   12:a1    7:3  
            12   13:b1    8:4  
            13   14:a1    8:3  
///

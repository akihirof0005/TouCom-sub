ENTRY       G06977                      Glycan
NODE        5
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     0
            4   Man       -30     0
            5   Man       -40     0
EDGE        4
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    3:3  
            4     5:a1    4:2  
///

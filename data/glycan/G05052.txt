ENTRY       G05052                      Glycan
NODE        9
            1   Asn        25     2
            2   GlcNAc     17     2
            3   LFuc        7     7
            4   GlcNAc      7    -3
            5   Man        -2    -3
            6   Man       -10    -3
            7   GlcNAc    -18    -3
            8   Gal       -26     1
            9   LFuc      -26    -7
EDGE        8
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:3  
            6     7:b1    6:4  
            7     8:b1    7:4  
            8     9:a1    7:3  
///

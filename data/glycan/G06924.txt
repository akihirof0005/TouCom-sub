ENTRY       G06924                      Glycan
NODE        5
            1   Cer        13     0
            2   Glc         4     0
            3   Gal        -5     0
            4   GalNAc    -14     5
            5   LFuc      -14    -5
EDGE        4
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    3:2  
///

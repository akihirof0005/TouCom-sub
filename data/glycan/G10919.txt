ENTRY       G10919                      Glycan
NODE        10
            1   Asn        22     0
            2   GlcNAc     14     0
            3   LFuc        4     5
            4   LFuc        4    -5
            5   GlcNAc      3     0
            6   Man        -6     0
            7   Man       -14     5
            8   Man       -14    -5
            9   GlcNAc    -22     5
            10  GlcNAc    -22    -5
EDGE        9
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:b1    2:4  
            5     6:b1    5:4  
            6     7:a1    6:6  
            7     8:a1    6:3  
            8     9:b1    7:2  
            9    10:b1    8:2  
///

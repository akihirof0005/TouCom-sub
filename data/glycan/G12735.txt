ENTRY       G12735                      Glycan
NODE        8
            1   *        28.8  -1.1
            2   BacNAc4NAc  20.2  -1.1
            3   LGalANAc3NAc   7.3  -1.1
            4   ManANAc3Am  -6.3  -1.1
            5   Ala     -18.3   1.9
            6   GlcNAc3NAN -19.3  -1.1
            7   Formyl  -22.7   1.9
            8   *       -28.3  -1.1
EDGE        7
            1     2:b1    1    
            2     3:a1    2:3  
            3     4:b1    3:4  
            4     6:b1    4:4  
            5     6       5    
            6     7       5    
            7     8       6:4  
BRACKET     1 -25.4   3.9 -25.4  -4.1
            1  26.6  -4.2  26.6   3.8
            1 n
///

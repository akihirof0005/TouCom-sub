ENTRY       G01078                      Glycan
NODE        4
            1   GlcNAc      7     0
            2   Gal        -3     5
            3   LFuc       -3    -5
            4   S          -7     2
EDGE        3
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4       2:3  
///

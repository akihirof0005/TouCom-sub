ENTRY       G08156                      Glycan
NODE        6
            1   GlcN       12     2
            2   S           7    -2
            3   LIdoA       2     2
            4   S          -3    -2
            5   GlcN       -8     2
            6   S         -13    -2
EDGE        5
            1     2       1:2  
            2     3:a1    1:4  
            3     4       3:2  
            4     5:a1    3:4  
            5     6       5:6  
///

ENTRY       G00740                      Glycan
NODE        5
            1   Glc         0     0
            2   Gal       -10     5
            3   LFuc      -10    -5
            4   Gal       -20    10
            5   LFuc      -20     0
EDGE        4
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:a1    2:3  
            4     5:a1    2:2  
///

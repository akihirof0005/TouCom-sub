ENTRY       G11578                      Glycan
NODE        11
            1   GalNAc     30     0
            2   Gal        19     0
            3   GlcNAc      9     5
            4   Gal         9    -5
            5   Gal        -1     5
            6   Gal        -1    -5
            7   Gal       -11     5
            8   Gal       -11    -5
            9   Gal       -21     5
            10  LFuc      -21    -5
            11  LFuc      -31     5
EDGE        10
            1     2:b1    1:3  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:4  
            5     6:a1    4:4  
            6     7:a1    5:4  
            7     8:a1    6:3  
            8     9:a1    7:3  
            9    10:a1    8:2  
            10   11:a1    9:2  
///

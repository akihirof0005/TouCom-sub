ENTRY       G06090                      Glycan
NODE        5
            1   Glc         0     0
            2   Gal       -10     0
            3   Gal       -20     5
            4   GlcNAc    -20    -5
            5   Neu       -30     5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    2:3  
            4     5:a2    3:3  
///

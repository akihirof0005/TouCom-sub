ENTRY       G09443                      Glycan
NODE        4
            1   GlcN       10     0
            2   GlcA        0     5
            3   S           0    -5
            4   S         -10     5
EDGE        3
            1     2:b1    1:4  
            2     3       1:2  
            3     4       2:2  
///

ENTRY       G08519                      Glycan
NODE        4
            1   D/LAraf  10.2   0.5
            2   D/LAraf   0.2   0.5
            3   D/LAraf  -9.8   5.5
            4   D/LAraf  -9.8  -4.5
EDGE        3
            1     2:a1    1:5  
            2     3:a1    2:5  
            3     4:a1    2:3  
///

ENTRY       G08176                      Glycan
NODE        3
            1   Man      11.6  -0.4
            2   Abe       2.6  -0.4
            3   Gal3Me4Chloro6Chloro -12.4  -0.4
EDGE        2
            1     2:a1    1:3  
            2     3:a1    2:2  
///

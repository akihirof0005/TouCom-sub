ENTRY       G08218                      Glycan
NODE        4
            1   GlcNAc-onic    14     0
            2   GlcNAc      3     0
            3   GlcNAc     -6     0
            4   GlcNAc    -15     0
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
///

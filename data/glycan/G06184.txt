ENTRY       G06184                      Glycan
NODE        8
            1   Glc         0     0
            2   Glc       -10     0
            3   Glc       -20     0
            4   Glc       -30     5
            5   Glc       -30    -5
            6   Glc       -40     5
            7   Glc       -40    -5
            8   Glc       -50     5
EDGE        7
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:4  
            5     6:a1    4:4  
            6     7:a1    5:4  
            7     8:a1    6:4  
///

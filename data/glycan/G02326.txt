ENTRY       G02326                      Glycan
NODE        4
            1   GalNAc     10     0
            2   Gal         0     0
            3   GlcNAc    -10     5
            4   GlcNAc    -10    -5
EDGE        3
            1     2:b1    1:3  
            2     3:b1    2:6  
            3     4:b1    2:3  
///

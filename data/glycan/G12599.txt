ENTRY       G12599                      Glycan
NODE        4
            1   LRha       15     0
            2   GalA        4     0
            3   LRha       -5     0
            4   GalA      -15     0
EDGE        3
            1     2:a1    1:2  
            2     3:a1    2:4  
            3     4:a1    3:2  
BRACKET     1  -9.0   4.0  -9.0  -3.0
            1   8.0  -3.0   8.0   4.0
            1 n
///

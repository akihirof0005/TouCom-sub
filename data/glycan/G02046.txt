ENTRY       G02046                      Glycan
NODE        3
            1   Glc         5     0
            2   Man        -5     5
            3   Gal        -5    -5
EDGE        2
            1     2:a1    1:3  
            2     3:b1    1:2  
///

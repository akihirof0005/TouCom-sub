ENTRY       G08097                      Glycan
NODE        5
            1   Glc        13     0
            2   Glc         5     5
            3   Glc         5    -5
            4   Glc        -4    -5
            5   Glc       -13    -5
EDGE        4
            1     2:a1    1:6  
            2     3:a1    1:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
///

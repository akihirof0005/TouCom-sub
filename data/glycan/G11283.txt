ENTRY       G11283                      Glycan
NODE        16
            1   GlcN       34     5
            2   GlcN       27     5
            3   Kdo        20     5
            4   Kdo        15     0
            5   LAra4N     14    10
            6   Lgro-manHep    11     5
            7   Glc         3    10
            8   Lgro-manHep     0     0
            9   GalA       -8    -5
            10  Lgro-manHep   -11     5
            11  GalN      -14     0
            12  gro-manHep   -16   -10
            13  GalNAc    -22     0
            14  Lgro-manHep   -28   -10
            15  Gal       -29     0
            16  Glc       -35     0
EDGE        15
            1     2:b1    1:6  
            2     3:a2    2:6  
            3     4:a2    3:4  
            4     5:b1    3:8  
            5     6:a1    3:5  
            6     7:b1    6:4  
            7     8:a1    6:3  
            8     9:a1    8:3  
            9    10:a1    8:7  
            10   11:a1    9:4  
            11   12:a1    9:2  
            12   13:1    11:4,6
            13   14:a1   12:2  
            14   15:b1   13:4  
            15   16:a1   15:6  
///

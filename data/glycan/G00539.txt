ENTRY       G00539                      Glycan
NODE        8
            1   Cer        21     0
            2   Glc        13     0
            3   Gal         5     0
            4   GalNAc     -4     5
            5   Neu5Ac     -4    -5
            6   Gal       -13     5
            7   Neu5Ac    -14    -5
            8   LFuc      -21     5
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
            6     7:a2    5:8  
            7     8:a1    6:2  
///

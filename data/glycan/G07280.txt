ENTRY       G07280                      Glycan
NODE        4
            1   GlcNAc   10.2   0.5
            2   Gal       2.2  -4.5
            3   LFuc      1.2   5.5
            4   L2,6dlyxHex  -9.8  -4.5
EDGE        3
            1     2:b1    1:3  
            2     3:a1    1:4  
            3     4:a1    2:2  
///

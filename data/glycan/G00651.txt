ENTRY       G00651                      Glycan
NODE        4
            1   GlcN        0     0
            2   GlcN      -10     0
            3   Kdo       -20     0
            4   Kdo       -30     0
EDGE        3
            1     2:b1    1:6  
            2     3:a2    2:6  
            3     4:a2    3:4  
///

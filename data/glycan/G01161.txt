ENTRY       G01161                      Glycan
NODE        8
            1   Glc        25    -2
            2   Gal        15    -2
            3   GlcNAc      5    -2
            4   Gal        -5     3
            5   LFuc       -5    -7
            6   GlcNAc    -15     3
            7   LFuc      -25     8
            8   Gal       -25    -2
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6:b1    4:3  
            6     7:a1    6:4  
            7     8:b1    6:3  
///

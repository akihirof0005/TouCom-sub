ENTRY       G04546                      Glycan
NODE        10
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20     5
            4   GlcNAc    -20    -5
            5   Gal       -30     5
            6   Gal       -30    -5
            7   GlcNAc    -40     0
            8   GlcNAc    -40   -10
            9   Gal       -50     0
            10  Gal       -50   -10
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:4  
            5     6:b1    4:4  
            6     7:b1    6:6  
            7     8:b1    6:3  
            8     9:b1    7:4  
            9    10:b1    8:4  
///

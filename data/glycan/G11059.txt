ENTRY       G11059                      Glycan
NODE        6
            1   Ser/Thr    15     0
            2   Glc         8     0
            3   Glc         1     0
            4   Glc        -7     5
            5   Glc        -7    -5
            6   Glc       -15     5
EDGE        5
            1     2:b1    1    
            2     3:a1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:4  
            5     6:a1    4:4  
///

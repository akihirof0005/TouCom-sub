ENTRY       G03182                      Glycan
NODE        5
            1   GalA        0     0
            2   LRha      -10     0
            3   Gal       -20     5
            4   GalA      -20    -5
            5   LRha      -30    -5
EDGE        4
            1     2:a1    1:4  
            2     3:b1    2:4  
            3     4:a1    2:2  
            4     5:a1    4:4  
///

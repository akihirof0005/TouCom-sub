ENTRY       G01130                      Glycan
NODE        3
            1   Glc         5     0
            2   LRha       -5     5
            3   Glc        -5    -5
EDGE        2
            1     2:a1    1:3  
            2     3:b1    1:2  
///

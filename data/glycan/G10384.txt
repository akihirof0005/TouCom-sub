ENTRY       G10384                      Glycan
NODE        7
            1   LCym     30.3   0.5
            2   LCym     20.3   0.5
            3   LCym     10.3   0.5
            4   LCym      0.3   0.5
            5   Dig      -8.7   0.5
            6   Dig     -16.7   0.5
            7   2,6daraHex3Me -29.7   0.5
EDGE        6
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:b1    5:4  
            6     7:b1    6:4  
///

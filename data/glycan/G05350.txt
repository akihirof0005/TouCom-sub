ENTRY       G05350                      Glycan
NODE        5
            1   Glc      17.2   0.5
            2   Gal       9.2   0.5
            3   GlcNAc    0.2   0.5
            4   Gal      -8.8   0.5
            5   Neu5Ac  -16.8   0.5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a2    4    
///

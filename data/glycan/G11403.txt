ENTRY       G11403                      Glycan
NODE        7
            1   Kdo        23     3
            2   Lgro-manHep    13     3
            3   Glc         3     8
            4   Lgro-manHep     0    -2
            5   Glc       -10     3
            6   Lgro-manHep   -13    -7
            7   Gal       -23    -7
EDGE        6
            1     2:a1    1:5  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:a1    4:3  
            5     6:a1    4:2  
            6     7:b1    6:2  
///

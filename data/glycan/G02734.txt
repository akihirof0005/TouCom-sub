ENTRY       G02734                      Glycan
NODE        12
            1   GlcNAc     18     0
            2   Man        10     0
            3   Man         3     6
            4   Man         3    -6
            5   Man        -4    10
            6   Man        -4     2
            7   Man        -4    -2
            8   Man        -4   -10
            9   Man       -11    10
            10  Man       -11   -10
            11  Man       -18    10
            12  Man       -18   -10
EDGE        11
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
            5     6:a1    3:3  
            6     7:a1    4:6  
            7     8:a1    4:2  
            8     9:a1    5:2  
            9    10:a1    8:2  
            10   11:a1    9:3  
            11   12:a1   10:3  
///

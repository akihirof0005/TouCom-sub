ENTRY       G06857                      Glycan
NODE        5
            1   LRha       18     0
            2   GlcNAc      9     0
            3   Gal         0     0
            4   LRha       -9     0
            5   LRha      -18     0
EDGE        4
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:a1    3:2  
            4     5:a1    4:3  
///

ENTRY       G01504                      Glycan
NODE        4
            1   Fruf        5     4
            2   Glc         5    -4
            3   Gal        -5     4
            4   Gal        -5    -4
EDGE        3
            1     3:a1    1:1  
            2     1:b2    2:a1 
            3     4:a1    2:6  
///

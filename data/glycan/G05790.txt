ENTRY       G05790                      Glycan
NODE        6
            1   Cer        17     0
            2   Glc         9     0
            3   Gal         1     0
            4   LFuc       -7    -5
            5   GalNAc     -8     5
            6   Gal3,4Py   -18     5
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:2  
            4     5:a1    3:3  
            5     6:b1    5:3  
///

ENTRY       G05599                      Glycan
NODE        8
            1   GlcNAc     15     0
            2   Gal         5     0
            3   GlcNAc     -5     8
            4   GlcNAc     -5    -8
            5   Gal       -15    13
            6   LFuc      -15     3
            7   Gal       -15    -3
            8   LFuc      -15   -13
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:4  
            5     6:a1    3:3  
            6     7:b1    4:4  
            7     8:a1    4:3  
///

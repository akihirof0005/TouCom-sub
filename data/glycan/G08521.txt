ENTRY       G08521                      Glycan
NODE        3
            1   LFuc        0     0
            2   GlcA      -10     0
            3   GalNAc    -20     0
EDGE        2
            1     2:b1    1:3  
            2     3:a1    2:3  
///

ENTRY       G11024                      Glycan
NODE        15
            1   Asn        27     5
            2   GlcNAc     18     5
            3   LFuc        8    10
            4   GlcNAc      8     0
            5   Man        -1     0
            6   Man        -9     7
            7   Man        -9    -7
            8   GlcNAc    -18    12
            9   GlcNAc    -18     2
            10  GlcNAc    -18    -2
            11  GlcNAc    -18   -12
            12  Gal       -27    12
            13  Gal       -27     2
            14  Gal       -27    -2
            15  Gal       -27   -12
EDGE        14
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:6  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    7:2  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:4  
            14   15:b1   11:4  
///

ENTRY       G03395                      Glycan
NODE        3
            1   Man       4.6  -0.4
            2   Glc      -3.4  -5.4
            3   Abe      -4.4   4.6
EDGE        2
            1     2:a1    1:2  
            2     3:a1    1:3  
///

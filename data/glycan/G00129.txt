ENTRY       G00129                      Glycan
NODE        9
            1   Cer        17    -2
            2   Glc        11    -2
            3   Gal         5    -2
            4   GalNAc     -3     2
            5   Neu5Ac     -3    -6
            6   Gal       -11    -2
            7   Neu5Ac    -12     6
            8   Neu5Ac    -12    -6
            9   Neu5Ac    -18    -2
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
            6     7:a2    4:6  
            7     8:a2    5:8  
            8     9:a2    6:3  
///

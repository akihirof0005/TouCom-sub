ENTRY       G10755                      Glycan
NODE        9
            1   Asn        21     0
            2   GlcNAc     13     0
            3   GlcNAc      3     0
            4   Man        -6     0
            5   Man       -14     5
            6   Man       -14    -5
            7   GlcNAc    -16     0
            8   Man       -22     5
            9   GlcNAc    -22    -5
EDGE        8
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:4  
            7     8:a1    5:3  
            8     9:b1    6:2  
///

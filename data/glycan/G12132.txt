ENTRY       G12132                      Glycan
NODE        8
            1   Cer        27     0
            2   Glc        20     0
            3   Gal        13     0
            4   GlcNAc      5     0
            5   Gal        -3     0
            6   GlcNAc    -11     0
            7   Gal       -19     0
            8   Gal       -27     0
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:b1    7:3  
///

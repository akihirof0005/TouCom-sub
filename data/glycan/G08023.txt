ENTRY       G08023                      Glycan
NODE        3
            1   Gal6Me      5     0
            2   GalNAc     -5     5
            3   LFuc       -5    -5
EDGE        2
            1     2:a1    1:3  
            2     3:a1    1:2  
///

ENTRY       G00780                      Glycan
NODE        6
            1   GlcN       20     0
            2   GlcN       12     0
            3   GlcN        4     0
            4   GlcN       -4     0
            5   GlcN      -12     0
            6   GlcN      -20     0
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:b1    5:4  
///

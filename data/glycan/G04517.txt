ENTRY       G04517                      Glycan
NODE        11
            1   GlcNAc     24     1
            2   GlcNAc     15     1
            3   Man         7     1
            4   Man        -1     6
            5   Man        -1    -4
            6   GlcNAc     -9     6
            7   GlcNAc     -9    -4
            8   GalNAc    -18     6
            9   GalNAc    -18    -4
            10  S         -25     9
            11  S         -25    -8
EDGE        10
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:2  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10       8:3  
            10   11       9:3  
///

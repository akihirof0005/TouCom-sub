ENTRY       G12330                      Glycan
NODE        5
            1   Ser        13     2
            2   Xyl         5     2
            3   P           0    -1
            4   Gal        -4     2
            5   Gal       -13     2
EDGE        4
            1     2:a1    1:3  
            2     3       2:2  
            3     4:b1    2:4  
            4     5:b1    4:3  
///

ENTRY       G09584                      Glycan
NODE        3
            1   L6dTal   11.4     0
            2   LRha      1.4     0
            3   Glc3Me4,6Py -10.6     0
EDGE        2
            1     2:a1    1:2  
            2     3:b1    2:3  
///

ENTRY       G02218                      Glycan
NODE        10
            1   GlcNAc   23.2   4.5
            2   LFuc     14.2   9.5
            3   GlcNAc   14.2  -0.5
            4   Man       6.2  -0.5
            5   Man      -0.8   4.5
            6   Man      -0.8  -5.5
            7   GlcNAc   -8.8   4.5
            8   GlcNAc   -8.8  -5.5
            9   GalNAc  -17.8  -5.5
            10  S       -22.8  -8.5
EDGE        9
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    8:4  
            9    10       9    
///

ENTRY       G06463                      Glycan
NODE        6
            1   GalNAc     17     2
            2   Gal         7     2
            3   S           3    -2
            4   GlcNAc     -3     2
            5   Gal       -13     2
            6   S         -17    -2
EDGE        5
            1     2:b1    1:3  
            2     3       2:6  
            3     4:b1    2:3  
            4     5:b1    4:4  
            5     6       5:6  
///

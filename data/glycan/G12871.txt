ENTRY       G12871                      Glycan
NODE        16
            1   GlcN       31    -1
            2   P          28    -4
            3   GlcN       23    -1
            4   P          19    -4
            5   Kdo        16    -1
            6   Kdo        10    -4
            7   Lgro-manHep     7     2
            8   P           1     5
            9   Lgro-manHep    -5     2
            10  P         -13     2
            11  Glc       -14    -2
            12  Lgro-manHep   -17     6
            13  Gal       -20    -2
            14  Glc       -26    -5
            15  GlcN      -27     1
            16  Glc       -32    -5
EDGE        15
            1     2       1    
            2     3:b1    1:6  
            3     4       3:4  
            4     5:a2    3:6  
            5     6:a2    5:4  
            6     7:a1    5:5  
            7     8       7:4  
            8     9:a1    7:3  
            9    10       9:4  
            10   11:a1    9:3  
            11   12:a1    9:7  
            12   13:a1   11:3  
            13   14:a1   13:2  
            14   15:a1   13:3  
            15   16:a1   14:2  
///

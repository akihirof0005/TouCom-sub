ENTRY       G04311                      Glycan
NODE        10
            1   GalNAc     26   0.5
            2   Gal        17  -4.5
            3   GlcNAc     16   5.5
            4   Gal         8   5.5
            5   GlcNAc      8  -4.5
            6   Gal         1   5.5
            7   Gal        -1  -4.5
            8   GlcNAc    -10  -4.5
            9   Gal       -18  -4.5
            10  Neu5Gc    -27  -4.5
EDGE        9
            1     2:b1    1:3  
            2     3:b1    1:6  
            3     5:b1    2:3  
            4     4:1     3:4  
            5     6:1     4:3  
            6     7:b1    5:4  
            7     8:b1    7:3  
            8     9:1     8:4  
            9    10:a2    9:3  
///

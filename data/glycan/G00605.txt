ENTRY       G00605                      Glycan
NODE        10
            1   GlcNAc     31     0
            2   GlcNAc     21     0
            3   Man        12     0
            4   Man         5     4
            5   Man         5    -4
            6   Man        -3    -4
            7   Man       -11    -4
            8   Glc       -18    -4
            9   Glc       -25    -4
            10  Glc       -32    -4
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    5:2  
            6     7:a1    6:2  
            7     8:a1    7:3  
            8     9:a1    8:3  
            9    10:a1    9:2  
///

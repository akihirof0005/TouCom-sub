ENTRY       G03675                      Glycan
NODE        13
            1   Cer        38     0
            2   Glc        31     0
            3   Gal        23     0
            4   GlcNAc     14     0
            5   Gal         5     0
            6   GlcNAc     -3     5
            7   GlcNAc     -3    -5
            8   Gal       -12     5
            9   Gal       -12    -5
            10  Neu5Ac    -21     5
            11  GlcNAc    -21    -5
            12  Gal       -30    -5
            13  Neu5Ac    -39    -5
EDGE        12
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:6  
            6     7:b1    5:3  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10:a2    8:3  
            10   11:b1    9:3  
            11   12:b1   11:4  
            12   13:a2   12:3  
///

ENTRY       G11757                      Glycan
NODE        8
            1   *          24     0
            2   LRha       16     0
            3   LRha        8     0
            4   Glc         0     0
            5   Gal        -7    -4
            6   LRha3,4pyr   -10     4
            7   LRha      -15    -4
            8   *         -25    -4
EDGE        7
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:a1    4:4  
            6     7:a1    5:3  
            7     8       7:3  
BRACKET     1 -22.0   9.0 -22.0 -11.0
            1  21.0 -11.0  21.0   9.0
            1 n
///

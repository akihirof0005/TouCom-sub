ENTRY       G07751                      Glycan
NODE        5
            1   GalNAc   13.2   0.5
            2   Gal       4.2  -4.5
            3   GlcNAc    3.2   5.5
            4   Gal      -4.8   5.5
            5   GlcNAc  -12.8   5.5
EDGE        4
            1     2:b1    1:3  
            2     3:b1    1:6  
            3     4:1     3    
            4     5:1     4    
///

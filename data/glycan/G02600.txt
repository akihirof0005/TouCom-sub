ENTRY       G02600                      Glycan
NODE        3
            1   Gal         0     0
            2   Glc       -10     5
            3   LRha      -10    -5
EDGE        2
            1     2:a1    1:4  
            2     3:a1    1:3  
///

ENTRY       G00226                      Glycan
NODE        4
            1   Ser/Thr    10     0
            2   GalNAc      0     0
            3   Gal        -9    -5
            4   Neu5Gc    -10     5
EDGE        3
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:a2    2:6  
///

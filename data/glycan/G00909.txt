ENTRY       G00909                      Glycan
NODE        3
            1   Qui       5.1  -0.1
            2   Qui      -2.9   4.9
            3   Qui4NAc  -4.9  -5.1
EDGE        2
            1     2:b1    1:4  
            2     3:b1    1:2  
///

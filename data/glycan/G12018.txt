ENTRY       G12018                      Glycan
NODE        11
            1   Asn        28     2
            2   GlcNAc     19     2
            3   LFuc       10     6
            4   GlcNAc     10    -2
            5   Man         1    -2
            6   Man        -6     2
            7   Man        -6    -6
            8   Man       -13     2
            9   GlcNAc    -14    -6
            10  GalNAc    -23    -6
            11  S         -29    -6
EDGE        10
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:a1    6:6  
            8     9:b1    7:2  
            9    10:b1    9:4  
            10   11      10:4  
///

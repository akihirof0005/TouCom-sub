ENTRY       G01245                      Glycan
NODE        5
            1   Glc        15     0
            2   Gal         5     0
            3   GalN       -5     5
            4   Neu5Ac     -5    -5
            5   Gal       -15     5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a2    2:3  
            4     5:b1    3:3  
///

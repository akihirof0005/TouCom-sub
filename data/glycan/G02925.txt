ENTRY       G02925                      Glycan
NODE        7
            1   Glc        16    -2
            2   Glc         8    -2
            3   Glc         0     3
            4   Glc         0    -7
            5   Glc        -8     3
            6   Glc       -16     8
            7   GlcN      -16    -2
EDGE        6
            1     2:b1    1:6  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:6  
            5     6:b1    5:6  
            6     7:b1    5:3  
///

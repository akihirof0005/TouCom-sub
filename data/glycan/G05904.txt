ENTRY       G05904                      Glycan
NODE        8
            1   GlcNAc      0     0
            2   GlcNAc    -10     5
            3   LFuc      -10    -5
            4   Man       -20     5
            5   Man       -30    10
            6   Man       -30     5
            7   Xyl       -30     0
            8   GlcNAc    -40    10
EDGE        7
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    2:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    4:2  
            7     8:b1    5:2  
///

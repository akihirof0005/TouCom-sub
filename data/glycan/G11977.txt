ENTRY       G11977                      Glycan
NODE        12
            1   Asn        28     2
            2   GlcNAc     20     2
            3   LFuc       11     6
            4   GlcNAc     11    -2
            5   Man         3    -2
            6   Man        -5     2
            7   Man        -5    -6
            8   GlcNAc     -6    -2
            9   GlcNAc    -13     2
            10  GlcNAc    -13    -6
            11  Gal       -21    -6
            12  Neu5Ac    -29    -6
EDGE        11
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    5:4  
            8     9:b1    6:2  
            9    10:b1    7:2  
            10   11:b1   10:4  
            11   12:a2   11:6  
///

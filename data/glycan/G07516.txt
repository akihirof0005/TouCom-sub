ENTRY       G07516                      Glycan
NODE        3
            1   Cer        10     0
            2   Glc         0     0
            3   Neu5Ac    -10     0
EDGE        2
            1     2:b1    1:1  
            2     3:a2    2:3  
///

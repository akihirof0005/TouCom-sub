ENTRY       G08070                      Glycan
NODE        4
            1   Glc         3     3
            2   Gal         3    -3
            3   Glc        -4     3
            4   Glc        -4    -3
EDGE        3
            1     3:a1    1:4  
            2     1:a1    2:b1 
            3     4:b1    2:4  
///

ENTRY       G12451                      Glycan
NODE        3
            1   Glc       6.2   0.5
            2   Xyl       0.2   0.5
            3   Xyl      -5.8   0.5
EDGE        2
            1     2:1     1    
            2     3:1     2    
///

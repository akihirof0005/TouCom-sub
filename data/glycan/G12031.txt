ENTRY       G12031                      Glycan
NODE        10
            1   GlcNAc   21.2   2.5
            2   LFuc     12.2   6.5
            3   GlcNAc   12.2  -1.5
            4   Man       3.2  -1.5
            5   Man      -3.8   2.5
            6   Man      -3.8  -5.5
            7   Man     -10.8   2.5
            8   GlcNAc  -11.8  -5.5
            9   Man     -17.8   2.5
            10  GalNAc  -21.8  -5.5
EDGE        9
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5    
            7     8:b1    6:2  
            8     9:a1    7    
            9    10:b1    8:4  
///

ENTRY       G01844                      Glycan
NODE        8
            1   GlcNAc     31     0
            2   Gal        22     0
            3   GlcNAc     13     0
            4   Gal         4     0
            5   GlcNAc     -5     0
            6   Gal       -14     0
            7   GlcNAc    -23     0
            8   Gal       -32     0
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7:b1    6:3  
            7     8:b1    7:4  
///

ENTRY       G06425                      Glycan
NODE        6
            1   Gal        12    -2
            2   P           8    -7
            3   GlcNAc      2    -2
            4   Gal        -8     3
            5   LFuc       -8    -7
            6   S         -12     8
EDGE        5
            1     2       1:1  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6       4:3  
///

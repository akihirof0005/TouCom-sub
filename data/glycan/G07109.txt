ENTRY       G07109                      Glycan
NODE        5
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   Gal       -20     5
            5   LFuc      -20    -5
EDGE        4
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:3  
            4     5:a1    3:2  
///

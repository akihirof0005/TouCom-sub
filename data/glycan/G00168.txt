ENTRY       G00168                      Glycan
NODE        8
            1   Cer        21    -2
            2   Glc        13    -2
            3   Gal         5    -2
            4   GalNAc     -4     3
            5   Neu5Ac     -4    -7
            6   Gal       -13     3
            7   GalNAc    -22     8
            8   Neu5Ac    -22    -2
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
            6     7:b1    6:4  
            7     8:a2    6:3  
///

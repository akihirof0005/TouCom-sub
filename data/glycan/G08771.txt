ENTRY       G08771                      Glycan
NODE        3
            1   LGal        0     0
            2   Gal       -10     0
            3   GalNAc    -20     0
EDGE        2
            1     2:b1    1:2  
            2     3:a1    2:3  
///

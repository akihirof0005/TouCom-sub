ENTRY       G06742                      Glycan
NODE        5
            1   Ser/Thr  14.2   0.5
            2   GalNAc    4.2   0.5
            3   Gal      -4.8   5.5
            4   GlcNAc   -5.8  -4.5
            5   Gal     -14.8  -4.5
EDGE        4
            1     2:b1    1    
            2     3:b1    2:3  
            3     4:b1    2    
            4     5:b1    4:4  
///

ENTRY       G11580                      Glycan
NODE        12
            1   GalNAc     30     3
            2   GlcNAc     20     8
            3   Gal        20    -2
            4   GlcNAc     10     3
            5   Gal        10    -7
            6   Gal         0     3
            7   Gal         0    -7
            8   Gal       -10     3
            9   Gal       -10    -7
            10  Gal       -20     3
            11  LFuc      -20    -7
            12  LFuc      -30     3
EDGE        11
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    3:6  
            4     5:b1    3:3  
            5     6:b1    4:4  
            6     7:a1    5:4  
            7     8:a1    6:4  
            8     9:a1    7:3  
            9    10:a1    8:3  
            10   11:a1    9:2  
            11   12:a1   10:2  
///

ENTRY       G08491                      Glycan
NODE        3
            1   LRha        5     0
            2   GlcA       -5     5
            3   Gal        -5    -5
EDGE        2
            1     2:b1    1:4  
            2     3:a1    1:3  
///

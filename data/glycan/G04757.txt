ENTRY       G04757                      Glycan
NODE        8
            1   Glc        17    -2
            2   Man         9    -2
            3   GlcNAc      0     2
            4   GlcNAc      0    -6
            5   Gal        -8     6
            6   LFuc       -8    -2
            7   Gal        -9    -6
            8   Neu5Ac    -18    -6
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:4  
            5     6:a1    3:3  
            6     7:b1    4:4  
            7     8:a2    7:6  
///

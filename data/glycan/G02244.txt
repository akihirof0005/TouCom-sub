ENTRY       G02244                      Glycan
NODE        9
            1   Glc      29.6   2.6
            2   Glc      29.6  -3.4
            3   LRha     21.6   2.6
            4   LRha     12.6   2.6
            5   LAco      3.6   2.6
            6   LAco     -5.4   2.6
            7   LRha    -14.4   2.6
            8   Man     -22.4   2.6
            9   Man     -30.4   2.6
EDGE        8
            1     2:a1    1:a1 
            2     3:a1    1:3  
            3     4:a1    3:3  
            4     5:a1    4:2  
            5     6:a1    5:2  
            6     7:a1    6:2  
            7     8:a1    7:2  
            8     9:a1    8:3  
///

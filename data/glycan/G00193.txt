ENTRY       G00193                      Glycan
NODE        16
            1   Cer        43    -2
            2   Glc        37    -2
            3   Gal        31    -2
            4   GlcNAc     24    -2
            5   Gal        17    -2
            6   GlcNAc     10    -2
            7   Gal         3    -2
            8   GlcNAc     -4    -2
            9   Gal       -10     2
            10  LFuc      -11    -6
            11  GlcNAc    -17     2
            12  Gal       -23     6
            13  LFuc      -24    -2
            14  GlcNAc    -30     6
            15  Gal       -37     6
            16  Neu5Ac    -44     6
EDGE        15
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:b1    7:3  
            8     9:b1    8:4  
            9    10:a1    8:3  
            10   11:b1    9:3  
            11   12:b1   11:4  
            12   13:a1   11:3  
            13   14:b1   12:3  
            14   15:b1   14:4  
            15   16:a2   15:3  
///

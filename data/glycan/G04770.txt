ENTRY       G04770                      Glycan
NODE        5
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20     0
            4   LFuc      -30     5
            5   Gal       -30    -5
EDGE        4
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4:a1    3:4  
            4     5:b1    3:3  
///

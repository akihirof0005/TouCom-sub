ENTRY       G02274                      Glycan
NODE        6
            1   GalNAc     15    -2
            2   GlcNAc      5     3
            3   Gal         5    -7
            4   Gal        -5     8
            5   LFuc       -5    -2
            6   Neu5Ac    -15     8
EDGE        5
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a1    2:3  
            5     6:a2    4:3  
///

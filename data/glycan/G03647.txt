ENTRY       G03647                      Glycan
NODE        3
            1   Glc       7.2   0.5
            2   Glc      -0.8   0.5
            3   Glc      -7.8   0.5
EDGE        2
            1     2:b1    1:6  
            2     3:b1    2    
///

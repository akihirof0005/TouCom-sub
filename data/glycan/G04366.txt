ENTRY       G04366                      Glycan
NODE        19
            1   Asn        29     5
            2   GlcNAc     21     5
            3   LFuc       12    10
            4   GlcNAc     12     0
            5   Man         3     0
            6   Man        -5     6
            7   Man        -5    -6
            8   GlcNAc    -13    10
            9   GlcNAc    -13     2
            10  GlcNAc    -13    -2
            11  GlcNAc    -13   -10
            12  Gal       -21    10
            13  Gal       -21     2
            14  Gal       -21    -2
            15  Gal       -21   -10
            16  Neu5Ac    -29    10
            17  Neu5Ac    -29     2
            18  Neu5Ac    -29    -2
            19  Neu5Ac    -29   -10
EDGE        18
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:6  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    7:2  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:4  
            14   15:b1   11:4  
            15   16:a2   12:3  
            16   17:a2   13:6  
            17   18:a2   14:3  
            18   19:a2   15:6  
///

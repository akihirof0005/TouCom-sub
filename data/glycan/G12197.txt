ENTRY       G12197                      Glycan
NODE        17
            1   Asn      35.2   0.5
            2   GlcNAc   26.2   0.5
            3   GlcNAc   16.2   0.5
            4   Man       7.2   0.5
            5   Man      -0.8   6.5
            6   Man      -0.8  -5.5
            7   GlcNAc   -2.8   0.5
            8   GlcNAc   -8.8  10.5
            9   GlcNAc   -8.8   1.5
            10  GlcNAc   -8.8  -1.5
            11  GlcNAc   -8.8 -10.5
            12  Man     -17.8  10.5
            13  Man     -17.8  -1.5
            14  GlcNAc  -25.8  10.5
            15  GlcNAc  -25.8   2.5
            16  GlcNAc  -25.8  -5.5
            17  Gal     -34.8  -5.5
EDGE        16
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:4  
            7     8:b1    5:4  
            8     9:b1    5:2  
            9    10:b1    6:4  
            10   11:b1    6:2  
            11   12:b1    8:4  
            12   13:b1   10:4  
            13   14:b1   12    
            14   15:b1   13    
            15   16:b1   13    
            16   17:b1   16:4  
///

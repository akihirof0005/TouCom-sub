ENTRY       G02190                      Glycan
NODE        10
            1   GlcNAc     24  -0.1
            2   GlcNAc     15  -0.1
            3   Man         7  -0.1
            4   Man         0   4.9
            5   Man         0  -5.1
            6   GlcNAc     -8   4.9
            7   GlcNAc     -8  -5.1
            8   Gal       -16   4.9
            9   Gal       -16  -5.1
            10  Neu5Ac4Ac   -25  -5.1
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:2  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10:a2    9:6  
///

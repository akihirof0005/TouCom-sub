ENTRY       G01555                      Glycan
NODE        5
            1   GlcNAc     20     0
            2   GlcNAc     10     0
            3   GlcNAc      0     0
            4   GlcNAc    -10     0
            5   GlcN6Ac   -20     0
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
///

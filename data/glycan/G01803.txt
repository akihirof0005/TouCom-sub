ENTRY       G01803                      Glycan
NODE        9
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   GalNAc    -30     5
            5   Neu5Ac    -30    -5
            6   Gal       -40     5
            7   Neu5Ac    -40    -5
            8   Gal       -50    10
            9   LFuc      -50     0
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
            6     7:a2    5:8  
            7     8:a1    6:3  
            8     9:a1    6:2  
///

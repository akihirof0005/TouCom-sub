ENTRY       G00724                      Glycan
NODE        4
            1   Cer         0     0
            2   Glc       -10     0
            3   Man       -20     0
            4   GlcNAc    -30     0
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
///

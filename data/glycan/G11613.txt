ENTRY       G11613                      Glycan
NODE        6
            1   Cer        13     3
            2   Gal         3     3
            3   Glc        -5    -2
            4   Gal        -6     8
            5   Gal       -14     3
            6   LFuc      -14    -7
EDGE        5
            1     2:b1    1    
            2     3:b1    2:3  
            3     4:b1    2:6  
            4     5:b1    3:4  
            5     6:a1    3:3  
///

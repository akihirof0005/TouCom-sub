ENTRY       G02135                      Glycan
NODE        3
            1   Glc        10     0
            2   Glc         0     0
            3   Man       -10     0
EDGE        2
            1     2:b1    1:4  
            2     3:b1    2:4  
///

ENTRY       G01281                      Glycan
NODE        9
            1   GlcNAc     29     0
            2   GlcNAc     19     0
            3   Man        10     0
            4   Man         3     5
            5   Man         3    -5
            6   Man        -5    -5
            7   Man       -13    -5
            8   Glc       -21    -5
            9   Glc       -29    -5
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    5:2  
            6     7:a1    6:2  
            7     8:a1    7:3  
            8     9:a1    8:3  
///

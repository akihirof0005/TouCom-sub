ENTRY       G08199                      Glycan
NODE        4
            1   Gal      11.6  -0.4
            2   LRha      3.6  -0.4
            3   Man      -4.4  -0.4
            4   Tyv     -12.4  -0.4
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:3  
///

ENTRY       G00434                      Glycan
NODE        6
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30     0
            5   Man       -40     5
            6   Man       -40    -5
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    4:6  
            5     6:a1    4:3  
///

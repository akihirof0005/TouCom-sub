ENTRY       G07048                      Glycan
NODE        5
            1   Man        12     4
            2   2,5-Anhydro-Man    12    -3
            3   Man         4     4
            4   Man        -4     4
            5   Man       -12     4
EDGE        4
            1     2:1     1:a1 
            2     3:a1    1:6  
            3     4:a1    3:2  
            4     5:a1    4:2  
///

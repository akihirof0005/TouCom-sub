ENTRY       G11831                      Glycan
NODE        7
            1   *          26     2
            2   Gal        16     2
            3   LRha        7     2
            4   LRha       -2     2
            5   GlcA2Ac   -12     2
            6   Gal       -21     6
            7   *         -27    -5
EDGE        6
            1     2:b1    1    
            2     3:a1    2:3  
            3     4:a1    3:3  
            4     5:b1    4:3  
            5     6:a1    5:4  
            6     7       5:3  
BRACKET     1 -23.0   9.0 -23.0  -6.0
            1  21.0  -6.0  21.0   9.0
            1 1
///

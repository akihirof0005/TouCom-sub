ENTRY       G01726                      Glycan
NODE        4
            1   Man        10     0
            2   Gal         0     5
            3   Man         0    -5
            4   Man       -10    -5
EDGE        3
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
///

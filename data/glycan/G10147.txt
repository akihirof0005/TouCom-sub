ENTRY       G10147                      Glycan
NODE        4
            1   Ser/Thr  12.2   0.5
            2   GalNAc    3.2   0.5
            3   Gal      -4.8   0.5
            4   Gal     -11.8   0.5
EDGE        3
            1     2:1     1    
            2     3:1     2    
            3     4:1     3    
///

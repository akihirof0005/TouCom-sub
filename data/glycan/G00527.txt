ENTRY       G00527                      Glycan
NODE        4
            1   GalNAc     12     3
            2   S           5    -2
            3   L4-en-thrHexA    -4     3
            4   S         -13    -2
EDGE        3
            1     2       1:4  
            2     3:a1    1:3  
            3     4       3:2  
///

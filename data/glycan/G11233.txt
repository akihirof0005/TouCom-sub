ENTRY       G11233                      Glycan
NODE        7
            1   *          24     0
            2   LRha       14     0
            3   LRha        5     5
            4   GlcNAc      4    -5
            5   LRha       -5     5
            6   LRha      -15     5
            7   *         -25     5
EDGE        6
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:b1    2:2  
            4     5:a1    3:3  
            5     6:a1    5:2  
            6     7       6:2  
BRACKET     1 -21.0  10.0 -21.0 -10.0
            1  21.0 -10.0  21.0  10.0
            1 n
///

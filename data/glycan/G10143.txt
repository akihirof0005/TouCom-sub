ENTRY       G10143                      Glycan
NODE        3
            1   Glc       8.2   0.5
            2   D/LGal    0.2   0.5
            3   Neu5Gc   -8.8   0.5
EDGE        2
            1     2:1     1    
            2     3:2     2    
///

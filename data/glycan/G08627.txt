ENTRY       G08627                      Glycan
NODE        3
            1   1,6-Anhydro-GlcNAc     0     0
            2   Man       -15     0
            3   Man       -25     0
EDGE        2
            1     2:b1    1:4  
            2     3:a1    2:3  
///

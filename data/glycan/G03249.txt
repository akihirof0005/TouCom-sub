ENTRY       G03249                      Glycan
NODE        6
            1   2,5-Anhydro-Man    17     2
            2   GlcA        5     2
            3   GlcNAc     -4     2
            4   S          -8    -2
            5   LIdoA     -13     2
            6   S         -17    -2
EDGE        5
            1     2:b1    1:4  
            2     3:a1    2:4  
            3     4       3:6  
            4     5:a1    3:4  
            5     6       5:2  
///

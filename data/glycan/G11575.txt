ENTRY       G11575                      Glycan
NODE        6
            1   *          21     0
            2   GlcNAc     14     0
            3   LRha        4     0
            4   ManNAc     -6     0
            5   Man       -16     0
            6   *         -22     0
EDGE        5
            1     2:a1    1    
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5:b1    4:3  
            5     6       5:2  
BRACKET     1 -19.0   2.0 -19.0  -2.0
            1  18.0  -2.0  18.0   2.0
            1 n
///

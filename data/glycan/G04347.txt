ENTRY       G04347                      Glycan
NODE        11
            1   Asn        26     2
            2   GlcNAc     18     2
            3   LFuc        8     7
            4   GlcNAc      8    -3
            5   Man        -1    -3
            6   Man        -9     1
            7   Man        -9    -7
            8   GlcNAc    -18     1
            9   GlcNAc    -18    -7
            10  Gal       -27     1
            11  Gal       -27    -7
EDGE        10
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:b1    9:4  
///

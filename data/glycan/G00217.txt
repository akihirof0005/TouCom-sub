ENTRY       G00217                      Glycan
NODE        8
            1   GalNAc     16     2
            2   Gal         8    -2
            3   GlcNAc      7     6
            4   GlcNAc      0    -2
            5   Gal        -1     6
            6   Gal        -8    -2
            7   GlcNAc    -16     2
            8   GlcNAc    -16    -6
EDGE        7
            1     2:b1    1:3  
            2     3:b1    1:6  
            3     4:b1    2:3  
            4     5:b1    3:4  
            5     6:b1    4:4  
            6     7:b1    6:6  
            7     8:b1    6:3  
///

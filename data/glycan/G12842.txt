ENTRY       G12842                      Glycan
NODE        11
            1   Kdo        22     0
            2   P          18    -3
            3   Lgro-manHep    12     0
            4   Glc         3     0
            5   Lgro-manHep     0     5
            6   Lgro-manHep     0    -5
            7   gro-manHep    -6     0
            8   Lgro-manHep   -12    -5
            9   Gal       -15    -3
            10  gro-manHep   -18     3
            11  GlcN      -22    -5
EDGE        10
            1     2       1:4  
            2     3:a1    1:5  
            3     4:b1    3:4  
            4     5:a1    3:6  
            5     6:a1    3:3  
            6     7:a1    4:6  
            7     8:a1    6:2  
            8     9:b1    7:4  
            9    10:a1    7:6  
            10   11:a1    8:7  
///

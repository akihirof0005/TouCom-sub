ENTRY       G06559                      Glycan
NODE        4
            1   Cer        12    -2
            2   Glc         3    -2
            3   Neu5Ac     -7    -2
            4   S         -13     2
EDGE        3
            1     2:b1    1:1  
            2     3:a2    2:6  
            3     4       3:8  
///

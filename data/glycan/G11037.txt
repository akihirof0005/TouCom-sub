ENTRY       G11037                      Glycan
NODE        21
            1   Asn        39     5
            2   GlcNAc     31     5
            3   LFuc       21    10
            4   GlcNAc     21     0
            5   Man        12     0
            6   Man         4     6
            7   Man         4    -6
            8   GlcNAc     -4    10
            9   GlcNAc     -4     2
            10  GlcNAc     -4    -2
            11  GlcNAc     -4   -10
            12  Gal       -13    10
            13  Gal       -13     2
            14  Gal       -13    -2
            15  Gal       -13   -10
            16  Neu5Ac    -22    10
            17  GlcNAc    -22     2
            18  Neu5Ac    -22    -2
            19  Neu5Ac    -22   -10
            20  Gal       -31     2
            21  Neu5Ac    -39     2
EDGE        20
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:6  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    7:2  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:4  
            14   15:b1   11:4  
            15   16:a2   12:3  
            16   17:b1   13:3  
            17   18:a2   14:3  
            18   19:a2   15:3  
            19   20:b1   17:4  
            20   21:a2   20:3  
///

ENTRY       G08391                      Glycan
NODE        3
            1   GlcNAc   11.2   0.5
            2   3dxylHex   0.2   0.5
            3   LFuc    -10.8   0.5
EDGE        2
            1     2:b1    1:3  
            2     3:a1    2:2  
///

ENTRY       G06512                      Glycan
NODE        5
            1   GlcNAc     16     0
            2   GlcNAc      6     0
            3   Kdo        -4     0
            4   Kdo       -14    -5
            5   Lgro-manHep   -17     5
EDGE        4
            1     2:b1    1:6  
            2     3:a2    2:6  
            3     4:a2    3:4  
            4     5:a1    3:5  
///

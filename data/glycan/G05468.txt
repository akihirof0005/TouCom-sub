ENTRY       G05468                      Glycan
NODE        7
            1   GlcNAc     28     0
            2   GlcNAc     18     0
            3   Kdo         8     0
            4   Kdo        -2    -5
            5   Lgro-manHep    -4     5
            6   Lgro-manHep   -17     5
            7   Glc       -28     5
EDGE        6
            1     2:b1    1:6  
            2     3:a2    2:6  
            3     4:a2    3:4  
            4     5:a1    3:5  
            5     6:a1    5:3  
            6     7:a1    6:3  
///

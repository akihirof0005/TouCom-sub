ENTRY       G09636                      Glycan
NODE        3
            1   Gal         0     0
            2   LFuc      -10     5
            3   Gal       -10    -5
EDGE        2
            1     2:a1    1:4  
            2     3:a1    1:3  
///

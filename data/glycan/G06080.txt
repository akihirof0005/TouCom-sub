ENTRY       G06080                      Glycan
NODE        6
            1   Cer        21     0
            2   Glc        13     0
            3   Gal         5     0
            4   GlcNAc     -4     0
            5   Gal       -13     0
            6   GlcA3OX   -22     0
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
///

ENTRY       G08076                      Glycan
NODE        4
            1   GalNAc      9     0
            2   GlcNAc     -1     5
            3   GlcNAc     -1    -5
            4   Gal       -10    -5
EDGE        3
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
///

ENTRY       G12847                      Glycan
NODE        9
            1   *          24     0
            2   P          18     0
            3   Gro        13     0
            4   Qui4NAc     5     0
            5   GlcNAc     -4     0
            6   Gal       -12     0
            7   Glc       -19     4
            8   GlcNAc    -20    -4
            9   *         -25     0
EDGE        8
            1     2       1    
            2     3:3     2    
            3     4:b1    3:1  
            4     5:a1    4:3  
            5     6:b1    5:3  
            6     7:a1    6:4  
            7     8:b1    6:2  
            8     9       6:3  
BRACKET     1 -20.0   2.0 -20.0  -2.0
            1  20.0  -2.0  20.0   2.0
            1 n
///

ENTRY       G05855                      Glycan
NODE        7
            1   Gal         0     0
            2   LFuc      -10     5
            3   GlcNAc    -10    -5
            4   LFuc      -20     0
            5   Gal       -20   -10
            6   GalNAc    -30    -5
            7   LFuc      -30   -15
EDGE        6
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:a1    3:6  
            4     5:b1    3:3  
            5     6:a1    5:3  
            6     7:a1    5:2  
///

ENTRY       G02845                      Glycan
NODE        9
            1   GlcNAc     20    -2
            2   GlcNAc     11    -2
            3   Man         3    -2
            4   Man        -4     3
            5   Man        -4    -7
            6   GlcNAc    -12     8
            7   GlcNAc    -12    -2
            8   Gal       -20     8
            9   Gal       -20    -2
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:6  
            6     7:b1    4:2  
            7     8:b1    6:4  
            8     9:b1    7:4  
///

ENTRY       G06310                      Glycan
NODE        6
            1   Cer        17     0
            2   Glc         9     0
            3   Man         1     0
            4   Man        -7     5
            5   Xyl        -7    -5
            6   GlcNAc    -17     5
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    3:2  
            5     6:b1    4:2  
///

ENTRY       G11446                      Glycan
NODE        11
            1   Asn        29     3
            2   GlcNAc     21     3
            3   LFuc       13     8
            4   GlcNAc     13    -2
            5   Man         4    -2
            6   Man        -3     3
            7   Man        -3    -7
            8   GlcNAc    -11     3
            9   GlcNAc    -11    -7
            10  GalNAc    -20    -7
            11  Neu5Ac    -29    -7
EDGE        10
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:2  
            9    10:b1    9:4  
            10   11:a2   10:6  
///

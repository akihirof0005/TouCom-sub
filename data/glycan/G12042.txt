ENTRY       G12042                      Glycan
NODE        10
            1   2,5-Anhydro-Man    31     1
            2   GlcA       19     1
            3   GlcN       11     1
            4   S           6    -1
            5   GlcA        3     1
            6   GlcNAc     -6     1
            7   GlcA      -15     1
            8   GlcN      -23     1
            9   S         -28    -1
            10  GlcA      -31     1
EDGE        9
            1     2:b1    1:3  
            2     3:a1    2:4  
            3     4       3:2  
            4     5:b1    3:4  
            5     6:a1    5:4  
            6     7:b1    6:4  
            7     8:a1    7:4  
            8     9       8:2  
            9    10:b1    8:4  
///

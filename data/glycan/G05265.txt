ENTRY       G05265                      Glycan
NODE        7
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30     5
            5   Man       -30    -5
            6   GlcNAc    -40    -5
            7   Gal       -50    -5
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    5:4  
            6     7:b1    6:3  
///

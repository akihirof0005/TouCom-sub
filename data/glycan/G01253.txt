ENTRY       G01253                      Glycan
NODE        4
            1   Fruf        5     6
            2   Glc6Ac      5    -1
            3   Glc6Ac     -5     4
            4   Glc        -5    -6
EDGE        3
            1     1:b2    2:a1 
            2     3:b1    2:3  
            3     4:b1    2:2  
///

ENTRY       G04735                      Glycan
NODE        10
            1   Asn        30     2
            2   GlcNAc     21     2
            3   GlcNAc     12     2
            4   Man         4     2
            5   Man        -4     7
            6   Man        -4    -3
            7   Man       -13     7
            8   GlcNAc    -13    -3
            9   GalNAc    -23    -3
            10  S         -30    -7
EDGE        9
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:6  
            7     8:b1    6:2  
            8     9:b1    8:4  
            9    10       9:4  
///

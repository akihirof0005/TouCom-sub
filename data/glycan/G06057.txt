ENTRY       G06057                      Glycan
NODE        6
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20     5
            4   GlcNAc    -20    -5
            5   Gal       -30    -5
            6   Gal       -40    -5
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    4:4  
            5     6:a1    5:3  
///

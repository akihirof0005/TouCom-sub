ENTRY       G02195                      Glycan
NODE        12
            1   GlcNAc     20     5
            2   LFuc       11    10
            3   GlcNAc     11     0
            4   Man         3     0
            5   Man        -4     6
            6   Man        -4    -6
            7   GlcNAc    -12    10
            8   GlcNAc    -12     2
            9   GlcNAc    -12    -2
            10  GlcNAc    -12   -10
            11  Gal       -20    10
            12  Gal       -20   -10
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    7:4  
            11   12:b1   10:4  
///

ENTRY       G03039                      Glycan
NODE        5
            1   Man4Me      0     0
            2   Man       -10     5
            3   Man       -10    -5
            4   GlcNAc    -20     5
            5   GlcNAc    -20    -5
EDGE        4
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:b1    2:2  
            4     5:b1    3:2  
///

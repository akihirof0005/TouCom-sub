ENTRY       G02233                      Glycan
NODE        7
            1   GlcN       20     0
            2   Man        12     0
            3   Man         4     0
            4   Man        -4     0
            5   Galf      -12     5
            6   Man       -12    -5
            7   Galf      -20    -5
EDGE        6
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    3:2  
            4     5:b1    4:3  
            5     6:a1    4:2  
            6     7:b1    6:3  
///

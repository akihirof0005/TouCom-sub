ENTRY       G01907                      Glycan
NODE        8
            1   Gal        20     2
            2   GlcNAc     10     2
            3   S           4    -2
            4   Gal         0     2
            5   S          -5    -2
            6   GlcNAc    -10     2
            7   S         -16    -2
            8   Gal       -20     2
EDGE        7
            1     2:b1    1:3  
            2     3       2:6  
            3     4:b1    2:4  
            4     5       4:6  
            5     6:b1    4:3  
            6     7       6:6  
            7     8:b1    6:4  
///

ENTRY       G04257                      Glycan
NODE        10
            1   QuiNAc   27.4  -0.1
            2   GlcNAc   17.4  -0.1
            3   Man       8.4  -0.1
            4   Man      -0.6   4.9
            5   Man      -0.6  -5.1
            6   GlcNAc   -9.6   4.9
            7   GlcNAc   -9.6  -5.1
            8   Gal     -18.6   4.9
            9   Gal     -18.6  -5.1
            10  Neu5Ac  -27.6  -5.1
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:2  
            6     7:a1    5:2  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10:a2    9:3  
///

ENTRY       G09289                      Glycan
NODE        3
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
EDGE        2
            1     2:b1    1:6  
            2     3:b1    1:4  
///

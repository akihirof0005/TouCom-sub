ENTRY       G07843                      Glycan
NODE        4
            1   Cer         0     0
            2   Gal       -10     0
            3   GalNAc    -20     5
            4   Gal       -20    -5
EDGE        3
            1     2:a1    1:1  
            2     3:a1    2:3  
            3     4:a1    2:2  
///

ENTRY       G04489                      Glycan
NODE        10
            1   GlcNAc     25     0
            2   GlcNAc     15     0
            3   Man         6     0
            4   Man        -2     4
            5   Man        -2    -4
            6   Man        -9     8
            7   Man        -9     0
            8   GlcNAc    -10    -4
            9   GalNAc    -19    -4
            10  S         -25    -8
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    4:3  
            7     8:b1    5:2  
            8     9:b1    8:4  
            9    10       9:4  
///

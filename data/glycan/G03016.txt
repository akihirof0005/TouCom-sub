ENTRY       G03016                      Glycan
NODE        5
            1   6dxylHex-4-ulop  14.3   0.4
            2   Qui       2.3   0.4
            3   Fuc      -5.7   5.4
            4   Qui      -5.7  -4.6
            5   Fuc     -13.7   5.4
EDGE        4
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    2:2  
            4     5:b1    3:2  
///

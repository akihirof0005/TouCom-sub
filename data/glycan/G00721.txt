ENTRY       G00721                      Glycan
NODE        5
            1   Fruf       10     4
            2   Glc        10    -4
            3   Fruf        0     4
            4   Fruf        0    -4
            5   Fruf      -10     4
EDGE        4
            1     3:b2    1:1  
            2     1:b2    2:a1 
            3     4:b2    2:6  
            4     5:b2    3:1  
///

ENTRY       G08148                      Glycan
NODE        4
            1   GalNAc      0     0
            2   GlcNAc    -10     0
            3   LRha      -20     0
            4   GlcA      -30     0
EDGE        3
            1     2:a1    1:6  
            2     3:a1    2:4  
            3     4:b1    3:3  
///

ENTRY       G12080                      Glycan
NODE        9
            1   Asn        22    -2
            2   GlcNAc     13    -2
            3   GlcNAc      3    -2
            4   Man        -6    -2
            5   Man       -14     3
            6   Man       -14    -7
            7   Man       -22     8
            8   Man       -22    -2
            9   Man       -22    -7
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:6  
            7     8:a1    5:3  
            8     9:a1    6:2  
///

ENTRY       G02316                      Glycan
NODE        5
            1   LAra       10     0
            2   Glc         0     5
            3   Glc         0    -5
            4   Xyl       -10     5
            5   Glc       -10    -5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    1:2  
            3     4:b1    2:2  
            4     5:b1    3:4  
///

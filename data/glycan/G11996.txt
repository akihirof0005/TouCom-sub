ENTRY       G11996                      Glycan
NODE        11
            1   GlcN       28  -0.1
            2   S          23   2.9
            3   S          23  -3.1
            4   GlcA       19  -0.1
            5   GlcNAc     11  -0.1
            6   LIdoA       2  -0.1
            7   S          -3  -3.1
            8   GlcN       -7  -0.1
            9   S         -12  -3.1
            10  L4-en-thrHexA   -19  -0.1
            11  S         -28  -0.1
EDGE        10
            1     2       1:6  
            2     3       1:2  
            3     4:b1    1:4  
            4     5:a1    4:4  
            5     6:a1    5:4  
            6     7       6:2  
            7     8:a1    6:4  
            8     9       8:2  
            9    10:a1    8:4  
            10   11      10:2  
///

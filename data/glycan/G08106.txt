ENTRY       G08106                      Glycan
NODE        4
            1   Glc        11     0
            2   P           5     0
            3   Rib-ol     -2     0
            4   GalNAc    -12     0
EDGE        3
            1     2       1:6  
            2     3:5     2    
            3     4:b1    3:1  
///

ENTRY       G01877                      Glycan
NODE        9
            1   GalNAc     23     0
            2   GlcNAc     13     5
            3   Gal        13    -5
            4   S           6     1
            5   Gal         3     5
            6   GlcNAc     -7     5
            7   S         -14     1
            8   Gal       -17     5
            9   S         -23     1
EDGE        8
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4       2:6  
            4     5:b1    2:4  
            5     6:b1    5:3  
            6     7       6:6  
            7     8:b1    6:4  
            8     9       8:6  
///

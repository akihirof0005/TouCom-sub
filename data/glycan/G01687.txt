ENTRY       G01687                      Glycan
NODE        6
            1   GlcNAc     25     0
            2   GlcNAc     15     0
            3   Man         5     0
            4   Man        -5     0
            5   GlcNAc    -15     0
            6   Gal       -25     0
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    4:2  
            5     6:b1    5:4  
///

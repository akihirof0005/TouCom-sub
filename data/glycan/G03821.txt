ENTRY       G03821                      Glycan
NODE        12
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     5
            4   Man       -20    -5
            5   GlcNAc    -30    10
            6   GlcNAc    -30     0
            7   GlcNAc    -30    -5
            8   Gal       -40    10
            9   Gal       -40     0
            10  Gal       -40    -5
            11  Neu5Ac    -50     0
            12  Neu5Ac    -50    -5
EDGE        11
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:b1    3:6  
            5     6:b1    3:2  
            6     7:b1    4:2  
            7     8:b1    5:4  
            8     9:b1    6:4  
            9    10:b1    7:4  
            10   11:a2    9:6  
            11   12:a2   10:6  
///

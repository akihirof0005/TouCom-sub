ENTRY       G10866                      Glycan
NODE        11
            1   Asn        21     0
            2   GlcNAc     13     0
            3   GlcNAc      3     0
            4   Man        -6     0
            5   Man       -14     6
            6   GlcNAc    -14     0
            7   Man       -14    -6
            8   GlcNAc    -22    10
            9   GlcNAc    -22     2
            10  GlcNAc    -22    -2
            11  GlcNAc    -22   -10
EDGE        10
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:b1    4:4  
            6     7:a1    4:3  
            7     8:b1    5:4  
            8     9:b1    5:2  
            9    10:b1    7:4  
            10   11:b1    7:2  
///

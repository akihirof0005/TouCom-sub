ENTRY       G08653                      Glycan
NODE        4
            1   Glc       4.2   1.5
            2   D/LAll    4.2  -5.5
            3   Glc      -3.8   6.5
            4   Glc      -3.8  -3.5
EDGE        3
            1     2:1     1:a1 
            2     3:b1    1:6  
            3     4:b1    1:3  
///

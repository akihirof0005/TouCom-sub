ENTRY       G04983                      Glycan
NODE        7
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   Neu5Ac    -30     0
            5   Gal       -40     0
            6   Gal       -50     0
            7   Galf      -60     0
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
            4     5:a1    4:4  
            5     6:a1    5:4  
            6     7:b1    6:3  
///

ENTRY       G12588                      Glycan
NODE        15
            1   GlcNAc     28     0
            2   GlcNAc     19     0
            3   Man        11     0
            4   Man         4     5
            5   Man         4    -5
            6   GlcNAc     -4     8
            7   GlcNAc     -4     2
            8   GlcNAc     -4    -2
            9   GlcNAc     -4    -8
            10  Gal       -12     8
            11  Gal       -12     2
            12  Gal       -12    -2
            13  Gal       -12    -8
            14  GlcNAc    -20     8
            15  Gal       -28     8
EDGE        14
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:6  
            6     7:b1    4:2  
            7     8:b1    5:4  
            8     9:b1    5:2  
            9    10:b1    6:4  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:3  
            14   15:b1   14:4  
///

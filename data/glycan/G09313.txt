ENTRY       G09313                      Glycan
NODE        3
            1   1,6-Anhydro-Glc     0     0
            2   LRha      -13     5
            3   Glc       -13    -5
EDGE        2
            1     2:a1    1:4  
            2     3:a1    1:3  
///

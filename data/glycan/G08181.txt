ENTRY       G08181                      Glycan
NODE        4
            1   LRha        0     0
            2   GalA      -10     0
            3   LRha      -20     0
            4   GalA      -30     0
EDGE        3
            1     2:a1    1:2  
            2     3:a1    2:4  
            3     4:a1    3:2  
///

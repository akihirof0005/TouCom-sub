ENTRY       G03386                      Glycan
NODE        10
            1   GalNAc     26     2
            2   S          21    -2
            3   GlcA       16     2
            4   GalNAc      6     2
            5   S           1    -2
            6   GlcA       -4     2
            7   S          -9    -2
            8   GalNAc    -14     2
            9   S         -19    -2
            10  L4-en-thrHexA   -27     2
EDGE        9
            1     2       1:4  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5       4:6  
            5     6:b1    4:3  
            6     7       6:2  
            7     8:b1    6:4  
            8     9       8:4  
            9    10:a1    8:3  
///

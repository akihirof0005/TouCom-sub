ENTRY       G05734                      Glycan
NODE        8
            1   Asn        16    -2
            2   GlcNAc      9    -2
            3   GlcNAc      0     2
            4   LFuc        0    -6
            5   Man        -9     2
            6   Man       -17     7
            7   Man       -17     2
            8   Xylf      -17    -3
EDGE        7
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:b1    3:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    5:2  
///

ENTRY       G02239                      Glycan
NODE        8
            1   GlcNAc     20     0
            2   GlcNAc     11     0
            3   Man         3     0
            4   Man        -4     5
            5   Man        -4    -5
            6   Man       -12     5
            7   Man       -12    -5
            8   Man       -20    -5
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    5:2  
            7     8:a1    7:2  
///

ENTRY       G05132                      Glycan
NODE        11
            1   Glc         0     0
            2   Glc       -10     0
            3   Glc       -20     0
            4   Glc       -30     5
            5   Glc       -30    -5
            6   Glc       -40     5
            7   Glc       -40    -5
            8   Glc       -50     5
            9   Glc       -50    -5
            10  Glc       -60     5
            11  Glc       -70     5
EDGE        10
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:4  
            5     6:a1    4:4  
            6     7:a1    5:4  
            7     8:a1    6:4  
            8     9:a1    7:4  
            9    10:a1    8:4  
            10   11:a1   10:4  
///

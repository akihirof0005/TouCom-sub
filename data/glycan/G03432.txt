ENTRY       G03432                      Glycan
NODE        3
            1   LRha        5     0
            2   LFuc       -5     5
            3   Man        -5    -5
EDGE        2
            1     2:b1    1:3  
            2     3:a1    1:2  
///

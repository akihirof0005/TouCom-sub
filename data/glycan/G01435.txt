ENTRY       G01435                      Glycan
NODE        9
            1   Man        15     0
            2   Man         5     7
            3   Man         5    -7
            4   GlcNAc     -5     7
            5   GlcNAc     -5    -7
            6   Gal       -15    12
            7   LFuc      -15     2
            8   Gal       -15    -2
            9   LFuc      -15   -12
EDGE        8
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:b1    2:2  
            4     5:b1    3:2  
            5     6:b1    4:4  
            6     7:a1    4:3  
            7     8:b1    5:4  
            8     9:a1    5:3  
///

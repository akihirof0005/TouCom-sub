ENTRY       G12587                      Glycan
NODE        5
            1   GalNAc     15     0
            2   GlcNAc      5     0
            3   Gal        -5     0
            4   Gal       -15     5
            5   LFuc      -15    -5
EDGE        4
            1     2:b1    1:6  
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:a1    3:2  
///

ENTRY       G12466                      Glycan
NODE        3
            1   Glc         5    -1
            2   P          -1     2
            3   Gal        -5    -1
EDGE        2
            1     2       1:6  
            2     3:b1    1:4  
///

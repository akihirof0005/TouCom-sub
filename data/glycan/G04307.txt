ENTRY       G04307                      Glycan
NODE        12
            1   Asn        26     3
            2   GlcNAc     18     3
            3   LFuc        8     8
            4   GlcNAc      8    -2
            5   Man        -1    -2
            6   Man        -9     3
            7   GlcNAc     -9    -2
            8   Man        -9    -7
            9   GlcNAc    -18     3
            10  GlcNAc    -18    -7
            11  Gal       -26     7
            12  LFuc      -26    -1
EDGE        11
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:b1    5:4  
            7     8:a1    5:3  
            8     9:b1    6:2  
            9    10:b1    8:2  
            10   11:b1    9:4  
            11   12:a1    9:3  
///

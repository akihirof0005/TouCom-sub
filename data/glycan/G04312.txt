ENTRY       G04312                      Glycan
NODE        11
            1   Asn        36     0
            2   GlcNAc     29     0
            3   LFuc       19     5
            4   GlcNAc     19    -5
            5   Man        11    -5
            6   Man         4    -5
            7   GlcNAc     -4    -5
            8   Gal       -12    -5
            9   GlcNAc    -20    -5
            10  Gal       -28    -5
            11  LFuc      -36    -5
EDGE        10
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:b1    6:2  
            7     8:b1    7:4  
            8     9:b1    8:3  
            9    10:b1    9:4  
            10   11:a1   10:2  
///

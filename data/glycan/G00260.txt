ENTRY       G00260                      Glycan
NODE        10
            1   GlcNAc     21     3
            2   LFuc       11     8
            3   GlcNAc     11    -2
            4   Man         2    -2
            5   Man        -6     3
            6   Man        -6    -7
            7   GlcNAc    -14     3
            8   GlcNAc    -14    -7
            9   Gal       -22     3
            10  Gal       -22    -7
EDGE        9
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
///

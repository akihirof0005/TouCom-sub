ENTRY       G04570                      Glycan
NODE        11
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     5
            4   GlcNAc    -20     0
            5   Man       -20    -5
            6   GlcNAc    -30     5
            7   GlcNAc    -30     0
            8   GlcNAc    -30    -5
            9   Gal       -40     5
            10  LFuc      -40     0
            11  Gal       -40    -5
EDGE        10
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:a1    2:3  
            5     6:b1    3:2  
            6     7:b1    4:4  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:a1    7:6  
            10   11:b1    8:4  
///

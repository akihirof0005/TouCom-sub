ENTRY       G11254                      Glycan
NODE        7
            1   *          26     0
            2   GlcNAc     17     0
            3   Glc         8     0
            4   Gal         0     0
            5   GlcNAc     -9     0
            6   GalA      -18     0
            7   *         -27     0
EDGE        6
            1     2:a1    1    
            2     3:a1    2:6  
            3     4:b1    3:6  
            4     5:b1    4:2  
            5     6:a1    5:3  
            6     7       6:4  
BRACKET     1 -23.0   1.0 -23.0  -2.0
            1  22.0  -2.0  22.0   1.0
            1 n
///

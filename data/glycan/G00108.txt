ENTRY       G00108                      Glycan
NODE        4
            1   Cer        13     0
            2   Glc         5     0
            3   Gal        -3     0
            4   Neu5Ac    -13     0
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
///

ENTRY       G06363                      Glycan
NODE        5
            1   Glc         0     0
            2   Glc       -10     0
            3   Gal       -20     5
            4   Gal       -20    -5
            5   Gal       -30    -5
EDGE        4
            1     2:a1    1:3  
            2     3:b1    2:4  
            3     4:a1    2:2  
            4     5:a1    4:2  
///

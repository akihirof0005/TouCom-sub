ENTRY       G07185                      Glycan
NODE        4
            1   1,5-Anhydro-Glc     0     0
            2   Gal       -15     5
            3   LFuc      -15    -5
            4   Neu5Ac    -25     5
EDGE        3
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:a2    2:3  
///

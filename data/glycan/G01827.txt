ENTRY       G01827                      Glycan
NODE        9
            1   GalNAc     20     0
            2   GlcNAc     10     5
            3   Gal        10    -5
            4   S           3     2
            5   Gal         0     5
            6   Neu5Ac      0    -5
            7   GlcNAc    -10     5
            8   S         -17     2
            9   Gal       -20     5
EDGE        8
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4       2:6  
            4     5:b1    2:4  
            5     6:a2    3:3  
            6     7:b1    5:3  
            7     8       7:6  
            8     9:b1    7:4  
///

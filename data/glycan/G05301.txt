ENTRY       G05301                      Glycan
NODE        6
            1   GalNAc      0     0
            2   Gal       -10     0
            3   GalNAc    -20     0
            4   Gal       -30     5
            5   Neu5Gc    -30    -5
            6   GalNAc    -40     5
EDGE        5
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:4  
///

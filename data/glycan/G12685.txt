ENTRY       G12685                      Glycan
NODE        8
            1   Glc-ol   19.6   2.4
            2   LRha     10.6   6.4
            3   LRha     10.6  -1.6
            4   LRha      2.6  -1.6
            5   LRha     -4.4  -5.6
            6   GlcNAc   -5.4   2.4
            7   Gal     -13.4   2.4
            8   LRha    -20.4   2.4
EDGE        7
            1     2:a1    1:3  
            2     3:a1    1:1  
            3     4:a1    3:2  
            4     5:a1    4:2  
            5     6:b1    4:4  
            6     7:a1    6:3  
            7     8:a1    7:3  
///

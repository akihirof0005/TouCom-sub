ENTRY       G06137                      Glycan
NODE        6
            1   Gal         0     0
            2   Man       -10     0
            3   Glc       -20     0
            4   Gal       -30     5
            5   Gal       -30    -5
            6   Gal       -40     5
EDGE        5
            1     2:b1    1:3  
            2     3:a1    2:3  
            3     4:b1    3:6  
            4     5:a1    3:2  
            5     6:b1    4:4  
///

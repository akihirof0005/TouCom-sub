ENTRY       G02258                      Glycan
NODE        6
            1   GlcNAc     20     0
            2   Gal        10     5
            3   LFuc       10    -5
            4   GlcNAc      0     5
            5   Gal       -10     5
            6   Neu5Ac    -20     5
EDGE        5
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    2:3  
            4     5:b1    4:4  
            5     6:a2    5:6  
///

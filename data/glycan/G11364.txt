ENTRY       G11364                      Glycan
NODE        8
            1   Glc        20     3
            2   Glc        10     3
            3   Xyl         0     8
            4   Glc         0    -2
            5   LAraf     -10     8
            6   Xyl       -10     3
            7   Glc       -10    -7
            8   Xyl       -20    -7
EDGE        7
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:a1    3:2  
            5     6:a1    4:6  
            6     7:b1    4:4  
            7     8:a1    7:6  
///

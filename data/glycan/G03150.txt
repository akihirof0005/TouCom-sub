ENTRY       G03150                      Glycan
NODE        4
            1   Fuc         0     0
            2   Glc       -10     5
            3   Glc       -10    -5
            4   Xyl       -20     5
EDGE        3
            1     2:b1    1:3  
            2     3:b1    1:2  
            3     4:b1    2:2  
///

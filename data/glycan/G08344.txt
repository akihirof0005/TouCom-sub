ENTRY       G08344                      Glycan
NODE        4
            1   P          10     0
            2   Gal         0     0
            3   Man       -10     5
            4   Glc       -10    -5
EDGE        3
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:a1    2:2  
///

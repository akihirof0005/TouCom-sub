ENTRY       G05531                      Glycan
NODE        8
            1   Xyl        26     2
            2   Gal        16     2
            3   S          11    -2
            4   Gal         6     2
            5   GlcA       -4     2
            6   GalNAc    -14     2
            7   S         -20    -2
            8   L4-en-thrHexA   -27     2
EDGE        7
            1     2:b1    1:4  
            2     3       2:6  
            3     4:b1    2:3  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7       6:6  
            7     8:a1    6:3  
///

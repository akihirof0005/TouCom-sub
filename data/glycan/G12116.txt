ENTRY       G12116                      Glycan
NODE        8
            1   GlcNAc     16     2
            2   GlcNAc      7     2
            3   Man        -1     2
            4   Man        -9     5
            5   Man        -9    -1
            6   Man       -17     5
            7   Man       -17     2
            8   Man       -17    -4
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:2  
            6     7:a1    5:6  
            7     8:a1    5:3  
///

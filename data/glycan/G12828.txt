ENTRY       G12828                      Glycan
NODE        10
            1   Glc        13     2
            2   Glc         6     2
            3   Xyl         0     6
            4   Glc         0    -2
            5   Gal        -6     6
            6   Xyl        -6     1
            7   Glc        -6    -5
            8   LFuc      -13     6
            9   Xyl       -13     1
            10  Xyl       -13    -5
EDGE        9
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    3:2  
            5     6:a1    4:6  
            6     7:b1    4:4  
            7     8:a1    5:2  
            8     9:b1    6:2  
            9    10:a1    7:6  
///

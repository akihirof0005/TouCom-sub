ENTRY       G01449                      Glycan
NODE        6
            1   Glc        20     0
            2   Gal        10     0
            3   GlcNAc      0     0
            4   Neu5Ac    -10     5
            5   Gal       -10    -5
            6   LFuc      -20    -5
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:a2    3:6  
            4     5:b1    3:3  
            5     6:a1    5:2  
///

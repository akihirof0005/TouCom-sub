ENTRY       G01645                      Glycan
NODE        10
            1   GlcNAc     24     3
            2   LFuc       15     7
            3   GlcNAc     15    -1
            4   Man         6    -1
            5   Man        -1     4
            6   Man        -1    -6
            7   GlcNAc     -9     4
            8   GlcNAc     -9    -6
            9   Gal       -17    -6
            10  Neu5Ac    -25    -6
EDGE        9
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    8:4  
            9    10:a2    9:6  
///

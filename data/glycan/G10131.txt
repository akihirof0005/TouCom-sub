ENTRY       G10131                      Glycan
NODE        5
            1   Gal         0     0
            2   LAra      -10     5
            3   Gal       -10    -5
            4   LAraf     -20     5
            5   Gal       -20    -5
EDGE        4
            1     2:1     1:4  
            2     3:1     1:3  
            3     4:1     2:3  
            4     5:1     3:3  
///

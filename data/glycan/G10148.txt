ENTRY       G10148                      Glycan
NODE        3
            1   Lgro-manHep    14     0
            2   Lgro-manHep     0     0
            3   Lgro-manHep   -14     0
EDGE        2
            1     2:1     1:2  
            2     3:1     2:3  
///

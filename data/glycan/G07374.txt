ENTRY       G07374                      Glycan
NODE        4
            1   Man      11.8   0.1
            2   3,6dxylHex   0.9   0.1
            3   Gal       -11  -4.8
            4   Abe     -11.4   4.9
EDGE        3
            1     2:a1    1:3  
            2     3:a1    2:2  
            3     4:a1    2    
///

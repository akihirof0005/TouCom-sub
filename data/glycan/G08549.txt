ENTRY       G08549                      Glycan
NODE        4
            1   D/LGalA  15.2   0.5
            2   D/LRha    5.2   0.5
            3   D/LGalA  -4.8   0.5
            4   D/LRha  -14.8   0.5
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:2  
            3     4:a1    3:4  
///

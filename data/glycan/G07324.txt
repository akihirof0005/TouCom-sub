ENTRY       G07324                      Glycan
NODE        4
            1   Gal         0     0
            2   Galf      -10     0
            3   Galf      -20     0
            4   Galf      -30     0
EDGE        3
            1     2:b1    1:5  
            2     3:b1    2:5  
            3     4:b1    3:5  
///

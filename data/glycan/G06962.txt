ENTRY       G06962                      Glycan
NODE        4
            1   ManA6Me     0     0
            2   Gal       -10     0
            3   Glc       -20     5
            4   LRha      -20    -5
EDGE        3
            1     2:b1    1:2  
            2     3:b1    2:4  
            3     4:a1    2:2  
///

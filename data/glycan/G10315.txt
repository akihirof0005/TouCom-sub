ENTRY       G10315                      Glycan
NODE        8
            1   Cer        22    -2
            2   Glc        12    -2
            3   Gal         2    -2
            4   GlcNAc     -8    -2
            5   Gal       -18     3
            6   LFuc      -18    -7
            7   S         -23     7
            8   S         -23    -1
EDGE        7
            1     2:1     1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     7       5:3  
            7     8       5:2  
///

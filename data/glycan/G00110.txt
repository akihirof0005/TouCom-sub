ENTRY       G00110                      Glycan
NODE        6
            1   Cer        16     0
            2   Glc         9     0
            3   Gal         2     0
            4   GalNAc     -7     4
            5   Neu5Ac     -7    -4
            6   Gal       -16     4
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
///

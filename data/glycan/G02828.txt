ENTRY       G02828                      Glycan
NODE        7
            1   Glc        23     0
            2   Gal        16     0
            3   GlcNAc      8     0
            4   Gal         0     0
            5   GlcNAc     -8     0
            6   Gal       -16     0
            7   Neu5Gc    -24     0
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7:a2    6:3  
///

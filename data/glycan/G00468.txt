ENTRY       G00468                      Glycan
NODE        8
            1   GlcNAc     20     3
            2   LFuc       10     8
            3   GlcNAc     10    -2
            4   Man         0    -2
            5   Man       -10     3
            6   Man       -10    -7
            7   GlcNAc    -20     3
            8   GlcNAc    -20    -7
EDGE        7
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
///

ENTRY       G05580                      Glycan
NODE        7
            1   Glc         0     0
            2   Gal       -10     0
            3   Gal       -20     0
            4   GalNAc    -30     0
            5   Gal       -40     0
            6   Gal       -50     5
            7   LFuc      -50    -5
EDGE        6
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:a1    5:3  
            6     7:a1    5:2  
///

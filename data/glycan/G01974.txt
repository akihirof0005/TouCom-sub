ENTRY       G01974                      Glycan
NODE        4
            1   P          15     0
            2   GlcNAc      5     0
            3   Gal        -5     0
            4   Neu5Ac    -15     0
EDGE        3
            1     2:a1    1    
            2     3:b1    2:4  
            3     4:a2    3:6  
///

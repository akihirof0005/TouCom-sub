ENTRY       G00074                      Glycan
NODE        11
            1   Cer        29     0
            2   Glc        23     0
            3   Gal        17     0
            4   GlcNAc     10     0
            5   Gal         3     0
            6   GlcNAc     -4     0
            7   Gal       -11     0
            8   GalNAc    -17     5
            9   LFuc      -17    -5
            10  Gal       -24     5
            11  LFuc      -30     5
EDGE        10
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:a1    7:3  
            8     9:a1    7:2  
            9    10:b1    8:3  
            10   11:a1   10:2  
///

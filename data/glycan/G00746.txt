ENTRY       G00746                      Glycan
NODE        4
            1   Glc         0     0
            2   Glc       -10     0
            3   Glc       -20     0
            4   Glc       -30     0
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    3:4  
///

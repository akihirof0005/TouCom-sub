ENTRY       G02102                      Glycan
NODE        3
            1   Glc         9     0
            2   Gal         0     0
            3   GalNAc    -10     0
EDGE        2
            1     2:b1    1:4  
            2     3:b1    2:3  
///

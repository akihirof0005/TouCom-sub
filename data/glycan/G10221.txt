ENTRY       G10221                      Glycan
NODE        3
            1   D/LGlc   10.2   0.5
            2   D/LGlc    0.2   0.5
            3   D/LGlc   -9.8   0.5
EDGE        2
            1     2:1     1:4  
            2     3:1     2:3  
///

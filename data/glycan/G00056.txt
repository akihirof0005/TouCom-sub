ENTRY       G00056                      Glycan
NODE        7
            1   Cer        17     0
            2   Glc        11     0
            3   Gal         5     0
            4   GlcNAc     -2     0
            5   Gal       -10     5
            6   LFuc      -10    -5
            7   LFuc      -17     5
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     7:a1    5:2  
///

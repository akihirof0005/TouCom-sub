ENTRY       G04306                      Glycan
NODE        12
            1   GlcNAc      0     0
            2   LFuc      -10     5
            3   GlcNAc    -10    -5
            4   Man       -20    -5
            5   Man       -30     5
            6   GlcNAc    -30    -5
            7   Man       -30   -15
            8   Man       -40    10
            9   Man       -40     0
            10  GlcNAc    -40   -10
            11  GlcNAc    -40   -20
            12  Gal       -50   -10
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:b1    4:4  
            6     7:a1    4:3  
            7     8:a1    5:6  
            8     9:a1    5:3  
            9    10:b1    7:4  
            10   11:b1    7:2  
            11   12:b1   10:4  
///

ENTRY       G00625                      Glycan
NODE        5
            1   Qui      10.3   0.4
            2   Qui       2.3   6.4
            3   Qui4NAc   0.3  -5.6
            4   2,6daraHex3NAc  -3.7   0.4
            5   2,6daraHex3NAc  -9.7   6.4
EDGE        4
            1     2:b1    1:4  
            2     3:b1    1:2  
            3     4:b1    1:3  
            4     5:b1    2:3  
///

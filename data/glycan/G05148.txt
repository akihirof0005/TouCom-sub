ENTRY       G05148                      Glycan
NODE        8
            1   Cer        31     0
            2   Glc        23     0
            3   Man        15     0
            4   GlcNAc      6     0
            5   GalNAc     -4     0
            6   GalNAc    -14     0
            7   Gal       -23     0
            8   GlcNAc    -32     0
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    5:4  
            6     7:a1    6:4  
            7     8:b1    7:4  
///

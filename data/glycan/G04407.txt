ENTRY       G04407                      Glycan
NODE        9
            1   GlcNAc     27    -2
            2   Gal        18    -2
            3   GlcNAc      9    -2
            4   Gal         0     3
            5   LFuc        0    -7
            6   GlcNAc     -9     3
            7   Gal       -18     8
            8   LFuc      -18    -2
            9   Neu5Ac    -27     8
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6:b1    4:3  
            6     7:b1    6:4  
            7     8:a1    6:3  
            8     9:a2    7:3  
///

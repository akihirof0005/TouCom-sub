ENTRY       G12235                      Glycan
NODE        6
            1   GlcNAc     22     0
            2   GlcA       14     0
            3   GlcNAc      6     0
            4   GlcA       -2     0
            5   GlcNAc    -10     0
            6   L4-en-thrHexA   -22     0
EDGE        5
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    5:3  
///

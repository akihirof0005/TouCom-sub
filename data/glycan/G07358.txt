ENTRY       G07358                      Glycan
NODE        3
            1   Glc      12.6  -0.1
            2   Gal4Ac    2.6  -0.1
            3   Neu5Ac4Ac7Ac8Ac9Ac -13.4  -0.1
EDGE        2
            1     2:b1    1:4  
            2     3:a2    2:3  
///

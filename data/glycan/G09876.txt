ENTRY       G09876                      Glycan
NODE        3
            1   Glc6SH      0     0
            2   Glc       -10     5
            3   Glc       -10    -5
EDGE        2
            1     2:a1    1:6  
            2     3:a1    1:4  
///

ENTRY       G08270                      Glycan
NODE        4
            1   GalNAc     12     2
            2   GlcNAc      1     2
            3   Gal        -8     2
            4   S         -12    -2
EDGE        3
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4       3:6  
///

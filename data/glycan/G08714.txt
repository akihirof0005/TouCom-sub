ENTRY       G08714                      Glycan
NODE        3
            1   LAra      5.1  -0.1
            2   Glc      -4.9   4.9
            3   Qui      -4.9  -5.1
EDGE        2
            1     2:b1    1:3  
            2     3:b1    1:2  
///

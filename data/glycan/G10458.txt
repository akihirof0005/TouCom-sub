ENTRY       G10458                      Glycan
NODE        6
            1   GalNAc   10.1  -2.5
            2   Gal       1.1  -7.5
            3   GlcNAc    0.1   2.5
            4   Kdn        -7  -7.5
            5   LFuc     -8.9  -1.2
            6   GalNAc   -9.9   6.7
EDGE        5
            1     2:b1    1:3  
            2     3:b1    1:6  
            3     4:a2    2:3  
            4     5:a1    3:3  
            5     6:b1    3:4  
///

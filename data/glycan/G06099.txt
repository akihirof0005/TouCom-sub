ENTRY       G06099                      Glycan
NODE        6
            1   Glc         0     0
            2   Gal       -10     0
            3   Gal       -20     0
            4   Gal       -30     0
            5   Glc       -40     0
            6   Glc       -50     0
EDGE        5
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:2  
            5     6:b1    5:6  
///

ENTRY       G00043                      Glycan
NODE        8
            1   Cer        19     2
            2   Glc        12     2
            3   Gal         5     2
            4   GlcNAc     -3     2
            5   LFuc      -11     7
            6   Gal       -11    -3
            7   LFuc      -18    -7
            8   GalNAc    -19     1
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:a1    4:4  
            5     6:b1    4:3  
            6     7:a1    6:2  
            7     8:a1    6:3  
///

ENTRY       G04655                      Glycan
NODE        10
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30     5
            5   Man       -30    -5
            6   GlcNAc    -40     5
            7   GlcNAc    -40     0
            8   GlcNAc    -40   -10
            9   Gal       -50     0
            10  GalNAc    -50   -10
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
///

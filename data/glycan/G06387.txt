ENTRY       G06387                      Glycan
NODE        6
            1   Lgro-manHep  19.1  -0.1
            2   Lgro-manHep   4.1  -0.1
            3   Glc      -7.9   4.9
            4   Glc      -7.9  -0.1
            5   Gal      -7.9  -5.1
            6   LQui3NAc -18.9  -5.1
EDGE        5
            1     2:a1    1:2  
            2     3:a1    2:6  
            3     4:a1    2:4  
            4     5:a1    2:3  
            5     6:b1    5:3  
///

ENTRY       G04724                      Glycan
NODE        8
            1   QuiNAc   24.6   0.3
            2   GlcNAc   14.6   0.3
            3   Man       5.6   0.3
            4   Man      -1.4   5.3
            5   Man      -1.4  -4.7
            6   GlcNAc   -9.4  -4.7
            7   Gal     -17.4  -4.7
            8   Neu5Ac  -25.4  -4.7
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    5:2  
            6     7:b1    6:4  
            7     8:a2    7:3  
///

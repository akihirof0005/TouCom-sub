ENTRY       G00222                      Glycan
NODE        6
            1   Ser/Thr    17     0
            2   GalNAc      8     0
            3   Gal         0    -4
            4   GlcNAc     -1     4
            5   Gal        -9     4
            6   Neu5Ac    -17     4
EDGE        5
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:b1    2:6  
            4     5:b1    4:4  
            5     6:a2    5:3  
///

ENTRY       G06427                      Glycan
NODE        6
            1   Glc         0     0
            2   Glc       -10     0
            3   Glc       -20     0
            4   Glc       -30     0
            5   Glc       -40     0
            6   Glc       -50     0
EDGE        5
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
            5     6:b1    5:6  
///

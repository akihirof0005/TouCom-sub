ENTRY       G10779                      Glycan
NODE        12
            1   Asn        30     3
            2   GlcNAc     20     3
            3   LFuc       10     8
            4   GlcNAc     10    -2
            5   Man         0    -2
            6   Man       -10     3
            7   GlcNAc    -10    -2
            8   Man       -10    -7
            9   GlcNAc    -20     3
            10  GlcNAc    -20    -7
            11  Gal       -30     3
            12  Gal       -30    -7
EDGE        11
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:b1    5:4  
            7     8:a1    5:3  
            8     9:b1    6:2  
            9    10:b1    8:2  
            10   11:b1    9:4  
            11   12:b1   10:4  
///

ENTRY       G09446                      Glycan
NODE        5
            1   GlcN1Ac3Ac    12     0
            2   S           2     5
            3   L4-en-thrHexA3Ac     2     0
            4   S           2    -5
            5   S         -12     0
EDGE        4
            1     2       1:6  
            2     3:a1    1:4  
            3     4       1:2  
            4     5       3:2  
///

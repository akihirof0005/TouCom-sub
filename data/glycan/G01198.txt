ENTRY       G01198                      Glycan
NODE        4
            1   Glc        10     0
            2   Glc         0     5
            3   Glc         0    -5
            4   Xyl       -10     5
EDGE        3
            1     2:b1    1:4  
            2     3:b1    1:3  
            3     4:b1    2:4  
///

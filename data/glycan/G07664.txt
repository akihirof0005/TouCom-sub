ENTRY       G07664                      Glycan
NODE        9
            1   GlcN       18     0
            2   S          14    -3
            3   GlcA        9     0
            4   S           5    -3
            5   GlcN        0     0
            6   S          -4     3
            7   S          -4    -3
            8   L4-en-thrHexA   -11     0
            9   S         -18    -3
EDGE        8
            1     2       1:2  
            2     3:b1    1:4  
            3     4       3:2  
            4     5:a1    3:4  
            5     6       5:6  
            6     7       5:2  
            7     8:a1    5:4  
            8     9       8:2  
///

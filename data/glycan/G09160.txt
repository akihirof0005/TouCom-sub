ENTRY       G09160                      Glycan
NODE        3
            1   Glc         0     0
            2   Glc       -10     0
            3   GlcNAc    -20     0
EDGE        2
            1     2:a1    1:4  
            2     3:b1    2:6  
///

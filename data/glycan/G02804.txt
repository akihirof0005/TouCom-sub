ENTRY       G02804                      Glycan
NODE        11
            1   Gal        23     1
            2   GlcNAc     14     1
            3   S           9    -2
            4   Gal         6     1
            5   S           2    -2
            6   GlcNAc     -2     1
            7   S          -7     4
            8   Gal       -10     1
            9   LFuc      -10    -4
            10  GlcNAc    -18     1
            11  S         -23    -2
EDGE        10
            1     2:b1    1:3  
            2     3       2:6  
            3     4:b1    2:4  
            4     5       4:6  
            5     6:b1    4:3  
            6     7       6:6  
            7     8:b1    6:4  
            8     9:a1    6:3  
            9    10:b1    8:3  
            10   11      10:6  
///

ENTRY       G05528                      Glycan
NODE        7
            1   Cer        20     0
            2   Glc        12     0
            3   Gal         4     0
            4   GalNAc     -5     0
            5   Gal       -14     0
            6   Gal       -21     5
            7   LFuc      -21    -5
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:3  
            6     7:a1    5:2  
///

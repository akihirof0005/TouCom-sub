ENTRY       G12001                      Glycan
NODE        11
            1   2,5-Anhydro-Man    41     0
            2   GlcA       30     0
            3   GlcNAc     22     0
            4   GlcA       14     0
            5   GlcNAc      6     0
            6   GlcA       -2     0
            7   GlcNAc    -10     0
            8   GlcA      -18     0
            9   GlcNAc    -26     0
            10  GlcA      -34     0
            11  GlcNAc    -42     0
EDGE        10
            1     2:b1    1:3  
            2     3:a1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:4  
            5     6:b1    5:4  
            6     7:a1    6:4  
            7     8:b1    7:4  
            8     9:a1    8:4  
            9    10:b1    9:4  
            10   11:a1   10:4  
///

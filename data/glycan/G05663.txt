ENTRY       G05663                      Glycan
NODE        13
            1   GalNAc      0     0
            2   Neu5Ac    -10     5
            3   GlcNAc    -10    -5
            4   Gal       -20    -5
            5   Neu5Ac    -30     0
            6   GlcNAc    -30   -10
            7   Gal       -40   -10
            8   Neu5Ac    -50    -5
            9   GlcNAc    -50   -15
            10  Gal       -60   -15
            11  Neu5Ac    -70   -10
            12  GlcNAc    -70   -20
            13  Neu5Ac    -80   -20
EDGE        12
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5:a2    4:6  
            5     6:b1    4:3  
            6     7:b1    6:4  
            7     8:a2    7:6  
            8     9:b1    7:3  
            9    10:b1    9:4  
            10   11:a2   10:6  
            11   12:b1   10:3  
            12   13:a2   12:4  
///

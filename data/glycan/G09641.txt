ENTRY       G09641                      Glycan
NODE        4
            1   GlcN        5     0
            2   S           1     4
            3   S           1    -4
            4   GlcA       -5     0
EDGE        3
            1     2       1:3  
            2     3       1:2  
            3     4:b1    1:4  
///

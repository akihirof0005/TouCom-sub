ENTRY       G00341                      Glycan
NODE        10
            1   GlcNAc     20    -2
            2   GlcNAc     10    -2
            3   Man         1    -2
            4   Man        -6     2
            5   Man        -6    -6
            6   Man       -13     6
            7   Man       -13    -2
            8   Man       -14    -6
            9   Man       -21     6
            10  Man       -21    -2
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    4:3  
            7     8:a1    5:2  
            8     9:a1    6:2  
            9    10:a1    7:2  
///

ENTRY       G11320                      Glycan
NODE        13
            1   GlcNAc     23     3
            2   GlcNAc     14     3
            3   Man         6     3
            4   Man        -1     8
            5   Man        -1    -2
            6   GlcNAc     -9     8
            7   GlcNAc     -9     3
            8   GlcNAc     -9    -7
            9   Gal       -17     8
            10  Gal       -17     3
            11  Gal       -17    -7
            12  Gal       -24     8
            13  Gal       -24     3
EDGE        12
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:a1    9:4  
            12   13:a1   10:4  
///

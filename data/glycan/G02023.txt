ENTRY       G02023                      Glycan
NODE        5
            1   Gal        13     2
            2   GlcNAc      3     2
            3   LFuc       -7     7
            4   Gal        -7    -3
            5   S         -13    -7
EDGE        4
            1     2:b1    1:3  
            2     3:a1    2:4  
            3     4:b1    2:3  
            4     5       4:3  
///

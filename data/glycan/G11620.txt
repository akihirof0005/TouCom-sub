ENTRY       G11620                      Glycan
NODE        6
            1   *          28  -0.2
            2   QuiNAc     19  -0.2
            3   GalANFormyl3Ac6N     6  -0.2
            4   GalANAc6N    -8  -0.2
            5   LRha      -19  -0.2
            6   *         -28  -0.2
EDGE        5
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:a1    3:4  
            4     5:a1    4:4  
            5     6       5:2  
BRACKET     1 -24.1   2.6 -24.1  -2.9
            1  25.2  -2.9  25.2   2.6
            1 n
///

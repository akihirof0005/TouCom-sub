ENTRY       G00025                      Glycan
NODE        4
            1   Ser/Thr    10     0
            2   GalNAc     -1     0
            3   Gal        -9    -4
            4   GlcNAc    -10     4
EDGE        3
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:b1    2:6  
///

ENTRY       G11343                      Glycan
NODE        15
            1   GlcNAc     24     3
            2   GlcNAc     15     3
            3   Man         7     3
            4   Man         0     8
            5   GlcNAc      0     3
            6   Man         0    -2
            7   GlcNAc     -8     8
            8   GlcNAc     -8     3
            9   GlcNAc     -8    -7
            10  Gal       -16     8
            11  Gal       -16     3
            12  Gal       -16    -7
            13  Gal       -23     3
            14  Neu5Ac    -24     8
            15  Neu5Ac    -24    -7
EDGE        14
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    3:4  
            5     6:a1    3:3  
            6     7:b1    4:2  
            7     8:b1    6:4  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   14:a2   10:6  
            13   13:a1   11:4  
            14   15:a2   12:6  
///

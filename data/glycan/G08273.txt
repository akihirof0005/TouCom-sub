ENTRY       G08273                      Glycan
NODE        4
            1   Gal        11     2
            2   GlcNAc      2     2
            3   Gal        -7     2
            4   S         -11    -2
EDGE        3
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4       3:3  
///

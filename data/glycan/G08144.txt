ENTRY       G08144                      Glycan
NODE        4
            1   Gal        19     0
            2   L3,6-Anhydro-Gal2Me     6     0
            3   Gal        -7     0
            4   L3,6-Anhydro-Gal2Me   -20     0
EDGE        3
            1     2:a1    1:3  
            2     3:b1    2:4  
            3     4:a1    3:3  
///

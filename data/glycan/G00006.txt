ENTRY       G00006                      Glycan
NODE        8
            1   PP-Dol     22     1
            2   GlcNAc     15     1
            3   GlcNAc      7     1
            4   Man        -2     1
            5   Man        -9     7
            6   Man        -9    -6
            7   Man       -16    -6
            8   Man       -23    -6
EDGE        7
            1     2:a1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    6:2  
            7     8:a1    7:2  
///

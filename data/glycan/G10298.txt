ENTRY       G10298                      Glycan
NODE        5
            1   Glc         7     0
            2   S           3    -4
            3   Gal        -3     0
            4   S          -7     4
            5   S          -7    -4
EDGE        4
            1     2       1:6  
            2     3:b1    1:4  
            3     4       3:6  
            4     5       3:3  
///

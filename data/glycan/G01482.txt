ENTRY       G01482                      Glycan
NODE        8
            1   GlcN       20     1
            2   S          16     6
            3   S          16    -5
            4   GlcA       10     1
            5   GlcN        0     1
            6   S          -4    -5
            7   L4-en-thrHexA   -14     1
            8   S         -21    -5
EDGE        7
            1     2       1:6  
            2     3       1:2  
            3     4:b1    1:4  
            4     5:a1    4:4  
            5     6       5:2  
            6     7:a1    5:4  
            7     8       7:2  
///

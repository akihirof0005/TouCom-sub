ENTRY       G06832                      Glycan
NODE        5
            1   Kdo      12.2   0.5
            2   Man       4.2   0.5
            3   Gal      -3.8   5.5
            4   GalA     -3.8  -4.5
            5   Kdo     -11.8   5.5
EDGE        4
            1     2:a1    1:5  
            2     3:a1    2:6  
            3     4:a1    2:4  
            4     5:2     3:6  
///

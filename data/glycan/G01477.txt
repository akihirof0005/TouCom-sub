ENTRY       G01477                      Glycan
NODE        5
            1   GlcNAc     15     0
            2   Gal         5     0
            3   GlcNAc     -5     5
            4   GlcNAc     -5    -5
            5   Gal       -15     5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:4  
///

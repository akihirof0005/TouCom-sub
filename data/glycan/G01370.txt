ENTRY       G01370                      Glycan
NODE        3
            1   Glc        10     0
            2   Man4Me      0     0
            3   GlcNAc    -10     0
EDGE        2
            1     2:a1    1:6  
            2     3:b1    2:2  
///

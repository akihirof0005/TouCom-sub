ENTRY       G04960                      Glycan
NODE        9
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   GlcNAc    -20     0
            5   GlcNAc    -20   -10
            6   Gal       -30     0
            7   Gal       -30   -10
            8   LFuc      -40     0
            9   LFuc      -40   -10
EDGE        8
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    3:6  
            4     5:b1    3:3  
            5     6:b1    4:4  
            6     7:b1    5:3  
            7     8:a1    6:2  
            8     9:a1    7:2  
///

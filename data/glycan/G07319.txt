ENTRY       G07319                      Glycan
NODE        4
            1   Cer        15     0
            2   Gal         5     0
            3   Gal        -5     0
            4   GalNAc    -15     0
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
///

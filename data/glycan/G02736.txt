ENTRY       G02736                      Glycan
NODE        12
            1   GlcNAc     22    -2
            2   GlcNAc     13    -2
            3   Man         5    -2
            4   Man        -2     3
            5   Man        -2    -7
            6   Man        -9     8
            7   Man        -9    -2
            8   Man        -9    -7
            9   Man       -16     8
            10  Man       -16    -2
            11  Man       -16    -7
            12  Man       -23     8
EDGE        11
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    4:3  
            7     8:a1    5:2  
            8     9:a1    6:2  
            9    10:a1    7:2  
            10   11:a1    8:2  
            11   12:a1    9:2  
///

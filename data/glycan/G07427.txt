ENTRY       G07427                      Glycan
NODE        4
            1   LRha        0     0
            2   LRha2Ac4Ac   -10     0
            3   LRha4Ac   -20     0
            4   LRha2Ac   -30     0
EDGE        3
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:a1    3:3  
///

ENTRY       G04907                      Glycan
NODE        10
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     5
            4   Man       -20    -5
            5   Man       -30     5
            6   Man       -30    -5
            7   Man       -40     5
            8   Man       -40    -5
            9   Glc       -50    -5
            10  Glc       -60    -5
EDGE        9
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:3  
            5     6:a1    4:2  
            6     7:a1    5:2  
            7     8:a1    6:2  
            8     9:a1    8:3  
            9    10:a1    9:3  
///

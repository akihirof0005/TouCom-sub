ENTRY       G00526                      Glycan
NODE        4
            1   GlcNAc     15     0
            2   GlcA        5     0
            3   GlcNAc     -5     0
            4   GlcA      -15     0
EDGE        3
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:3  
///

ENTRY       G02772                      Glycan
NODE        10
            1   Cer        26     0
            2   Glc        19     0
            3   Gal        12     0
            4   GlcNAc      4     0
            5   Gal        -4     0
            6   GlcNAc    -12     5
            7   GlcNAc    -12    -5
            8   Gal       -20     5
            9   Gal       -20    -5
            10  Gal       -27     5
EDGE        9
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:6  
            6     7:b1    5:3  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10:a1    8:3  
///

ENTRY       G05217                      Glycan
NODE        8
            1   GlcN       20     2
            2   Man        10     2
            3   Man         0     7
            4   Man         0    -3
            5   Gal       -10     7
            6   Man       -10    -3
            7   P         -15    -7
            8   Gal       -20     7
EDGE        7
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:2  
            5     6:a1    4:6  
            6     8:a1    5:6  
            7     7       6:6  
///

ENTRY       G03558                      Glycan
NODE        4
            1   2,5-Anhydro-Man     7     0
            2   S          -3     4
            3   S          -3    -4
            4   GlcA       -8     0
EDGE        3
            1     2       1:4  
            2     3       1:1  
            3     4:b1    1:3  
///

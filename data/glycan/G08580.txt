ENTRY       G08580                      Glycan
NODE        4
            1   GlcNAc     16     0
            2   GlcA        6     0
            3   GlcNAc     -4     0
            4   L4-en-thrHexA   -17     0
EDGE        3
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:a1    3:3  
///

ENTRY       G05908                      Glycan
NODE        8
            1   LRha        0     0
            2   LRha      -10     0
            3   GlcNAc    -20     0
            4   LRha      -30     0
            5   LRha      -40     0
            6   LRha      -50     0
            7   GlcNAc    -60     0
            8   LRha      -70     0
EDGE        7
            1     2:a1    1:2  
            2     3:b1    2:2  
            3     4:a1    3:3  
            4     5:a1    4:2  
            5     6:a1    5:2  
            6     7:b1    6:2  
            7     8:a1    7:3  
///

ENTRY       G07470                      Glycan
NODE        5
            1   Man        15     2
            2   Man         5     2
            3   Man        -5     2
            4   P          -9    -2
            5   Man       -15     2
EDGE        4
            1     2:a1    1:2  
            2     3:a1    2:2  
            3     4       3:6  
            4     5:a1    3:2  
///

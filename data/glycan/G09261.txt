ENTRY       G09261                      Glycan
NODE        3
            1   Gal        17     0
            2   L3,6-Anhydro-Gal2Me    -1     0
            3   Gal       -17     0
EDGE        2
            1     2:a1    1:3  
            2     3:b1    2:4  
///

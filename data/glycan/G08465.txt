ENTRY       G08465                      Glycan
NODE        4
            1   Xyl-onic     0     0
            2   Xyl       -10     0
            3   Xyl       -20     0
            4   GlcA4Me   -30     0
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:2  
///

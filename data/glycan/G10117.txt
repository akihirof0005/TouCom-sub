ENTRY       G10117                      Glycan
NODE        4
            1   Glc      12.2   0.5
            2   LRha      5.2   0.5
            3   Glc      -1.8   0.5
            4   L4-en-thrHexA -11.8   0.5
EDGE        3
            1     2:1     1    
            2     3:1     2    
            3     4:1     3    
///

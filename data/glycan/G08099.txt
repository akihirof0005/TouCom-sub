ENTRY       G08099                      Glycan
NODE        3
            1   Gal       5.4     0
            2   Gal      -2.6     5
            3   L2,6dlyxHex  -5.6    -5
EDGE        2
            1     2:a1    1:3  
            2     3:a1    1:2  
///

ENTRY       G12325                      Glycan
NODE        5
            1   Glc        12     4
            2   Glc        12    -4
            3   Glc         4    -4
            4   Glc        -4    -4
            5   Glc       -12    -4
EDGE        4
            1     2:a1    1:a1 
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
///

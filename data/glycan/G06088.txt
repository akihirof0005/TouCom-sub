ENTRY       G06088                      Glycan
NODE        6
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   Gal       -20     5
            5   Fuc       -20    -5
            6   Fuc       -30     5
EDGE        5
            1     2:b1    1:6  
            2     3:b1    1:4  
            3     4:b1    2:4  
            4     5:a1    3:2  
            5     6:a1    4:2  
///

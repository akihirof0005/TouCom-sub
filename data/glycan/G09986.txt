ENTRY       G09986                      Glycan
NODE        3
            1   LFucNAm  19.2   0.5
            2   gro-L3,9dgalNon-2-ulop5N7NAc   0.2   0.5
            3   GlcNAc  -18.8   0.5
EDGE        2
            1     2:2     1:3  
            2     3:a1    2:8  
///

ENTRY       G06688                      Glycan
NODE        6
            1   Glc      17.7     0
            2   Glc       9.7     5
            3   Man       9.7    -5
            4   Man       0.7    -5
            5   LRha     -8.3    -5
            6   Abe     -17.3    -5
EDGE        5
            1     2:a1    1:4  
            2     3:a1    1:3  
            3     4:a1    3:2  
            4     5:a1    4:2  
            5     6:a1    5:3  
///

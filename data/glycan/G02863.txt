ENTRY       G02863                      Glycan
NODE        9
            1   GlcA       32     0
            2   GlcNAc     24     0
            3   GlcA       16     0
            4   GlcNAc      8     0
            5   GlcA        0     0
            6   GlcNAc     -8     0
            7   GlcA      -16     0
            8   GlcNAc    -24     0
            9   GlcA      -32     0
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7:b1    6:3  
            7     8:b1    7:4  
            8     9:b1    8:3  
///

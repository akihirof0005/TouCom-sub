ENTRY       G09856                      Glycan
NODE        3
            1   Gal         0     0
            2   GlcNAc    -10     5
            3   LFuc      -10    -5
EDGE        2
            1     2:b1    1:3  
            2     3:a1    1:2  
///

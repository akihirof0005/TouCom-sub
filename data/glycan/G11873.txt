ENTRY       G11873                      Glycan
NODE        16
            1   Asn        28     4
            2   GlcNAc     20     4
            3   LFuc       11     7
            4   GlcNAc     11     1
            5   Man         2     1
            6   Man        -5     4
            7   Man        -5    -3
            8   GlcNAc    -13     4
            9   GlcNAc    -13     0
            10  GlcNAc    -13    -6
            11  Gal       -21     4
            12  Gal       -21     0
            13  Gal       -21    -6
            14  Neu5Ac    -29     4
            15  Neu5Ac    -29     0
            16  Neu5Ac    -29    -6
EDGE        15
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    7:2  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   13:b1   10:4  
            13   14:a2   11:6  
            14   15:a2   12:6  
            15   16:a2   13:6  
///

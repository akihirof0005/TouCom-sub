ENTRY       G05293                      Glycan
NODE        9
            1   Glc         0     0
            2   Glc       -10     0
            3   Xyl       -20     5
            4   Glc       -20    -5
            5   Gal4Ac    -30     5
            6   Xyl       -30     0
            7   Glc       -30   -10
            8   LFuc      -40     5
            9   Xyl       -40   -10
EDGE        8
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    3:2  
            5     6:a1    4:6  
            6     7:b1    4:4  
            7     8:a1    5:2  
            8     9:a1    7:6  
///

ENTRY       G00832                      Glycan
NODE        3
            1   Man         0     0
            2   Man       -10     0
            3   GlcNAc    -20     0
EDGE        2
            1     2:a1    1:6  
            2     3:b1    2:2  
///

ENTRY       G03310                      Glycan
NODE        4
            1   Man         0     0
            2   Man       -10     0
            3   Man       -20     5
            4   Man       -20    -5
EDGE        3
            1     2:a1    1:6  
            2     3:a1    2:6  
            3     4:a1    2:2  
///

ENTRY       G08874                      Glycan
NODE        3
            1   GlcNAc     10     0
            2   LAra        0     0
            3   LFuc      -10     0
EDGE        2
            1     2:a1    1:3  
            2     3:a1    2:2  
///

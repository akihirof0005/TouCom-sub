ENTRY       G07951                      Glycan
NODE        3
            1   Man         0     0
            2   Man3Me    -10     5
            3   Man       -10    -5
EDGE        2
            1     2:a1    1:6  
            2     3:a1    1:3  
///

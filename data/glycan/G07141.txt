ENTRY       G07141                      Glycan
NODE        4
            1   Glc         0     0
            2   Glc       -10     0
            3   Xyl       -20     0
            4   Gal       -30     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    3:2  
///

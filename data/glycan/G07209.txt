ENTRY       G07209                      Glycan
NODE        5
            1   Man         0     0
            2   GlcNAc    -10     5
            3   GlcNAc    -10    -5
            4   Gal       -20     5
            5   Gal       -20    -5
EDGE        4
            1     2:b1    1:4  
            2     3:a1    1:2  
            3     4:b1    2:4  
            4     5:b1    3:4  
///

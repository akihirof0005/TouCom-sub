ENTRY       G12596                      Glycan
NODE        7
            1   *          20    -3
            2   GalNAc      8    -3
            3   Glc         0    -3
            4   Gal        -7     0
            5   Glc        -7    -6
            6   GlcA      -14    -3
            7   *         -20     6
EDGE        6
            1     2:b1    1    
            2     3:b1    2:3  
            3     4:a1    3:6  
            4     5:b1    3:2  
            5     6:b1    4:3  
            6     7       4:4  
BRACKET     1 -16.0   8.0 -16.0 -10.0
            1  14.0 -10.0  14.0   8.0
            1 n
///

ENTRY       G11526                      Glycan
NODE        8
            1   *          26    -2
            2   GlcNAc     18    -2
            3   Glc         9    -2
            4   GlcNAc     -1    -2
            5   Gal       -10    -2
            6   P         -15     3
            7   Rib-ol    -22     3
            8   *         -27    -2
EDGE        7
            1     2:b1    1    
            2     3:b1    2:3  
            3     4:a1    3:3  
            4     5:b1    4:3  
            5     6       5:3  
            6     8       5:2  
            7     7:5     6    
BRACKET     1 -25.0   6.0 -25.0  -5.0
            1  22.0  -5.0  22.0   6.0
            1 n
///

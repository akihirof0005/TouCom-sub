ENTRY       G06853                      Glycan
NODE        3
            1   GlcNAc   10.2   0.5
            2   D/LGal    0.2   0.5
            3   Neu5Ac   -9.8   0.5
EDGE        2
            1     2:b1    1:4  
            2     3:a2    2:6  
///

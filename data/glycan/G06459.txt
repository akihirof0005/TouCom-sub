ENTRY       G06459                      Glycan
NODE        6
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     5
            4   Man       -20    -5
            5   Man       -30     5
            6   Man       -40     5
EDGE        5
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:2  
            5     6:a1    5:2  
///

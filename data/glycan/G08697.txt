ENTRY       G08697                      Glycan
NODE        7
            1   GlcN       17     0
            2   S          13     4
            3   S          13    -4
            4   GlcA        7     0
            5   GlcNAc     -3     0
            6   S          -8    -4
            7   L4-en-thrHexA   -17     0
EDGE        6
            1     2       1:3  
            2     3       1:2  
            3     4:a1    1:4  
            4     5:a1    4:4  
            5     6       5:6  
            6     7:a1    5:4  
///

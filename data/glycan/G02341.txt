ENTRY       G02341                      Glycan
NODE        3
            1   GalNAc     10     0
            2   Gal         0     0
            3   Neu5Ac    -10     0
EDGE        2
            1     2:b1    1:3  
            2     3:b2    2:3  
///

ENTRY       G07749                      Glycan
NODE        4
            1   Kdo      13.2   0.5
            2   Man       4.2   0.5
            3   Gal      -4.8   0.5
            4   Kdo     -12.8   0.5
EDGE        3
            1     2:a1    1:5  
            2     3:a1    2:6  
            3     4:2     3:6  
///

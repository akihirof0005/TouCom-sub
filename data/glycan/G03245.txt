ENTRY       G03245                      Glycan
NODE        4
            1   Glc         0     0
            2   LRha      -10     5
            3   LRha      -10    -5
            4   Glc       -20     5
EDGE        3
            1     2:a1    1:4  
            2     3:a1    1:2  
            3     4:b1    2:4  
///

ENTRY       G06879                      Glycan
NODE        5
            1   Glc         0     0
            2   Gal       -10     0
            3   Gal       -20     5
            4   Gal       -20    -5
            5   GlcA      -30     5
EDGE        4
            1     2:a1    1:3  
            2     3:b1    2:3  
            3     4:a1    2:2  
            4     5:b1    3:4  
///

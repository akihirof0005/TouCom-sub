ENTRY       G03819                      Glycan
NODE        13
            1   GalNAc     50     2
            2   Gal        40     2
            3   GlcNAc     30     2
            4   Gal        20     2
            5   GlcNAc     10     2
            6   Gal         0     2
            7   GlcNAc    -10     2
            8   S         -16    -2
            9   Gal       -20     2
            10  GlcNAc    -30     2
            11  S         -36    -2
            12  Gal       -40     2
            13  Neu5Ac    -50     2
EDGE        12
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7:b1    6:3  
            7     8       7:6  
            8     9:b1    7:4  
            9    10:b1    9:3  
            10   11      10:6  
            11   12:b1   10:4  
            12   13:a2   12:3  
///

ENTRY       G06001                      Glycan
NODE        6
            1   GalA     18.2   0.5
            2   GalA      9.2   0.5
            3   GalA     -0.8   5.5
            4   3dlyxHep-2-ulop-aric  -4.8  -4.5
            5   GalA     -9.8   5.5
            6   Araf    -18.8  -4.5
EDGE        5
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:2     2:3  
            4     5:a1    3:4  
            5     6:b1    4:5  
///

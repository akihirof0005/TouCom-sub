ENTRY       G00124                      Glycan
NODE        5
            1   Cer        18     0
            2   Glc        10     0
            3   Gal         2     0
            4   GalNAc     -7     0
            5   Gal       -18     0
EDGE        4
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:3  
///

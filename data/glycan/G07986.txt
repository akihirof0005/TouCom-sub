ENTRY       G07986                      Glycan
NODE        3
            1   Glc         0     0
            2   Glc       -10     5
            3   Fuc       -10    -5
EDGE        2
            1     2:b1    1:3  
            2     3:b1    1:2  
///

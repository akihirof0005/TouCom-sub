ENTRY       G06520                      Glycan
NODE        3
            1   GalNAc6Me     0     0
            2   Gal       -10     0
            3   Neu5Ac    -20     0
EDGE        2
            1     2:b1    1:3  
            2     3:a2    2:3  
///

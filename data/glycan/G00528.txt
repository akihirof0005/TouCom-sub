ENTRY       G00528                      Glycan
NODE        3
            1   GlcN        8     3
            2   S           3    -2
            3   L4-en-thrHexA    -8     3
EDGE        2
            1     2       1:2  
            2     3:a1    1:4  
///

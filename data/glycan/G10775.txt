ENTRY       G10775                      Glycan
NODE        10
            1   Asn        30     3
            2   GlcNAc     20     3
            3   LFuc       10     8
            4   GlcNAc     10    -2
            5   Man         0    -2
            6   Man       -10     3
            7   Man       -10    -7
            8   GlcNAc    -20     3
            9   GlcNAc    -20    -7
            10  Gal       -30    -7
EDGE        9
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:2  
            9    10:b1    9:4  
///

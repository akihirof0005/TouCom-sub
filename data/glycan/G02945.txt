ENTRY       G02945                      Glycan
NODE        7
            1   Gal        20     0
            2   GlcNAc     11     0
            3   Gal         3     0
            4   GlcNAc     -5     5
            5   GlcNAc     -5    -5
            6   Gal       -13     5
            7   LFuc      -21     5
EDGE        6
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:6  
            4     5:b1    3:3  
            5     6:b1    4:4  
            6     7:a1    6:2  
///

ENTRY       G12369                      Glycan
NODE        4
            1   Ser/Thr    14     0
            2   Man         3     0
            3   Man        -7     0
            4   P         -14     0
EDGE        3
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4       3:6  
///

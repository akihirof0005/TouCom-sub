ENTRY       G06432                      Glycan
NODE        5
            1   Man        10     0
            2   Man         0     5
            3   Man         0    -5
            4   Man       -10     5
            5   Man       -10    -5
EDGE        4
            1     2:a1    1:6  
            2     3:a1    1:2  
            3     4:a1    2:6  
            4     5:a1    3:2  
///

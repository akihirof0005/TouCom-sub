ENTRY       G00316                      Glycan
NODE        6
            1   ManA       25     0
            2   LGulA      15     0
            3   LGulA       5     0
            4   ManA       -5     0
            5   ManA      -15     0
            6   LGulA     -25     0
EDGE        5
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:a1    5:4  
///

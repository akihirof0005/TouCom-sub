ENTRY       G07655                      Glycan
NODE        4
            1   L4dTho   15.6   0.5
            2   GlcNAc    4.6   0.5
            3   GalNAc   -5.4   0.5
            4   Gal     -15.4   0.5
EDGE        3
            1     2:b1    1:2  
            2     3:a1    2:3  
            3     4:b1    3:3  
///

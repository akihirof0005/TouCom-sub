ENTRY       G00501                      Glycan
NODE        3
            1   Glc        10     0
            2   Gal         0     0
            3   Gal       -10     0
EDGE        2
            1     2:a1    1:6  
            2     3:a1    2:6  
///

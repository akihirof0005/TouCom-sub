ENTRY       G07466                      Glycan
NODE        4
            1   P        19.1  -0.1
            2   4-C-methyl-GlcA6N   6.1  -0.1
            3   GlcNAc   -7.9  -0.1
            4   QuiNAc  -18.9  -0.1
EDGE        3
            1     2:a1    1    
            2     3:b1    2:2  
            3     4:b1    3:4  
///

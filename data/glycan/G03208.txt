ENTRY       G03208                      Glycan
NODE        4
            1   Cer      15.2   0.5
            2   Glc       8.2   0.5
            3   Gal       1.2   0.5
            4   3,4dmanNon-2-ulop5NAc-onic -15.8   0.5
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
///

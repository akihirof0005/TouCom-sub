ENTRY       G06091                      Glycan
NODE        10
            1   GlcNAc     22     0
            2   S          17    -4
            3   LIdoA      12     0
            4   S           7    -4
            5   GlcNAc      2     0
            6   S          -3     4
            7   S          -3    -4
            8   GlcA       -8     0
            9   GlcNAc    -18     0
            10  S         -23    -4
EDGE        9
            1     2       1:6  
            2     3:a1    1:4  
            3     4       3:2  
            4     5:a1    3:4  
            5     6       5:6  
            6     7       5:3  
            7     8:b1    5:4  
            8     9:a1    8:4  
            9    10       9:6  
///

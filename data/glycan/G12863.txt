ENTRY       G12863                      Glycan
NODE        6
            1   Gro      16.2   2.8
            2   Glc       9.2   2.8
            3   GalN      0.2   2.8
            4   acyl     -4.9  -1.8
            5   Gal      -8.8   2.8
            6   Gal     -15.8   2.8
EDGE        5
            1     2:a1    1:1  
            2     3:b1    2:2  
            3     4       3:2  
            4     5:b1    3:6  
            5     6:a1    5:6  
///

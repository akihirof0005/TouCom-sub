ENTRY       G03652                      Glycan
NODE        5
            1   Cer      17.2   0.5
            2   Glc       9.2   0.5
            3   Gal       1.2   0.5
            4   GlcNAc   -7.8   0.5
            5   Neu5Ac  -16.8   0.5
EDGE        4
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    4    
///

ENTRY       G00566                      Glycan
NODE        4
            1   GalNAc     10     0
            2   GlcNAc      0     5
            3   Gal         0    -5
            4   LFuc      -10    -5
EDGE        3
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:a1    3:2  
///

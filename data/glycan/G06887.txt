ENTRY       G06887                      Glycan
NODE        5
            1   Man         0     0
            2   Man       -10     0
            3   Gal3,4Py   -20     5
            4   Glc       -20     0
            5   GlcA      -20    -5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    2:4  
            4     5:b1    2:3  
///

ENTRY       G11474                      Glycan
NODE        6
            1   *          25     0
            2   Glc4Ac     15     0
            3   GlcNAc      5     0
            4   GlcA4Ac    -5     0
            5   Glc6Ac    -15     0
            6   *         -25     0
EDGE        5
            1     2:b1    1    
            2     3:a1    2:3  
            3     4:b1    3:3  
            4     5:a1    4:3  
            5     6       5:4  
BRACKET     1 -22.0   3.0 -22.0  -3.0
            1  22.0  -3.0  22.0   3.0
            1 n
///

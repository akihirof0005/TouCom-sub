ENTRY       G06285                      Glycan
NODE        6
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20     0
            4   LFuc      -30     5
            5   Gal       -30    -5
            6   LFuc      -40    -5
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:b1    3:3  
            5     6:a1    5:2  
///

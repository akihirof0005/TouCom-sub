ENTRY       G09639                      Glycan
NODE        3
            1   Glc         0     0
            2   Gal       -10     5
            3   Glc       -10    -5
EDGE        2
            1     2:b1    1:4  
            2     3:a1    1:2  
///

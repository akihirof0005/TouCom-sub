ENTRY       G00969                      Glycan
NODE        3
            1   Glc         0     0
            2   Glc       -10     5
            3   Glc       -10    -5
EDGE        2
            1     2:a1    1:4  
            2     3:a1    1:2  
///

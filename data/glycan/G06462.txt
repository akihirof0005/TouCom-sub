ENTRY       G06462                      Glycan
NODE        5
            1   Cym      20.6  -0.4
            2   Cym      11.6  -0.4
            3   2,6dlyxHex3Me  -0.4  -0.4
            4   Glc     -12.4  -0.4
            5   Glc     -20.4  -0.4
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:6  
///

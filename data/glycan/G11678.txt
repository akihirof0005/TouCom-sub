ENTRY       G11678                      Glycan
NODE        7
            1   GlcN       30     0
            2   GlcN       21     0
            3   Kdo        11     0
            4   gro-manHep    -1     0
            5   Glc       -12     0
            6   Gal       -21     0
            7   Galf      -30     0
EDGE        6
            1     2:b1    1:6  
            2     3:a1    2:6  
            3     4:a1    3:5  
            4     5:b1    4:4  
            5     6:b1    5:4  
            6     7:b1    6:3  
///

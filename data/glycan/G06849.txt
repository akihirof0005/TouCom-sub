ENTRY       G06849                      Glycan
NODE        5
            1   Man         0     0
            2   Gal       -10     5
            3   Man       -10    -5
            4   Gal       -20     0
            5   Man       -20   -10
EDGE        4
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:a1    3:6  
            4     5:b1    3:4  
///

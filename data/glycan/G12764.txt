ENTRY       G12764                      Glycan
NODE        6
            1   *          17     0
            2   LRha        7     0
            3   Gal        -1     0
            4   Glc        -8     3
            5   Neu5Ac     -9    -3
            6   *         -18     3
EDGE        5
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6       4:4  
BRACKET     1 -12.0   5.0 -12.0  -5.0
            1  11.0  -5.0  11.0   5.0
            1 n
///

ENTRY       G02861                      Glycan
NODE        8
            1   GalNAc     28     0
            2   Gal        19     0
            3   GlcNAc     11     0
            4   Gal         3     0
            5   GlcNAc     -5     0
            6   Gal       -13     0
            7   GlcNAc    -21     0
            8   Gal       -29     0
EDGE        7
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7:b1    6:3  
            7     8:b1    7:4  
///

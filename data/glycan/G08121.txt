ENTRY       G08121                      Glycan
NODE        3
            1   GalA        0     0
            2   Xyl       -10     0
            3   LRha      -20     0
EDGE        2
            1     2:b1    1:3  
            2     3:a1    2:2  
///

ENTRY       G05883                      Glycan
NODE        8
            1   Gal        26     2
            2   L3,6-Anhydro-Gal    14     2
            3   Gal         2     2
            4   LGal       -6     2
            5   S         -11    -2
            6   Gal       -14     2
            7   LGal      -22     2
            8   S         -27    -2
EDGE        7
            1     2:a1    1:3  
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5       4:6  
            5     6:b1    4:4  
            6     7:a1    6:3  
            7     8       7:6  
///

ENTRY       G06609                      Glycan
NODE        6
            1   LRha       15     3
            2   Glc-ol     15    -3
            3   LRha        5     3
            4   LRha        5    -3
            5   GlcNAc     -5     3
            6   Gal       -15     3
EDGE        5
            1     3:a1    1:2  
            2     1:a1    2:1  
            3     4:a1    2:3  
            4     5:b1    3:4  
            5     6:b1    5:3  
///

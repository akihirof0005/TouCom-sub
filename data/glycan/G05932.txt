ENTRY       G05932                      Glycan
NODE        6
            1   Glc6Me     18     4
            2   Glc        18    -3
            3   LRha        8     4
            4   Glc        -1     4
            5   LRha      -10     4
            6   Xyl       -19     4
EDGE        5
            1     3:a1    1:3  
            2     1:a1    2:a1 
            3     4:b1    3:3  
            4     5:a1    4:3  
            5     6:b1    5:2  
///

ENTRY       G08844                      Glycan
NODE        4
            1   Gal         0     0
            2   Glc       -10     5
            3   LRha      -10    -5
            4   Man       -20    -5
EDGE        3
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:a1    3:4  
///

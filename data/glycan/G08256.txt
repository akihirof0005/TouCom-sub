ENTRY       G08256                      Glycan
NODE        6
            1   Kdo      12.2   0.5
            2   Hep       5.2   0.5
            3   Glc      -2.8   5.5
            4   Hep      -2.8  -4.5
            5   GlcNAc  -11.8  -4.5
            6   D/LHexNAc -12.8   5.5
EDGE        5
            1     2:1     1    
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     6:1     3:4  
            5     5:a1    4:2  
///

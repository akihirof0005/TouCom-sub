ENTRY       G04300                      Glycan
NODE        11
            1   Asn      30.2   0.5
            2   GlcNAc   22.2   0.5
            3   GlcNAc   12.2   0.5
            4   Man       3.2   0.5
            5   Man      -4.8   5.5
            6   Man      -4.8  -4.5
            7   GlcNAc  -13.8   5.5
            8   GlcNAc  -13.8  -4.5
            9   GalNAc  -23.8   5.5
            10  GalNAc  -23.8  -4.5
            11  S       -29.8  -4.5
EDGE        10
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11      10    
///

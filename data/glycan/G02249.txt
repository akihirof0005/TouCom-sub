ENTRY       G02249                      Glycan
NODE        9
            1   Xyl        19    -2
            2   Xyl        13    -2
            3   Xyl         7    -2
            4   Xyl         0     2
            5   LAraf       0    -6
            6   Xyl        -6     2
            7   Xyl       -13     6
            8   LAraf     -14    -2
            9   Xyl       -19     6
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6:b1    4:4  
            6     7:b1    6:4  
            7     8:a1    6:3  
            8     9:b1    7:4  
///

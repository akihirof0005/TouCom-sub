ENTRY       G04964                      Glycan
NODE        9
            1   Kdo        22     0
            2   Lgro-manHep    12     0
            3   P           6    -4
            4   P           2    -4
            5   Lgro-manHep     0     0
            6   P          -7    -4
            7   Lgro-manHep   -12     5
            8   Glc       -12     0
            9   Glc       -22     0
EDGE        8
            1     2:a1    1:5  
            2     3       2:4  
            3     5:a1    2:3  
            4     4       3    
            5     6       5:4  
            6     7:a1    5:7  
            7     8:a1    5:3  
            8     9:a1    8:3  
///

ENTRY       G03950                      Glycan
NODE        13
            1   Cer        31     5
            2   Glc        24     5
            3   Gal        17     5
            4   GlcNAc      9    10
            5   GlcNAc      9     0
            6   GlcNAc      0     0
            7   Gal        -8     0
            8   GlcNAc    -16     5
            9   GlcNAc    -16    -5
            10  Gal       -24     5
            11  Gal       -24    -5
            12  GalNAc    -32     0
            13  LFuc      -32   -10
EDGE        12
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:6  
            4     5:b1    3:3  
            5     6:b1    5:4  
            6     7:b1    6:4  
            7     8:b1    7:6  
            8     9:b1    7:3  
            9    10:b1    8:4  
            10   11:b1    9:4  
            11   12:a1   11:3  
            12   13:a1   11:2  
///

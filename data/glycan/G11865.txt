ENTRY       G11865                      Glycan
NODE        18
            1   GlcNAc     28     3
            2   LFuc       18     6
            3   GlcNAc     18     0
            4   Man         8     0
            5   Man         0     5
            6   Man         0    -5
            7   GlcNAc     -9     8
            8   GlcNAc     -9     2
            9   GlcNAc     -9    -2
            10  GlcNAc     -9    -8
            11  Gal       -19     8
            12  Gal       -19     2
            13  Gal       -19    -2
            14  Gal       -19    -8
            15  Gal       -28     8
            16  Gal       -28     2
            17  Gal       -28    -2
            18  Gal       -28    -8
EDGE        17
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:4  
            14   15:a1   11:3  
            15   16:a1   12:3  
            16   17:a1   13:3  
            17   18:a1   14:3  
///

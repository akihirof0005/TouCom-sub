ENTRY       G03501                      Glycan
NODE        3
            1   Glc         5     4
            2   Glc         5    -3
            3   Glc        -5     4
EDGE        2
            1     3:b1    1:4  
            2     1:a1    2:a1 
///

ENTRY       G04736                      Glycan
NODE        10
            1   Asn        30     2
            2   GlcNAc     20     2
            3   GlcNAc     11     2
            4   Man         3     2
            5   Man        -5     7
            6   Man        -5    -3
            7   Man       -14     7
            8   GlcNAc    -14    -3
            9   GalNAc    -24    -3
            10  S         -31    -7
EDGE        9
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    8:4  
            9    10       9:4  
///

ENTRY       G05836                      Glycan
NODE        7
            1   Fru         0     0
            2   Fruf      -10     0
            3   Fruf      -20     0
            4   Fruf      -30     0
            5   Fruf      -40     0
            6   Fruf      -50     0
            7   Fruf      -60     0
EDGE        6
            1     2:b2    1:6  
            2     3:b2    2:6  
            3     4:b2    3:6  
            4     5:b2    4:6  
            5     6:b2    5:6  
            6     7:b2    6:6  
///

ENTRY       G02909                      Glycan
NODE        8
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   GlcNAc    -10    -5
            4   Gal       -20    10
            5   LFuc      -20     0
            6   Gal       -20    -5
            7   LFuc      -30    10
            8   LFuc      -30    -5
EDGE        7
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a1    2:3  
            5     6:b1    3:3  
            6     7:a1    4:2  
            7     8:a1    6:2  
///

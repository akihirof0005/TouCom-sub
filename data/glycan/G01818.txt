ENTRY       G01818                      Glycan
NODE        11
            1   Asn        25     4
            2   GlcNAc     17     4
            3   LFuc        8     9
            4   GlcNAc      7    -1
            5   Man        -2    -1
            6   Man       -10     3
            7   Man       -10    -5
            8   GlcNAc    -18    -1
            9   GlcNAc    -18    -9
            10  Gal       -26    -1
            11  Gal       -26    -9
EDGE        10
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    7:4  
            8     9:b1    7:2  
            9    10:b1    8:4  
            10   11:b1    9:4  
///

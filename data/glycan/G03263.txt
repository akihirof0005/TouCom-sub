ENTRY       G03263                      Glycan
NODE        4
            1   GlcNAc      0     0
            2   LRha      -10     0
            3   ManNAc    -20     0
            4   Man       -30     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:b1    3:3  
///

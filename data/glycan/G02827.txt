ENTRY       G02827                      Glycan
NODE        8
            1   GlcNAc     19     0
            2   Man        11     0
            3   Man         4     5
            4   Man         4    -5
            5   GlcNAc     -4     5
            6   GlcNAc     -4    -5
            7   Gal       -12     5
            8   Neu5Ac    -20     5
EDGE        7
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:b1    3:2  
            5     6:b1    4:2  
            6     7:b1    5:4  
            7     8:a2    7:6  
///

ENTRY       G00360                      Glycan
NODE        3
            1   Gal        10     0
            2   LRha        0     0
            3   Man       -10     0
EDGE        2
            1     2:a1    1:3  
            2     3:b1    2:4  
///

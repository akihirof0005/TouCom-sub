ENTRY       G03598                      Glycan
NODE        18
            1   GlcNAc      0     0
            2   LFuc      -10     5
            3   GlcNAc    -10    -5
            4   Man       -20    -5
            5   Man       -30    10
            6   Man       -30    -5
            7   Xyl       -30   -20
            8   GlcNAc    -40    10
            9   GlcNAc    -40    -5
            10  GalNAc    -50    10
            11  GalNAc    -50    -5
            12  Gal3Me    -60    15
            13  Gal3Me    -60     5
            14  Gal3Me    -60     0
            15  Gal3Me    -60   -10
            16  Gal3Me    -70     5
            17  Gal3Me    -70   -10
            18  Gal3Me    -80   -10
EDGE        17
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:2  
            7     8:b1    5:2  
            8     9:b1    6:2  
            9    10:b1    8:4  
            10   11:b1    9:4  
            11   12:b1   10:6  
            12   13:b1   10:3  
            13   14:b1   11:6  
            14   15:b1   11:3  
            15   16:b1   13:6  
            16   17:b1   15:6  
            17   18:b1   17:6  
///

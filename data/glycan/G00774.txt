ENTRY       G00774                      Glycan
NODE        9
            1   GlcNAc     17     2
            2   LFuc        9     6
            3   GlcNAc      8    -2
            4   Man        -1    -2
            5   Man        -9     2
            6   Man        -9    -6
            7   GlcNAc    -18     6
            8   GlcNAc    -18    -2
            9   GlcNAc    -18    -6
EDGE        8
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:2  
///

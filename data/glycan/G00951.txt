ENTRY       G00951                      Glycan
NODE        8
            1   Asn        21     0
            2   GlcNAc     13     0
            3   GlcNAc      3     0
            4   Man        -6     0
            5   Man       -14     5
            6   Man       -14    -5
            7   Man       -22     5
            8   GlcNAc    -22    -5
EDGE        7
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:3  
            7     8:b1    6:2  
///

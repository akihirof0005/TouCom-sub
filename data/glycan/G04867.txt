ENTRY       G04867                      Glycan
NODE        9
            1   GlcNAc     27     0
            2   GlcNAc     17     0
            3   Man         8     0
            4   Man         0     5
            5   Xyl         0    -5
            6   Man        -1     0
            7   GlcNAc     -9     0
            8   GalNAc    -18     0
            9   Gal3Me    -27     0
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    3:2  
            5     6:a1    3:3  
            6     7:b1    6:2  
            7     8:b1    7:4  
            8     9:b1    8:3  
///

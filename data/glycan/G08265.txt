ENTRY       G08265                      Glycan
NODE        3
            1   GlcNAc    6.2   0.5
            2   Gal      -2.8  -4.5
            3   L3,6dlyxHex  -5.8   5.5
EDGE        2
            1     2:b1    1:3  
            2     3:a1    1:4  
///

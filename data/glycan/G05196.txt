ENTRY       G05196                      Glycan
NODE        10
            1   GlcNAc     22     0
            2   Man        12     0
            3   Man         2     5
            4   Man         2    -5
            5   Man        -8     5
            6   Man        -8    -5
            7   P         -15     5
            8   P         -15    -5
            9   Man       -22     5
            10  Man       -22    -5
EDGE        9
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
            5     6:a1    4:2  
            6     7       5:6  
            7     8       6:6  
            8     9:a1    7    
            9    10:a1    8    
///

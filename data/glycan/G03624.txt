ENTRY       G03624                      Glycan
NODE        11
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30     5
            5   Man       -30    -5
            6   GlcNAc    -40    -5
            7   GalNAc    -50    -5
            8   Gal3Me    -60     0
            9   Gal3Me    -60   -10
            10  Gal3Me    -70     0
            11  Gal3Me    -70   -10
EDGE        10
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    5:2  
            6     7:b1    6:4  
            7     8:b1    7:6  
            8     9:b1    7:3  
            9    10:b1    8:6  
            10   11:b1    9:6  
///

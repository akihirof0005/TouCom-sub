ENTRY       G04166                      Glycan
NODE        9
            1   GlcNAc      0     0
            2   Gal       -10     0
            3   GlcNAc    -20     0
            4   Gal       -30     0
            5   GlcNAc    -40     5
            6   GlcNAc    -40    -5
            7   Gal       -50     5
            8   Gal       -50    -5
            9   Neu5Ac    -60    -5
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:6  
            5     6:b1    4:3  
            6     7:b1    5:4  
            7     8:b1    6:4  
            8     9:a2    8:3  
///

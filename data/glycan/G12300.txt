ENTRY       G12300                      Glycan
NODE        5
            1   Glc        10     5
            2   Fruf       10    -4
            3   Fruf        0     5
            4   Fruf        0    -4
            5   Fruf      -10    -4
EDGE        4
            1     3:b2    1:6  
            2     1:a1    2:b2 
            3     4:b2    2:6  
            4     5:b2    4:6  
///

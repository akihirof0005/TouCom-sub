ENTRY       G06097                      Glycan
NODE        16
            1   Glc        26     0
            2   Man        18     0
            3   Man        10     5
            4   Man        10    -5
            5   GlcNAc      1     9
            6   GlcNAc      1     1
            7   GlcNAc      1    -1
            8   GlcNAc      1    -9
            9   Gal        -8     9
            10  Gal        -8     1
            11  Gal        -8    -1
            12  Gal        -8    -9
            13  GlcNAc    -17     1
            14  GlcNAc    -17    -1
            15  Gal       -26     1
            16  Gal       -26    -1
EDGE        15
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:b1    3:6  
            5     6:b1    3:2  
            6     7:b1    4:4  
            7     8:b1    4:2  
            8     9:b1    5:4  
            9    10:b1    6:4  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1   10:3  
            13   14:b1   11:3  
            14   15:b1   13:4  
            15   16:b1   14:4  
///

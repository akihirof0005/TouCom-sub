ENTRY       G01602                      Glycan
NODE        3
            1   GalNAc     10     0
            2   LFucNAc     0     0
            3   ManNAc    -10     0
EDGE        2
            1     2:a1    1:3  
            2     3:b1    2:3  
///

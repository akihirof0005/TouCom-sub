ENTRY       G06014                      Glycan
NODE        6
            1   Xyl      12.1  -0.1
            2   Glc       5.1   4.9
            3   Qui       5.1  -5.1
            4   Glc      -2.9   4.9
            5   Glc      -2.9  -5.1
            6   Glc3Me  -11.9  -5.1
EDGE        5
            1     2:b1    1:4  
            2     3:b1    1:2  
            3     4:b1    2:3  
            4     5:b1    3:4  
            5     6:b1    5:3  
///

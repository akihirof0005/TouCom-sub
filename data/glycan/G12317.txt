ENTRY       G12317                      Glycan
NODE        5
            1   Ser      17.2   0.5
            2   GalNAc    8.2   0.5
            3   GlcNAc   -1.8   0.5
            4   Gal     -10.8   0.5
            5   S       -16.8   0.5
EDGE        4
            1     2:1     1:3  
            2     3:b1    2:6  
            3     4:b1    3:4  
            4     5       4:3  
///

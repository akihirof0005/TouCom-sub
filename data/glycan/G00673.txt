ENTRY       G00673                      Glycan
NODE        3
            1   Glc       9.1  -0.1
            2   Glc       0.1  -0.1
            3   Qui4N    -9.9  -0.1
EDGE        2
            1     2:a1    1:4  
            2     3:a1    2:4  
///

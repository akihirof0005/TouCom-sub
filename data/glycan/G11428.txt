ENTRY       G11428                      Glycan
NODE        4
            1   GalNAc     15     0
            2   Gal         5     0
            3   GlcA       -5     0
            4   GlcNAc    -15     0
EDGE        3
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4:b1    3:4  
///

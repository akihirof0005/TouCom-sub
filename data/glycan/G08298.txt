ENTRY       G08298                      Glycan
NODE        5
            1   GlcA       11     2
            2   GalNAc      1     2
            3   S           0    -2
            4   S          -4    -2
            5   L4-en-thrHexA   -12     2
EDGE        4
            1     2:b1    1:4  
            2     3       2:6  
            3     4       2:4  
            4     5:a1    2:3  
///

ENTRY       G11525                      Glycan
NODE        10
            1   GalNAc     23     3
            2   Neu5Ac     13     8
            3   GlcNAc     13    -2
            4   Gal         3    -2
            5   GlcNAc     -6     3
            6   GlcNAc     -6    -7
            7   Gal       -15    -7
            8   Neu5Ac    -16     8
            9   Gal       -16    -2
            10  GalNAc    -24    -7
EDGE        9
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5:b1    4:6  
            5     6:b1    4:3  
            6     8:a2    5:6  
            7     9:b1    5:4  
            8     7:b1    6:3  
            9    10:a1    7:3  
///

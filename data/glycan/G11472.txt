ENTRY       G11472                      Glycan
NODE        6
            1   *          20     0
            2   Glc        10     0
            3   Gal         0     0
            4   GalNAc    -10     5
            5   GalNAc    -10    -5
            6   *         -20     5
EDGE        5
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    3:4  
            4     5:b1    3:3  
            5     6       4:3  
BRACKET     1 -17.0   9.0 -17.0 -10.0
            1  17.0 -10.0  17.0   9.0
            1 n
///

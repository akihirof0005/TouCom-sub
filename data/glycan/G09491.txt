ENTRY       G09491                      Glycan
NODE        4
            1   GlcN        5     0
            2   S          -5     5
            3   LIdoA      -5     0
            4   S          -5    -5
EDGE        3
            1     2       1:6  
            2     3:a1    1:4  
            3     4       1:2  
///

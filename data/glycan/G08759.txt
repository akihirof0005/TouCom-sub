ENTRY       G08759                      Glycan
NODE        4
            1   D/LGlcNAc   9.2  -2.5
            2   S         1.6   1.9
            3   Gal      -2.8  -2.5
            4   S        -8.4   2.2
EDGE        3
            1     2       1:6  
            2     3:b1    1:4  
            3     4       3:6  
///

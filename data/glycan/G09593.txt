ENTRY       G09593                      Glycan
NODE        5
            1   GlcN        7     0
            2   S           3     4
            3   S           3    -4
            4   LIdoA      -3     0
            5   S          -7    -4
EDGE        4
            1     2       1:3  
            2     3       1:2  
            3     4:a1    1:4  
            4     5       4:2  
///

ENTRY       G07151                      Glycan
NODE        4
            1   Gal         9     0
            2   GlcNAc      0     0
            3   Gal        -9     5
            4   LFuc       -9    -5
EDGE        3
            1     2:b1    1:6  
            2     3:b1    2:4  
            3     4:a1    2:3  
///

ENTRY       G01666                      Glycan
NODE        8
            1   GlcNAc     35     0
            2   GlcNAc     25     0
            3   GlcNAc     15     0
            4   GlcNAc      5     0
            5   GlcNAc     -5     0
            6   GlcNAc    -15     0
            7   GlcNAc    -25     0
            8   GlcNAc    -35     0
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:b1    5:4  
            6     7:b1    6:4  
            7     8:b1    7:4  
///

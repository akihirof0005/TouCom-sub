ENTRY       G03978                      Glycan
NODE        11
            1   GalNAc      0     0
            2   Gal       -10     0
            3   GlcNAc    -20     0
            4   Gal       -30     0
            5   GlcNAc    -40     5
            6   GlcNAc    -40    -5
            7   Gal       -50     5
            8   Gal       -50    -5
            9   GlcNAc    -60    -5
            10  Gal       -70    -5
            11  Neu5Gc    -80    -5
EDGE        10
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:6  
            5     6:b1    4:3  
            6     7:b1    5:4  
            7     8:b1    6:4  
            8     9:b1    8:3  
            9    10:b1    9:4  
            10   11:a2   10:3  
///

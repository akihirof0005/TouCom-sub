ENTRY       G02805                      Glycan
NODE        8
            1   Glc        17    -2
            2   Gal         9    -2
            3   GlcNAc      0     3
            4   GlcNAc      0    -7
            5   Gal        -9     8
            6   LFuc       -9    -2
            7   Gal        -9    -7
            8   Neu5Ac    -18    -7
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:4  
            5     6:a1    3:3  
            6     7:b1    4:4  
            7     8:a2    7:6  
///

ENTRY       G05970                      Glycan
NODE        5
            1   Gal        15     2
            2   GlcNAc      5     2
            3   S           1    -2
            4   Gal        -5     2
            5   Neu5Ac    -15     2
EDGE        4
            1     2:b1    1:3  
            2     3       2:6  
            3     4:b1    2:4  
            4     5:a2    4:3  
///

ENTRY       G11182                      Glycan
NODE        4
            1   GalNAc     10     0
            2   S           0     5
            3   GlcA        0     0
            4   S         -10    -5
EDGE        3
            1     2       1:6  
            2     3:b1    1:3  
            3     4       3:2  
///

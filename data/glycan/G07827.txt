ENTRY       G07827                      Glycan
NODE        3
            1   Glc         0     0
            2   FucNAc4N   -10     0
            3   Glc       -20     0
EDGE        2
            1     2:b1    1:3  
            2     3:b1    2:3  
///

ENTRY       G07299                      Glycan
NODE        4
            1   Glc         0     0
            2   Glc       -10     0
            3   Glc       -20     5
            4   Glc       -20    -5
EDGE        3
            1     2:b1    1:2  
            2     3:b1    2:6  
            3     4:b1    2:2  
///

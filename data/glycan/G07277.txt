ENTRY       G07277                      Glycan
NODE        5
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   GalNAc    -20    10
            5   LFuc      -20     0
EDGE        4
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a1    2:3  
///

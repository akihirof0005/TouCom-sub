ENTRY       G10122                      Glycan
NODE        5
            1   Glc      13.2   0.5
            2   Gal       5.2   0.5
            3   GlcNAc   -3.8   0.5
            4   Gal     -12.8  -4.5
            5   Neu5Ac  -13.8   5.5
EDGE        4
            1     2:1     1:4  
            2     3:1     2:3  
            3     4:1     3:3  
            4     5:2     3:4  
///

ENTRY       G07273                      Glycan
NODE        5
            1   Gal      14.6  -0.4
            2   Man       7.6  -0.4
            3   Man       0.6  -0.4
            4   LRha     -6.4  -0.4
            5   Abe     -14.4  -0.4
EDGE        4
            1     2:a1    1:3  
            2     3:a1    2:2  
            3     4:b1    3:2  
            4     5:a1    4:3  
///

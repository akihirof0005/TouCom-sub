ENTRY       G02320                      Glycan
NODE        4
            1   GlcA       10     0
            2   LAra        0     5
            3   Gal         0    -5
            4   Glc       -10     5
EDGE        3
            1     2:a1    1:3  
            2     3:b1    1:2  
            3     4:b1    2:2  
///

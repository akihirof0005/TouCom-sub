ENTRY       G06702                      Glycan
NODE        6
            1   GalNAc   12.2   0.5
            2   Gal       3.2  -4.5
            3   GlcNAc    2.2   5.5
            4   LFuc     -3.8  -4.5
            5   Gal      -4.8   5.5
            6   LFuc    -11.8   5.5
EDGE        5
            1     2:b1    1:3  
            2     3:b1    1:6  
            3     4:a1    2    
            4     5:1     3    
            5     6:a1    5    
///

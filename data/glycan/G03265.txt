ENTRY       G03265                      Glycan
NODE        4
            1   GlcNAc     15     0
            2   Man         4     0
            3   Man        -5     0
            4   GlcNAc    -15     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:b1    3:4  
///

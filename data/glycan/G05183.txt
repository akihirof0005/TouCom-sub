ENTRY       G05183                      Glycan
NODE        9
            1   Gal        36     0
            2   Glc        26     0
            3   Glc        16     0
            4   Glc         6     0
            5   Glc        -4     0
            6   Glc       -14     5
            7   GlcA      -14    -5
            8   GlcA      -24    -5
            9   Gal2Ac3Ac4,6Py   -36    -5
EDGE        8
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:6  
            5     6:b1    5:6  
            6     7:b1    5:4  
            7     8:a1    7:3  
            8     9:a1    8:4  
///

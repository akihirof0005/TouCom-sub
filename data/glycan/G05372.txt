ENTRY       G05372                      Glycan
NODE        8
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30     5
            5   Man       -30    -5
            6   Man       -40     5
            7   GlcNAc    -40    -5
            8   Gal       -50    -5
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    7:4  
///

ENTRY       G02183                      Glycan
NODE        21
            1   GlcNAc     28     5
            2   LFuc       21    10
            3   GlcNAc     20     0
            4   Man        13     0
            5   Man         7     6
            6   Man         7    -6
            7   GlcNAc      0    10
            8   GlcNAc      0     2
            9   GlcNAc      0    -2
            10  GlcNAc      0   -10
            11  Gal        -7    10
            12  Gal        -7     2
            13  Gal        -7    -2
            14  Gal        -7   -10
            15  GlcNAc    -14    10
            16  GlcNAc    -14     2
            17  Neu5Ac    -14   -10
            18  Gal       -21    10
            19  Gal       -21     2
            20  Neu5Ac    -28    10
            21  Neu5Ac    -28     2
EDGE        20
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:4  
            14   15:b1   11:3  
            15   16:b1   12:3  
            16   17:a2   14:3  
            17   18:b1   15:4  
            18   19:b1   16:4  
            19   20:a2   18:3  
            20   21:a2   19:3  
///

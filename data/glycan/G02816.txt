ENTRY       G02816                      Glycan
NODE        9
            1   GalNAc     29     0
            2   GlcNAc     19     0
            3   Gal        11     0
            4   GlcNAc      3     0
            5   Gal        -5     0
            6   GlcNAc    -13     0
            7   Gal       -21     0
            8   LFuc      -28    -5
            9   GalNAc    -29     5
EDGE        8
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:a1    7:2  
            8     9:a1    7:3  
///

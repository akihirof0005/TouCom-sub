ENTRY       G04115                      Glycan
NODE        19
            1   Asn        29     0
            2   GlcNAc     21     0
            3   GlcNAc     11     0
            4   Man         2     0
            5   Man        -6     6
            6   Man        -6    -6
            7   GlcNAc    -14    11
            8   GlcNAc    -14     6
            9   GlcNAc    -14     1
            10  GlcNAc    -14    -2
            11  GlcNAc    -14   -10
            12  Gal       -22    11
            13  Gal       -22     6
            14  Gal       -22     1
            15  Gal       -22    -2
            16  Gal       -22   -10
            17  Neu5Ac    -30    11
            18  Gal       -30     6
            19  Gal       -30     1
EDGE        18
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:4  
            8     9:b1    5:2  
            9    10:b1    6:4  
            10   11:b1    6:2  
            11   12:b1    7:4  
            12   13:b1    8:4  
            13   14:b1    9:4  
            14   15:b1   10:4  
            15   16:b1   11:4  
            16   17:a2   12:6  
            17   18:a1   13:4  
            18   19:a1   14:4  
///

ENTRY       G10638                      Glycan
NODE        4
            1   Asn        10     0
            2   Gal         0     0
            3   GalNAc    -10     5
            4   LFuc      -10    -5
EDGE        3
            1     2:b1    1    
            2     3:a1    2:3  
            3     4:a1    2:2  
///

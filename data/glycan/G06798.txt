ENTRY       G06798                      Glycan
NODE        5
            1   Gal         0     0
            2   GalA      -10     0
            3   GalA      -20     0
            4   GalA      -30     0
            5   GalA      -40     0
EDGE        4
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
///

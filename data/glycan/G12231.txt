ENTRY       G12231                      Glycan
NODE        6
            1   Cer        18     0
            2   Glc        11     0
            3   Gal         4     0
            4   Neu5Ac     -4     0
            5   Neu5Ac    -13     0
            6   S         -19     0
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
            4     5:a2    4:8  
            5     6       5:8  
///

ENTRY       G00431                      Glycan
NODE        15
            1   GlcNAc     24     3
            2   LFuc       16     7
            3   GlcNAc     15    -2
            4   Man         7    -2
            5   Man         0     2
            6   Man         0    -6
            7   GlcNAc     -8     6
            8   GlcNAc     -8    -2
            9   GlcNAc     -8    -6
            10  Gal       -16     6
            11  Gal       -16    -2
            12  Gal       -16    -6
            13  Neu5Ac    -24     6
            14  Neu5Ac    -24    -2
            15  Neu5Ac    -24    -6
EDGE        14
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   13:a2   10:3  
            13   14:a2   11:3  
            14   15:a2   12:3  
///

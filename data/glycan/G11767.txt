ENTRY       G11767                      Glycan
NODE        5
            1   *          21  -0.1
            2   FucNAc     11  -0.1
            3   LFucNAc     0  -0.1
            4   ManANAc3Ac   -12  -0.1
            5   *         -22  -0.1
EDGE        4
            1     2:b1    1    
            2     3:a1    2:3  
            3     4:b1    3:4  
            4     5       4:4  
BRACKET     1 -18.0   6.9 -18.0  -7.1
            1  17.0  -7.1  17.0   6.9
            1 n
///

ENTRY       G06043                      Glycan
NODE        7
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     5
            4   Man       -20    -5
            5   Man       -30    10
            6   Man       -30     0
            7   Glc       -30    -5
EDGE        6
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
            5     6:a1    3:3  
            6     7:a1    4:3  
///

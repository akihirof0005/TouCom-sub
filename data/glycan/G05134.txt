ENTRY       G05134                      Glycan
NODE        8
            1   GlcNAc      0     0
            2   Gal       -10     0
            3   GlcNAc    -20     0
            4   Gal       -30     5
            5   LFuc      -30    -5
            6   GlcNAc    -40     5
            7   Gal       -50    10
            8   LFuc      -50     0
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6:b1    4:3  
            6     7:b1    6:4  
            7     8:a1    6:3  
///

ENTRY       G06690                      Glycan
NODE        5
            1   Glc        16     0
            2   Gal         8     0
            3   GlcNAc     -1     0
            4   Gal       -10     0
            5   Fuc       -17     0
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:a1    4:2  
///

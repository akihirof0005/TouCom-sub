ENTRY       G00163                      Glycan
NODE        7
            1   Ser        21     0
            2   Xyl        15     0
            3   Gal         8     0
            4   Gal         1     0
            5   GlcA       -6     0
            6   GlcNAc    -14     0
            7   GlcA      -22     0
EDGE        6
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:a1    5:4  
            6     7:b1    6:4  
///

ENTRY       G07633                      Glycan
NODE        5
            1   GlcNAc    9.2  -2.5
            2   Gal       0.2   2.5
            3   LFuc      0.2  -7.5
            4   LFuc     -7.8  -2.5
            5   GalNAc   -8.8   7.5
EDGE        4
            1     2:b1    1:4  
            2     3:1     1:3  
            3     4:a1    2:2  
            4     5:a1    2:3  
///

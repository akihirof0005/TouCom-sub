ENTRY       G03129                      Glycan
NODE        4
            1   Rha         0     0
            2   Rha       -10     0
            3   Rha       -20     0
            4   Rha       -30     0
EDGE        3
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:a1    3:2  
///

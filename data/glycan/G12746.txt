ENTRY       G12746                      Glycan
NODE        7
            1   *          20    -2
            2   GalNAc     11    -2
            3   GlcA        3    -2
            4   ManNAc     -5    -2
            5   Gal       -13    -2
            6   LRha      -13    -6
            7   *         -21     7
EDGE        6
            1     2:b1    1    
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     7       4:6  
BRACKET     1 -16.0   7.0 -16.0  -8.0
            1  16.0  -8.0  16.0   7.0
            1 n
///

ENTRY       G06124                      Glycan
NODE        6
            1   Glc         0     0
            2   Gal       -10     0
            3   GalNAc    -20     0
            4   Gal       -30     0
            5   Gal       -40     5
            6   LFuc      -40    -5
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:a1    4:3  
            5     6:a1    4:2  
///

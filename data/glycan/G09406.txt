ENTRY       G09406                      Glycan
NODE        3
            1   Glc         0     0
            2   Glc       -10     0
            3   Glc4N     -20     0
EDGE        2
            1     2:a1    1:4  
            2     3:a1    2:4  
///

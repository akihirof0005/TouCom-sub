ENTRY       G12240                      Glycan
NODE        6
            1   Glc        17     0
            2   Gal         9     0
            3   GlcNAc      0     0
            4   Gal        -9     0
            5   LFuc      -17    -4
            6   GalNAc    -18     4
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:a1    4:2  
            5     6:a1    4:3  
///

ENTRY       G04098                      Glycan
NODE        13
            1   Asn        30    -2
            2   GlcNAc     21    -2
            3   GlcNAc     11    -2
            4   Man         2    -2
            5   Man        -6     2
            6   Man        -6    -6
            7   Man       -14     6
            8   Man       -14    -2
            9   Man       -14    -6
            10  Man       -22     6
            11  Man       -22    -6
            12  Man       -30     6
            13  Man       -30    -6
EDGE        12
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:6  
            7     8:a1    5:3  
            8     9:a1    6:2  
            9    10:a1    7:2  
            10   11:a1    9:2  
            11   12:a1   10:3  
            12   13:a1   11:3  
///

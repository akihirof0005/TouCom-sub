ENTRY       G02419                      Glycan
NODE        3
            1   Xyl       8.2   0.5
            2   GlcA      0.2   0.5
            3   GalNAc   -8.8   0.5
EDGE        2
            1     2:b1    1    
            2     3:a1    2    
///

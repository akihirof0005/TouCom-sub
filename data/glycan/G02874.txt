ENTRY       G02874                      Glycan
NODE        5
            1   Cer        17     0
            2   Glc        10     0
            3   Gal         3     0
            4   Neu5Ac     -5     0
            5   Neu5Ac7Ac9Ac   -17     0
EDGE        4
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
            4     5:a2    4:8  
///

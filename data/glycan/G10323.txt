ENTRY       G10323                      Glycan
NODE        3
            1   Xyl         0     0
            2   Gal       -10     0
            3   Neu       -20     0
EDGE        2
            1     2:b1    1:4  
            2     3:a2    2:3  
///

ENTRY       G00106                      Glycan
NODE        6
            1   Cer        17     0
            2   Glc        10     0
            3   Gal         3     0
            4   Gal        -4     0
            5   Gal       -11     0
            6   Gal       -18     0
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:3  
            5     6:a1    5:3  
///

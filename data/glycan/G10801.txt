ENTRY       G10801                      Glycan
NODE        10
            1   Asn        25     2
            2   GlcNAc     17     2
            3   GlcNAc      7     2
            4   Man        -2     2
            5   Man       -10     6
            6   Man       -10    -2
            7   GlcNAc    -18     2
            8   GlcNAc    -18    -6
            9   Gal       -26     2
            10  Gal       -26    -6
EDGE        9
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    6:4  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
///

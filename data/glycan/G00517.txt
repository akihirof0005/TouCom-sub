ENTRY       G00517                      Glycan
NODE        3
            1   Fruf        5     4
            2   Glc         5    -3
            3   Glc        -5    -3
EDGE        2
            1     2:a1    1:b2 
            2     3:a1    2:4  
///

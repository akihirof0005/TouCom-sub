ENTRY       G02216                      Glycan
NODE        8
            1   GlcN       20     3
            2   Man        10     3
            3   Man         0     8
            4   Gal         0    -2
            5   Man       -10     8
            6   Gal       -10     3
            7   Gal       -10    -7
            8   Gal       -20     3
EDGE        7
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:2  
            5     6:a1    4:6  
            6     7:a1    4:2  
            7     8:a1    6:2  
///

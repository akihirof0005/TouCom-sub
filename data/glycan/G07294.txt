ENTRY       G07294                      Glycan
NODE        4
            1   Fuc         0     0
            2   Glc       -10     0
            3   Glc       -20     5
            4   LRha      -20    -5
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:a1    2:2  
///

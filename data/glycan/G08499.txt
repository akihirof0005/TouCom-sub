ENTRY       G08499                      Glycan
NODE        4
            1   Fruf        9     4
            2   Man-ol      9    -4
            3   Fruf        0    -4
            4   Fruf       -9    -4
EDGE        3
            1     1:b2    2:1  
            2     3:b2    2:6  
            3     4:b2    3:6  
///

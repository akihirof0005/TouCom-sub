ENTRY       G09466                      Glycan
NODE        5
            1   Cer      15.2   0.5
            2   GlcA      7.2   0.5
            3   GlcN     -0.8   0.5
            4   Gal      -7.8   0.5
            5   Man     -14.8   0.5
EDGE        4
            1     2:1     1:1  
            2     3:1     2    
            3     4:1     3    
            4     5:1     4    
///

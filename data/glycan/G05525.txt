ENTRY       G05525                      Glycan
NODE        9
            1   Kdo      17.2  -2.5
            2   Glc       9.2   2.5
            3   Kdo       9.2  -7.5
            4   Glc       1.2   7.5
            5   Glc       1.2   2.5
            6   Glc       1.2  -2.5
            7   Glc      -6.8   7.5
            8   GlcNAc   -7.8   2.5
            9   Gal     -16.8   2.5
EDGE        8
            1     2:a1    1:5  
            2     3:2     1:4  
            3     4:b1    2:6  
            4     5:b1    2:4  
            5     6:b1    2:3  
            6     7:a1    4:2  
            7     8:a1    5:2  
            8     9:b1    8:4  
///

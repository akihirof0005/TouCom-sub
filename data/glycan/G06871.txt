ENTRY       G06871                      Glycan
NODE        5
            1   GalNAc     11     2
            2   Neu5Ac      1     7
            3   GlcNAc      1    -3
            4   Gal        -9    -3
            5   S         -12    -7
EDGE        4
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5       4:6  
///

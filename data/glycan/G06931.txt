ENTRY       G06931                      Glycan
NODE        5
            1   Glc         0     0
            2   Gal       -10     5
            3   Glc       -10    -5
            4   Glc       -20    -5
            5   GlcNAc    -30    -5
EDGE        4
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:a1    3:2  
            4     5:b1    4:6  
///

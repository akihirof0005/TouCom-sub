ENTRY       G03117                      Glycan
NODE        5
            1   Fruf       15     4
            2   Glc        15    -4
            3   Fruf        5     4
            4   Fruf       -5     4
            5   Fruf      -15     4
EDGE        4
            1     3:b2    1:6  
            2     1:b2    2:a1 
            3     4:b2    3:6  
            4     5:b2    4:6  
///

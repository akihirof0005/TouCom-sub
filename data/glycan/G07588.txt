ENTRY       G07588                      Glycan
NODE        5
            1   Glc6Ac      0     0
            2   Glc       -10     0
            3   LRha      -20     0
            4   LRha      -30     0
            5   GlcA      -40     0
EDGE        4
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:a1    3:3  
            4     5:b1    4:2  
///

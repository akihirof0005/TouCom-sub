ENTRY       G02886                      Glycan
NODE        8
            1   Cer        23     0
            2   Glc        16     0
            3   Gal         9     0
            4   GlcNAc      1     0
            5   Gal        -7     0
            6   GlcNAc    -15     0
            7   LFuc      -23     5
            8   Gal       -23    -5
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:b1    5:3  
            6     7:a1    6:4  
            7     8:b1    6:3  
///

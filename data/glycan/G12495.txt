ENTRY       G12495                      Glycan
NODE        3
            1   GlcNAc   13.2   0.5
            2   Gal       4.2   0.5
            3   gro-L3daltNon-2-ulop5NAc-onic -12.8   0.5
EDGE        2
            1     2:b1    1:3  
            2     3:b2    2:3  
///

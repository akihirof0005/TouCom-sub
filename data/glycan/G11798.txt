ENTRY       G11798                      Glycan
NODE        3
            1   LAra       10     0
            2   Gal         0     0
            3   Gal       -10     0
EDGE        2
            1     2:b1    1:6  
            2     3:b1    2:6  
BRACKET     1  -3.0   2.0  -3.0  -2.0
            1   3.0  -2.0   3.0   2.0
            1 n
///

ENTRY       G03143                      Glycan
NODE        5
            1   P          12     0
            2   Gal         5     0
            3   Glc        -4     5
            4   Man        -4    -5
            5   Man       -13    -5
EDGE        4
            1     2:a1    1    
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:a1    4:2  
///

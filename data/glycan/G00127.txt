ENTRY       G00127                      Glycan
NODE        7
            1   Cer        17     0
            2   Glc        11     0
            3   Gal         5     0
            4   GalNAc     -3     0
            5   Gal       -10    -4
            6   Neu5Ac    -11     4
            7   Neu5Ac    -18    -4
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:a2    4:6  
            6     7:a2    5:3  
///

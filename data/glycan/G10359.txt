ENTRY       G10359                      Glycan
NODE        3
            1   Gal         5     2
            2   S           0    -2
            3   LFuc       -5     2
EDGE        2
            1     2       1:3  
            2     3:a1    1:2  
///

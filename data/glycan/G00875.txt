ENTRY       G00875                      Glycan
NODE        20
            1   GlcNAc     40     4
            2   LFuc       30     9
            3   GlcNAc     30    -1
            4   Man        20    -1
            5   Man        10     4
            6   Man        10    -6
            7   GlcNAc      0     7
            8   GlcNAc      0     1
            9   GlcNAc      0    -3
            10  GlcNAc      0    -9
            11  Gal       -10     7
            12  Gal       -10     1
            13  Gal       -10    -3
            14  Gal       -10    -9
            15  Neu5Ac    -20     7
            16  GlcNAc    -20     1
            17  Neu5Ac    -20    -3
            18  Neu5Ac    -20    -9
            19  Gal       -30     1
            20  Neu5Ac    -40     1
EDGE        19
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:4  
            14   15:a2   11:3  
            15   16:b1   12:3  
            16   17:a2   13:3  
            17   18:a2   14:3  
            18   19:b1   16:4  
            19   20:a2   19:3  
///

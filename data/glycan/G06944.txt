ENTRY       G06944                      Glycan
NODE        5
            1   Gal         0     0
            2   Man       -10     0
            3   Man       -20     0
            4   GalA      -30     0
            5   Man4,6Py   -40     0
EDGE        4
            1     2:a1    1:3  
            2     3:a1    2:2  
            3     4:a1    3:3  
            4     5:a1    4:4  
///

ENTRY       G11513                      Glycan
NODE        8
            1   GalNAc     20     0
            2   Gal        12     0
            3   GlcNAc      4     0
            4   GalNAc     -5     5
            5   LFuc       -5    -5
            6   LFuc      -13     5
            7   LFuc      -13    -5
            8   LFuc      -20     5
EDGE        7
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6:a1    4:3  
            6     7:a1    5:2  
            7     8:a1    6:2  
///

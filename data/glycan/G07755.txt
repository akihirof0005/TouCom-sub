ENTRY       G07755                      Glycan
NODE        4
            1   GalNAc      0     0
            2   GlcNAc    -10     0
            3   GalNAc    -20     5
            4   GalNAc    -20    -5
EDGE        3
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    2:3  
///

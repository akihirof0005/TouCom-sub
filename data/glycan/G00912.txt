ENTRY       G00912                      Glycan
NODE        4
            1   GalNAc      0     0
            2   Gal       -10     0
            3   Gal       -20     5
            4   LFuc      -20    -5
EDGE        3
            1     2:b1    1:3  
            2     3:a1    2:3  
            3     4:a1    2:2  
///

ENTRY       G12081                      Glycan
NODE        10
            1   Asn        26    -2
            2   GlcNAc     17    -2
            3   GlcNAc      7    -2
            4   Man        -2    -2
            5   Man       -10     3
            6   Man       -10    -7
            7   Man       -18     8
            8   Man       -18    -2
            9   Man       -18    -7
            10  Man       -26    -7
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:6  
            7     8:a1    5:3  
            8     9:a1    6:2  
            9    10:a1    9:2  
///

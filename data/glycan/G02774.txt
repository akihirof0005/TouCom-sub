ENTRY       G02774                      Glycan
NODE        11
            1   GlcNAc     20    -4
            2   GlcNAc     11     0
            3   LFuc       11    -8
            4   Man         3     0
            5   Man        -4     5
            6   Man        -4     0
            7   Xyl        -4    -5
            8   GlcNAc    -12     5
            9   GlcNAc    -12     0
            10  LFuc      -20     9
            11  Gal       -20     1
EDGE        10
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    2:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:2  
            7     8:b1    5:2  
            8     9:b1    6:2  
            9    10:a1    8:6  
            10   11:b1    8:4  
///

ENTRY       G11946                      Glycan
NODE        12
            1   GlcNAc     25     2
            2   LFuc       15     5
            3   GlcNAc     15    -1
            4   Man         7    -1
            5   Man        -1     2
            6   Man        -1    -5
            7   GlcNAc     -9     2
            8   GlcNAc     -9    -5
            9   Gal       -17     2
            10  Gal       -17    -5
            11  S         -21     2
            12  Neu5Ac    -25    -5
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11       9:3  
            11   12:a2   10:3  
///

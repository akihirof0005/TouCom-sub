ENTRY       G11912                      Glycan
NODE        13
            1   Asn        27     1
            2   GlcNAc     18     1
            3   GlcNAc      8     1
            4   Man        -1     1
            5   Man       -10     5
            6   Man       -10    -4
            7   GlcNAc    -11     1
            8   Man       -18     8
            9   Man       -18     2
            10  GlcNAc    -19    -1
            11  GlcNAc    -19    -7
            12  Gal       -28    -1
            13  Gal       -28    -7
EDGE        12
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:4  
            7     8:a1    5:6  
            8     9:a1    5:3  
            9    10:b1    6:4  
            10   11:b1    6:2  
            11   12:b1   10:4  
            12   13:b1   11:4  
///

ENTRY       G10314                      Glycan
NODE        6
            1   Glc        15    -1
            2   Gal         7    -1
            3   GlcNAc     -2    -1
            4   Gal       -11     4
            5   LFuc      -11    -6
            6   S         -15     7
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6       4:3  
///

ENTRY       G07531                      Glycan
NODE        4
            1   GlcNAc   11.2   0.5
            2   LFuc      2.2   5.5
            3   3dxylHex   0.2  -4.5
            4   LFuc    -10.8  -4.5
EDGE        3
            1     2:a1    1:4  
            2     3:b1    1:3  
            3     4:a1    3:2  
///

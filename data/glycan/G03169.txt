ENTRY       G03169                      Glycan
NODE        4
            1   GlcNAc    8.6  -0.4
            2   Col      -0.4   4.6
            3   Gal      -0.4  -5.4
            4   LFuc     -9.4  -5.4
EDGE        3
            1     2:a1    1:4  
            2     3:b1    1:3  
            3     4:a1    3:2  
///

ENTRY       G02331                      Glycan
NODE        4
            1   Xyl      12.2   0.5
            2   Gal       5.2   0.5
            3   Gal      -1.8   0.5
            4   L4-en-thrHexA -11.8   0.5
EDGE        3
            1     2:b1    1    
            2     3:b1    2    
            3     4:a1    3    
///

ENTRY       G04743                      Glycan
NODE        9
            1   Kdo      28.2   0.5
            2   Lgro-manHep  18.2   0.5
            3   Glc       7.2   5.5
            4   Lgro-manHep   4.2  -4.5
            5   Gal      -0.8   5.5
            6   GlcNAc   -7.8  -4.5
            7   GlcNAc   -9.8   5.5
            8   Gal     -18.8   5.5
            9   GalNAc  -27.8   5.5
EDGE        8
            1     2:a1    1    
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:b1    3:4  
            5     6:a1    4:2  
            6     7:b1    5:3  
            7     8:b1    7:4  
            8     9:b1    8:3  
///

ENTRY       G08036                      Glycan
NODE        8
            1   GlcN       18     2
            2   S          14    -2
            3   IdoA        9     2
            4   S           5    -2
            5   GlcN        0     2
            6   S          -4    -2
            7   L4-en-thrHexA   -11     2
            8   S         -18    -2
EDGE        7
            1     2       1:2  
            2     3:a1    1:4  
            3     4       3:2  
            4     5:a1    3:4  
            5     6       5:2  
            6     7:a1    5:4  
            7     8       7:2  
///

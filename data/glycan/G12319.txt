ENTRY       G12319                      Glycan
NODE        5
            1   GlcN     11.2   2.5
            2   Man       3.2   2.5
            3   P        -0.8  -1.5
            4   Man      -4.8   2.5
            5   P       -10.8   2.5
EDGE        4
            1     2:a1    1:4  
            2     3       2    
            3     4:a1    2:6  
            4     5       4:6  
///

ENTRY       G06522                      Glycan
NODE        7
            1   Glc        35     0
            2   Glc        27     0
            3   3,6-Anhydro-Glc    15     0
            4   3,6-Anhydro-Glc    -1     0
            5   Glc       -13     0
            6   3,6-Anhydro-Glc   -25     0
            7   Glc       -36     0
EDGE        6
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
            5     6:a1    5:4  
            6     7:a1    6:4  
///

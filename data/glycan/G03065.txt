ENTRY       G03065                      Glycan
NODE        4
            1   Cer      14.6  -0.1
            2   Glc       4.6  -0.1
            3   Gal      -5.4  -0.1
            4   Neu5Gc1N -15.4  -0.1
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
///

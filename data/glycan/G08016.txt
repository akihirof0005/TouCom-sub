ENTRY       G08016                      Glycan
NODE        3
            1   Kdo       5.2   0.5
            2   Kdo      -3.8  -4.5
            3   D/LHep   -4.8   5.5
EDGE        2
            1     2:a2    1:4  
            2     3:a1    1:5  
///

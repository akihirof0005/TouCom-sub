ENTRY       G08091                      Glycan
NODE        3
            1   2dlyxHex   7.4     2
            2   Gal      -2.6     2
            3   S        -6.6    -2
EDGE        2
            1     2:b1    1:3  
            2     3       2:3  
///

ENTRY       G06756                      Glycan
NODE        5
            1   GalNAc      0     0
            2   GlcNAc    -10     0
            3   Gal       -20     0
            4   GlcNAc    -30     0
            5   Gal       -40     0
EDGE        4
            1     2:b1    1:6  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:3  
///

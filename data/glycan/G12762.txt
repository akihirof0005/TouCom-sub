ENTRY       G12762                      Glycan
NODE        7
            1   *          17     3
            2   Gal        10     3
            3   Glc         4     6
            4   LRha3Ac     3     0
            5   Man        -5     0
            6   Par       -11     3
            7   *         -17    -6
EDGE        6
            1     2:a1    1    
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    4:4  
            5     6:a1    5:3  
            6     7       5:2  
BRACKET     1 -13.0   7.0 -13.0  -6.0
            1  13.0  -6.0  13.0   7.0
            1 n
///

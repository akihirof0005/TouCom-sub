ENTRY       G05016                      Glycan
NODE        9
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20     0
            4   Gal       -30     0
            5   GlcNAc    -40     0
            6   Gal       -50     5
            7   LFuc      -50    -5
            8   Gal       -60    10
            9   LFuc      -60     0
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7:a1    5:3  
            7     8:a1    6:3  
            8     9:a1    6:2  
///

ENTRY       G12003                      Glycan
NODE        11
            1   PP-Dol     26    -2
            2   GlcNAc     17    -2
            3   GlcNAc      7    -2
            4   Man        -2    -2
            5   Man       -10     2
            6   Man       -10    -6
            7   Man       -18     6
            8   Man       -18    -2
            9   Man       -18    -6
            10  Man       -26     6
            11  Man       -26    -6
EDGE        10
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:6  
            7     8:a1    5:3  
            8     9:a1    6:2  
            9    10:a1    7:2  
            10   11:a1    9:2  
///

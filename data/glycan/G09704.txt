ENTRY       G09704                      Glycan
NODE        3
            1   D/LAraf   5.2   0.5
            2   D/LAraf  -4.8   5.5
            3   D/LAraf  -4.8  -4.5
EDGE        2
            1     2:a1    1:5  
            2     3:a1    1:3  
///

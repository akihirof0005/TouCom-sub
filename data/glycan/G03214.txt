ENTRY       G03214                      Glycan
NODE        5
            1   Xyl         0     0
            2   Xyl       -10     0
            3   Xyl       -20     0
            4   LAraf     -30     0
            5   Xyl       -40     0
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5:b1    4:2  
///

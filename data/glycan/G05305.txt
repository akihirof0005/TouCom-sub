ENTRY       G05305                      Glycan
NODE        8
            1   GalNAc     15     0
            2   GlcNAc      5     8
            3   GlcNAc      5    -8
            4   Gal        -5    13
            5   LFuc       -5     3
            6   Gal        -5    -3
            7   LFuc       -5   -13
            8   Neu5Ac    -15    13
EDGE        7
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a1    2:3  
            5     6:b1    3:4  
            6     7:a1    3:3  
            7     8:a2    4:3  
///

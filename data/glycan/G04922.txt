ENTRY       G04922                      Glycan
NODE        9
            1   Glc6Me   25.6   0.6
            2   Glc      25.6  -6.4
            3   LRha     16.6   0.6
            4   Glc       8.6   0.6
            5   Glc       1.6   0.6
            6   LRha     -6.4   0.6
            7   LAco    -15.4   5.6
            8   Xyl     -15.4  -4.4
            9   LRha4Me -25.4   5.6
EDGE        8
            1     2:a1    1:a1 
            2     3:a1    1:3  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:a1    5:3  
            6     7:a1    6:3  
            7     8:b1    6:2  
            8     9:a1    7:2  
///

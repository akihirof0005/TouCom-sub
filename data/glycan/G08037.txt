ENTRY       G08037                      Glycan
NODE        4
            1   2,5-Anhydro-Man    16     0
            2   Man         2     0
            3   Man        -7     0
            4   Man       -16     0
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    3:2  
///

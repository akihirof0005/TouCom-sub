ENTRY       G04112                      Glycan
NODE        16
            1   Asn        33     0
            2   GlcNAc     22     0
            3   GlcNAc     12     0
            4   Man         3     0
            5   Man        -6     9
            6   Man        -6    -9
            7   GlcNAc    -15    14
            8   GlcNAc    -15     4
            9   GlcNAc    -15    -4
            10  GlcNAc    -15   -14
            11  Gal       -24    14
            12  Gal       -24     9
            13  LFuc      -24    -1
            14  Gal       -24    -4
            15  Gal       -24   -14
            16  Neu5Ac    -33   -14
EDGE        15
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:a1    8:3  
            13   14:b1    9:4  
            14   15:b1   10:4  
            15   16:a2   15:6  
///

ENTRY       G01456                      Glycan
NODE        7
            1   Xyl        30     0
            2   Xyl        20     0
            3   Xyl        10     0
            4   Xyl         0     0
            5   Xyl       -10     0
            6   Xyl       -20     0
            7   Xyl       -30     0
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:b1    5:4  
            6     7:b1    6:4  
///

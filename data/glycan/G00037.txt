ENTRY       G00037                      Glycan
NODE        5
            1   Cer        16     0
            2   Glc         7     0
            3   Gal        -1     0
            4   GlcNAc     -9     0
            5   Gal       -17     0
EDGE        4
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:3  
///

ENTRY       G01019                      Glycan
NODE        3
            1   Xyl         5     0
            2   Xyl        -5     5
            3   GlcA4Me6Me    -5    -5
EDGE        2
            1     2:b1    1:4  
            2     3:a1    1:2  
///

ENTRY       G07164                      Glycan
NODE        6
            1   GlcN     17.2   0.5
            2   P        11.2   0.5
            3   GlcN      5.2   0.5
            4   GlcN     -3.8   0.5
            5   P        -9.8   0.5
            6   LAra4N  -16.8   0.5
EDGE        5
            1     2       1:1  
            2     3:a1    2    
            3     4:b1    3:6  
            4     5       4:4  
            5     6:1     5    
///

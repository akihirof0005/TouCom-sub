ENTRY       G09946                      Glycan
NODE        3
            1   GlcNAc      0     0
            2   ManANAc   -10     0
            3   ManANAc   -20     0
EDGE        2
            1     2:1     1:3  
            2     3:1     2:4  
///

ENTRY       G00553                      Glycan
NODE        4
            1   GlcN        7     0
            2   S           0     5
            3   S           0    -5
            4   L4-en-thrHexA    -7     0
EDGE        3
            1     2       1:6  
            2     3       1:2  
            3     4:a1    1:4  
///

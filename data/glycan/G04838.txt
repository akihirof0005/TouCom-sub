ENTRY       G04838                      Glycan
NODE        10
            1   Glc        24     3
            2   Fruf       24    -3
            3   Gal        17     3
            4   Glc        17    -3
            5   Glc        10    -3
            6   Glc         3    -3
            7   Glc        -4    -3
            8   Glc       -11    -3
            9   Glc       -18    -3
            10  Glc       -25    -3
EDGE        9
            1     3:a1    1:6  
            2     1:a1    2:b2 
            3     4:b1    2:6  
            4     5:b1    4:4  
            5     6:b1    5:4  
            6     7:b1    6:4  
            7     8:b1    7:4  
            8     9:b1    8:4  
            9    10:b1    9:4  
///

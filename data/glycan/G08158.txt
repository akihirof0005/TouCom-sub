ENTRY       G08158                      Glycan
NODE        3
            1   QuiNAc   10.1  -0.1
            2   GalANAc  -0.9  -0.1
            3   LRha    -10.9  -0.1
EDGE        2
            1     2:a1    1:3  
            2     3:a1    2:4  
///

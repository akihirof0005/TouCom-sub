ENTRY       G06685                      Glycan
NODE        6
            1   GalNAc     15     2
            2   S          11    -2
            3   GlcA        5     2
            4   GalNAc     -5     2
            5   S          -9    -2
            6   GlcA      -15     2
EDGE        5
            1     2       1:4  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5       4:6  
            5     6:b1    4:3  
///

ENTRY       G05757                      Glycan
NODE        8
            1   Glc         0     0
            2   Man       -10     0
            3   Man       -20     5
            4   Xyl       -20    -5
            5   GlcNAc    -30     5
            6   LFuc      -40     5
            7   GlcA4Me   -50    10
            8   GalNAc3Me   -50     0
EDGE        7
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:b1    2:2  
            4     5:b1    3:2  
            5     6:a1    5:4  
            6     7:b1    6:4  
            7     8:a1    6:3  
///

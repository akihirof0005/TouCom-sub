ENTRY       G10387                      Glycan
NODE        13
            1   Cer        30     0
            2   Glc        22     0
            3   Gal        14     0
            4   GlcNAc      5     0
            5   Gal        -4     0
            6   GlcNAc    -13     5
            7   GlcNAc    -13    -5
            8   Gal       -22     9
            9   LFuc      -22     1
            10  Gal       -22    -1
            11  LFuc      -22    -9
            12  Gal       -30     9
            13  Gal       -30    -1
EDGE        12
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:6  
            6     7:b1    5:3  
            7     8:b1    6:4  
            8     9:a1    6:3  
            9    10:b1    7:4  
            10   11:a1    7:3  
            11   12:a1    8:3  
            12   13:a1   10:3  
///

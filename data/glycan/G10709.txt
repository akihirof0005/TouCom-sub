ENTRY       G10709                      Glycan
NODE        12
            1   Asn        30     0
            2   GlcNAc     20     0
            3   GlcNAc     10     0
            4   Man         0     0
            5   Man       -10    10
            6   GlcNAc    -10     0
            7   Man       -10   -10
            8   Man       -20    15
            9   Man       -20     5
            10  GlcNAc    -20    -5
            11  GlcNAc    -20   -15
            12  Gal       -30    -5
EDGE        11
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:b1    4:4  
            6     7:a1    4:3  
            7     8:a1    5:6  
            8     9:a1    5:3  
            9    10:b1    7:4  
            10   11:b1    7:2  
            11   12:b1   10:4  
///

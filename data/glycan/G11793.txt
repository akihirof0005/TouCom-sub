ENTRY       G11793                      Glycan
NODE        3
            1   GlcNAc     10     0
            2   LRha        0     0
            3   GlcNAc    -10     0
EDGE        2
            1     2:b1    1:4  
            2     3:a1    2:3  
///

ENTRY       G12352                      Glycan
NODE        4
            1   Glc         5     4
            2   Fruf        5    -3
            3   Fruf       -5     4
            4   Fruf       -5    -3
EDGE        3
            1     2:b2    1:a1 
            2     3:b2    1:6  
            3     4:b2    2:1  
///

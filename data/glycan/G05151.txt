ENTRY       G05151                      Glycan
NODE        9
            1   Xyl        26     2
            2   Gal        16     2
            3   S          10    -2
            4   Gal         6     2
            5   S           0    -2
            6   GlcA       -4     2
            7   GalNAc    -14     2
            8   S         -20    -2
            9   L4-en-thrHexA   -27     2
EDGE        8
            1     2:b1    1:4  
            2     3       2:6  
            3     4:b1    2:3  
            4     5       4:6  
            5     6:b1    4:3  
            6     7:b1    6:4  
            7     8       7:4  
            8     9:a1    7:3  
///

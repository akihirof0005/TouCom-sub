ENTRY       G10261                      Glycan
NODE        5
            1   Cer        20     0
            2   Glc        10     0
            3   Gal         0     0
            4   Gal       -10     0
            5   GalNAc    -20     0
EDGE        4
            1     2:1     1:1  
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:b1    4:3  
///

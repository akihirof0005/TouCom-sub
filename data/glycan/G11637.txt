ENTRY       G11637                      Glycan
NODE        13
            1   GlcN       37     5
            2   GlcN4PAra4N    27     5
            3   Kdo        17     5
            4   LAra4N     10    10
            5   Kdo        10     0
            6   Lgro-manHep     7     5
            7   Glc        -2    10
            8   Lgro-manHep    -5     0
            9   GalA      -15    -5
            10  Lgro-manHep   -17     5
            11  GalN      -23     0
            12  gro-manHep   -25   -10
            13  Lgro-manHep   -37   -10
EDGE        12
            1     2:b1    1:6  
            2     3:a2    2:6  
            3     4:b1    3:8  
            4     5:a2    3:4  
            5     6:a1    3:5  
            6     7:b1    6:4  
            7     8:a1    6:3  
            8     9:a1    8:3  
            9    10:a1    8:7  
            10   11:a1    9:4  
            11   12:a1    9:2  
            12   13:a1   12:2  
///

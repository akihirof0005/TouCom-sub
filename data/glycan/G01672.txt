ENTRY       G01672                      Glycan
NODE        6
            1   GlcN       15     0
            2   Man         5     0
            3   Man        -5     5
            4   Gal        -5    -5
            5   Man       -15     5
            6   Gal       -15    -5
EDGE        5
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:2  
            5     6:a1    4:6  
///

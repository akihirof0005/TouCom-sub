ENTRY       G05036                      Glycan
NODE        8
            1   Man         0     0
            2   Gal       -10     5
            3   Man       -10    -5
            4   Man       -20    -5
            5   Gal       -30     0
            6   Man       -30   -10
            7   Man       -40   -10
            8   Gal       -50   -10
EDGE        7
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:b1    4:4  
            6     7:b1    6:4  
            7     8:a1    7:6  
///

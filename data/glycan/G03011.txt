ENTRY       G03011                      Glycan
NODE        6
            1   GlcNAc     19     2
            2   S          14    -2
            3   GlcNAc      9     2
            4   GlcNAc     -1     2
            5   GlcNAc    -11     2
            6   GlcN      -20     2
EDGE        5
            1     2       1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:b1    5:4  
///

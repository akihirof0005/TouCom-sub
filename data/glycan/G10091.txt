ENTRY       G10091                      Glycan
NODE        11
            1   ManA6Me     0     0
            2   ManA6Me   -10     0
            3   ManA6Me   -20     0
            4   ManA6Me   -30     0
            5   ManA6Me   -40     0
            6   ManA6Me   -50     0
            7   ManA6Me   -60     0
            8   ManA6Me   -70     0
            9   ManA6Me   -80     0
            10  ManA6Me   -90     0
            11  ManA6Me  -100     0
EDGE        10
            1     2:1     1:3  
            2     3:1     2:3  
            3     4:1     3:3  
            4     5:1     4:3  
            5     6:1     5:3  
            6     7:1     6:3  
            7     8:1     7:3  
            8     9:1     8:3  
            9    10:1     9:3  
            10   11:1    10:3  
///

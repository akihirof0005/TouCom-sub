ENTRY       G11814                      Glycan
NODE        6
            1   *        29.2   0.5
            2   GlcNAc   19.2   0.5
            3   L6dTal2Ac   7.2   0.5
            4   GlcA     -3.8   0.5
            5   GlcNAc4Lac6Ac -17.8   0.5
            6   *       -29.8   0.5
EDGE        5
            1     2:b1    1    
            2     3:a1    2:3  
            3     4:b1    3:3  
            4     5:a1    4:2  
            5     6       5:3  
BRACKET     1 -26.3   2.5 -26.3  -1.5
            1  25.5  -1.4  25.5   2.6
            1 n
///

ENTRY       G00495                      Glycan
NODE        6
            1   GlcNAc     19     0
            2   Man        11     0
            3   Man         4     0
            4   GlcNAc     -4     0
            5   Gal       -12     0
            6   Neu5Ac    -20     0
EDGE        5
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:b1    3:2  
            4     5:b1    4:4  
            5     6:a2    5:3  
///

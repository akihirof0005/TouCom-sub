ENTRY       G06869                      Glycan
NODE        6
            1   Cer      16.2   0.5
            2   Glc       9.2   0.5
            3   Gal       2.2   0.5
            4   GalNAc   -5.8   0.5
            5   Neu5Gc8Me -15.8   5.5
            6   Neu5Gc8Me -15.8  -4.5
EDGE        5
            1     2:1     1:1  
            2     3:1     2:4  
            3     4:1     3:3  
            4     5:2     4:6  
            5     6:2     4:3  
///

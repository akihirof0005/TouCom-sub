ENTRY       G05529                      Glycan
NODE        6
            1   GalNAc     12     0
            2   GlcNAc      2     5
            3   Gal         2    -5
            4   Gal        -8     5
            5   Neu5Ac     -8    -5
            6   S         -13     1
EDGE        5
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a2    3:3  
            5     6       4:3  
///

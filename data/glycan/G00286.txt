ENTRY       G00286                      Glycan
NODE        6
            1   GlcNAc     15     3
            2   LFuc        5     8
            3   GlcNAc      5    -2
            4   Man        -5    -2
            5   Man       -15     3
            6   Man       -15    -7
EDGE        5
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
///

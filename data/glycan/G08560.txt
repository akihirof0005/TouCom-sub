ENTRY       G08560                      Glycan
NODE        3
            1   GlcA        0     0
            2   LAraf     -10     5
            3   Glc       -10    -5
EDGE        2
            1     2:a1    1:3  
            2     3:b1    1:2  
///

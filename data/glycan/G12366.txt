ENTRY       G12366                      Glycan
NODE        4
            1   Xyl        13     0
            2   Xyl         5     0
            3   Xyl        -3     0
            4   LAraf     -13     0
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:3  
///

ENTRY       G02009                      Glycan
NODE        4
            1   LRha       15     0
            2   GlcNAc      5     0
            3   Gal        -5     0
            4   LRha      -15     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:a1    3:3  
///

ENTRY       G08324                      Glycan
NODE        3
            1   Glc         0     0
            2   Man       -10     5
            3   Man       -10    -5
EDGE        2
            1     2:a1    1:6  
            2     3:a1    1:3  
///

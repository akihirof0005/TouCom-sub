ENTRY       G09257                      Glycan
NODE        4
            1   LIdoA       6     2
            2   S           2    -1
            3   Glc        -4     2
            4   S          -7    -1
EDGE        3
            1     2       1:2  
            2     3:a1    1:4  
            3     4       3:2  
///

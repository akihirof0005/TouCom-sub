ENTRY       G04455                      Glycan
NODE        8
            1   GlcNAc   14.1   2.5
            2   GlcNAc    4.1   7.5
            3   Gal       4.1  -2.5
            4   Kdn      -3.3  -2.5
            5   Gal      -5.9   7.5
            6   LFuc    -12.4   2.1
            7   LFuc    -12.4  -7.9
            8   LFuc    -14.9   7.5
EDGE        7
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     5:b1    2:4  
            4     4:a2    3:3  
            5     6:a1    4:5  
            6     7:a1    4:4  
            7     8:a1    5:2  
///

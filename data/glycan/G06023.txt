ENTRY       G06023                      Glycan
NODE        6
            1   Lgro-manHep    19     0
            2   Lgro-manHep     4     0
            3   Lgro-manHep    -8     5
            4   Glc        -8     0
            5   Lgro-manHep    -8    -5
            6   GlcN      -20    -5
EDGE        5
            1     2:a1    1:2  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:a1    2:3  
            5     6:a1    5:7  
///

ENTRY       G11771                      Glycan
NODE        8
            1   GlcNAc   18.8  -0.2
            2   GlcNAc    8.8  -0.2
            3   Man      -1.2  -0.2
            4   Man      -9.2   4.8
            5   Man      -9.2  -0.2
            6   Xyl      -9.5  -5.1
            7   GlcNAc  -19.2   4.8
            8   GlcNAc  -19.2  -0.2
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    3:2  
            6     7:b1    4:2  
            7     8:b1    5:2  
///

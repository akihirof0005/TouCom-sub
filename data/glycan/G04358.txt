ENTRY       G04358                      Glycan
NODE        7
            1   Kdo        21     0
            2   Lgro-manHep    11     0
            3   P           9     5
            4   Glc        -1     5
            5   Gal        -1    -5
            6   Neu5Ac    -11    -5
            7   Neu5Ac    -21    -5
EDGE        6
            1     2:a1    1:5  
            2     3       2:6  
            3     4:b1    2:4  
            4     5:b1    2:3  
            5     6:a2    5:3  
            6     7:a2    6:8  
///

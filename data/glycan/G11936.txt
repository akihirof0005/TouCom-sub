ENTRY       G11936                      Glycan
NODE        13
            1   Asn        26     3
            2   GlcNAc     17     3
            3   LFuc        8     6
            4   GlcNAc      7     0
            5   Man        -2     0
            6   Man        -9     4
            7   Man        -9    -4
            8   GlcNAc    -18     4
            9   GlcNAc    -18    -4
            10  Gal       -27     7
            11  LFuc      -27     1
            12  Gal       -27    -1
            13  LFuc      -27    -7
EDGE        12
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:2  
            9    10:b1    8:4  
            10   11:a1    8:3  
            11   12:b1    9:4  
            12   13:a1    9:3  
///

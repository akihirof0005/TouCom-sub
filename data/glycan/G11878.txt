ENTRY       G11878                      Glycan
NODE        16
            1   Asn      30.2   2.5
            2   GlcNAc   21.2   2.5
            3   LFuc     12.2   6.5
            4   GlcNAc   11.2  -1.5
            5   Man       2.2  -1.5
            6   Man      -4.8   2.5
            7   Man      -4.8  -6.5
            8   GlcNAc  -13.8   6.5
            9   GlcNAc  -13.8  -1.5
            10  GlcNAc  -13.8  -6.5
            11  Gal     -21.8   6.5
            12  Gal     -21.8  -1.5
            13  Gal     -21.8  -6.5
            14  Neu5Ac  -29.8   6.5
            15  Neu5Ac  -29.8  -1.5
            16  Neu5Ac  -29.8  -6.5
EDGE        15
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:6  
            8     9:b1    6:2  
            9    10:b1    7:2  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   13:b1   10:4  
            13   14:a2   11    
            14   15:a2   12    
            15   16:a2   13    
///

ENTRY       G06444                      Glycan
NODE        6
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     0
            4   GlcNAc    -30     0
            5   Gal       -40     5
            6   LFuc      -40    -5
EDGE        5
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:a1    4:3  
///

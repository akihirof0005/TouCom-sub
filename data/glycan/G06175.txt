ENTRY       G06175                      Glycan
NODE        6
            1   GalNAc      0     0
            2   Neu5Ac    -10     5
            3   Gal       -10    -5
            4   GlcNAc    -20    -5
            5   Gal       -30     0
            6   LFuc      -30   -10
EDGE        5
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    4:3  
///

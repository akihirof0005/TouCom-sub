ENTRY       G07852                      Glycan
NODE        4
            1   GlcA        0     0
            2   GlcNAc    -10     0
            3   Gal       -20     0
            4   Man       -30     0
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    3:2  
///

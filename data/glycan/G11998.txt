ENTRY       G11998                      Glycan
NODE        11
            1   GlcNAc     24     3
            2   LFuc       15     6
            3   GlcNAc     15    -1
            4   Man         6    -1
            5   Man        -1     2
            6   Man        -1    -5
            7   GlcNAc     -9     2
            8   GlcNAc     -9    -5
            9   Gal       -17     2
            10  Gal       -17    -5
            11  Neu5Ac    -25     2
EDGE        10
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:3  
            9    10:b1    8:3  
            10   11:a2    9:3  
///

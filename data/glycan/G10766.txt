ENTRY       G10766                      Glycan
NODE        16
            1   Asn        35     5
            2   GlcNAc     25     5
            3   LFuc       15    10
            4   GlcNAc     15     0
            5   Man         5     0
            6   Man        -5     5
            7   Man        -5    -5
            8   GlcNAc    -15     5
            9   GlcNAc    -15     0
            10  GlcNAc    -15   -10
            11  Gal       -25     5
            12  Gal       -25     0
            13  Gal       -25   -10
            14  Neu5Ac    -35     5
            15  Neu5Ac    -35     0
            16  Neu5Ac    -35   -10
EDGE        15
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    7:2  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   13:b1   10:4  
            13   14:a2   11:6  
            14   15:a2   12:3  
            15   16:a2   13:6  
///

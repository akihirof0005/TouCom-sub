ENTRY       G12486                      Glycan
NODE        3
            1   GlcA6Me     5     0
            2   Gal        -5    -4
            3   LAraf      -6     4
EDGE        2
            1     2:b1    1:2  
            2     3:a1    1:4  
///

ENTRY       G03293                      Glycan
NODE        4
            1   Man        12     0
            2   Man         2     0
            3   P          -5     0
            4   GlcNAc    -12     0
EDGE        3
            1     2:a1    1:2  
            2     3       2:6  
            3     4:a1    3    
///

ENTRY       G00387                      Glycan
NODE        3
            1   Man        10     0
            2   GlcNAc      0     0
            3   Gal       -10     0
EDGE        2
            1     2:b1    1:6  
            2     3:b1    2:4  
///

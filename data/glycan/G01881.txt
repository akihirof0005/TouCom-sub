ENTRY       G01881                      Glycan
NODE        9
            1   Cer        26    -2
            2   Glc        18    -2
            3   Man        10    -2
            4   Man         2     3
            5   Xylf        2    -7
            6   GlcNAc     -7     3
            7   LFuc      -16     3
            8   GlcA4Me   -25     8
            9   GalNAc3Me   -26    -2
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5:b1    3:2  
            5     6:b1    4:2  
            6     7:a1    6:4  
            7     8:b1    7:4  
            8     9:a1    7:3  
///

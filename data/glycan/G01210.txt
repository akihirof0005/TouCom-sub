ENTRY       G01210                      Glycan
NODE        4
            1   GlcN       15     0
            2   GlcN        5     0
            3   GlcN       -5     0
            4   GlcN      -15     0
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
///

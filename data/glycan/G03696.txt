ENTRY       G03696                      Glycan
NODE        9
            1   Glc         0     0
            2   Gal       -10     0
            3   GalNAc    -20     5
            4   Neu5Ac    -20    -5
            5   Gal       -30     5
            6   Neu5Ac    -30    -5
            7   Neu5Ac    -40     5
            8   Neu5Ac    -40    -5
            9   Neu5Ac    -50     5
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a2    2:3  
            4     5:a1    3:3  
            5     6:a2    4:8  
            6     7:a2    5:3  
            7     8:a2    6:8  
            8     9:a2    7:8  
///

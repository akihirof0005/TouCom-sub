ENTRY       G06039                      Glycan
NODE        7
            1   Asn      21.2   0.5
            2   GlcNAc   13.2   0.5
            3   GlcNAc    3.2   0.5
            4   Man      -5.8   0.5
            5   Man     -13.8   4.5
            6   Man     -13.8  -3.5
            7   GlcNAc  -21.8   4.5
EDGE        6
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:1     5:4  
///

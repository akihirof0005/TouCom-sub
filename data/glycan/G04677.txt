ENTRY       G04677                      Glycan
NODE        6
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20     5
            4   Neu5Ac    -20    -5
            5   Gal       -30     5
            6   Neu5Ac    -40     5
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a2    2:3  
            4     5:b1    3:3  
            5     6:a2    5:3  
///

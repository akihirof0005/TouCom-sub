ENTRY       G02310                      Glycan
NODE        5
            1   Gal        15     0
            2   Glc         5     0
            3   Xyl        -5     5
            4   Glc        -5    -5
            5   LRha      -15     5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    2:2  
            4     5:a1    3:4  
///

ENTRY       G11934                      Glycan
NODE        13
            1   Cer        27     1
            2   Glc        20     1
            3   Gal        13     1
            4   GlcNAc      5     1
            5   Gal        -3     1
            6   GlcNAc    -11     5
            7   GlcNAc    -11    -4
            8   Gal       -19     5
            9   Gal       -19    -4
            10  LFuc      -26     2
            11  LFuc      -26    -7
            12  GalNAc    -27     8
            13  GalNAc    -27    -1
EDGE        12
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:6  
            6     7:b1    5:3  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10:a1    8:2  
            10   12:a1    8:3  
            11   11:a1    9:2  
            12   13:a1    9:3  
///

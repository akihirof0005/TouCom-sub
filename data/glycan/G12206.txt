ENTRY       G12206                      Glycan
NODE        7
            1   GlcNAc     18     2
            2   LFuc        9     5
            3   GlcNAc      8    -1
            4   Man        -1    -1
            5   Man        -9     2
            6   Man        -9    -4
            7   GlcNAc    -19     2
EDGE        6
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
///

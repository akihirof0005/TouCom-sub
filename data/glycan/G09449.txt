ENTRY       G09449                      Glycan
NODE        4
            1   GlcA       10     0
            2   GlcN        0     0
            3   S         -10     5
            4   S         -10    -5
EDGE        3
            1     2:a1    1:4  
            2     3       2:6  
            3     4       2:2  
///

ENTRY       G06234                      Glycan
NODE        15
            1   GlcNAc     30     0
            2   GlcNAc     20     0
            3   Man        10     0
            4   Man         0     8
            5   Man         0    -8
            6   GlcNAc    -10    13
            7   GlcNAc    -10     3
            8   GlcNAc    -10    -3
            9   GlcNAc    -10   -13
            10  Gal       -20    13
            11  Gal       -20     3
            12  Gal       -20    -3
            13  Gal       -20   -13
            14  Neu5Ac    -30    13
            15  Neu5Ac    -30    -3
EDGE        14
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:6  
            6     7:b1    4:2  
            7     8:b1    5:4  
            8     9:b1    5:2  
            9    10:b1    6:4  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:a2   10:6  
            14   15:a2   12:6  
///

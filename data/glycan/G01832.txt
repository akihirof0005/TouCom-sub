ENTRY       G01832                      Glycan
NODE        8
            1   Cer        23     0
            2   Glc        16     0
            3   Gal         9     0
            4   GalNAc      1     0
            5   Gal        -8     0
            6   GalNAc    -16     5
            7   Neu5Gc    -16    -5
            8   Gal       -24     5
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7:a2    5:3  
            7     8:b1    6:3  
///

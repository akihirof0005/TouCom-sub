ENTRY       G03211                      Glycan
NODE        4
            1   Glc         0     0
            2   Gal       -10     0
            3   Gal       -20     0
            4   GalNAc    -30     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:b1    3:3  
///

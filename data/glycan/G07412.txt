ENTRY       G07412                      Glycan
NODE        4
            1   Glc2Me3Me4Me    19     0
            2   Glc2Me3Me4Me     6     0
            3   Glc3Me6Me    -6     0
            4   Glc2Me3Me4Me6Me   -19     0
EDGE        3
            1     2:b1    1:6  
            2     3:b1    2:6  
            3     4:a1    3:4  
///

ENTRY       G07650                      Glycan
NODE        3
            1   Lgro-L3,9dmanNon-2-ulop5N7NFormyl-onic  16.2   0.5
            2   FucNAc4Ac  -7.8   0.5
            3   Xyl     -16.8   0.5
EDGE        2
            1     2:b1    1:4  
            2     3:b1    2:3  
///

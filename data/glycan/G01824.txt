ENTRY       G01824                      Glycan
NODE        8
            1   GlcNAc     25     0
            2   GlcNAc     15     0
            3   Man         6     0
            4   Man        -1     4
            5   Man        -1    -4
            6   GlcNAc     -9    -4
            7   Gal       -17    -4
            8   Neu5Ac    -25    -4
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    5:2  
            6     7:b1    6:4  
            7     8:a2    7:6  
///

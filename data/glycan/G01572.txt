ENTRY       G01572                      Glycan
NODE        3
            1   GlcA        5     0
            2   LAra       -5     5
            3   Gal        -5    -5
EDGE        2
            1     2:b1    1:3  
            2     3:b1    1:2  
///

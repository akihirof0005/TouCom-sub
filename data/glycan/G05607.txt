ENTRY       G05607                      Glycan
NODE        7
            1   GalA        0     0
            2   GalA      -10     0
            3   GalA      -20     0
            4   GalA      -30     0
            5   GalA      -40     0
            6   GalA      -50     0
            7   GalA      -60     0
EDGE        6
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
            5     6:a1    5:4  
            6     7:a1    6:4  
///

ENTRY       G05006                      Glycan
NODE        9
            1   Asn        25     2
            2   GlcNAc     17     2
            3   GlcNAc      7     2
            4   Man        -2     2
            5   Man       -10    -2
            6   GlcNAc    -11     6
            7   GlcNAc    -18     2
            8   GlcNAc    -18    -6
            9   Fuc       -26     2
EDGE        8
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:3  
            5     6:b1    4:4  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8     9:b1    7:4  
///

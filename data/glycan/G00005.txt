ENTRY       G00005                      Glycan
NODE        6
            1   PP-Dol     15     1
            2   GlcNAc      8     1
            3   GlcNAc      0     1
            4   Man        -9     1
            5   Man       -16     7
            6   Man       -16    -6
EDGE        5
            1     2:a1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
///

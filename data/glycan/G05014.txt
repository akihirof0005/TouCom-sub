ENTRY       G05014                      Glycan
NODE        9
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   Gal       -20     5
            5   GlcNAc    -20     0
            6   LFuc      -20   -10
            7   LFuc      -30     5
            8   Gal       -30     0
            9   LFuc      -40     0
EDGE        8
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:b1    3:3  
            5     6:a1    3:2  
            6     7:a1    4:2  
            7     8:b1    5:3  
            8     9:a1    8:2  
///

ENTRY       G12345                      Glycan
NODE        4
            1   GalNAc      8    -1
            2   S           2     2
            3   Gal        -2    -1
            4   S          -8    -1
EDGE        3
            1     2       1:6  
            2     3:b1    1:3  
            3     4       3:6  
///

ENTRY       G09253                      Glycan
NODE        5
            1   2,5-Anhydro-Man    17     0
            2   LIdoA       2     0
            3   GlcN       -8     5
            4   S          -8    -5
            5   S         -18     5
EDGE        4
            1     2:a1    1:3  
            2     3:a1    2:4  
            3     4       2:2  
            4     5       3:2  
///

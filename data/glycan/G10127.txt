ENTRY       G10127                      Glycan
NODE        5
            1   GalNAc   15.2   0.5
            2   Gal       7.2   0.5
            3   GlcNAc   -0.8   0.5
            4   Gal      -8.8   0.5
            5   Gal     -15.8   0.5
EDGE        4
            1     2:1     1:3  
            2     3:1     2:3  
            3     4:1     3:4  
            4     5:1     4:3  
///

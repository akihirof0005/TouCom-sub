ENTRY       G00205                      Glycan
NODE        11
            1   Asn        28     0
            2   GlcNAc     20     0
            3   GlcNAc     11     0
            4   Man         3     0
            5   Man        -4     5
            6   Man        -4    -5
            7   GlcNAc    -12     5
            8   GlcNAc    -12    -5
            9   Gal       -20     5
            10  Gal       -20    -5
            11  Neu5Ac    -28     5
EDGE        10
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:a2    9:3  
///

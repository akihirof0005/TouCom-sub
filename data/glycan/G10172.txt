ENTRY       G10172                      Glycan
NODE        4
            1   LRha     13.2   0.5
            2   GalA      5.2   0.5
            3   D/LMan   -3.8   0.5
            4   GalA    -12.8   0.5
EDGE        3
            1     2:1     1:2  
            2     3:1     2:4  
            3     4:1     3:2  
///

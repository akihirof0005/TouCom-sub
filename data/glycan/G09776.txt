ENTRY       G09776                      Glycan
NODE        3
            1   GlcN        0     0
            2   LFuc      -10     5
            3   GlcN      -10    -5
EDGE        2
            1     2:a1    1:6  
            2     3:b1    1:4  
///

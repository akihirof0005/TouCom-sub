ENTRY       G12967                      Glycan
NODE        7
            1   *        27.2   0.5
            2   GlcNAc   18.3   0.5
            3   Gal      10.2   0.5
            4   LRha      2.8   0.5
            5   LRha     -5.6   0.5
            6   GlcNAc4Rlac6Ac   -17   0.5
            7   *         -27   0.5
EDGE        6
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:a1    3:2  
            4     5:a1    4:2  
            5     6:b1    5:2  
            6     7       6:3  
BRACKET     1 -23.7   1.7 -23.7  -0.4
            1  24.0  -0.6  24.0   1.5
            1 n
///

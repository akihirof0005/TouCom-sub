ENTRY       G11893                      Glycan
NODE        15
            1   GlcNAc     25     1
            2   LFuc       16     4
            3   GlcNAc     15    -2
            4   Man         6    -2
            5   Man        -1     2
            6   Man        -1    -5
            7   GlcNAc    -10     5
            8   GlcNAc    -10    -1
            9   GlcNAc    -10    -5
            10  Gal       -18     5
            11  Gal       -18    -1
            12  Gal       -18    -5
            13  Neu5Ac    -26     5
            14  Neu5Ac    -26    -1
            15  Neu5Ac    -26    -5
EDGE        14
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   13:a2   10:3  
            13   14:a2   11:6  
            14   15:a2   12:6  
///

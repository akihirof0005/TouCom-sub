ENTRY       G12878                      Glycan
NODE        8
            1   *          19     0
            2   Glc        12     0
            3   Glc         5     0
            4   Man        -2     4
            5   Glc        -2    -4
            6   *          -9    -4
            7   Man       -10     4
            8   GlcA      -19     4
EDGE        7
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    3:4  
            5     7:a1    4:6  
            6     6       5:4  
            7     8:a1    7:6  
BRACKET     1  -5.0  -2.0  -5.0  -6.0
            1  16.0  -2.0  16.0   2.0
            1 n
///

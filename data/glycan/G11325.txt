ENTRY       G11325                      Glycan
NODE        14
            1   GlcNAc     24     3
            2   GlcNAc     15     3
            3   Man         7     3
            4   Man        -1     8
            5   GlcNAc     -1     3
            6   Man        -1    -2
            7   GlcNAc     -9     8
            8   GlcNAc     -9     3
            9   GlcNAc     -9    -7
            10  Gal       -17     8
            11  Gal       -17     3
            12  Gal       -17    -7
            13  Gal       -24     8
            14  Gal       -24     3
EDGE        13
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    3:4  
            5     6:a1    3:3  
            6     7:b1    4:2  
            7     8:b1    6:4  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   13:a1   10:4  
            13   14:a1   11:4  
///

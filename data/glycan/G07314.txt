ENTRY       G07314                      Glycan
NODE        5
            1   Xyl         0     0
            2   GalA      -10     0
            3   LRha      -20     0
            4   Xyl       -30     0
            5   Xyl       -40     0
EDGE        4
            1     2:a1    1:4  
            2     3:a1    2:2  
            3     4:b1    3:3  
            4     5:b1    4:4  
///

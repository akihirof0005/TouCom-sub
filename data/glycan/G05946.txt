ENTRY       G05946                      Glycan
NODE        7
            1   GlcNAc      0     0
            2   GlcA      -10     0
            3   GlcNAc    -20     0
            4   GlcA      -30     0
            5   GlcNAc    -40     0
            6   GlcA      -50     0
            7   GlcNAc    -60     0
EDGE        6
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
///

ENTRY       G02309                      Glycan
NODE        5
            1   Glc4X      14     4
            2   Glc        14    -3
            3   Glc         6     4
            4   Glc4,6Py    -3     4
            5   Glc3Me4,6Py   -15     4
EDGE        4
            1     2:a1    1:a1 
            2     3:b1    1:6  
            3     4:b1    3:4  
            4     5:b1    4:3  
///

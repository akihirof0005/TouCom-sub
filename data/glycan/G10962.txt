ENTRY       G10962                      Glycan
NODE        15
            1   Asn        29     2
            2   GlcNAc     21     2
            3   LFuc       11     7
            4   GlcNAc     11    -3
            5   Man         2    -3
            6   Man        -6     1
            7   Man        -6    -7
            8   GlcNAc    -14     5
            9   GlcNAc    -14    -3
            10  GlcNAc    -14    -7
            11  Gal       -22     5
            12  Gal       -22    -3
            13  Gal       -22    -7
            14  Neu5Ac    -30    -3
            15  Neu5Ac    -30    -7
EDGE        14
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:6  
            8     9:b1    6:2  
            9    10:b1    7:2  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   13:b1   10:4  
            13   14:a2   12:3  
            14   15:a2   13:3  
///

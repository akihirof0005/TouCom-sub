ENTRY       G10699                      Glycan
NODE        12
            1   Asn        29     0
            2   GlcNAc     20     0
            3   GlcNAc     10     0
            4   Man         1     0
            5   Man        -6     4
            6   Man        -6    -4
            7   GlcNAc    -14     4
            8   GlcNAc    -14    -4
            9   Gal       -22     4
            10  Gal       -22    -4
            11  Neu5Gc    -30     4
            12  Neu5Gc    -30    -4
EDGE        11
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:a2    9:6  
            11   12:a2   10:6  
///

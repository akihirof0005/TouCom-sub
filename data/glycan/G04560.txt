ENTRY       G04560                      Glycan
NODE        8
            1   GalNAc      0     0
            2   Neu5Gc    -10     5
            3   Gal       -10    -5
            4   Neu5Gc    -20     5
            5   Gal       -20    -5
            6   Neu5Gc    -30     5
            7   GalNAc    -30    -5
            8   LFuc      -40    -5
EDGE        7
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:a2    2:8  
            4     5:b1    3:4  
            5     6:a2    4:8  
            6     7:b1    5:3  
            7     8:a1    7:3  
///

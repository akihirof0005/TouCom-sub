ENTRY       G11581                      Glycan
NODE        13
            1   GalNAc     30     3
            2   GlcNAc     20     8
            3   Gal        20    -2
            4   Gal        10     8
            5   GlcNAc     10     3
            6   Gal        10    -7
            7   Gal         0     3
            8   Gal         0    -7
            9   Gal       -10     3
            10  Gal       -10    -7
            11  Gal       -20     3
            12  LFuc      -20    -7
            13  LFuc      -30     3
EDGE        12
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:b1    3:6  
            5     6:b1    3:3  
            6     7:b1    5:4  
            7     8:a1    6:4  
            8     9:a1    7:4  
            9    10:a1    8:3  
            10   11:a1    9:3  
            11   12:a1   10:2  
            12   13:a1   11:2  
///

ENTRY       G10886                      Glycan
NODE        4
            1   Asn        15     0
            2   Glc         5     0
            3   Gal        -5     0
            4   Gal       -15     0
EDGE        3
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:a1    3:6  
///

ENTRY       G12450                      Glycan
NODE        3
            1   Gal         9     0
            2   Gal         0     0
            3   LFuc      -10     0
EDGE        2
            1     2:b1    1:3  
            2     3:a1    2:2  
///

ENTRY       G04044                      Glycan
NODE        12
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   GlcNAc    -30     0
            5   Gal       -40     0
            6   GlcNAc    -50     0
            7   Gal       -60     0
            8   GlcNAc    -70     0
            9   Gal       -80     0
            10  GlcNAc    -90     0
            11  Gal      -100     0
            12  Gal      -110     0
EDGE        11
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:b1    7:3  
            8     9:b1    8:4  
            9    10:b1    9:3  
            10   11:b1   10:4  
            11   12:a1   11:3  
///

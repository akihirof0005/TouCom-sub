ENTRY       G04658                      Glycan
NODE        10
            1   GlcNAc      0     0
            2   LFuc      -10     5
            3   GlcNAc    -10    -5
            4   Man       -20    -5
            5   Man       -30     0
            6   Man       -30   -10
            7   GlcNAc    -40     0
            8   GlcNAc    -40   -10
            9   Gal       -50     0
            10  Gal       -50   -10
EDGE        9
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10:b1    8:4  
///

ENTRY       G06513                      Glycan
NODE        6
            1   GlcA     18.2  -2.5
            2   GalNAc    9.2  -2.5
            3   LIdoA     0.2  -2.5
            4   GalNAc   -8.8  -2.5
            5   S       -15.3   2.3
            6   GlcA    -17.8  -2.5
EDGE        5
            1     2:b1    1    
            2     3:a1    2    
            3     4:b1    3    
            4     5       4:6  
            5     6:b1    4    
///

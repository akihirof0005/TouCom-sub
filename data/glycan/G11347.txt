ENTRY       G11347                      Glycan
NODE        10
            1   Kdo        24     3
            2   Lgro-manHep    14     3
            3   Gal         5     8
            4   Lgro-manHep     2    -2
            5   Glc        -7    -7
            6   Gal        -9    -2
            7   Lgro-manHep   -10     3
            8   GlcN      -17    -2
            9   Glc       -24     3
            10  Gal       -24    -7
EDGE        9
            1     2:a1    1:5  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:b1    4:2  
            5     6:a1    4:3  
            6     7:a1    4:7  
            7     8:a1    6:4  
            8     9:b1    8:6  
            9    10:b1    8:4  
///

ENTRY       G01825                      Glycan
NODE        12
            1   LRha       32    -4
            2   GalA       24    -4
            3   LRha       16     0
            4   GlcA       16    -8
            5   GalA        8     1
            6   LRha        0     5
            7   GlcA        0    -3
            8   GalA       -8     5
            9   LRha      -16     9
            10  GlcA      -16     1
            11  GalA      -24     9
            12  GlcA      -32     9
EDGE        11
            1     2:a1    1:2  
            2     3:a1    2:4  
            3     4:b1    2:3  
            4     5:a1    3:2  
            5     6:a1    5:4  
            6     7:b1    5:3  
            7     8:a1    6:2  
            8     9:a1    8:4  
            9    10:b1    8:3  
            10   11:a1    9:2  
            11   12:b1   11:3  
///

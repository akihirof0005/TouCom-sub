ENTRY       G12246                      Glycan
NODE        4
            1   Asn        11     0
            2   GlcNAc      4     0
            3   Gal        -3     0
            4   Neu5Ac    -11     0
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a2    3:6  
///

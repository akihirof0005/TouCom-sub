ENTRY       G05498                      Glycan
NODE        7
            1   Cer        25     0
            2   Glc        17     0
            3   Gal         9     0
            4   Gal         1     0
            5   GalNAc     -8     0
            6   Gal       -17     0
            7   LFuc      -25     0
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:3  
            6     7:a1    6:2  
///

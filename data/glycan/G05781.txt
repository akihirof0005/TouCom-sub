ENTRY       G05781                      Glycan
NODE        7
            1   P          19     3
            2   Gal        11     3
            3   GlcNAc      1     3
            4   LFuc       -9     8
            5   Gal        -9    -2
            6   GalNAc    -19     3
            7   LFuc      -19    -7
EDGE        6
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:a1    3:4  
            4     5:b1    3:3  
            5     6:a1    5:3  
            6     7:a1    5:2  
///

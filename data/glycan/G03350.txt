ENTRY       G03350                      Glycan
NODE        4
            1   Gal         0     0
            2   Glc       -10     0
            3   LRha      -20     0
            4   GalNAc    -30     0
EDGE        3
            1     2:b1    1:6  
            2     3:b1    2:4  
            3     4:a1    3:3  
///

ENTRY       G07774                      Glycan
NODE        3
            1   Fruf        5     4
            2   Glc         5    -4
            3   Glc6Ac     -5    -4
EDGE        2
            1     2:a1    1:b2 
            2     3:b1    2:2  
///

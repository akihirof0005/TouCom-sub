ENTRY       G12669                      Glycan
NODE        4
            1   *          12     0
            2   GlcA        4     0
            3   Galf       -4     0
            4   *         -12     0
EDGE        3
            1     2:b1    1    
            2     3:b1    2:4  
            3     4       3:3  
BRACKET     1  -8.0   3.0  -8.0  -2.0
            1   9.0  -2.0   9.0   3.0
            1 n
///

ENTRY       G05319                      Glycan
NODE        7
            1   GlcNAc      0     0
            2   Gal       -10     5
            3   LFuc      -10    -5
            4   GlcNAc    -20     5
            5   LFuc      -30    10
            6   Gal       -30     0
            7   Neu5Ac    -40     0
EDGE        6
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    2:3  
            4     5:a1    4:4  
            5     6:b1    4:3  
            6     7:a2    6:3  
///

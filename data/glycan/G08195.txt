ENTRY       G08195                      Glycan
NODE        4
            1   Glc        12     0
            2   Glc         5     0
            3   Man        -2     0
            4   L4-en-thrHexA   -13     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:a1    3:2  
///

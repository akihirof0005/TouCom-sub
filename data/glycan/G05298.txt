ENTRY       G05298                      Glycan
NODE        9
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30     0
            5   Man       -40     0
            6   Man       -50     0
            7   Glc       -60     0
            8   Glc       -70     0
            9   Glc       -80     0
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5:a1    4:2  
            5     6:a1    5:2  
            6     7:a1    6:2  
            7     8:a1    7:3  
            8     9:a1    8:2  
///

ENTRY       G04092                      Glycan
NODE        9
            1   Man         0     0
            2   Man       -10     5
            3   Man       -10    -5
            4   GlcNAc    -20     5
            5   GlcNAc    -20    -5
            6   Gal       -30     5
            7   Gal       -30    -5
            8   Neu5Ac    -40     5
            9   Neu5Ac    -40    -5
EDGE        8
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:b1    2:2  
            4     5:b1    3:2  
            5     6:b1    4:4  
            6     7:b1    5:4  
            7     8:a2    6:3  
            8     9:a2    7:3  
///

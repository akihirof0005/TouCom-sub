ENTRY       G06597                      Glycan
NODE        3
            1   Neu5Gc9Ac    14     0
            2   Neu5Gc9Ac     0     0
            3   Neu5Gc9Ac   -14     0
EDGE        2
            1     2:a2    1:8  
            2     3:a2    2:8  
///

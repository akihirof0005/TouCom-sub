ENTRY       G02809                      Glycan
NODE        10
            1   Glc      24.2   0.5
            2   Gal      17.2   0.5
            3   GlcNAc    9.2   0.5
            4   D/LGal    0.2   0.5
            5   GlcNAc   -8.8   5.5
            6   GlcNAc   -8.8  -4.5
            7   Gal     -16.8   5.5
            8   Gal     -16.8  -4.5
            9   LFuc    -23.8   5.5
            10  LFuc    -23.8  -4.5
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:b1    4:6  
            5     6:b1    4:3  
            6     7:b1    5:3  
            7     8:b1    6:3  
            8     9:a1    7:2  
            9    10:a1    8:2  
///

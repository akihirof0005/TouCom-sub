ENTRY       G11918                      Glycan
NODE        13
            1   GlcNAc     27     2
            2   LFuc       18     5
            3   GlcNAc     17    -1
            4   Man         8    -1
            5   Man        -1     3
            6   Man        -1    -5
            7   GlcNAc     -2    -1
            8   GlcNAc    -10     3
            9   GlcNAc    -10    -5
            10  Gal       -19     3
            11  GalNAc    -19    -5
            12  S         -25    -5
            13  Neu5Ac    -28     3
EDGE        12
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:4  
            7     8:b1    5:2  
            8     9:b1    6:2  
            9    10:b1    8:4  
            10   11:b1    9:4  
            11   13:a2   10:6  
            12   12      11:4  
///

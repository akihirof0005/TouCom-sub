ENTRY       G02168                      Glycan
NODE        3
            1   GalNAc      6     3
            2   S          -3    -2
            3   LIdoA      -7     3
EDGE        2
            1     2       1:6  
            2     3:a1    1:3  
///

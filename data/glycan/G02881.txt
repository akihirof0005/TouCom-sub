ENTRY       G02881                      Glycan
NODE        8
            1   Glc        20     0
            2   Gal        12     0
            3   GlcNAc      4     5
            4   GlcNAc      4    -5
            5   Gal        -4     5
            6   Gal        -4    -5
            7   GlcNAc    -12     5
            8   Gal       -20     5
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:4  
            5     6:b1    4:3  
            6     7:b1    5:3  
            7     8:b1    7:4  
///

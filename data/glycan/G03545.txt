ENTRY       G03545                      Glycan
NODE        3
            1   Qui       6.3   0.4
            2   Qui      -1.7  -4.6
            3   2,6daraHex3N  -5.7   5.4
EDGE        2
            1     2:b1    1:2  
            2     3:b1    1:3  
///

ENTRY       G07006                      Glycan
NODE        5
            1   Gal         0     0
            2   GlcNAc    -10     0
            3   Gal       -20     0
            4   GalNAc    -30     5
            5   LFuc      -30    -5
EDGE        4
            1     2:b1    1:6  
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5:a1    3:2  
///

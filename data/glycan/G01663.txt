ENTRY       G01663                      Glycan
NODE        6
            1   Cer      17.6  -0.1
            2   Glc       9.6  -0.1
            3   Gal       1.6  -0.1
            4   GalNAc   -7.4   4.9
            5   Neu5Ac1N  -7.4  -5.1
            6   Gal     -17.4   4.9
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
///

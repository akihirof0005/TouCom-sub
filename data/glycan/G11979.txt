ENTRY       G11979                      Glycan
NODE        12
            1   GlcNAc     22     2
            2   LFuc       12     5
            3   GlcNAc     12    -1
            4   Man         3    -1
            5   Man        -5     3
            6   Man        -5    -5
            7   GlcNAc     -6    -1
            8   GlcNAc    -14     3
            9   GlcNAc    -14    -5
            10  Gal       -23     6
            11  LFuc      -23     0
            12  Gal       -23    -5
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:4  
            7     8:b1    5:2  
            8     9:b1    6:2  
            9    10:b1    8:4  
            10   11:a1    8:3  
            11   12:b1    9:4  
///

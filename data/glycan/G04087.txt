ENTRY       G04087                      Glycan
NODE        11
            1   GalNAc     32    -2
            2   Gal        24    -2
            3   GlcNAc     15    -2
            4   Gal         7    -2
            5   GlcNAc     -1    -2
            6   S          -8     3
            7   Gal        -9    -2
            8   GlcNAc    -17    -2
            9   S         -23     3
            10  Gal       -25    -2
            11  Neu5Gc    -33    -2
EDGE        10
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6       5:6  
            6     7:b1    5:4  
            7     8:b1    7:3  
            8     9       8:6  
            9    10:b1    8:4  
            10   11:a2   10:3  
///

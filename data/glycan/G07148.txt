ENTRY       G07148                      Glycan
NODE        5
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20     0
            4   Gal       -30     5
            5   LFuc      -30    -5
EDGE        4
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a1    3:3  
///

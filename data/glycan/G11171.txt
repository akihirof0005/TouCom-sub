ENTRY       G11171                      Glycan
NODE        3
            1   Kdo         5     0
            2   Kdo        -3    -4
            3   Lgro-manHep    -6     4
EDGE        2
            1     2:a2    1:4  
            2     3:a1    1:5  
///

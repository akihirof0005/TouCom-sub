ENTRY       G01987                      Glycan
NODE        5
            1   Cer        16     0
            2   Glc         8     0
            3   Gal         0     0
            4   Gal        -8     0
            5   GlcNAc    -17     0
EDGE        4
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:b1    4:3  
///

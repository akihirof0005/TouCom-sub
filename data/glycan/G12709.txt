ENTRY       G12709                      Glycan
NODE        7
            1   *          22     0
            2   LRha       12     0
            3   Gal         4     0
            4   LRha       -4     0
            5   Man       -12     4
            6   GlcA      -12    -4
            7   *         -22    -4
EDGE        6
            1     2:a1    1    
            2     3:b1    2:2  
            3     4:a1    3:3  
            4     5:a1    4:4  
            5     6:b1    4:2  
            6     7       6:4  
BRACKET     1 -16.0   7.0 -16.0  -8.0
            1  16.0  -8.0  16.0   7.0
            1 n
///

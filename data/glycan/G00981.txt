ENTRY       G00981                      Glycan
NODE        8
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   GlcNAc    -30     0
            5   Gal       -40     0
            6   GlcNAc    -50     0
            7   LFuc      -60     5
            8   Gal       -60    -5
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:a1    6:4  
            7     8:b1    6:3  
///

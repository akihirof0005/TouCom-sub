ENTRY       G11961                      Glycan
NODE        11
            1   Asn        27     2
            2   GlcNAc     18     2
            3   LFuc        8     6
            4   GlcNAc      8    -2
            5   Man        -1    -2
            6   Man        -9     2
            7   Man        -9    -6
            8   GlcNAc    -10    -2
            9   GlcNAc    -18     2
            10  GlcNAc    -18    -6
            11  Gal       -27     2
EDGE        10
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    5:4  
            8     9:b1    6:2  
            9    10:b1    7:2  
            10   11:b1    9:4  
///

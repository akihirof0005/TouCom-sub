ENTRY       G01408                      Glycan
NODE        9
            1   Cer        34     0
            2   Glc        27     0
            3   Gal        20     0
            4   GlcNAc     11     0
            5   Gal         2     0
            6   GlcNAc     -7     0
            7   Gal       -16     0
            8   Neu5Ac    -25     0
            9   Neu5Ac    -35     0
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:a2    7:3  
            8     9:a2    8:8  
///

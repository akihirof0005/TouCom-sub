ENTRY       G08890                      Glycan
NODE        3
            1   Glc         0     0
            2   LRha      -10     0
            3   Apif      -20     0
EDGE        2
            1     2:a1    1:6  
            2     3:b1    2:2  
///

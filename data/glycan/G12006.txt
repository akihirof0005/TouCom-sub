ENTRY       G12006                      Glycan
NODE        11
            1   GlcNAc     25     0
            2   GlcNAc     15     0
            3   Man         6     0
            4   Man        -1     4
            5   Man        -1    -4
            6   GlcNAc     -9     4
            7   GlcNAc     -9    -4
            8   Gal       -17     4
            9   Gal       -17    -4
            10  Gal       -24     4
            11  Neu5Ac    -25    -4
EDGE        10
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:2  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10:a1    8:3  
            10   11:a2    9:6  
///

ENTRY       G10819                      Glycan
NODE        11
            1   Asn        30    -2
            2   GlcNAc     20    -2
            3   GlcNAc     10    -2
            4   Man         0    -2
            5   Man       -10     3
            6   Man       -10    -7
            7   Man       -20     8
            8   Man       -20    -2
            9   Man       -20    -7
            10  Man       -30    -2
            11  Man       -30    -7
EDGE        10
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:6  
            7     8:a1    5:3  
            8     9:a1    6:2  
            9    10:a1    8:2  
            10   11:a1    9:2  
///

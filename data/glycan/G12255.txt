ENTRY       G12255                      Glycan
NODE        6
            1   Ser        16    -1
            2   Xyl         8    -1
            3   Gal         0    -1
            4   Gal        -8    -1
            5   S         -13     2
            6   GlcA      -17    -1
EDGE        5
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5       4:4  
            5     6:b1    4:3  
///

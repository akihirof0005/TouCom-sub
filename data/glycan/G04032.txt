ENTRY       G04032                      Glycan
NODE        12
            1   Glc        17     0
            2   Man         9     0
            3   Man         1     5
            4   Man         1    -5
            5   GlcNAc     -8     9
            6   GlcNAc     -8     1
            7   GlcNAc     -8    -1
            8   GlcNAc     -8    -9
            9   Gal       -17     9
            10  Gal       -17     1
            11  Gal       -17    -1
            12  Gal       -17    -9
EDGE        11
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:b1    3:6  
            5     6:b1    3:2  
            6     7:b1    4:4  
            7     8:b1    4:2  
            8     9:b1    5:4  
            9    10:b1    6:4  
            10   11:b1    7:4  
            11   12:b1    8:4  
///

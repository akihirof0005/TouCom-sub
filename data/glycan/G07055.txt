ENTRY       G07055                      Glycan
NODE        5
            1   Ser/Thr    15     0
            2   Man         8     0
            3   Man         0     0
            4   Man        -8     0
            5   Man       -16     0
EDGE        4
            1     2:a1    1    
            2     3:a1    2:2  
            3     4:a1    3:3  
            4     5:a1    4:3  
///

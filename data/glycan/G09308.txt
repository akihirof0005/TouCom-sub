ENTRY       G09308                      Glycan
NODE        4
            1   P        12.2   0.5
            2   GlcNAc    5.2   0.5
            3   GlcNAc   -4.8   0.5
            4   P       -11.8   0.5
EDGE        3
            1     2:1     1    
            2     3:b1    2:6  
            3     4       3:4  
///

ENTRY       G07111                      Glycan
NODE        4
            1   LAra        0     0
            2   LRha      -10     0
            3   Gal       -20     0
            4   Glc       -30     0
EDGE        3
            1     2:a1    1:2  
            2     3:b1    2:4  
            3     4:b1    3:3  
///

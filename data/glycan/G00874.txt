ENTRY       G00874                      Glycan
NODE        15
            1   GlcNAc     25     5
            2   LFuc       15    10
            3   GlcNAc     15     0
            4   Man         5     0
            5   Man        -5     9
            6   GlcNAc     -5     0
            7   Man        -5    -8
            8   GlcNAc    -15    14
            9   GlcNAc    -15     4
            10  GlcNAc    -15    -3
            11  GlcNAc    -15   -13
            12  Gal       -25    14
            13  Gal       -25     4
            14  Gal       -25    -3
            15  Gal       -25   -13
EDGE        14
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:b1    4:4  
            6     7:a1    4:3  
            7     8:b1    5:6  
            8     9:b1    5:2  
            9    10:b1    7:4  
            10   11:b1    7:2  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:4  
            14   15:b1   11:4  
///

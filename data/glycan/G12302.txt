ENTRY       G12302                      Glycan
NODE        5
            1   P          16     0
            2   GlcNAc     10     0
            3   GlcNAc      0     0
            4   Man        -9     0
            5   Man       -17     0
EDGE        4
            1     2:a1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:3  
///

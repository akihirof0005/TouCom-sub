ENTRY       G05101                      Glycan
NODE        9
            1   GlcNAc     20     0
            2   Man        10     0
            3   Man         0     8
            4   Man         0    -8
            5   Man       -10    13
            6   Man       -10     3
            7   Man       -10    -3
            8   Man       -10   -13
            9   Man       -20    -3
EDGE        8
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
            5     6:a1    3:3  
            6     7:a1    4:6  
            7     8:a1    4:2  
            8     9:a1    7:2  
///

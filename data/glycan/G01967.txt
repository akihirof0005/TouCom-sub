ENTRY       G01967                      Glycan
NODE        6
            1   Glc        17     0
            2   Glc        10     0
            3   Glc         3     0
            4   Glc        -4     0
            5   Glc       -11     0
            6   Glc       -18     0
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
            5     6:b1    5:4  
///

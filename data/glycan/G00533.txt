ENTRY       G00533                      Glycan
NODE        8
            1   GlcNAc     17    -2
            2   GlcNAc      7    -2
            3   Man        -2    -2
            4   Man       -10     2
            5   Man       -10    -6
            6   GlcNAc    -18     6
            7   GlcNAc    -18    -2
            8   GlcNAc    -18    -6
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:6  
            6     7:b1    4:2  
            7     8:b1    5:2  
///

ENTRY       G09849                      Glycan
NODE        4
            1   2,5-Anhydro-Man    10     2
            2   S           1    -2
            3   LIdoA      -5     2
            4   S         -10    -2
EDGE        3
            1     2       1:4  
            2     3:a1    1:3  
            3     4       3:2  
///

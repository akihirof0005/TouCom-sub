ENTRY       G07468                      Glycan
NODE        5
            1   Man         0     0
            2   Man       -10     5
            3   Man       -10    -5
            4   Man       -20    10
            5   Man       -20     0
EDGE        4
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:a1    2:6  
            4     5:a1    2:2  
///

ENTRY       G02647                      Glycan
NODE        4
            1   Araf        0     0
            2   Araf      -10     0
            3   Araf      -20     0
            4   Man       -30     0
EDGE        3
            1     2:1     1:5  
            2     3:1     2:2  
            3     4:1     3:5  
///

ENTRY       G07333                      Glycan
NODE        4
            1   Man         0     0
            2   Man       -10     5
            3   Xyl       -10    -5
            4   Xyl       -20     5
EDGE        3
            1     2:a1    1:3  
            2     3:b1    1:2  
            3     4:b1    2:2  
///

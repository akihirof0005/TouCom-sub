ENTRY       G01851                      Glycan
NODE        7
            1   GalNAc     18     1
            2   GalNAc      8     1
            3   Gal        -1    -4
            4   GlcNAc     -2     5
            5   Neu5Ac    -10    -4
            6   Gal       -11     5
            7   Gal       -19     5
EDGE        6
            1     2:a1    1:3  
            2     3:b1    2:3  
            3     4:b1    2:6  
            4     5:a2    3:3  
            5     6:b1    4:4  
            6     7:b1    6:4  
///

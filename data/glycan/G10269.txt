ENTRY       G10269                      Glycan
NODE        12
            1   Cer        35     0
            2   Glc        28     0
            3   Gal        21     0
            4   GlcNAc     13     0
            5   Gal         5     0
            6   GlcNAc     -3     0
            7   Gal       -11     0
            8   GlcNAc    -19     5
            9   GlcNAc    -19    -5
            10  Gal       -27     5
            11  Gal       -27    -5
            12  Neu5Ac    -35    -5
EDGE        11
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:b1    7:6  
            8     9:b1    7:3  
            9    10:b1    8:4  
            10   11:b1    9:4  
            11   12:a2   11:3  
///

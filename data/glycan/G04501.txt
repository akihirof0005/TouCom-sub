ENTRY       G04501                      Glycan
NODE        11
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30     5
            5   Man       -30    -5
            6   Man       -40    10
            7   Man       -40     0
            8   Man       -40    -5
            9   Man       -50    10
            10  Man       -50     0
            11  Man       -50    -5
EDGE        10
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    4:2  
            7     8:a1    5:2  
            8     9:a1    6:6  
            9    10:a1    7:6  
            10   11:a1    8:2  
///

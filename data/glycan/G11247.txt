ENTRY       G11247                      Glycan
NODE        8
            1   Kdo        19     3
            2   Lgro-manHep     9     3
            3   Glc        -2     8
            4   Lgro-manHep    -3    -2
            5   Gal       -13     3
            6   Glc       -13    -7
            7   Gal       -20     8
            8   Gal       -20    -2
EDGE        7
            1     2:a1    1:5  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:b1    4:3  
            5     6:b1    4:2  
            6     7:b1    5:3  
            7     8:a1    5:2  
///

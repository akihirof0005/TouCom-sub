ENTRY       G09809                      Glycan
NODE        4
            1   D/LRha4Me  16.6   0.5
            2   D/LFuc2Me   3.6   0.5
            3   D/LRha   -7.4   0.5
            4   1dAlt-ol -17.4   0.5
EDGE        3
            1     2:1     1    
            2     3:1     2    
            3     4:6     3:1  
///

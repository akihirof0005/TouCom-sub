ENTRY       G05263                      Glycan
NODE        10
            1   2,5-Anhydro-Man    42     2
            2   GlcA       27     2
            3   GlcNAc     17     2
            4   GlcA        7     2
            5   GlcNAc     -3     2
            6   GlcA      -13     2
            7   GlcN      -23     2
            8   S         -28    -2
            9   GlcA      -33     2
            10  GlcNAc    -43     2
EDGE        9
            1     2:b1    1:3  
            2     3:a1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:4  
            5     6:b1    5:4  
            6     7:a1    6:4  
            7     8       7:2  
            8     9:b1    7:4  
            9    10:a1    9:4  
///

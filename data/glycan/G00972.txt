ENTRY       G00972                      Glycan
NODE        4
            1   GlcNAc     12     3
            2   S           5    -2
            3   L4-en-thrHexA    -3     3
            4   S         -12    -2
EDGE        3
            1     2       1:6  
            2     3:a1    1:4  
            3     4       3:2  
///

ENTRY       G07959                      Glycan
NODE        3
            1   Xyl      16.2   0.5
            2   FucNAc    6.2   0.5
            3   Lgro-L3,9dmanNon-2-ulop5N7NFormyl-onic -15.8   0.5
EDGE        2
            1     2:b1    1:4  
            2     3:a2    2:3  
///

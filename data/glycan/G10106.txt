ENTRY       G10106                      Glycan
NODE        3
            1   GalNAc    7.2   2.5
            2   S         3.2  -1.5
            3   L4-en-thrHexA  -6.8   2.5
EDGE        2
            1     2       1    
            2     3:a1    1:3  
///

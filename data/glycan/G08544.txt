ENTRY       G08544                      Glycan
NODE        4
            1   Glc       8.2   0.5
            2   Gal       0.2   0.5
            3   GalNAc   -8.8   5.5
            4   LFuc     -8.8  -4.5
EDGE        3
            1     2:b1    1:4  
            2     3:1     2:3  
            3     4:a1    2:2  
///

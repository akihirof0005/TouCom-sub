ENTRY       G06836                      Glycan
NODE        5
            1   GlcA        0     0
            2   Gal       -10     5
            3   GlcA      -10    -5
            4   Man       -20    -5
            5   Man       -30    -5
EDGE        4
            1     2:b1    1:4  
            2     3:a1    1:2  
            3     4:b1    3:4  
            4     5:b1    4:4  
///

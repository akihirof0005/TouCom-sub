ENTRY       G07993                      Glycan
NODE        3
            1   GlcNAc      0     0
            2   Glc       -10     5
            3   Gal       -10    -5
EDGE        2
            1     2:b1    1:6  
            2     3:b1    1:4  
///

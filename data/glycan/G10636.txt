ENTRY       G10636                      Glycan
NODE        4
            1   Asn        15     0
            2   GlcNAc      5     0
            3   GlcNAc     -5     0
            4   Man       -15     0
EDGE        3
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
///

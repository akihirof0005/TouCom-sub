ENTRY       G04632                      Glycan
NODE        8
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   GalNAc    -30     5
            5   Neu5Ac    -30    -5
            6   Gal       -40     5
            7   Gal       -50     5
            8   Gal       -60     5
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
            6     7:a1    6:4  
            7     8:b1    7:4  
///

ENTRY       G12402                      Glycan
NODE        4
            1   GlcA        9  -0.3
            2   Man        -1  -0.3
            3   LAco      -10   3.7
            4   GlcA      -10  -4.3
EDGE        3
            1     2:a1    1:4  
            2     3:1     2:4  
            3     4:b1    2:2  
///

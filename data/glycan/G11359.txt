ENTRY       G11359                      Glycan
NODE        7
            1   *          24     0
            2   GlcNAc     14     0
            3   GalNAc      4     0
            4   Glc        -6     5
            5   GalNAc     -6    -5
            6   Glc       -16    -5
            7   *         -25    -5
EDGE        6
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:a1    3:4  
            4     5:a1    3:3  
            5     6:a1    5:4  
            6     7       6:4  
BRACKET     1 -21.0   7.0 -21.0  -7.0
            1  20.0  -7.0  20.0   7.0
            1 n
///

ENTRY       G08276                      Glycan
NODE        3
            1   Glc      10.6  -0.1
            2   Gal2X     2.6  -0.1
            3   Neu5Ac4Ac7Ac8Ac9Ac -11.4  -0.1
EDGE        2
            1     2:b1    1:4  
            2     3:a2    2:3  
///

ENTRY       G01359                      Glycan
NODE        4
            1   GlcNAc     15     0
            2   Kdo         5     0
            3   Kdo        -5     0
            4   Kdo       -15     0
EDGE        3
            1     2:a2    1:6  
            2     3:a2    2:4  
            3     4:a2    3:8  
///

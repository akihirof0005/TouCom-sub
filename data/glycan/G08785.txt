ENTRY       G08785                      Glycan
NODE        3
            1   LFuc        0     0
            2   Gal       -10     0
            3   Gal       -20     0
EDGE        2
            1     2:b1    1:4  
            2     3:a1    2:4  
///

ENTRY       G03209                      Glycan
NODE        4
            1   1,6-Anhydro-Glc     0     0
            2   Gal       -13     0
            3   GlcNAc    -23     0
            4   Gal       -33     0
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    3:4  
///

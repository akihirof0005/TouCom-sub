ENTRY       G00954                      Glycan
NODE        7
            1   GlcNAc     14     0
            2   LFuc        4     5
            3   GlcNAc      4     0
            4   LFuc        4    -5
            5   Man        -6     0
            6   Man       -14     5
            7   Man       -14    -5
EDGE        6
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:a1    1:3  
            4     5:b1    3:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
///

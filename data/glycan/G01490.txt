ENTRY       G01490                      Glycan
NODE        4
            1   GalNAc      9     0
            2   Gal         0    -5
            3   Neu5Ac     -1     5
            4   LFuc      -10    -5
EDGE        3
            1     2:b1    1:3  
            2     3:a2    1:6  
            3     4:a1    2:2  
///

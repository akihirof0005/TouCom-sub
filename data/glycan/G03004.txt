ENTRY       G03004                      Glycan
NODE        8
            1   Glc        23     0
            2   Glc        15     0
            3   Glc         7     0
            4   Glc        -1     0
            5   Glc        -9     0
            6   Glc       -17     0
            7   Glc       -24     5
            8   Glc       -24    -5
EDGE        7
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
            5     6:a1    5:4  
            6     7:a1    6:6  
            7     8:a1    6:4  
///

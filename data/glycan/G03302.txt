ENTRY       G03302                      Glycan
NODE        4
            1   Gal6Me     20     0
            2   L3,6-Anhydro-Gal     7     0
            3   Gal6Me     -7     0
            4   L3,6-Anhydro-Gal   -20     0
EDGE        3
            1     2:a1    1:3  
            2     3:b1    2:4  
            3     4:a1    3:3  
///

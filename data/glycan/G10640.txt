ENTRY       G10640                      Glycan
NODE        4
            1   Asn         9     0
            2   GlcNAc      1     0
            3   LFuc       -8     4
            4   GlcNAc     -9    -4
EDGE        3
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
///

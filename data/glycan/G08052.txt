ENTRY       G08052                      Glycan
NODE        4
            1   Gal         9     0
            2   Glc         0     5
            3   LRha        0    -5
            4   Glc       -10    -5
EDGE        3
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:b1    3:4  
///

ENTRY       G01947                      Glycan
NODE        6
            1   Cer      17.4     0
            2   Glc       9.4     0
            3   Gal       1.4     0
            4   GalNAc   -7.6     5
            5   L3daraHep-2-ulop5NAc-onic -13.6    -5
            6   Gal     -16.6     5
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b2    3:3  
            5     6:b1    4:3  
///

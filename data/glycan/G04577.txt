ENTRY       G04577                      Glycan
NODE        10
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30     5
            5   Man       -30     0
            6   Xyl       -30    -5
            7   GlcNAc    -40     5
            8   GlcNAc    -40     0
            9   LFuc      -50    10
            10  Gal       -50     0
EDGE        9
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    3:2  
            6     7:b1    4:2  
            7     8:b1    5:2  
            8     9:a1    7:6  
            9    10:b1    7:4  
///

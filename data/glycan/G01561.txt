ENTRY       G01561                      Glycan
NODE        4
            1   Glc        15     0
            2   Gal         5     0
            3   Glc        -5     0
            4   Glc       -15     0
EDGE        3
            1     2:a1    1:2  
            2     3:b1    2:6  
            3     4:b1    3:6  
///

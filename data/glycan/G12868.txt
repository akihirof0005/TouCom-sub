ENTRY       G12868                      Glycan
NODE        4
            1   Xyl        11     0
            2   Xyl         4     0
            3   GlcAMe     -4     0
            4   Hex       -12     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:2  
            3     4:a1    3:2  
///

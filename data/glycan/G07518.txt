ENTRY       G07518                      Glycan
NODE        3
            1   Lgro-7dgalHep   6.2   0.5
            2   Gal      -6.8   5.5
            3   LFuc     -6.8  -4.5
EDGE        2
            1     2:a1    1:3  
            2     3:a1    1:2  
///

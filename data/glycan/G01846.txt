ENTRY       G01846                      Glycan
NODE        7
            1   GalNAc      0     0
            2   Neu5Gc    -10     5
            3   Gal       -10    -5
            4   Gal       -20    -5
            5   GalNAc    -30    -5
            6   GalNAc    -40     0
            7   Neu5Gc    -40   -10
EDGE        6
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7:a2    5:3  
///

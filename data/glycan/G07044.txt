ENTRY       G07044                      Glycan
NODE        4
            1   4dlyxHex  10.2   0.5
            2   Rha       0.2   5.5
            3   Man       0.2  -4.5
            4   GlcNAc   -9.8  -4.5
EDGE        3
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:b1    3:2  
///

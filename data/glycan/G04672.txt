ENTRY       G04672                      Glycan
NODE        10
            1   Asn        27     0
            2   GlcNAc     19     0
            3   GlcNAc      9     0
            4   Man         0     0
            5   Man        -8     4
            6   Man        -8    -4
            7   GlcNAc    -17     4
            8   GlcNAc    -17    -4
            9   GalNAc    -27     4
            10  GalNAc    -27    -4
EDGE        9
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
///

ENTRY       G10697                      Glycan
NODE        11
            1   Asn        25     2
            2   GlcNAc     17     2
            3   GlcNAc      7     2
            4   Man        -2     2
            5   Man       -10     7
            6   Man       -10    -3
            7   GlcNAc    -11     2
            8   Man       -18     7
            9   GlcNAc    -18     1
            10  GlcNAc    -18    -7
            11  Gal       -26     1
EDGE        10
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:4  
            7     8:a1    5:3  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    9:4  
///

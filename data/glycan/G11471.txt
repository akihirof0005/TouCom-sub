ENTRY       G11471                      Glycan
NODE        7
            1   *          20    -2
            2   Glc        12    -2
            3   Gal         2    -2
            4   GalNAc     -8     2
            5   GalNAc6Ac    -9    -6
            6   Glc       -18     6
            7   *         -21    -2
EDGE        6
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    3:4  
            4     5:b1    3:3  
            5     6:a1    4:6  
            6     7       4:3  
BRACKET     1 -19.0   9.0 -19.0 -11.0
            1  18.0 -11.0  18.0   9.0
            1 n
///

ENTRY       G06686                      Glycan
NODE        5
            1   Kdo        18     0
            2   Lgro-manHep     8     0
            3   Glc        -4     5
            4   Lgro-manHep    -7    -5
            5   GlcA      -19    -5
EDGE        4
            1     2:a1    1:5  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:a1    4:2  
///

ENTRY       G02246                      Glycan
NODE        8
            1   GalA       21     0
            2   LRha       14     0
            3   GalA        7     0
            4   LRha        0     0
            5   Gal        -7     5
            6   GalA       -7    -5
            7   LRha      -14    -5
            8   Gal       -21    -5
EDGE        7
            1     2:a1    1:4  
            2     3:a1    2:2  
            3     4:a1    3:4  
            4     5:b1    4:4  
            5     6:a1    4:2  
            6     7:a1    6:4  
            7     8:b1    7:4  
///

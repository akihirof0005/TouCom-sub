ENTRY       G04414                      Glycan
NODE        9
            1   GlcNAc      0     0
            2   LFuc      -10     5
            3   GlcNAc    -10    -5
            4   Man       -20    -5
            5   Man       -30     0
            6   Man       -30   -10
            7   GlcNAc    -40   -10
            8   Gal       -50   -10
            9   Neu5Ac    -60   -10
EDGE        8
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    6:2  
            7     8:b1    7:3  
            8     9:a2    8:3  
///

ENTRY       G01926                      Glycan
NODE        7
            1   Glc        22     0
            2   Gal        14     0
            3   GlcNAc      5     0
            4   Gal        -4     5
            5   LFuc       -4    -5
            6   GlcNAc    -13     5
            7   Gal       -22     5
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a1    3:3  
            5     6:b1    4:3  
            6     7:b1    6:3  
///

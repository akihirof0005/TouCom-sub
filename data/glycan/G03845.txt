ENTRY       G03845                      Glycan
NODE        16
            1   Asn        29     4
            2   GlcNAc     21     4
            3   GlcNAc     11     4
            4   Man         2     4
            5   Man        -6     9
            6   Man        -6    -1
            7   GlcNAc    -14     9
            8   GlcNAc    -14     3
            9   GlcNAc    -14    -5
            10  Gal       -22     9
            11  Gal       -22     3
            12  Neu5Ac    -22    -1
            13  Gal       -22    -9
            14  Neu5Ac    -30     9
            15  Neu5Ac    -30     3
            16  Neu5Ac    -30    -9
EDGE        15
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:4  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:a2    9:6  
            12   13:b1    9:4  
            13   14:a2   10:3  
            14   15:a2   11:3  
            15   16:a2   13:6  
///

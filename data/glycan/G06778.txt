ENTRY       G06778                      Glycan
NODE        4
            1   GalNAc      0     0
            2   Gal       -10     5
            3   Gal       -10    -5
            4   GlcNAc    -20    -5
EDGE        3
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    3:3  
///

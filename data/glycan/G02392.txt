ENTRY       G02392                      Glycan
NODE        4
            1   2,6daraHex3Me  18.3   0.4
            2   Dig       6.3   0.4
            3   2,6daraHex3Me  -5.7   0.4
            4   Dig     -17.7   0.4
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
///

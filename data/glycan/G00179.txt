ENTRY       G00179                      Glycan
NODE        9
            1   Asn        20     0
            2   GlcNAc     13     0
            3   GlcNAc      3     0
            4   Man        -5     0
            5   Man       -13     5
            6   Man       -13     0
            7   Xyl       -13    -5
            8   GlcNAc    -21     5
            9   GlcNAc    -21     0
EDGE        8
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:2  
            7     8:b1    5:2  
            8     9:b1    6:2  
///

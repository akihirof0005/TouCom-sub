ENTRY       G06908                      Glycan
NODE        6
            1   Glc      17.6  -0.4
            2   Man       9.6   4.6
            3   Glc       9.6  -5.4
            4   Man       0.6   4.6
            5   LRha     -8.4   4.6
            6   Abe     -17.4   4.6
EDGE        5
            1     2:a1    1:3  
            2     3:a1    1:2  
            3     4:a1    2:2  
            4     5:a1    4:2  
            5     6:a1    5:3  
///

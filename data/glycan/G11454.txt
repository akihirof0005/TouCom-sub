ENTRY       G11454                      Glycan
NODE        6
            1   *          21     0
            2   Man        14     0
            3   GlcNAc      4     0
            4   Man        -6     0
            5   Man       -15     0
            6   *         -21     0
EDGE        5
            1     2:a1    1    
            2     3:a1    2:6  
            3     4:b1    3:3  
            4     5:a1    4:2  
            5     6       5:2  
BRACKET     1 -18.0   2.0 -18.0  -2.0
            1  18.0  -2.0  18.0   2.0
            1 n
///

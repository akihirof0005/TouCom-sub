ENTRY       G07113                      Glycan
NODE        5
            1   Gal      13.6  -0.4
            2   LRha      4.6  -0.4
            3   Man      -4.4  -0.4
            4   Tyv     -13.4   4.6
            5   Gal     -13.4  -5.4
EDGE        4
            1     2:a1    1:3  
            2     3:a1    2:4  
            3     4:a1    3:3  
            4     5:a1    3:2  
///

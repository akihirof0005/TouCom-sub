ENTRY       G03644                      Glycan
NODE        4
            1   Gal         0     0
            2   Glc       -10     0
            3   Glc       -20     0
            4   Glc       -30     0
EDGE        3
            1     2:a1    1:6  
            2     3:b1    2:6  
            3     4:b1    3:4  
///

ENTRY       G02966                      Glycan
NODE        7
            1   Cer        21     2
            2   Glc        14     2
            3   Gal         7     2
            4   GlcNAc     -1     2
            5   Gal        -9     2
            6   GlcA6Me   -17     2
            7   S         -22    -1
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7       6:3  
///

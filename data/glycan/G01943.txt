ENTRY       G01943                      Glycan
NODE        9
            1   GlcN       15     0
            2   S          10     5
            3   S          10    -5
            4   LIdoA       5     0
            5   S           0    -5
            6   GlcN       -5     0
            7   S         -10     5
            8   S         -10    -5
            9   GlcA      -16     0
EDGE        8
            1     2       1:6  
            2     3       1:2  
            3     4:a1    1:4  
            4     5       4:2  
            5     6:a1    4:4  
            6     7       6:6  
            7     8       6:2  
            8     9:b1    6:4  
///

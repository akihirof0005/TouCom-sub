ENTRY       G05045                      Glycan
NODE        7
            1   Cer      24.9   0.5
            2   Glc      16.9   0.5
            3   Gal       8.9   0.5
            4   Neu5Ac   -0.1   0.5
            5   Gal      -9.1   0.5
            6   Gal     -17.1   0.5
            7   D/LAra  -25.1   0.5
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:6  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:1     6    
///

ENTRY       G01941                      Glycan
NODE        7
            1   GlcNAc   13.7  -1.6
            2   S         7.2     2
            3   Gal       4.7  -1.6
            4   S        -0.8     2
            5   GlcNAc   -4.3  -1.6
            6   S       -10.8     2
            7   Gal     -13.3  -1.6
EDGE        6
            1     2       1:6  
            2     3:b1    1:4  
            3     4       3:6  
            4     5:b1    3:3  
            5     6       5:6  
            6     7:b1    5:4  
///

ENTRY       G00281                      Glycan
NODE        11
            1   GlcNAc     22     3
            2   GlcNAc     12     3
            3   Man         3     3
            4   Man        -5     8
            5   Man        -5    -2
            6   GlcNAc    -14     8
            7   GlcNAc    -14     3
            8   GlcNAc    -14    -7
            9   Gal       -23     8
            10  Gal       -23     3
            11  Gal       -23    -7
EDGE        10
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    7:4  
            10   11:b1    8:4  
///

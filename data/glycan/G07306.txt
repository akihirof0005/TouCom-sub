ENTRY       G07306                      Glycan
NODE        5
            1   P          12     0
            2   P           5     0
            3   Gal        -2     0
            4   Glc       -12     5
            5   Man       -12    -5
EDGE        4
            1     2       1    
            2     3:a1    2    
            3     4:b1    3:4  
            4     5:a1    3:3  
///

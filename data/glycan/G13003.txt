ENTRY       G13003                      Glycan
NODE        6
            1   *          21     0
            2   GalNAc     12     0
            3   GalNAc      3     0
            4   Gal      -5.1   0.1
            5   LFuc    -12.6   0.1
            6   *         -21     0
EDGE        5
            1     2:b1    1    
            2     3:a1    2:3  
            3     4:b1    3:3  
            4     5:a1    4:2  
            5     6       5:4  
BRACKET     1 -17.0   2.0 -17.0  -2.0
            1  17.0  -2.0  17.0   2.0
            1 n
///

ENTRY       G07885                      Glycan
NODE        4
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     0
            4   GlcNAc    -30     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    3:6  
///

ENTRY       G02957                      Glycan
NODE        7
            1   Asn        22     0
            2   GlcNAc     14     0
            3   GlcNAc      4     0
            4   Man        -5     0
            5   Man       -13     4
            6   Man       -13    -4
            7   GlcNAc    -22     4
EDGE        6
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:4  
///

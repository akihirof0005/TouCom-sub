ENTRY       G00792                      Glycan
NODE        6
            1   Cer        22     3
            2   Glc        12     3
            3   Neu5Gc      2     3
            4   Glc        -8     3
            5   Neu5Gc    -18     3
            6   S         -23    -2
EDGE        5
            1     2:b1    1:1  
            2     3:a2    2:6  
            3     4:a1    3:8  
            4     5:a2    4:6  
            5     6       5:8  
///

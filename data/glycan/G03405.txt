ENTRY       G03405                      Glycan
NODE        4
            1   GlcNAc      0     0
            2   GlcNAc    -10     5
            3   LFuc      -10    -5
            4   Man       -20     5
EDGE        3
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    2:4  
///

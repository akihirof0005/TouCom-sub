ENTRY       G03658                      Glycan
NODE        15
            1   Asn        29     3
            2   GlcNAc     20     3
            3   GlcNAc     10     3
            4   Man         0     3
            5   Man       -10    13
            6   Man       -10    -7
            7   GlcNAc    -20    18
            8   GlcNAc    -20     8
            9   GlcNAc    -20    -2
            10  GlcNAc    -20   -12
            11  Gal       -30    18
            12  Gal       -30     8
            13  Gal       -30    -2
            14  Gal       -30    -7
            15  LFuc      -30   -17
EDGE        14
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:4  
            14   15:a1   10:3  
///

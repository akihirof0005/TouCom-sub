ENTRY       G05748                      Glycan
NODE        7
            1   Glc         0     0
            2   Gal       -10     0
            3   GalNAc    -20     5
            4   LFuc      -20    -5
            5   Gal       -30     5
            6   GalNAc    -40     5
            7   Gal3Me    -50     5
EDGE        6
            1     2:b1    1:4  
            2     3:a1    2:3  
            3     4:a1    2:2  
            4     5:b1    3:3  
            5     6:a1    5:3  
            6     7:b1    6:3  
///

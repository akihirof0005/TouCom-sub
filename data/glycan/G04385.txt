ENTRY       G04385                      Glycan
NODE        9
            1   Ser/Thr    27     0
            2   GalNAc     20     0
            3   Gal        12     0
            4   GlcNAc      4     0
            5   Gal        -4     0
            6   GlcNAc    -12     0
            7   LFuc      -20     4
            8   Gal       -20    -4
            9   Neu5Ac    -28    -4
EDGE        8
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:b1    5:3  
            6     7:a1    6:4  
            7     8:b1    6:3  
            8     9:a2    8:3  
///

ENTRY       G11126                      Glycan
NODE        5
            1   GalNAc     15     3
            2   Gal         5     3
            3   LFuc       -4    -2
            4   GalNAc     -5     3
            5   Gal       -15     3
EDGE        4
            1     2:b1    1:3  
            2     3:a1    2:2  
            3     4:a1    2:3  
            4     5:a1    4:3  
///

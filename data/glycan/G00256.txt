ENTRY       G00256                      Glycan
NODE        4
            1   GlcNAc     10     0
            2   Gal         0     5
            3   LFuc        0    -5
            4   LFuc      -10     5
EDGE        3
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:a1    2:2  
///

ENTRY       G03280                      Glycan
NODE        3
            1   Man         0     0
            2   Man       -10     5
            3   Man2Me    -10    -5
EDGE        2
            1     2:a1    1:6  
            2     3:a1    1:3  
///

ENTRY       G10818                      Glycan
NODE        9
            1   Asn        20    -2
            2   GlcNAc     13    -2
            3   GlcNAc      4    -2
            4   Man        -5    -2
            5   Man       -12     3
            6   Man       -12    -7
            7   GlcNAc    -20     8
            8   GlcNAc    -20    -2
            9   GlcNAc    -20    -7
EDGE        8
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:2  
///

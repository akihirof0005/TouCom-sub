ENTRY       G12155                      Glycan
NODE        7
            1   Asn        22     0
            2   GlcNAc     13     0
            3   GlcNAc      3     0
            4   Man        -6     0
            5   Man       -13     3
            6   Man       -13    -3
            7   GlcNAc    -22    -3
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    6:2  
///

ENTRY       G10883                      Glycan
NODE        7
            1   Asn        21     0
            2   GlcNAc     13     0
            3   GlcNAc      3     0
            4   Man        -6     0
            5   Man       -14     4
            6   Man       -14    -4
            7   GlcNAc    -22     4
EDGE        6
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
///

ENTRY       G02353                      Glycan
NODE        12
            1   GlcNAc1N    21     2
            2   LFuc       12     6
            3   GlcNAc     12    -2
            4   Man         5    -2
            5   Man        -1     2
            6   Man        -1    -6
            7   GlcNAc     -8     2
            8   GlcNAc     -8    -6
            9   Gal       -15     2
            10  Gal       -15    -6
            11  Neu5Ac    -22     2
            12  Neu5Ac    -22    -6
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:a2    9:6  
            11   12:a2   10:6  
///

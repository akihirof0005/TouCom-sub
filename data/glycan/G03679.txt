ENTRY       G03679                      Glycan
NODE        16
            1   GlcNAc     25     0
            2   Man        15     0
            3   Man         5     8
            4   Man         5    -8
            5   GlcNAc     -5    13
            6   GlcNAc     -5     3
            7   GlcNAc     -5    -3
            8   GlcNAc     -5   -13
            9   Gal       -15    13
            10  Gal       -15     3
            11  Gal       -15    -3
            12  Gal       -15   -13
            13  GlcNAc    -25    13
            14  GlcNAc    -25     3
            15  GlcNAc    -25    -3
            16  GlcNAc    -25   -13
EDGE        15
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:b1    3:6  
            5     6:b1    3:2  
            6     7:b1    4:4  
            7     8:b1    4:2  
            8     9:b1    5:4  
            9    10:b1    6:4  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1    9:3  
            13   14:b1   10:3  
            14   15:b1   11:3  
            15   16:b1   12:3  
///

ENTRY       G09814                      Glycan
NODE        3
            1   L1,5daraHex4Me  13.6  -0.4
            2   LCym      0.6  -0.4
            3   L2,6dlyxHex3Me4Ac -13.4  -0.4
EDGE        2
            1     2:b1    1:3  
            2     3:a1    2:4  
///

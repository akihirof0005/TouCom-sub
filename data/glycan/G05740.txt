ENTRY       G05740                      Glycan
NODE        8
            1   Glc        20     0
            2   Glc        10     5
            3   Glc        10    -5
            4   Glc         0     5
            5   GlcA        0    -5
            6   Glc4,6Py   -10     5
            7   L4-en-thrHexA   -12    -5
            8   Gal4,6Py   -21     5
EDGE        7
            1     2:b1    1:6  
            2     3:b1    1:4  
            3     4:b1    2:4  
            4     5:b1    3:4  
            5     6:b1    4:4  
            6     7:b1    5:4  
            7     8:b1    6:3  
///

ENTRY       G12109                      Glycan
NODE        7
            1   Ino-P      21     0
            2   GlcN       14     0
            3   Man         7     0
            4   Man         0     0
            5   Galf       -7     0
            6   Gal       -14     0
            7   Gal       -22     0
EDGE        6
            1     2:1     1:6  
            2     3:a1    2:4  
            3     4:a1    3:3  
            4     5:b1    4:3  
            5     6:a1    5:3  
            6     7:a1    6:6  
///

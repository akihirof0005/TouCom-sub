ENTRY       G04388                      Glycan
NODE        11
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   GlcNAc    -30     0
            5   Gal       -40     0
            6   GlcNAc    -50     5
            7   GalNAc    -50     0
            8   LFuc      -50    -5
            9   Gal       -60     5
            10  GalNAc    -70    10
            11  LFuc      -70     0
EDGE        10
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:b1    5:6  
            6     7:a1    5:3  
            7     8:a1    5:2  
            8     9:b1    6:4  
            9    10:a1    9:3  
            10   11:a1    9:2  
///

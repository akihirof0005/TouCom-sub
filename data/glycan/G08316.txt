ENTRY       G08316                      Glycan
NODE        3
            1   Araf        0     0
            2   GlcNAc    -10     0
            3   GlcNAc    -20     0
EDGE        2
            1     2:b1    1:3  
            2     3:b1    2:4  
///

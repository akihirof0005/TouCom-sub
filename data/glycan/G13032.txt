ENTRY       G13032                      Glycan
NODE        7
            1   Gal      18.1  -1.8
            2   GlcNAc    9.1  -1.8
            3   S         2.1   2.2
            4   Gal       0.1  -1.8
            5   GlcNAc   -8.9  -1.8
            6   S       -15.9   2.2
            7   Gal     -17.9  -1.8
EDGE        6
            1     2:b1    1:3  
            2     3       2:6  
            3     4:b1    2:4  
            4     5:b1    4:3  
            5     6       5:6  
            6     7:b1    5:4  
///

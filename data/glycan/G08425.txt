ENTRY       G08425                      Glycan
NODE        3
            1   Glc       5.2   0.5
            2   Gal      -3.8  -4.5
            3   D/LApif  -4.8   5.5
EDGE        2
            1     2:b1    1:3  
            2     3:1     1:4  
///

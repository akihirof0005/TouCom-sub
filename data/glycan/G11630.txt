ENTRY       G11630                      Glycan
NODE        5
            1   Fruf       10     7
            2   Glc6Ac     10    -1
            3   Glc4Ac6Ac     0     4
            4   Glc         0    -6
            5   Glc       -10     4
EDGE        4
            1     1:a2    2:a1 
            2     3:b1    2:3  
            3     4:b1    2:2  
            4     5:b1    3:3  
///

ENTRY       G06448                      Glycan
NODE        8
            1   GalNAc     16     0
            2   S          11    -4
            3   GlcA        6     0
            4   S           2    -4
            5   GalNAc     -4     0
            6   S          -9     4
            7   S          -9    -4
            8   L4-en-thrHexA   -16     0
EDGE        7
            1     2       1:6  
            2     3:b1    1:3  
            3     4       3:2  
            4     5:b1    3:4  
            5     6       5:6  
            6     7       5:4  
            7     8:a1    5:3  
///

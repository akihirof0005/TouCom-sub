ENTRY       G01681                      Glycan
NODE        5
            1   Glc        13     0
            2   Man         3     0
            3   Gal        -7     5
            4   Galf       -7    -5
            5   S         -13     1
EDGE        4
            1     2:a1    1:2  
            2     3:b1    2:6  
            3     4:a1    2:3  
            4     5       3:3  
///

ENTRY       G06038                      Glycan
NODE        8
            1   Kdo      21.2   0.5
            2   Hep      13.2   0.5
            3   Glc       5.2   5.5
            4   Hep       5.2  -4.5
            5   Gal      -2.8   5.5
            6   GlcNAc   -3.8  -4.5
            7   GlcNAc  -11.8   5.5
            8   Gal     -20.8   5.5
EDGE        7
            1     2:a1    1:5  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:b1    3:4  
            5     6:a1    4:2  
            6     7:b1    5:3  
            7     8:b1    7:4  
///

ENTRY       G05554                      Glycan
NODE        7
            1   Glc        18    -2
            2   GlcNAc      9    -2
            3   Man         0    -2
            4   Man        -9     3
            5   Man        -9    -7
            6   Man       -18     8
            7   Man       -18    -2
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    4:3  
///

ENTRY       G02138                      Glycan
NODE        3
            1   LAra      5.4     0
            2   Glc      -4.6     5
            3   L6dTal   -4.6    -5
EDGE        2
            1     2:b1    1:3  
            2     3:a1    1:2  
///

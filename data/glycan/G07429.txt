ENTRY       G07429                      Glycan
NODE        7
            1   Gul-onic    22     2
            2   GlcNAc     12     2
            3   LIdoA       2     2
            4   GlcN       -8     2
            5   S         -13    -2
            6   LIdoA     -18     2
            7   S         -23    -2
EDGE        6
            1     2:a1    1:3  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5       4:2  
            5     6:a1    4:4  
            6     7       6:2  
///

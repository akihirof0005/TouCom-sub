ENTRY       G02072                      Glycan
NODE        5
            1   2,5-Anhydro-Man    16     2
            2   GlcA        3     2
            3   GlcNAc     -6     2
            4   S         -13    -2
            5   LIdoA     -16     2
EDGE        4
            1     2:b1    1:3  
            2     3:a1    2:4  
            3     4       3:6  
            4     5:a1    3:4  
///

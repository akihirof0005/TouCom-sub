ENTRY       G07379                      Glycan
NODE        3
            1   2,5-Anhydro-Man  14.2   0.5
            2   Gal       1.2   0.5
            3   ery-3dgalNon-2-ulop-onic -14.8   0.5
EDGE        2
            1     2:b1    1:3  
            2     3:a2    2:6  
///

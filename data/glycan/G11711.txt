ENTRY       G11711                      Glycan
NODE        5
            1   Asn        19     0
            2   GlcNAc     11     0
            3   GlcNAc      0     0
            4   Man       -10     0
            5   Man       -19     0
EDGE        4
            1     2       1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
///

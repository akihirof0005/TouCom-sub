ENTRY       G01699                      Glycan
NODE        5
            1   Man        15     0
            2   Gal         5     5
            3   Man         5    -5
            4   Man        -5    -5
            5   Man       -15    -5
EDGE        4
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
///

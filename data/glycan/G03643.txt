ENTRY       G03643                      Glycan
NODE        5
            1   Glc         0     0
            2   Gal       -10     0
            3   Glc       -20     0
            4   Glc       -30     0
            5   Glc       -40     0
EDGE        4
            1     2:a1    1:6  
            2     3:a1    2:6  
            3     4:b1    3:6  
            4     5:b1    4:4  
///

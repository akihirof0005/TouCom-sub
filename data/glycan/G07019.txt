ENTRY       G07019                      Glycan
NODE        5
            1   Kdo        15     0
            2   Lgro-manHep     4     0
            3   GlcN       -7     5
            4   GlcA       -7    -5
            5   Glc       -15    -5
EDGE        4
            1     2:a1    1:5  
            2     3:a1    2:7  
            3     4:b1    2:2  
            4     5:b1    4:3  
///

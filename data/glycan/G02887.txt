ENTRY       G02887                      Glycan
NODE        8
            1   Asn        24     0
            2   GlcNAc     16     0
            3   LFuc        8     5
            4   GlcNAc      7    -5
            5   Man        -1    -5
            6   Man        -8    -5
            7   GlcNAc    -16    -5
            8   Gal       -24    -5
EDGE        7
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:3  
            6     7:b1    6:2  
            7     8:b1    7:4  
///

ENTRY       G07900                      Glycan
NODE        3
            1   GlcNAc      9     0
            2   Gal         0     0
            3   LFuc3Me   -10     0
EDGE        2
            1     2:b1    1:4  
            2     3:a1    2:2  
///

ENTRY       G11290                      Glycan
NODE        8
            1   Ino-P      20     3
            2   Man        10     3
            3   Gal         0     3
            4   Gal       -10     8
            5   Gal       -10    -2
            6   LFuc      -20     3
            7   Gal       -20    -2
            8   LFuc      -20    -7
EDGE        7
            1     2:a1    1:4  
            2     3:b1    2:6  
            3     4:a1    3:3  
            4     5:a1    3:2  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:a1    5:2  
///

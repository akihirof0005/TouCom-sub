ENTRY       G10045                      Glycan
NODE        3
            1   PP-Dol    9.2   0.5
            2   GlcNAc    0.2   0.5
            3   GlcNAc   -9.8   0.5
EDGE        2
            1     2:1     1    
            2     3:1     2:4  
///

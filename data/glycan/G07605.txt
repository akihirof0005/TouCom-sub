ENTRY       G07605                      Glycan
NODE        3
            1   GlcNAc      0     0
            2   Gal       -10     0
            3   Gal       -20     0
EDGE        2
            1     2:b1    1:3  
            2     3:a1    2:4  
///

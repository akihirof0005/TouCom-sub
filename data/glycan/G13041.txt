ENTRY       G13041                      Glycan
NODE        7
            1   GalNAc   16.9  -2.3
            2   S         9.9   1.7
            3   GlcA      7.9  -2.3
            4   GalNAc   -1.1  -2.3
            5   S        -8.1   1.7
            6   LIdoA   -10.1  -2.3
            7   S       -17.1   1.7
EDGE        6
            1     2       1:4  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5       4:4  
            5     6:a1    4:3  
            6     7       6:2  
///

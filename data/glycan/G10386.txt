ENTRY       G10386                      Glycan
NODE        12
            1   Cer        30    -2
            2   Glc        22    -2
            3   Gal        14    -2
            4   GlcNAc      5    -2
            5   Gal        -4    -2
            6   GlcNAc    -13     3
            7   GlcNAc    -13    -7
            8   Gal       -22     8
            9   LFuc      -22    -2
            10  Gal       -22    -7
            11  Gal       -31     8
            12  Gal       -31    -7
EDGE        11
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:6  
            6     7:b1    5:3  
            7     8:b1    6:4  
            8     9:a1    6:3  
            9    10:b1    7:4  
            10   11:a1    8:3  
            11   12:a1   10:3  
///

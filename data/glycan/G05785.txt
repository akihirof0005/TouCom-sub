ENTRY       G05785                      Glycan
NODE        5
            1   Gal         0     0
            2   Gal       -10     0
            3   GalNAc    -20     0
            4   GalNAc    -30     5
            5   Neu5Gc    -30    -5
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:a2    3:3  
///

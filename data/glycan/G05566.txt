ENTRY       G05566                      Glycan
NODE        8
            1   GlcNAc     18    -2
            2   Man         9    -2
            3   Man         0     3
            4   Man         0    -7
            5   Man        -9     8
            6   Man        -9    -2
            7   Man       -18     8
            8   Man       -18    -2
EDGE        7
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
            5     6:a1    3:3  
            6     7:a1    5:2  
            7     8:a1    6:2  
///

ENTRY       G10321                      Glycan
NODE        5
            1   Cer        17     2
            2   Glc         7     2
            3   Neu5Ac     -3     2
            4   Neu5Ac    -13     2
            5   S         -18    -2
EDGE        4
            1     2:1     1:1  
            2     3:a2    2:3  
            3     4:a2    3:8  
            4     5       4:8  
///

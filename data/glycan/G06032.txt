ENTRY       G06032                      Glycan
NODE        6
            1   Kdo        17     0
            2   Lgro-manHep     7     0
            3   Glc        -5     5
            4   Lgro-manHep    -5    -5
            5   Gal       -15     5
            6   GlcNAc    -17    -5
EDGE        5
            1     2:a1    1:5  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:a1    3:4  
            5     6:a1    4:2  
///

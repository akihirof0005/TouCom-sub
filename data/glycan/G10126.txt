ENTRY       G10126                      Glycan
NODE        5
            1   Araf        0     0
            2   Araf      -10     0
            3   Araf      -20     0
            4   Man       -30     0
            5   Man       -40     0
EDGE        4
            1     2:1     1:3  
            2     3:1     2:2  
            3     4:1     3:5  
            4     5:1     4:2  
///

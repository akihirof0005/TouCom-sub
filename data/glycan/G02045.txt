ENTRY       G02045                      Glycan
NODE        5
            1   Glc        20     0
            2   Glc        10     0
            3   Glc         0     0
            4   Glc       -10     0
            5   Glc       -20     0
EDGE        4
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:6  
///

ENTRY       G09407                      Glycan
NODE        3
            1   Glc       5.2   3.5
            2   D/LGal-ol   5.2  -3.5
            3   Gal      -4.8   3.5
EDGE        2
            1     2:1     1:a1 
            2     3:a1    1:6  
///

ENTRY       G12810                      Glycan
NODE        6
            1   *          17     0
            2   Glc         9     0
            3   Gal         1     0
            4   GlcNAc     -7    -4
            5   Qui4NMal    -8     4
            6   *         -18    -4
EDGE        5
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:2  
            4     5:b1    3:6  
            5     6       4:3  
BRACKET     1 -13.0   7.0 -13.0  -8.0
            1  12.0  -8.0  12.0   7.0
            1 n
///

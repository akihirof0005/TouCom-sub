ENTRY       G03139                      Glycan
NODE        5
            1   1,6-Anhydro-Glc     0     0
            2   Glc       -13     0
            3   Glc       -23     0
            4   Glc       -33     0
            5   Glc       -43     0
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
///

ENTRY       G07058                      Glycan
NODE        5
            1   GlcNAc     14     0
            2   Man4Me      4     0
            3   Man        -5     5
            4   Man        -5    -5
            5   GlcNAc    -14    -5
EDGE        4
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:b1    4:2  
///

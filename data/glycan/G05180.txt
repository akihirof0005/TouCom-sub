ENTRY       G05180                      Glycan
NODE        8
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   Gal       -20     5
            5   GlcNAc    -20    -5
            6   LFuc      -30     5
            7   Gal       -30    -5
            8   GlcNAc    -40    -5
EDGE        7
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:b1    3:3  
            5     6:a1    4:2  
            6     7:b1    5:4  
            7     8:b1    7:3  
///

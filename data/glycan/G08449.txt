ENTRY       G08449                      Glycan
NODE        4
            1   GalA        0     0
            2   LRha      -10     0
            3   Xyl       -20     0
            4   Xyl       -30     0
EDGE        3
            1     2:a1    1:2  
            2     3:b1    2:3  
            3     4:b1    3:4  
///

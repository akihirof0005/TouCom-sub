ENTRY       G07217                      Glycan
NODE        4
            1   GlcNAc      9     0
            2   LFuc        0     5
            3   Gal4N       0    -5
            4   LFuc       -9    -5
EDGE        3
            1     2:a1    1:4  
            2     3:b1    1:3  
            3     4:a1    3:2  
///

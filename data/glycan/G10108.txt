ENTRY       G10108                      Glycan
NODE        12
            1   Ser        45     0
            2   Xyl        35     0
            3   Gal        25     0
            4   Gal        15     0
            5   GlcA        5     0
            6   GlcNAc     -5     0
            7   LIdoA     -15     0
            8   GlcN      -25     0
            9   S         -29     4
            10  S         -29    -4
            11  L4-en-thrHexA   -38     0
            12  S         -45    -4
EDGE        11
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:3  
            5     6:a1    5:4  
            6     7:a1    6:4  
            7     8:a1    7:4  
            8     9       8:6  
            9    10       8:2  
            10   11:a1    8:4  
            11   12      11:2  
///

ENTRY       G12480                      Glycan
NODE        3
            1   Man       3.6  -0.4
            2   Tyv      -4.4   4.6
            3   Gal      -4.4  -5.4
EDGE        2
            1     2:a1    1:3  
            2     3:a1    1:2  
///

ENTRY       G00325                      Glycan
NODE        3
            1   Glc         5     0
            2   Glc        -5     5
            3   LRha       -5    -5
EDGE        2
            1     2:b1    1:4  
            2     3:a1    1:2  
///

ENTRY       G05610                      Glycan
NODE        8
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   Gal       -20    10
            5   LFuc      -20     0
            6   GlcNAc    -30    10
            7   Gal       -40    10
            8   LFuc      -50    10
EDGE        7
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a1    2:3  
            5     6:b1    4:3  
            6     7:b1    6:3  
            7     8:a1    7:2  
///

ENTRY       G04567                      Glycan
NODE        10
            1   GlcNAc      0     0
            2   Man       -10     0
            3   Man       -20     5
            4   Man       -20    -5
            5   GlcNAc    -30     5
            6   GlcNAc    -30    -5
            7   Gal       -40     5
            8   Gal       -40    -5
            9   GlcNAc    -50    -5
            10  Gal       -60    -5
EDGE        9
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:b1    3:2  
            5     6:b1    4:2  
            6     7:b1    5:4  
            7     8:b1    6:4  
            8     9:b1    8:3  
            9    10:b1    9:4  
///

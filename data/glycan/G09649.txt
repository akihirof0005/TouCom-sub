ENTRY       G09649                      Glycan
NODE        3
            1   Gal         0     0
            2   GlcNAc    -10     5
            3   GlcNAc    -10    -5
EDGE        2
            1     2:b1    1:4  
            2     3:b1    1:3  
///

ENTRY       G12133                      Glycan
NODE        8
            1   Asn        25     0
            2   GlcNAc     16     0
            3   Man         7    -3
            4   LFuc        6     3
            5   Man         0    -3
            6   GlcNAc     -8    -3
            7   GlcNAc    -17    -3
            8   Gal       -25    -3
EDGE        7
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    2:6  
            4     5:a1    3:6  
            5     6:b1    5:2  
            6     7:b1    6:4  
            7     8:b1    7:4  
///

ENTRY       G01867                      Glycan
NODE        7
            1   GalNAc      0     0
            2   Neu5Gc    -10     5
            3   Gal       -10    -5
            4   Neu5Gc    -20     5
            5   Gal       -20    -5
            6   GalNAc    -30    -5
            7   LFuc      -40    -5
EDGE        6
            1     2:a2    1:6  
            2     3:b1    1:3  
            3     4:a2    2:8  
            4     5:b1    3:4  
            5     6:b1    5:3  
            6     7:a1    6:3  
///

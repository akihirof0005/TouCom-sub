ENTRY       G02746                      Glycan
NODE        9
            1   Cer        23     0
            2   Glc        17     0
            3   Gal        11     0
            4   GalNAc      4     5
            5   Neu5Gc      4    -5
            6   Gal        -3     5
            7   GlcNAc    -10     5
            8   Gal       -17     5
            9   Gal       -23     5
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
            5     6:b1    4:3  
            6     7:b1    6:3  
            7     8:b1    7:4  
            8     9:a1    8:3  
///

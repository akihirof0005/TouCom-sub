ENTRY       G07389                      Glycan
NODE        4
            1   Glc         8     0
            2   Gal         0     5
            3   Gal         0    -5
            4   Glc        -8    -5
EDGE        3
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:a1    3:2  
///

ENTRY       G08992                      Glycan
NODE        4
            1   GlcN        6     0
            2   S           2     4
            3   S           2    -4
            4   L4-en-thrHexA    -6     0
EDGE        3
            1     2       1:6  
            2     3       1:2  
            3     4:b1    1:4  
///

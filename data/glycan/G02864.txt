ENTRY       G02864                      Glycan
NODE        9
            1   GlcNAc     32     0
            2   GlcA       24     0
            3   GlcNAc     16     0
            4   GlcA        8     0
            5   GlcNAc      0     0
            6   GlcA       -8     0
            7   GlcNAc    -16     0
            8   GlcA      -24     0
            9   GlcNAc    -32     0
EDGE        8
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:b1    7:3  
            8     9:b1    8:4  
///

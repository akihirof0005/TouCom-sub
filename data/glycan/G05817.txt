ENTRY       G05817                      Glycan
NODE        4
            1   Glc1N       0     0
            2   Gal       -10     0
            3   Neu5Ac    -20     0
            4   Neu5Ac    -30     0
EDGE        3
            1     2:b1    1:4  
            2     3:a2    2:3  
            3     4:a2    3:8  
///

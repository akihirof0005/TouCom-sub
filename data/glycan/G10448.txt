ENTRY       G10448                      Glycan
NODE        5
            1   GalNAc   14.1   0.5
            2   Gal       4.1   0.5
            3   Kdn      -5.1   0.5
            4   LFuc      -15   5.2
            5   LFuc      -15  -4.8
EDGE        4
            1     2:b1    1:3  
            2     3:a2    2:3  
            3     4:a1    3:5  
            4     5:a1    3:4  
///

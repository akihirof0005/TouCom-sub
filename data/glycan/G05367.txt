ENTRY       G05367                      Glycan
NODE        5
            1   Glc      14.6  -0.1
            2   Gal       4.6  -0.1
            3   GalNAc   -5.4   4.9
            4   Neu5Ac1N  -5.4  -5.1
            5   Gal     -15.4   4.9
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a2    2:3  
            4     5:b1    3:3  
///

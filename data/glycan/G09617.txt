ENTRY       G09617                      Glycan
NODE        3
            1   Glc         0     0
            2   LFuc      -10     5
            3   Glc       -10    -5
EDGE        2
            1     2:b1    1:6  
            2     3:a1    1:3  
///

ENTRY       G06507                      Glycan
NODE        7
            1   D/LApi   22.2   0.5
            2   LRha     13.2   0.5
            3   LAceAf    4.2   0.5
            4   Gal      -4.8   0.5
            5   LAra    -12.8   5.5
            6   LFuc2Me -13.8  -4.5
            7   LRha    -21.8   5.5
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:a1    3:2  
            4     5:a1    4:4  
            5     6:a1    4:2  
            6     7:a1    5:2  
///

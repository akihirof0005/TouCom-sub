ENTRY       G07895                      Glycan
NODE        5
            1   2,5-Anhydro-Man    17     3
            2   GlcA        3     3
            3   GlcNAc     -7     3
            4   S         -14    -2
            5   LIdoA     -18     3
EDGE        4
            1     2:b1    1:4  
            2     3:a1    2:4  
            3     4       3:6  
            4     5:a1    3:4  
///

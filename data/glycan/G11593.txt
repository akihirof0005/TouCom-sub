ENTRY       G11593                      Glycan
NODE        6
            1   GalNAc     15     0
            2   Gal         5     0
            3   GlcNAc     -5     5
            4   Gal        -5    -5
            5   LFuc      -15     5
            6   GalNAc    -15    -5
EDGE        5
            1     2:b1    1:3  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:a1    3:3  
            5     6:b1    4:3  
///

ENTRY       G01218                      Glycan
NODE        4
            1   Qui      10.3   0.4
            2   Qui       2.3   5.4
            3   Qui       2.3  -4.6
            4   2,6daraHex3NAc -10.7   5.4
EDGE        3
            1     2:b1    1:4  
            2     3:b1    1:2  
            3     4:b1    2:3  
///

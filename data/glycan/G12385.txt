ENTRY       G12385                      Glycan
NODE        4
            1   Glc        13     0
            2   Gal         4     0
            3   Gal        -4     0
            4   Gal       -13     0
EDGE        3
            1     2:b1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:3  
///

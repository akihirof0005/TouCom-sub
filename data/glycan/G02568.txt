ENTRY       G02568                      Glycan
NODE        3
            1   GlcNAc      5     2
            2   S          -1    -1
            3   GlcA       -6     2
EDGE        2
            1     2       1:6  
            2     3:b1    1:4  
///

ENTRY       G11036                      Glycan
NODE        10
            1   Asn        21    -2
            2   GlcNAc     12    -2
            3   Man         3    -2
            4   Man        -5     3
            5   Man        -5    -7
            6   Man       -13     8
            7   Man       -13    -2
            8   Man       -13    -7
            9   Man       -21     8
            10  Man       -21    -7
EDGE        9
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    4:3  
            7     8:a1    5:2  
            8     9:a1    6:2  
            9    10:a1    8:2  
///

ENTRY       G08753                      Glycan
NODE        4
            1   2,5-Anhydro-Man     9     2
            2   S           0    -2
            3   Gal        -6     2
            4   S         -10    -2
EDGE        3
            1     2       1:6  
            2     3:b1    1:4  
            3     4       3:6  
///

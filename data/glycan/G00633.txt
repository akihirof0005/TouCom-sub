ENTRY       G00633                      Glycan
NODE        3
            1   Man         0     0
            2   GlcNAc    -10     5
            3   GlcNAc    -10    -5
EDGE        2
            1     2:b1    1:4  
            2     3:b1    1:2  
///

ENTRY       G09282                      Glycan
NODE        3
            1   Man         0     0
            2   Glc       -10     0
            3   Man       -20     0
EDGE        2
            1     2:b1    1:2  
            2     3:a1    2:4  
///

ENTRY       G00698                      Glycan
NODE        6
            1   GlcNAc     14     0
            2   GlcNAc      4     0
            3   Man        -5     0
            4   Man       -14     5
            5   Man       -14    -5
            6   GlcNAc    -15     0
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    3:4  
///

ENTRY       G01508                      Glycan
NODE        3
            1   Man         5     0
            2   Man        -5     5
            3   Xyl        -5    -5
EDGE        2
            1     2:a1    1:6  
            2     3:b1    1:2  
///

ENTRY       G10687                      Glycan
NODE        10
            1   Asn        25     3
            2   GlcNAc     15     3
            3   LFuc        5     8
            4   GlcNAc      5    -2
            5   Man        -5    -2
            6   Man       -15     3
            7   GlcNAc    -15    -2
            8   Man       -15    -7
            9   GlcNAc    -25     3
            10  GlcNAc    -25    -7
EDGE        9
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:b1    5:4  
            7     8:a1    5:3  
            8     9:b1    6:2  
            9    10:b1    8:2  
///

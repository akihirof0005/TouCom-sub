ENTRY       G01088                      Glycan
NODE        12
            1   GlcNAc     25     3
            2   LFuc       16     7
            3   GlcNAc     16    -1
            4   Man         7    -1
            5   Man        -1     4
            6   Man        -1    -6
            7   GlcNAc     -2    -1
            8   GlcNAc    -10     4
            9   GlcNAc    -10    -6
            10  Gal       -18     4
            11  Gal       -18    -6
            12  Neu5Ac    -26    -6
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:4  
            7     8:b1    5:2  
            8     9:b1    6:2  
            9    10:b1    8:4  
            10   11:b1    9:4  
            11   12:a2   11:6  
///

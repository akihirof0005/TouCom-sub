ENTRY       G05570                      Glycan
NODE        4
            1   Neu5Gc      0     0
            2   Neu5Gc    -10     0
            3   Neu5Gc    -20     0
            4   Neu5Gc    -30     0
EDGE        3
            1     2:a2    1:8  
            2     3:a2    2:8  
            3     4:a2    3:8  
///

ENTRY       G07994                      Glycan
NODE        4
            1   Kdo         0     0
            2   Man       -10     0
            3   GalA      -20     5
            4   Gal4Ac    -20    -5
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:4  
///

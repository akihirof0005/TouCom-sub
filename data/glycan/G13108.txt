ENTRY       G13108                      Glycan
NODE        4
            1   Ino-P     7.8   2.5
            2   Man       0.3  -3.1
            3   Man      -2.1   2.5
            4   acyl     -7.1  -3.1
EDGE        3
            1     2:a1    1:2  
            2     3:a1    1:6  
            3     4       2:6  
///

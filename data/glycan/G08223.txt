ENTRY       G08223                      Glycan
NODE        3
            1   2,6daraHex3Me   9.2   0.5
            2   Glc      -2.8   0.5
            3   Glc      -9.8   0.5
EDGE        2
            1     2:b1    1:4  
            2     3:b1    2:4  
///

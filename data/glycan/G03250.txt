ENTRY       G03250                      Glycan
NODE        4
            1   LRha       10     0
            2   Glc         0     5
            3   LRha        0    -5
            4   GlcNAc    -10    -5
EDGE        3
            1     2:a1    1:3  
            2     3:a1    1:2  
            3     4:b1    3:2  
///

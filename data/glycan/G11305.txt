ENTRY       G11305                      Glycan
NODE        6
            1   Ino-P      20     0
            2   Man        10     0
            3   Gal         0     0
            4   Gal       -10     5
            5   LFuc      -10    -5
            6   Man       -20     5
EDGE        5
            1     2:a1    1:4  
            2     3:b1    2:6  
            3     4:a1    3:3  
            4     5:a1    3:2  
            5     6:a1    4:6  
///

ENTRY       G07125                      Glycan
NODE        4
            1   Gal         0     0
            2   Man       -10     0
            3   Man       -20     0
            4   LRha      -30     0
EDGE        3
            1     2:a1    1:3  
            2     3:a1    2:2  
            3     4:a1    3:2  
///

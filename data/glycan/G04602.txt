ENTRY       G04602                      Glycan
NODE        9
            1   GalA        0     0
            2   GalA      -10     0
            3   LRha      -20     0
            4   GalA      -30     0
            5   LRha      -40     0
            6   GalA      -50     0
            7   GalA      -60     0
            8   GalA      -70     0
            9   GalA      -80     0
EDGE        8
            1     2:a1    1:2  
            2     3:a1    2:4  
            3     4:a1    3:2  
            4     5:a1    4:4  
            5     6:a1    5:2  
            6     7:a1    6:4  
            7     8:a1    7:4  
            8     9:a1    8:4  
///

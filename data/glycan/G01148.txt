ENTRY       G01148                      Glycan
NODE        8
            1   Cer        25     0
            2   Glc        15     0
            3   Gal         5     0
            4   GlcNAc     -5     0
            5   Neu5Ac    -15     5
            6   LFuc      -15     0
            7   Gal       -15    -5
            8   Neu5Ac    -25    -5
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:a2    4:6  
            5     6:a1    4:4  
            6     7:b1    4:3  
            7     8:a2    7:3  
///

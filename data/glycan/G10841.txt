ENTRY       G10841                      Glycan
NODE        13
            1   Asn        24     0
            2   GlcNAc     16     0
            3   GlcNAc      7     0
            4   Man        -1     0
            5   Man        -9     6
            6   Man        -9    -6
            7   Man       -17    10
            8   Man       -17     2
            9   Man       -17    -2
            10  Man       -17   -10
            11  Man       -25    10
            12  Man       -25    -2
            13  Man       -25   -10
EDGE        12
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:6  
            7     8:a1    5:3  
            8     9:a1    6:6  
            9    10:a1    6:2  
            10   11:a1    7:2  
            11   12:a1    9:2  
            12   13:a1   10:2  
///

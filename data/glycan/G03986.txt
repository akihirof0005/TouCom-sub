ENTRY       G03986                      Glycan
NODE        19
            1   P          57     3
            2   GlcN       50     3
            3   GlcN       42     3
            4   P          38    -1
            5   Kdo        34     3
            6   P          30    -1
            7   Lgro-manHep    24     3
            8   Glc        14     8
            9   Glc        14     3
            10  Lgro-manHep    11    -2
            11  Sedf        6     3
            12  Lgro-manHep    -2    -2
            13  GlcN      -12     3
            14  Gal       -12    -7
            15  Sed       -20    -7
            16  QuiN      -29    -7
            17  GalA      -38    -7
            18  Neu       -46    -7
            19  L4-en-thrHexA   -57    -7
EDGE        18
            1     2:a1    1    
            2     3:b1    2:6  
            3     4       3:4  
            4     5:a2    3:6  
            5     6       5:4  
            6     7:a1    5:5  
            7     8:a1    7:6  
            8     9:b1    7:4  
            9    10:a1    7:3  
            10   11:b2    9:6  
            11   12:a1   10:2  
            12   13:a1   12:7  
            13   14:b1   12:3  
            14   15:b2   14:3  
            15   16:b1   15:4  
            16   17:b1   16:3  
            17   18:a2   17:3  
            18   19:b1   18:4  
///

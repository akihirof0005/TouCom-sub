ENTRY       G08540                      Glycan
NODE        3
            1   P        10.1  -0.1
            2   GalA6N    0.1  -0.1
            3   QuiNAc  -10.9  -0.1
EDGE        2
            1     2:a1    1    
            2     3:b1    2:2  
///

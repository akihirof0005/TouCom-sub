ENTRY       G00368                      Glycan
NODE        14
            1   GlcNAc     20     5
            2   LFuc       11    10
            3   GlcNAc     11     0
            4   Man         2     0
            5   Man        -5     6
            6   Man        -5    -6
            7   GlcNAc    -13    10
            8   GlcNAc    -13     2
            9   GlcNAc    -13    -2
            10  GlcNAc    -13   -10
            11  Gal       -21    10
            12  Gal       -21     2
            13  Gal       -21    -2
            14  Gal       -21   -10
EDGE        13
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    7:4  
            11   12:b1    8:4  
            12   13:b1    9:4  
            13   14:b1   10:4  
///

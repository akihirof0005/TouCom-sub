ENTRY       G12022                      Glycan
NODE        11
            1   Asn        24     2
            2   GlcNAc     16     2
            3   LFuc        7     5
            4   GlcNAc      7    -1
            5   Man        -1    -1
            6   Man        -8     2
            7   Man        -8    -4
            8   GlcNAc    -16     5
            9   GlcNAc    -16    -1
            10  GalNAc    -25     5
            11  GalNAc    -25    -1
EDGE        10
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:6  
            8     9:b1    6:2  
            9    10:b1    8:4  
            10   11:b1    9:4  
///

ENTRY       G02749                      Glycan
NODE        11
            1   GlcNAc     20     3
            2   GlcNAc     11     3
            3   Man         3     3
            4   Man        -4     8
            5   Man        -4    -2
            6   GlcNAc    -12     8
            7   GlcNAc    -12     3
            8   GlcNAc    -12    -7
            9   Gal       -20     3
            10  Gal       -20    -7
            11  Gal6N     -21     8
EDGE        10
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:2  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8    11:b1    6:4  
            9     9:b1    7:4  
            10   10:b1    8:4  
///

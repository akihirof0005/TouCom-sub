ENTRY       G07227                      Glycan
NODE        5
            1   GlcNAc6Gc     0     0
            2   GlcNAc6Gc   -10     0
            3   GlcNAc6Gc   -20     0
            4   GlcNAc6Gc   -30     0
            5   GlcNAc6Gc   -40     0
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
///

ENTRY       G08112                      Glycan
NODE        7
            1   Xyl        22     2
            2   Gal        14     2
            3   Gal         7     2
            4   S           3    -1
            5   GlcA       -1     2
            6   GalNAc    -10     2
            7   L4-en-thrHexA   -22     2
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4       3:4  
            4     5:b1    3:3  
            5     6:b1    5:4  
            6     7:a1    6:3  
///

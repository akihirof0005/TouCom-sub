ENTRY       G00803                      Glycan
NODE        5
            1   Fruf       15     4
            2   Glc        15    -3
            3   Fruf        5    -3
            4   Fruf       -5    -3
            5   Fruf      -15    -3
EDGE        4
            1     2:a1    1:b2 
            2     3:b2    2:6  
            3     4:b2    3:1  
            4     5:b2    4:1  
///

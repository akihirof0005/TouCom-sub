ENTRY       G03971                      Glycan
NODE        11
            1   GalNAc      0     0
            2   Gal       -10     0
            3   GlcNAc    -20     5
            4   GlcNAc    -20    -5
            5   Gal       -30     5
            6   Gal       -30    -5
            7   GlcNAc    -40    -5
            8   Gal       -50    -5
            9   GlcNAc    -60    -5
            10  Gal       -70    -5
            11  Neu5Gc    -80    -5
EDGE        10
            1     2:b1    1:3  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:4  
            5     6:b1    4:4  
            6     7:b1    6:3  
            7     8:b1    7:4  
            8     9:b1    8:3  
            9    10:b1    9:4  
            10   11:a2   10:3  
///

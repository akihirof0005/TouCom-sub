ENTRY       G05627                      Glycan
NODE        8
            1   Kdo        25     0
            2   Lgro-Hep    15     0
            3   Glc         5     5
            4   Lgro-Hep     5    -5
            5   Gal        -5     5
            6   GlcNAc     -5    -5
            7   GlcNAc    -15     5
            8   Gal       -25     5
EDGE        7
            1     2:a1    1:5  
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:b1    3:4  
            5     6:a1    4:2  
            6     7:b1    5:3  
            7     8:b1    7:4  
///

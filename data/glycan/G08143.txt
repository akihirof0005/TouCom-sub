ENTRY       G08143                      Glycan
NODE        4
            1   LRha       14     0
            2   GlcNAc      4     0
            3   Gal        -5     0
            4   LRha      -14     0
EDGE        3
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:a1    3:2  
///

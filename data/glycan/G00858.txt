ENTRY       G00858                      Glycan
NODE        10
            1   GlcNAc     18     4
            2   LFuc        9     8
            3   GlcNAc      8     0
            4   Man        -1     0
            5   Man       -10     6
            6   Man       -10    -5
            7   GlcNAc    -19    10
            8   GlcNAc    -19     2
            9   GlcNAc    -19    -1
            10  GlcNAc    -19    -9
EDGE        9
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    6:2  
///

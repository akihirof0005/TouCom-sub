ENTRY       G04614                      Glycan
NODE        11
            1   GlcNAc     18     5
            2   LFuc        8    10
            3   GlcNAc      8     0
            4   Man        -1     0
            5   Man       -10     6
            6   Man       -10    -6
            7   GlcNAc    -11     0
            8   GlcNAc    -18    10
            9   GlcNAc    -18     2
            10  GlcNAc    -18    -2
            11  GlcNAc    -18   -10
EDGE        10
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:4  
            7     8:b1    5:6  
            8     9:b1    5:2  
            9    10:b1    6:4  
            10   11:b1    6:2  
///

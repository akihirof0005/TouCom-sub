ENTRY       G09311                      Glycan
NODE        3
            1   Glc         0     0
            2   Gal       -10     0
            3   Glc       -20     0
EDGE        2
            1     2:a1    1:3  
            2     3:a1    2:2  
///

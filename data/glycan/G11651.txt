ENTRY       G11651                      Glycan
NODE        7
            1   *        22.4  -0.1
            2   Rha      15.4  -0.1
            3   Rha       7.4  -0.1
            4   Rha      -0.6  -0.1
            5   Rha      -8.6  -0.1
            6   Rha     -16.6  -0.1
            7   *       -22.6  -0.1
EDGE        6
            1     2:a1    1    
            2     3:a1    2:2  
            3     4:a1    3:2  
            4     5:a1    4:3  
            5     6:b1    5:3  
            6     7       6:2  
BRACKET     1 -20.6   2.1 -20.6  -2.8
            1  19.8  -2.8  19.8   2.1
            1 n
///

ENTRY       G01175                      Glycan
NODE        6
            1   Xyl        22     0
            2   Gal        14     0
            3   Gal         6     0
            4   GlcA       -2     0
            5   GalNAc    -11     0
            6   L4-en-thrHexA   -23     0
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    5:3  
///

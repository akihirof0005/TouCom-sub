ENTRY       G09228                      Glycan
NODE        3
            1   D/LFru    5.2   3.5
            2   Glc       5.2  -3.5
            3   Gal      -4.8  -3.5
EDGE        2
            1     2:a1    1:2  
            2     3:b1    2:6  
///

ENTRY       G02279                      Glycan
NODE        6
            1   Cer      18.2   0.5
            2   Glc       9.2   0.5
            3   Gal       0.2   0.5
            4   GalNAc   -9.8   5.5
            5   L3daraHep-2-ulop5NAc7N-onic -16.8  -4.5
            6   Gal     -18.8   5.5
EDGE        5
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b2    3:3  
            5     6:b1    4:3  
///

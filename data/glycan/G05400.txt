ENTRY       G05400                      Glycan
NODE        10
            1   Glc         0     0
            2   Glc       -10     0
            3   Glc       -20     0
            4   Glc       -30     0
            5   Glc       -40     0
            6   Glc       -50     0
            7   Glc       -60     0
            8   Glc       -70     0
            9   Glc       -80     0
            10  Glc       -90     0
EDGE        9
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
            5     6:a1    5:4  
            6     7:a1    6:4  
            7     8:a1    7:6  
            8     9:a1    8:6  
            9    10:a1    9:6  
///

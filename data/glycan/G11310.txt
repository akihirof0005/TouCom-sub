ENTRY       G11310                      Glycan
NODE        11
            1   Anhydro-Man    23    -3
            2   Kdo        13    -3
            3   Man         5     3
            4   Kdo         5    -8
            5   Man        -2     3
            6   Man       -10     8
            7   Kdo       -10     3
            8   Gal       -10    -2
            9   Glc       -17     8
            10  Glc       -17    -2
            11  Glc       -24    -2
EDGE        10
            1     2:a2    1:6  
            2     3:a1    2:5  
            3     4:a2    2:4  
            4     5:a1    3:4  
            5     6:a1    5:6  
            6     7:a2    5:3  
            7     8:a1    5:2  
            8     9:a1    6:3  
            9    10:a1    8:3  
            10   11:a1   10:2  
///

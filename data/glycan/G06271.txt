ENTRY       G06271                      Glycan
NODE        6
            1   LIdoA    18.2  -2.5
            2   GalNAc    9.2  -2.5
            3   S         2.7   1.9
            4   GlcA      0.2  -2.5
            5   GalNAc   -8.8  -2.5
            6   LIdoA   -17.8  -2.5
EDGE        5
            1     2:b1    1    
            2     3       2:6  
            3     4:b1    2    
            4     5:b1    4    
            5     6:a1    5    
///

ENTRY       G02344                      Glycan
NODE        4
            1   Glc        10     0
            2   Fuc         0     5
            3   Glc         0    -5
            4   Xyl       -10     5
EDGE        3
            1     2:b1    1:6  
            2     3:b1    1:2  
            3     4:b1    2:2  
///

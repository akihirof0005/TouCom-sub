ENTRY       G09212                      Glycan
NODE        3
            1   L2,6dlyxHex3Me  12.8   0.1
            2   LCym     -0.2   0.1
            3   L2,6dlyxHex3Me -13.2   0.1
EDGE        2
            1     2:b1    1:4  
            2     3:a1    2:4  
///

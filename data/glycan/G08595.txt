ENTRY       G08595                      Glycan
NODE        3
            1   Man         0     0
            2   Gal       -10     0
            3   Gal       -20     0
EDGE        2
            1     2:b1    1:4  
            2     3:b1    2:4  
///

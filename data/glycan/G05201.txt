ENTRY       G05201                      Glycan
NODE        5
            1   Cer        22  -0.1
            2   Glc        14  -0.1
            3   Gal         6  -0.1
            4   Neu5Ac1NdiMe    -6  -0.1
            5   Neu5Ac1NdiMe   -23  -0.1
EDGE        4
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
            4     5:a2    4:8  
///

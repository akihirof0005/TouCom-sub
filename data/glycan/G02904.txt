ENTRY       G02904                      Glycan
NODE        5
            1   Cer        15     0
            2   Glc         8     0
            3   Neu5Gc      0     0
            4   Glc        -8     0
            5   Neu5Gc    -16     0
EDGE        4
            1     2:b1    1:1  
            2     3:a2    2:6  
            3     4:b1    3:8  
            4     5:a2    4:6  
///

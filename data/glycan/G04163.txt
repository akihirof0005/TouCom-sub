ENTRY       G04163                      Glycan
NODE        9
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   Gal       -20     5
            5   Neu5Ac    -20     0
            6   GlcNAc    -20   -10
            7   Neu5Ac    -30     5
            8   Gal       -30   -10
            9   Neu5Ac    -40   -10
EDGE        8
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a2    3:6  
            5     6:b1    3:3  
            6     7:a2    4:3  
            7     8:b1    6:4  
            8     9:a2    8:3  
///

ENTRY       G04866                      Glycan
NODE        10
            1   Glc         0     0
            2   Glc       -10     5
            3   Glc       -10    -5
            4   Glc       -20     5
            5   GlcA      -20    -5
            6   Glc       -30     5
            7   GlcA      -30    -5
            8   GlcA      -40     5
            9   Gal       -50     5
            10  Glc4,6Py   -60     5
EDGE        9
            1     2:b1    1:6  
            2     3:b1    1:4  
            3     4:b1    2:4  
            4     5:b1    3:4  
            5     6:b1    4:4  
            6     7:b1    5:4  
            7     8:b1    6:4  
            8     9:a1    8:4  
            9    10:b1    9:6  
///

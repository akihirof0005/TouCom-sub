ENTRY       G12379                      Glycan
NODE        4
            1   Cer        14     0
            2   GalNAc      5     0
            3   GlcNAc     -5     0
            4   Gal       -15     0
EDGE        3
            1     2:a1    1:1  
            2     3:b1    2:6  
            3     4:b1    3:4  
///

ENTRY       G03295                      Glycan
NODE        4
            1   LRha       10     0
            2   LRha        0     0
            3   LRha      -10     5
            4   LRha      -10    -5
EDGE        3
            1     2:a1    1:2  
            2     3:a1    2:3  
            3     4:a1    2:2  
///

ENTRY       G09431                      Glycan
NODE        5
            1   GlcN       12     3
            2   S           8    -2
            3   LIdoA       2     3
            4   GlcN       -8     3
            5   S         -12    -2
EDGE        4
            1     2       1:2  
            2     3:a1    1:4  
            3     4:a1    3:4  
            4     5       4:2  
///

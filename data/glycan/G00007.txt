ENTRY       G00007                      Glycan
NODE        12
            1   PP-Dol     23    -2
            2   GlcNAc     14    -2
            3   GlcNAc      6    -2
            4   Man        -3    -2
            5   Man       -10     4
            6   Man       -10    -9
            7   Man       -17     9
            8   Man       -17     0
            9   Man       -17    -9
            10  Man       -24     9
            11  Man       -24     0
            12  Man       -24    -9
EDGE        11
            1     2:a1    1    
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:6  
            7     8:a1    5:3  
            8     9:a1    6:2  
            9    10:a1    7:2  
            10   11:a1    8:2  
            11   12:a1    9:2  
///

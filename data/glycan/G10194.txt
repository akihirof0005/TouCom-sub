ENTRY       G10194                      Glycan
NODE        3
            1   GalA     10.2   0.5
            2   D/LMan    0.2   0.5
            3   GalA     -9.8   0.5
EDGE        2
            1     2:1     1:4  
            2     3:1     2:2  
///

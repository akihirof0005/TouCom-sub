ENTRY       G11504                      Glycan
NODE        5
            1   GalNAc     15     0
            2   Gal         5     5
            3   Gal         5    -5
            4   GlcNAc     -5    -5
            5   Gal       -15    -5
EDGE        4
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    3:3  
            4     5:b1    4:4  
///

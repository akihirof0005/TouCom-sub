ENTRY       G01254                      Glycan
NODE        5
            1   Glc        15     0
            2   LAra        5     0
            3   Glc        -5     0
            4   Glc       -15     5
            5   LRha      -15    -5
EDGE        4
            1     2:a1    1:6  
            2     3:b1    2:2  
            3     4:b1    3:3  
            4     5:a1    3:2  
///

ENTRY       G12703                      Glycan
NODE        6
            1   *          19    -2
            2   Glc        11    -2
            3   LRha2Ac     2    -2
            4   Gal        -6    -5
            5   GlcA      -13    -5
            6   *         -20     6
EDGE        5
            1     2:b1    1    
            2     3:a1    2:3  
            3     4:a1    3:3  
            4     6       3:4  
            5     5:a1    4:3  
BRACKET     1 -15.0   6.0 -15.0  -9.0
            1  14.0  -9.0  14.0   6.0
            1 n
///

ENTRY       G05199                      Glycan
NODE        5
            1   Cer      18.2   0.5
            2   Glc      11.2   0.5
            3   2dlyxHex   2.2   0.5
            4   Neu5Ac   -8.8   0.5
            5   Neu5Ac  -18.8   0.5
EDGE        4
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a2    3:3  
            4     5:a2    4:8  
///

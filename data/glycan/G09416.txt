ENTRY       G09416                      Glycan
NODE        3
            1   Glc         0     0
            2   Gal       -10     0
            3   Neu5Gc    -20     0
EDGE        2
            1     2:b1    1:4  
            2     3:2     2:3  
///

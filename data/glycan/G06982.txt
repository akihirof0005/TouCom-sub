ENTRY       G06982                      Glycan
NODE        5
            1   Cer         0     0
            2   Glc       -10     0
            3   Gal       -20     0
            4   Gal       -30     5
            5   LRha      -30    -5
EDGE        4
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:4  
            4     5:a1    3:3  
///

ENTRY       G06669                      Glycan
NODE        6
            1   Man         0     0
            2   Galf      -10     5
            3   Man       -10    -5
            4   GlcA      -20     5
            5   LRha      -20    -5
            6   Man       -30     5
EDGE        5
            1     2:b1    1:6  
            2     3:a1    1:2  
            3     4:a1    2:2  
            4     5:a1    3:2  
            5     6:b1    4:4  
///

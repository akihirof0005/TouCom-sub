ENTRY       G12860                      Glycan
NODE        9
            1   Glc        19    -1
            2   Gal        12    -1
            3   GlcNAc      4     2
            4   GlcNAc      4    -4
            5   Gal        -4     5
            6   LFuc       -4    -1
            7   Gal        -5    -4
            8   GlcNAc    -12     5
            9   Gal       -20     5
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    2:3  
            4     5:b1    3:4  
            5     6:a1    3:3  
            6     7:b1    4:3  
            7     8:b1    5:3  
            8     9:b1    8:3  
///

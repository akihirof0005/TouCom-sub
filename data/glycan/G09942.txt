ENTRY       G09942                      Glycan
NODE        12
            1   Man         0     0
            2   Man       -10     5
            3   Man       -10    -5
            4   GlcNAc    -20     5
            5   GlcNAc    -20     0
            6   GlcNAc    -20   -10
            7   Gal       -30     5
            8   Gal       -30     0
            9   Gal       -30   -10
            10  Neu5Ac    -40     5
            11  Neu5Ac    -40     0
            12  Neu5Ac    -40   -10
EDGE        11
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:b1    2:2  
            4     5:b1    3:4  
            5     6:b1    3:2  
            6     7:b1    4:4  
            7     8:b1    5:4  
            8     9:b1    6:4  
            9    10:a2    7:6  
            10   11:a2    8:3  
            11   12:a2    9:6  
///

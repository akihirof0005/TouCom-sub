ENTRY       G03020                      Glycan
NODE        5
            1   Rha4NFormyl    26     0
            2   Rha4NFormyl    13     0
            3   Rha4NFormyl     0     0
            4   Rha4NFormyl   -13     0
            5   Rha4NFormyl   -26     0
EDGE        4
            1     2:a1    1:2  
            2     3:a1    2:2  
            3     4:a1    3:2  
            4     5:a1    4:3  
///

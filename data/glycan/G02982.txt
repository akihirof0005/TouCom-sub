ENTRY       G02982                      Glycan
NODE        7
            1   GlcNAc     18    -2
            2   GlcNAc      8    -2
            3   Man        -1    -2
            4   Man        -9     2
            5   Man        -9    -6
            6   GlcNAc    -18     6
            7   GlcNAc    -18    -2
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:6  
            6     7:b1    4:2  
///

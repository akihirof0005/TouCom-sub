ENTRY       G07651                      Glycan
NODE        4
            1   Cer         0     0
            2   Man       -10     0
            3   Man       -20     0
            4   Man       -30     0
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:a1    3:3  
///

ENTRY       G05075                      Glycan
NODE        9
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   GlcNAc    -10    -5
            4   Gal       -20     5
            5   Gal       -20    -5
            6   GlcNAc    -30     5
            7   LFuc      -40    10
            8   Gal       -40     0
            9   LFuc      -50     0
EDGE        8
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:b1    3:4  
            5     6:b1    4:3  
            6     7:a1    6:4  
            7     8:b1    6:3  
            8     9:a1    8:2  
///

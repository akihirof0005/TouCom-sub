ENTRY       G01146                      Glycan
NODE        11
            1   GlcNAc     20     0
            2   Man        10     0
            3   Man         0    10
            4   Man         0   -10
            5   Man       -10    15
            6   Man       -10     5
            7   Man       -10    -5
            8   Man       -10   -15
            9   Man       -20    15
            10  Man       -20    -5
            11  Man       -20   -15
EDGE        10
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
            5     6:a1    3:3  
            6     7:a1    4:6  
            7     8:a1    4:2  
            8     9:a1    5:2  
            9    10:a1    7:6  
            10   11:a1    8:2  
///

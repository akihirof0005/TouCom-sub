ENTRY       G06904                      Glycan
NODE        6
            1   GlcNAc     23     0
            2   Man        13     0
            3   Man         3     0
            4   Man        -7     0
            5   P         -15     0
            6   Man       -23     0
EDGE        5
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    3:6  
            4     5       4:6  
            5     6:a1    5    
///

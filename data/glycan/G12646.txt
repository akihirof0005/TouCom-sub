ENTRY       G12646                      Glycan
NODE        4
            1   *          12     0
            2   LRha        5     0
            3   GalA       -4     0
            4   *         -12     0
EDGE        3
            1     2:a1    1    
            2     3:a1    2:2  
            3     4       3:4  
BRACKET     1  -8.0   3.0  -8.0  -3.0
            1   9.0  -3.0   9.0   3.0
            1 n
///

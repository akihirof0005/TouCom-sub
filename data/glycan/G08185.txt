ENTRY       G08185                      Glycan
NODE        4
            1   Glc        10     4
            2   Glc        10    -3
            3   Glc         0     4
            4   Glc       -10     4
EDGE        3
            1     3:b1    1:4  
            2     1:a1    2:a1 
            3     4:b1    3:6  
///

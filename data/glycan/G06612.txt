ENTRY       G06612                      Glycan
NODE        7
            1   GalNAc     16     0
            2   S          11    -4
            3   GlcA        6     0
            4   GalNAc     -4     0
            5   S          -9     4
            6   S          -9    -4
            7   L4-en-thrHexA   -16     0
EDGE        6
            1     2       1:4  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5       4:6  
            5     6       4:4  
            6     7:a1    4:3  
///

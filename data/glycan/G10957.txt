ENTRY       G10957                      Glycan
NODE        17
            1   Asn         0     0
            2   GlcNAc    -10     0
            3   LFuc      -20     5
            4   GlcNAc    -20    -5
            5   Man       -30    -5
            6   Man       -40     0
            7   GlcNAc    -40    -5
            8   Man       -40   -10
            9   GlcNAc    -50     0
            10  GlcNAc    -50    -5
            11  GlcNAc    -50   -15
            12  Gal       -60     0
            13  Gal       -60    -5
            14  Gal       -60   -15
            15  Neu5Ac    -70     0
            16  Neu5Ac    -70    -5
            17  Neu5Ac    -70   -15
EDGE        16
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:b1    5:4  
            7     8:a1    5:3  
            8     9:b1    6:2  
            9    10:b1    8:4  
            10   11:b1    8:2  
            11   12:b1    9:4  
            12   13:b1   10:4  
            13   14:b1   11:4  
            14   15:a2   12:3  
            15   16:a2   13:3  
            16   17:a2   14:3  
///

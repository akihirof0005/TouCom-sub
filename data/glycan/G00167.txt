ENTRY       G00167                      Glycan
NODE        7
            1   Cer        21     0
            2   Glc        13     0
            3   Gal         5     0
            4   GalNAc     -4     0
            5   Gal       -13     0
            6   GalNAc    -21     4
            7   Neu5Ac    -21    -4
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7:a2    5:3  
///

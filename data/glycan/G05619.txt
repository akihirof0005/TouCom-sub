ENTRY       G05619                      Glycan
NODE        7
            1   GalNAc      0     0
            2   GlcNAc    -10     5
            3   Gal       -10    -5
            4   Gal       -20    10
            5   LFuc      -20     0
            6   LFuc      -20    -5
            7   Neu5Ac    -30    10
EDGE        6
            1     2:b1    1:6  
            2     3:b1    1:3  
            3     4:b1    2:4  
            4     5:a1    2:3  
            5     6:a1    3:2  
            6     7:a2    4:3  
///

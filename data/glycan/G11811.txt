ENTRY       G11811                      Glycan
NODE        10
            1   GlcN-ol  27.4    -2
            2   GlcN     17.4    -2
            3   Kdo       8.4    -2
            4   Man       0.4    -2
            5   Man      -7.6     3
            6   Glc      -7.6    -2
            7   Glc      -7.6    -7
            8   GalN    -14.6     0
            9   QuiNAc  -15.6     7
            10  deltaGalNA -26.6     7
EDGE        9
            1     2:b1    1:6  
            2     3:b2    2:6  
            3     4:a1    3:5  
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     7:b1    4:2  
            7     8:a1    5:2  
            8     9:b1    5:4  
            9    10:b1    9:3  
///

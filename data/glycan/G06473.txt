ENTRY       G06473                      Glycan
NODE        6
            1   GalNAc      9    -2
            2   Gal         0    -6
            3   GlcNAc     -1     2
            4   LFuc       -8    -6
            5   Gal        -9     6
            6   LFuc       -9    -2
EDGE        5
            1     2:b1    1:3  
            2     3:b1    1:6  
            3     4:a1    2:2  
            4     5:b1    3:4  
            5     6:a1    3:3  
///

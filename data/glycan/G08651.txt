ENTRY       G08651                      Glycan
NODE        3
            1   GlcA        0     0
            2   Glc       -10     5
            3   LAra      -10    -5
EDGE        2
            1     2:b1    1:3  
            2     3:a1    1:2  
///

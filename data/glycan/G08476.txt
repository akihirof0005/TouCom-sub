ENTRY       G08476                      Glycan
NODE        4
            1   LGulA       0     0
            2   LGulA     -10     0
            3   LGulA     -20     0
            4   LGulA     -30     0
EDGE        3
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
///

ENTRY       G12801                      Glycan
NODE        6
            1   *          22     0
            2   LRha2Ac    13     0
            3   GalA        4     0
            4   GalNAc     -5     0
            5   GlcNAc    -14     0
            6   *         -22     0
EDGE        5
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:a1    3:4  
            4     5:b1    4:3  
            5     6       5:3  
BRACKET     1 -18.0   3.0 -18.0  -2.0
            1  18.0  -2.0  18.0   3.0
            1 n
///

ENTRY       G04538                      Glycan
NODE        11
            1   GlcA        0     0
            2   GlcNAc    -10     0
            3   GlcA      -20     0
            4   GlcNAc    -30     0
            5   GlcA      -40     0
            6   GlcNAc    -50     0
            7   GlcA      -60     0
            8   GlcNAc    -70     0
            9   GlcA      -80     0
            10  GlcNAc    -90     0
            11  GlcA     -100     0
EDGE        10
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:4  
            6     7:b1    6:3  
            7     8:b1    7:4  
            8     9:b1    8:3  
            9    10:b1    9:4  
            10   11:b1   10:3  
///

ENTRY       G00342                      Glycan
NODE        5
            1   Cer        15     0
            2   Glc         5     0
            3   Gal        -5     0
            4   GalNAc    -15     5
            5   Neu5Gc    -15    -5
EDGE        4
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:a2    3:3  
///

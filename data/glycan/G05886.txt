ENTRY       G05886                      Glycan
NODE        7
            1   Man         0     0
            2   Man       -10     0
            3   Man       -20     0
            4   Man       -30     0
            5   Man       -40     5
            6   Man       -40    -5
            7   Man       -50    -5
EDGE        6
            1     2:a1    1:2  
            2     3:a1    2:2  
            3     4:a1    3:2  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    6:2  
///

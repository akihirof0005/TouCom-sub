ENTRY       G01761                      Glycan
NODE        3
            1   Man         0     0
            2   Man       -10     0
            3   Gal       -20     0
EDGE        2
            1     2:a1    1:2  
            2     3:a1    2:2  
///

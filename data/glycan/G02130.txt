ENTRY       G02130                      Glycan
NODE        3
            1   1,6-Anhydro-Glc    11     0
            2   Glc         0     0
            3   3,4-Anhydro-Fuc   -11     0
EDGE        2
            1     2:a1    1:4  
            2     3:a1    2:4  
///

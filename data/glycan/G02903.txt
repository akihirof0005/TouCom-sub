ENTRY       G02903                      Glycan
NODE        7
            1   Man        21     0
            2   Man        14     0
            3   Man         7     0
            4   Man         0     0
            5   Man        -7     0
            6   Man       -14     0
            7   Man       -21     0
EDGE        6
            1     2:b1    1:2  
            2     3:b1    2:2  
            3     4:b1    3:2  
            4     5:b1    4:2  
            5     6:b1    5:2  
            6     7:b1    6:2  
///

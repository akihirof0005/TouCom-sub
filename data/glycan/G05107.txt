ENTRY       G05107                      Glycan
NODE        9
            1   GlcNAc     22     3
            2   LFuc       13     7
            3   GlcNAc     12    -1
            4   Man         3    -1
            5   Man        -5     4
            6   Man        -5    -6
            7   GlcNAc     -6    -1
            8   GlcNAc    -14     4
            9   Gal       -22     4
EDGE        8
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:4  
            7     8:b1    5:2  
            8     9:b1    8:4  
///

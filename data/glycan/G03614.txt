ENTRY       G03614                      Glycan
NODE        4
            1   Cer        10     0
            2   Gal         2     0
            3   Gal        -6     0
            4   P         -11     0
EDGE        3
            1     2:b1    1:1  
            2     3:b1    2:6  
            3     4       3:6  
///

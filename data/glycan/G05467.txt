ENTRY       G05467                      Glycan
NODE        6
            1   Man         0     0
            2   GlcNAc    -10     5
            3   GlcNAc    -10    -5
            4   Gal       -20     5
            5   Gal       -20    -5
            6   Neu5Ac    -30    -5
EDGE        5
            1     2:b1    1:4  
            2     3:b1    1:2  
            3     4:b1    2:4  
            4     5:b1    3:4  
            5     6:a2    5:6  
///

ENTRY       G07392                      Glycan
NODE        5
            1   GalNAc   13.2   0.5
            2   GlcNAc    3.2   5.5
            3   GlcNAc    3.2  -4.5
            4   Gal      -5.8   5.5
            5   LFuc    -13.8   5.5
EDGE        4
            1     2:b1    1:6  
            2     3:b1    1    
            3     4:b1    2:4  
            4     5:a1    4:2  
///

ENTRY       G07315                      Glycan
NODE        4
            1   Man         4     0
            2   Man        -5     5
            3   Man        -5     0
            4   Man        -5    -5
EDGE        3
            1     2:a1    1:6  
            2     3:a1    1:4  
            3     4:a1    1:2  
///

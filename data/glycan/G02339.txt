ENTRY       G02339                      Glycan
NODE        4
            1   Lgro-manHep    12     2
            2   P           4    -1
            3   Lgro-manHep    -4     2
            4   P         -12    -1
EDGE        3
            1     2       1:4  
            2     3:a1    1:3  
            3     4       3:4  
///

ENTRY       G03003                      Glycan
NODE        6
            1   GalNAc     17     0
            2   GlcNAc      7     0
            3   Gal        -1     0
            4   GlcNAc     -9     4
            5   GlcNAc     -9    -4
            6   Gal       -17    -4
EDGE        5
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:6  
            4     5:b1    3:3  
            5     6:b1    5:4  
///

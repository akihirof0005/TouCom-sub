ENTRY       G04694                      Glycan
NODE        9
            1   Man3Me     35     0
            2   Man3Me     26     0
            3   Man3Me     17     0
            4   Man3Me      8     0
            5   Man3Me     -1     0
            6   Man3Me    -10     0
            7   Man3Me    -19     0
            8   Man3Me    -28     0
            9   Man       -36     0
EDGE        8
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
            5     6:a1    5:4  
            6     7:a1    6:4  
            7     8:a1    7:4  
            8     9:a1    8:4  
///

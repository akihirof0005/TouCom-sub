ENTRY       G11167                      Glycan
NODE        11
            1   Kdo        26    -2
            2   Kdo        19    -6
            3   Lgro-manHep    16     2
            4   Kdo        12    -6
            5   Lgro-manHep     3     2
            6   Glc        -6    -3
            7   Lgro-manHep    -9     7
            8   Gal       -12     1
            9   Gal       -12    -7
            10  Glc       -19    -7
            11  GlcNAc    -27    -7
EDGE        10
            1     2:a2    1:4  
            2     3:a1    1:5  
            3     4:a2    2:4  
            4     5:a1    3:3  
            5     6:a1    5:3  
            6     7:a1    5:7  
            7     8:a1    6:6  
            8     9:a1    6:3  
            9    10:a1    9:2  
            10   11:a1   10:2  
///

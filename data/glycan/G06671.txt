ENTRY       G06671                      Glycan
NODE        8
            1   Kdo      16.2   2.5
            2   Hep       8.2   2.5
            3   Glc      -0.8   7.5
            4   D/LHep   -1.8  -2.5
            5   P        -6.8  -6.5
            6   Gal      -8.8   7.5
            7   GlcNAc  -11.8  -2.5
            8   Gal     -16.8   7.5
EDGE        7
            1     2:a1    1    
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     6:b1    3:4  
            5     5       4    
            6     7:a1    4:2  
            7     8:a1    6:4  
///

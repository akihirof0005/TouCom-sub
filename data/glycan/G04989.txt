ENTRY       G04989                      Glycan
NODE        9
            1   GlcNAc     23    -2
            2   GlcNAc     13    -2
            3   Man         4    -2
            4   Man        -4     2
            5   Man        -4    -6
            6   Man       -12     6
            7   Man       -12    -2
            8   GlcNAc    -13    -6
            9   GalNAc    -23    -6
EDGE        8
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:6  
            6     7:a1    4:3  
            7     8:b1    5:2  
            8     9:b1    8:4  
///

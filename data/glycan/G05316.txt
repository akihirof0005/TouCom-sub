ENTRY       G05316                      Glycan
NODE        6
            1   Ser      21.2   0.5
            2   GalNAc   13.2   0.5
            3   Gal       4.2   0.5
            4   GlcNAc   -4.8   0.5
            5   Gal     -12.8   0.5
            6   Neu5Ac  -21.8   0.5
EDGE        5
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:b1    3:3  
            4     5:b1    4    
            5     6:a2    5:3  
///

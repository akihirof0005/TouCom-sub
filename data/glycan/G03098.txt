ENTRY       G03098                      Glycan
NODE        5
            1   Xyl         8    -2
            2   Xyl         0     3
            3   GlcA4Me6Me    -3    -7
            4   Xyl        -9     8
            5   Xyl        -9    -2
EDGE        4
            1     2:b1    1:4  
            2     3:b1    1:2  
            3     4:b1    2:4  
            4     5:b1    2:3  
///

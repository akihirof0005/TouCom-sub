ENTRY       G10247                      Glycan
NODE        8
            1   Cer        17    -2
            2   Glc        11    -2
            3   Gal         5    -2
            4   GlcNAc     -2    -2
            5   Gal       -10     2
            6   LFuc      -10    -6
            7   S         -11     6
            8   Neu5Ac    -17     2
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     7       5:6  
            7     8:a2    5:3  
///

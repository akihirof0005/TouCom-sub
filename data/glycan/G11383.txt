ENTRY       G11383                      Glycan
NODE        7
            1   GlcNAc     16     0
            2   GlcNAc      7     0
            3   Man        -1     0
            4   Man        -8     5
            5   Man        -8    -5
            6   Man       -16     5
            7   GlcNAc    -16    -5
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:a1    4:3  
            6     7:b1    5:4  
///

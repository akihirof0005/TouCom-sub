ENTRY       G08254                      Glycan
NODE        3
            1   Galf        0     0
            2   Gal       -10     0
            3   Galf      -20     0
EDGE        2
            1     2:a1    1:3  
            2     3:b1    2:3  
///

ENTRY       G04381                      Glycan
NODE        16
            1   Asn        28     4
            2   GlcNAc     21     4
            3   LFuc       12     8
            4   GlcNAc     12     0
            5   Man         4     0
            6   Man        -4     4
            7   Man        -4    -4
            8   GlcNAc    -12     4
            9   GlcNAc    -12     0
            10  GlcNAc    -12    -8
            11  Gal       -20     4
            12  Gal       -20     0
            13  Gal       -20    -8
            14  Neu5Ac    -28     4
            15  Gal       -28     0
            16  Neu5Ac    -28    -8
EDGE        15
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    7:2  
            10   11:b1    8:4  
            11   12:b1    9:4  
            12   13:b1   10:4  
            13   14:a2   11:6  
            14   15:a1   12:3  
            15   16:a2   13:6  
///

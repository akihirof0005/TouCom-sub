ENTRY       G07971                      Glycan
NODE        8
            1   2,5-Anhydro-Man    17     0
            2   S           8    -4
            3   LIdoA       2     0
            4   S          -2    -4
            5   GlcN       -8     0
            6   S         -12     4
            7   S         -12    -4
            8   LIdoA     -18     0
EDGE        7
            1     2       1:1  
            2     3:a1    1:3  
            3     4       3:2  
            4     5:a1    3:4  
            5     6       5:6  
            6     7       5:2  
            7     8:a1    5:4  
///

ENTRY       G00808                      Glycan
NODE        4
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20     0
            4   Gal       -30     0
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:6  
            3     4:b1    3:4  
///

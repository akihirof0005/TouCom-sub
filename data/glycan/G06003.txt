ENTRY       G06003                      Glycan
NODE        7
            1   Ara         0     0
            2   Araf      -10     0
            3   Araf      -20     0
            4   Araf      -30     0
            5   Man       -40     0
            6   Man       -50     0
            7   Man       -60     0
EDGE        6
            1     2:a1    1:5  
            2     3:a1    2:5  
            3     4:b1    3:2  
            4     5:a1    4:5  
            5     6:a1    5:2  
            6     7:a1    6:2  
///

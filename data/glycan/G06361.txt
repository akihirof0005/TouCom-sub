ENTRY       G06361                      Glycan
NODE        7
            1   Glc      15.2  -2.5
            2   Gal       8.2   2.5
            3   LFuc      8.2  -7.5
            4   GlcNAc    0.2   2.5
            5   Gal      -7.8   7.5
            6   LFuc     -8.8  -2.5
            7   LFuc    -15.8   7.5
EDGE        6
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    2    
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     7:a1    5:2  
///

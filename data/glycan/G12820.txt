ENTRY       G12820                      Glycan
NODE        7
            1   *          18     1
            2   GlcNAc     10     1
            3   Glc         2     1
            4   GlcNAc     -6     1
            5   Gal       -12     4
            6   *         -14    -3
            7   Glc       -19     4
EDGE        6
            1     2:b1    1    
            2     3:b1    2:3  
            3     4:a1    3:4  
            4     5:b1    4:4  
            5     6       4:3  
            6     7:a1    5:2  
BRACKET     1 -11.0   0.4 -11.0  -3.0
            1  15.0  -1.0  15.0   2.6
            1 n
///

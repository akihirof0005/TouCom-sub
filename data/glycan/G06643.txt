ENTRY       G06643                      Glycan
NODE        6
            1   Glc         0     0
            2   Glc       -10     0
            3   Glc       -20     0
            4   Xyl       -30     5
            5   Glc       -30    -5
            6   Glc       -40    -5
EDGE        5
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:b1    3:4  
            5     6:b1    5:4  
///

ENTRY       G02826                      Glycan
NODE        10
            1   GlcNAc     37     0
            2   GlcA       29     0
            3   GlcNAc     21     0
            4   GlcA       13     0
            5   GlcNAc      5     0
            6   GlcA       -3     0
            7   GlcNAc    -11     0
            8   GlcA      -19     0
            9   GlcNAc    -27     0
            10  L4-en-thrHexA   -37     0
EDGE        9
            1     2:b1    1:3  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:b1    7:3  
            8     9:b1    8:4  
            9    10:a1    9:3  
///

ENTRY       G12336                      Glycan
NODE        5
            1   GalNAc   12.6  -1.6
            2   S         6.6   1.4
            3   GlcA      2.6  -1.6
            4   GalNAc   -7.4  -1.6
            5   S       -13.4   1.4
EDGE        4
            1     2       1:4  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5       4:4  
///

ENTRY       G12064                      Glycan
NODE        9
            1   Asn        22     3
            2   GlcNAc     13     3
            3   LFuc        3     6
            4   GlcNAc      3     0
            5   Man        -6     0
            6   Man       -14     3
            7   Man       -14    -3
            8   GlcNAc    -23     0
            9   GlcNAc    -23    -6
EDGE        8
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    7:4  
            8     9:b1    7:2  
///

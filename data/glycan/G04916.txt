ENTRY       G04916                      Glycan
NODE        7
            1   Glc         0     0
            2   Gal       -10     0
            3   GlcNAc    -20     0
            4   Gal       -30     0
            5   GlcNAc    -40     0
            6   Gal       -50     0
            7   Neu5Ac    -60     0
EDGE        6
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:b1    5:3  
            6     7:a2    6:3  
///

ENTRY       G08459                      Glycan
NODE        4
            1   Fuc         0     0
            2   Glc       -10     0
            3   Glc       -20     0
            4   Glc       -30     0
EDGE        3
            1     2:b1    1:3  
            2     3:1     2:6  
            3     4:1     3:4  
///

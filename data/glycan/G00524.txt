ENTRY       G00524                      Glycan
NODE        9
            1   GlcNAc     16    -2
            2   Man         7    -2
            3   Man        -1     3
            4   Man        -1    -7
            5   Man        -9     8
            6   Man        -9    -2
            7   Man        -9    -7
            8   Man       -17    -2
            9   Man       -17    -7
EDGE        8
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
            5     6:a1    3:3  
            6     7:a1    4:2  
            7     8:a1    6:2  
            8     9:a1    7:2  
///

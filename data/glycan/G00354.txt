ENTRY       G00354                      Glycan
NODE        12
            1   GlcNAc     22     3
            2   LFuc       12     8
            3   GlcNAc     12    -2
            4   Man         3    -2
            5   Man        -5     3
            6   Man        -5    -7
            7   GlcNAc    -14     8
            8   GlcNAc    -14    -2
            9   GlcNAc    -14    -7
            10  Gal       -22     8
            11  Gal       -22    -2
            12  Gal       -22    -7
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:4  
            7     8:b1    5:2  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:b1    8:4  
            11   12:b1    9:4  
///

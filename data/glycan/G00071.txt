ENTRY       G00071                      Glycan
NODE        8
            1   Cer        23     0
            2   Glc        17     0
            3   Gal        11     0
            4   GlcNAc      4     0
            5   Gal        -3     0
            6   GlcNAc    -10     0
            7   Gal       -17     0
            8   LFuc      -23     0
EDGE        7
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:3  
            6     7:b1    6:4  
            7     8:a1    7:2  
///

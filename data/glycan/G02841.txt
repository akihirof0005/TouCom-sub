ENTRY       G02841                      Glycan
NODE        9
            1   GlcNAc     20     3
            2   LFuc       11     8
            3   GlcNAc     11    -2
            4   Man         3    -2
            5   Man        -4     3
            6   Man        -4    -7
            7   GlcNAc    -12     3
            8   GlcNAc    -12    -7
            9   GalNAc    -21    -7
EDGE        8
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    8:4  
///

ENTRY       G02294                      Glycan
NODE        5
            1   Glc        15     0
            2   Glc         5     0
            3   Glc        -5     5
            4   Gal        -5    -5
            5   Gal       -15    -5
EDGE        4
            1     2:a1    1:3  
            2     3:b1    2:3  
            3     4:a1    2:2  
            4     5:a1    4:2  
///

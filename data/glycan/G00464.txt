ENTRY       G00464                      Glycan
NODE        8
            1   GlcNAc     17    -2
            2   Man         7    -2
            3   Man        -1     2
            4   Man        -1    -6
            5   Man        -9     6
            6   Man        -9    -2
            7   Man        -9    -6
            8   Man       -17    -6
EDGE        7
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
            5     6:a1    3:3  
            6     7:a1    4:2  
            7     8:a1    7:2  
///

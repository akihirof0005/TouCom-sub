ENTRY       G05994                      Glycan
NODE        8
            1   Kdo      15.2   2.5
            2   Hep       8.2   2.5
            3   Glc       0.2   7.5
            4   Hep       0.2  -2.5
            5   Gal      -7.8   7.5
            6   Glc      -7.8   1.5
            7   GlcNAc   -8.8  -6.5
            8   Gal     -15.8   1.5
EDGE        7
            1     2:a1    1    
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5:b1    3:4  
            5     6:b1    4:3  
            6     7:a1    4:2  
            7     8:b1    6:4  
///

ENTRY       G00090                      Glycan
NODE        9
            1   Cer        20    -2
            2   Glc        14    -2
            3   Gal         8    -2
            4   GlcNAc      1    -2
            5   Gal        -6     2
            6   LFuc       -6    -6
            7   GlcNAc    -13     2
            8   Gal       -21     6
            9   LFuc      -21    -2
EDGE        8
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:a1    4:3  
            6     7:b1    5:3  
            7     8:b1    7:4  
            8     9:a1    7:3  
///

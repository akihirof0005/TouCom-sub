ENTRY       G10110                      Glycan
NODE        8
            1   Glc      15.2   2.5
            2   Glc       8.2   2.5
            3   GlcNAc    0.2   6.5
            4   GlcNAc    0.2  -1.5
            5   Gal      -7.8   6.5
            6   Gal      -7.8  -5.5
            7   Neu5Ac   -8.8   2.5
            8   LFuc    -14.8   6.5
EDGE        7
            1     2:1     1:4  
            2     3:1     2:6  
            3     4:1     2:3  
            4     5:1     3:3  
            5     6:1     4:3  
            6     7:2     4:4  
            7     8:1     5:2  
///

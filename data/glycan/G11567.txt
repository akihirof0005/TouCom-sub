ENTRY       G11567                      Glycan
NODE        6
            1   *          19    -2
            2   LRha       12    -2
            3   LRha        4    -2
            4   LRha       -4    -2
            5   Fuc3NAc   -12     3
            6   *         -19    -2
EDGE        5
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:a1    3:3  
            4     5:a1    4:3  
            5     6       4:2  
BRACKET     1 -16.0   4.0 -16.0  -4.0
            1  16.0  -4.0  16.0   4.0
            1 n
///

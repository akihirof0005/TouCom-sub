ENTRY       G10297                      Glycan
NODE        4
            1   Glc         7     0
            2   Gal        -3     0
            3   S          -7     4
            4   S          -7    -4
EDGE        3
            1     2:b1    1:4  
            2     3       2:6  
            3     4       2:3  
///

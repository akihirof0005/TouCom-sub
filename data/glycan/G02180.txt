ENTRY       G02180                      Glycan
NODE        12
            1   GlcNAc     24     3
            2   LFuc       16     8
            3   GlcNAc     15    -2
            4   Man         7    -2
            5   Man        -1     3
            6   Man        -1    -7
            7   GlcNAc     -9     3
            8   GlcNAc     -9    -7
            9   Gal       -17     3
            10  Gal       -17    -7
            11  Neu5Ac    -25     3
            12  Neu5Gc    -25    -7
EDGE        11
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:2  
            8     9:b1    7:4  
            9    10:b1    8:4  
            10   11:a2    9:3  
            11   12:a2   10:3  
///

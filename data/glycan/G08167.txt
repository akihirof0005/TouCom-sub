ENTRY       G08167                      Glycan
NODE        3
            1   Kdo         6     1
            2   Gal        -3     5
            3   Lgro-manHep    -6    -4
EDGE        2
            1     2:a1    1:7  
            2     3:a1    1:4  
///

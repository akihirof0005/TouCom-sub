ENTRY       G05922                      Glycan
NODE        7
            1   Glc         0     0
            2   Gal       -10     5
            3   Glc       -10    -5
            4   Glc       -20    -5
            5   GalNAc    -30     0
            6   GlcNAc    -30   -10
            7   Gal       -40     0
EDGE        6
            1     2:a1    1:6  
            2     3:a1    1:3  
            3     4:a1    3:2  
            4     5:b1    4:4  
            5     6:a1    4:2  
            6     7:b1    5:3  
///

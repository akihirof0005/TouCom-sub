ENTRY       G02642                      Glycan
NODE        3
            1   GlcNAc      7     2
            2   S           1    -1
            3   L4-en-thrHexA    -7     2
EDGE        2
            1     2       1:6  
            2     3:b1    1:4  
///

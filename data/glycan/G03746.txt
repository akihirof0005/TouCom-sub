ENTRY       G03746                      Glycan
NODE        14
            1   GalNAc     50     2
            2   Gal        40     2
            3   GlcNAc     30     2
            4   Gal        20     2
            5   GlcNAc     10     2
            6   S           5    -2
            7   Gal         0     2
            8   GlcNAc    -10     2
            9   S         -15    -2
            10  Gal       -20     2
            11  GlcNAc    -30     2
            12  S         -35    -2
            13  Gal       -40     2
            14  Neu5Gc    -50     2
EDGE        13
            1     2:b1    1:3  
            2     3:b1    2:3  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6       5:6  
            6     7:b1    5:4  
            7     8:b1    7:3  
            8     9       8:6  
            9    10:b1    8:4  
            10   11:b1   10:3  
            11   12      11:6  
            12   13:b1   11:4  
            13   14:a2   13:3  
///

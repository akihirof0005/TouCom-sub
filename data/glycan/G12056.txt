ENTRY       G12056                      Glycan
NODE        10
            1   GlcNAc     20     2
            2   LFuc       11     5
            3   GlcNAc     11    -2
            4   Man         2    -2
            5   Man        -5     1
            6   Man        -5    -5
            7   Man       -13     1
            8   GlcNAc    -13    -5
            9   Man       -21     1
            10  Gal       -21    -5
EDGE        9
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:a1    7:2  
            9    10:b1    8:4  
///

ENTRY       G04261                      Glycan
NODE        11
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30    10
            5   Man       -30   -10
            6   GlcNAc    -40    15
            7   GlcNAc    -40     5
            8   GlcNAc    -40    -5
            9   GlcNAc    -40   -15
            10  Gal       -50     5
            11  GlcNAc    -60     5
EDGE        10
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:6  
            4     5:a1    3:3  
            5     6:b1    4:6  
            6     7:b1    4:2  
            7     8:b1    5:4  
            8     9:b1    5:2  
            9    10:b1    7:4  
            10   11:b1   10:3  
///

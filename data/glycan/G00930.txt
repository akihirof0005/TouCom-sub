ENTRY       G00930                      Glycan
NODE        5
            1   GlcNAc      0     0
            2   GlcNAc    -10     0
            3   Man       -20     0
            4   Man       -30     0
            5   Man       -40     0
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:a1    3:3  
            4     5:a1    4:2  
///

ENTRY       G02970                      Glycan
NODE        7
            1   P          19     2
            2   GlcN       13     2
            3   GlcN        4     2
            4   P           0    -1
            5   Kdo        -4     2
            6   Kdo       -12     2
            7   Kdo       -20     2
EDGE        6
            1     2:a1    1    
            2     3:b1    2:6  
            3     4       3:4  
            4     5:a2    3:6  
            5     6:a2    5:4  
            6     7:a2    6:8  
///

ENTRY       G11128                      Glycan
NODE        4
            1   Ser/Thr    15     0
            2   GalNAc      5     0
            3   Gal        -5     0
            4   LFuc      -15     0
EDGE        3
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:a1    3:2  
///

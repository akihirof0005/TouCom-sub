ENTRY       G07526                      Glycan
NODE        4
            1   LRha        0     0
            2   LRha      -10     0
            3   LRha      -20     0
            4   Fuc3NAc   -30     0
EDGE        3
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:a1    3:3  
///

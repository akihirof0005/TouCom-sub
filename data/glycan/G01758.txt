ENTRY       G01758                      Glycan
NODE        3
            1   gro-7dgalHep  11.4     0
            2   GlcNAc   -1.6     0
            3   Gal     -11.6     0
EDGE        2
            1     2:b1    1:6  
            2     3:b1    2:4  
///

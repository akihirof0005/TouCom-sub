ENTRY       G12269                      Glycan
NODE        6
            1   GalNAc     16    -1
            2   S           9     2
            3   GlcA        6    -1
            4   GalNAc     -4    -1
            5   S         -10     2
            6   L-4en-thrHexA   -17    -1
EDGE        5
            1     2       1:6  
            2     3:b1    1:3  
            3     4:b1    3:4  
            4     5       4:4  
            5     6:a1    4:3  
///

ENTRY       G06823                      Glycan
NODE        5
            1   Glc       9.1  -0.1
            2   LRha      0.1   4.9
            3   Glc       0.1  -5.1
            4   Qui      -8.9   4.9
            5   LRha     -8.9  -5.1
EDGE        4
            1     2:a1    1:6  
            2     3:b1    1:2  
            3     4:b1    2:4  
            4     5:a1    3:2  
///

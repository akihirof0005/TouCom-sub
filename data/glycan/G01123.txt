ENTRY       G01123                      Glycan
NODE        5
            1   Glc        15     0
            2   Glc         5     0
            3   Glc        -5     5
            4   Glc        -5    -5
            5   Glc       -15     5
EDGE        4
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:4  
            4     5:a1    3:4  
///

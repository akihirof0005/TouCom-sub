ENTRY       G01365                      Glycan
NODE        5
            1   Glc        16     0
            2   Glc         8     0
            3   Glc         0     0
            4   Glc        -8     0
            5   Glc       -16     0
EDGE        4
            1     2:a1    1:3  
            2     3:a1    2:3  
            3     4:a1    3:3  
            4     5:a1    4:3  
///

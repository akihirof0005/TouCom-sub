ENTRY       G02297                      Glycan
NODE        6
            1   GlcNAc     22     0
            2   GlcNAc     13     0
            3   GlcNAc      4     0
            4   GlcNAc     -5     0
            5   GlcNAc    -14     0
            6   GlcNAc    -23     0
EDGE        5
            1     2:b1    1:6  
            2     3:b1    2:6  
            3     4:b1    3:6  
            4     5:b1    4:6  
            5     6:b1    5:6  
///

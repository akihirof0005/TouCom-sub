ENTRY       G02335                      Glycan
NODE        5
            1   Cym      28.8   0.1
            2   Cym      19.8   0.1
            3   D/L2,6daraHex3Me   5.8   0.1
            4   2,6daraHex3Me -12.2   0.1
            5   2,6daraHex3Me -29.2   0.1
EDGE        4
            1     2:b1    1:4  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:4  
///

ENTRY       G05540                      Glycan
NODE        7
            1   Cer        21     0
            2   Glc        13     0
            3   Gal         5     0
            4   GlcNAc     -4     0
            5   Gal       -13     0
            6   Fuc       -21    -5
            7   GalNAc    -22     5
EDGE        6
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:4  
            4     5:b1    4:3  
            5     6:a1    5:2  
            6     7:a1    5:3  
///

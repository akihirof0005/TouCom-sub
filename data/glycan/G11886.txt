ENTRY       G11886                      Glycan
NODE        15
            1   GlcNAc     23     6
            2   LFuc       13     9
            3   GlcNAc     13     3
            4   Man         4     3
            5   Man        -5     9
            6   Man        -5    -4
            7   GlcNAc    -14     9
            8   GlcNAc    -14     0
            9   GlcNAc    -14    -9
            10  Gal       -23    12
            11  LFuc      -23     6
            12  Gal       -23     3
            13  LFuc      -23    -3
            14  Gal       -23    -6
            15  LFuc      -23   -12
EDGE        14
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:2  
            7     8:b1    6:4  
            8     9:b1    6:2  
            9    10:b1    7:4  
            10   11:a1    7:3  
            11   12:b1    8:4  
            12   13:a1    8:3  
            13   14:b1    9:4  
            14   15:a1    9:3  
///

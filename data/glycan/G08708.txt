ENTRY       G08708                      Glycan
NODE        4
            1   Glc        11     0
            2   Glc         1     5
            3   Man6Ac      1    -5
            4   3-en-eryHexA   -12    -5
EDGE        3
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    3:2  
///

ENTRY       G04650                      Glycan
NODE        8
            1   GlcN       15     1
            2   Man         6     1
            3   Man        -2     6
            4   Gal        -2    -4
            5   Man       -10     6
            6   Gal       -10     1
            7   Gal       -10    -9
            8   P         -15    10
EDGE        7
            1     2:a1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:2  
            5     6:a1    4:6  
            6     7:a1    4:2  
            7     8       5:6  
///

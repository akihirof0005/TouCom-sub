ENTRY       G09057                      Glycan
NODE        3
            1   Gal         0     0
            2   LRha      -10     0
            3   Glc2Ac3Ac4Ac6Ac   -20     0
EDGE        2
            1     2:a1    1:6  
            2     3:b1    2:4  
///

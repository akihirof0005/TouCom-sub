ENTRY       G04636                      Glycan
NODE        12
            1   LRha        0     0
            2   LRha      -10     0
            3   GlcNAc    -20     0
            4   LRha      -30     0
            5   LRha      -40     0
            6   LRha      -50     0
            7   GlcNAc    -60     0
            8   LRha      -70     0
            9   LRha      -80     0
            10  LRha      -90     0
            11  GlcNAc   -100     0
            12  LRha     -110     0
EDGE        11
            1     2:a1    1:2  
            2     3:b1    2:2  
            3     4:a1    3:3  
            4     5:a1    4:3  
            5     6:a1    5:2  
            6     7:b1    6:2  
            7     8:a1    7:3  
            8     9:a1    8:3  
            9    10:a1    9:2  
            10   11:b1   10:2  
            11   12:a1   11:3  
///

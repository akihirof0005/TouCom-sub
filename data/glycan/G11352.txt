ENTRY       G11352                      Glycan
NODE        6
            1   *          16   0.3
            2   Fuc         9   0.3
            3   LRhaAc      0   0.3
            4   Fuc        -9   4.3
            5   4daraHex   -11  -4.7
            6   *         -16   4.3
EDGE        5
            1     2:a1    1    
            2     3:b1    2:3  
            3     4:a1    3:3  
            4     5:a1    3:4  
            5     6       4:3  
BRACKET     1 -12.7   7.1 -12.7   2.1
            1  13.3  -2.4  13.3   2.6
            1 n
///

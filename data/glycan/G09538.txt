ENTRY       G09538                      Glycan
NODE        3
            1   Man         4     0
            2   GlcN       -5     5
            3   GlcN       -5    -5
EDGE        2
            1     2:b1    1:6  
            2     3:b1    1:2  
///

ENTRY       G02317                      Glycan
NODE        6
            1   Glc        23     0
            2   Glc        17     0
            3   2,3-Anhydro-Man     7     0
            4   Glc        -3     0
            5   2,3-Anhydro-Man   -13     0
            6   Glc       -23     0
EDGE        5
            1     2:a1    1:4  
            2     3:a1    2:4  
            3     4:a1    3:4  
            4     5:a1    4:4  
            5     6:a1    5:4  
///

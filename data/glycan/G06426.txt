ENTRY       G06426                      Glycan
NODE        6
            1   Man         0     0
            2   Galf      -10     5
            3   Man       -10    -5
            4   GlcA      -20     5
            5   Man       -30     5
            6   Man       -40     5
EDGE        5
            1     2:b1    1:6  
            2     3:a1    1:2  
            3     4:a1    2:2  
            4     5:b1    4:4  
            5     6:b1    5:2  
///

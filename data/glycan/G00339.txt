ENTRY       G00339                      Glycan
NODE        3
            1   Fruf        4     3
            2   Glc         4    -3
            3   Fruf       -4     3
EDGE        2
            1     3:b2    1:1  
            2     1:b2    2:a1 
///

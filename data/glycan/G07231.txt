ENTRY       G07231                      Glycan
NODE        5
            1   Ribf     12.2   0.5
            2   Gal       4.2   0.5
            3   Ribf     -3.8   5.5
            4   Gal      -3.8  -4.5
            5   Gal     -11.8   5.5
EDGE        4
            1     2:a1    1:2  
            2     3:b1    2:4  
            3     4:a1    2    
            4     5:a1    3:2  
///

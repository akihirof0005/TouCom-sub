ENTRY       G10378                      Glycan
NODE        5
            1   LCym     19.6  -0.4
            2   LCym      9.6  -0.4
            3   LCym     -0.4  -0.4
            4   LCym    -10.4  -0.4
            5   LCym    -20.4  -0.4
EDGE        4
            1     2:b1    1:4  
            2     3:a1    2:4  
            3     4:b1    3:4  
            4     5:a1    4:4  
///

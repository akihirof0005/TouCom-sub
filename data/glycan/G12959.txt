ENTRY       G12959                      Glycan
NODE        7
            1   *        20.1  -0.1
            2   GlcNAc   11.1  -0.2
            3   Gal       2.3  -0.3
            4   Glc      -4.7   3.6
            5   GlcNAc   -5.6  -4.3
            6   LRha    -12.5   3.6
            7   *       -20.1   3.6
EDGE        6
            1     2:a1    1    
            2     3:a1    2:3  
            3     4:a1    3:4  
            4     5:b1    3:3  
            5     6:a1    4:6  
            6     7       6:2  
BRACKET     1 -16.1   5.8 -16.1   1.9
            1  15.7  -2.4  15.7   1.5
            1 n
///

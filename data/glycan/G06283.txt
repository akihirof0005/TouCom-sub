ENTRY       G06283                      Glycan
NODE        5
            1   Rha        24     0
            2   Rha4NFormyl    14     0
            3   Rha4NFormyl     1     0
            4   Rha4NFormyl   -12     0
            5   Rha4NFormyl   -25     0
EDGE        4
            1     2:a1    1:2  
            2     3:a1    2:2  
            3     4:a1    3:2  
            4     5:a1    4:2  
///

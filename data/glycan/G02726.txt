ENTRY       G02726                      Glycan
NODE        11
            1   Cer        42     0
            2   Glc2Ac3Ac6Ac    33     0
            3   Gal2Ac4Ac6Ac    20     0
            4   GlcNAc3Ac6Ac     7     0
            5   Gal2Ac4Ac    -5     0
            6   GlcNAc3Ac6Ac   -15     5
            7   GlcNAc3Ac6Ac   -15    -5
            8   Gal2Ac4Ac6Ac   -28     5
            9   Gal2Ac4Ac6Ac   -28    -5
            10  Gal2Ac3Ac4Ac6Ac   -42     5
            11  Gal2Ac3Ac4Ac6Ac   -42    -5
EDGE        10
            1     2:b1    1:1  
            2     3:b1    2:4  
            3     4:b1    3:3  
            4     5:b1    4:4  
            5     6:b1    5:6  
            6     7:b1    5:3  
            7     8:b1    6:4  
            8     9:b1    7:4  
            9    10:a1    8:3  
            10   11:a1    9:3  
///

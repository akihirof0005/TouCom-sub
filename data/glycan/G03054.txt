ENTRY       G03054                      Glycan
NODE        6
            1   GlcNAc      0     0
            2   LFuc      -10     5
            3   GlcNAc    -10     0
            4   LFuc      -10    -5
            5   Man       -20     0
            6   Man       -30     0
EDGE        5
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:a1    1:3  
            4     5:b1    3:4  
            5     6:a1    5:6  
///

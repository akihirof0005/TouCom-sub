ENTRY       G12586                      Glycan
NODE        9
            1   GlcN       31     2
            2   GlcN       23     2
            3   Kdo        15     2
            4   Kdo         8     6
            5   Lgro-manHep     5    -2
            6   Lgro-manHep    -8    -2
            7   Glc       -18    -6
            8   Lgro-manHep   -21     2
            9   Glc       -31     2
EDGE        8
            1     2:b1    1:6  
            2     3:a2    2:6  
            3     4:a2    3:4  
            4     5:a1    3:5  
            5     6:a1    5:3  
            6     7:a1    6:3  
            7     8:a1    6:7  
            8     9:a1    8:7  
///

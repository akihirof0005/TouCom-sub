ENTRY       G05165                      Glycan
NODE        9
            1   LIdoA      17     0
            2   S          12    -4
            3   Glc         7     0
            4   S           2    -4
            5   LIdoA      -3     0
            6   S          -8    -4
            7   Glc       -13     0
            8   S         -18     4
            9   S         -18    -4
EDGE        8
            1     2       1:2  
            2     3:b1    1:4  
            3     4       3:4  
            4     5:a1    3:3  
            5     6       5:2  
            6     7:b1    5:4  
            7     8       7:4  
            8     9       7:3  
///

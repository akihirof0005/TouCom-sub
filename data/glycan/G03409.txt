ENTRY       G03409                      Glycan
NODE        3
            1   Glc         5     4
            2   Glc         5    -4
            3   Gal        -5     4
EDGE        2
            1     3:b1    1:4  
            2     1:b1    2:a1 
///

ENTRY       G01368                      Glycan
NODE        5
            1   GlcNAc     13     0
            2   Man         3     0
            3   Man        -5     4
            4   Man        -5    -4
            5   Man       -13     4
EDGE        4
            1     2:b1    1:4  
            2     3:a1    2:6  
            3     4:a1    2:3  
            4     5:a1    3:6  
///

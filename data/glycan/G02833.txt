ENTRY       G02833                      Glycan
NODE        10
            1   GlcNAc     20    -2
            2   GlcNAc     11     3
            3   LFuc       11    -7
            4   Man         3     3
            5   Man        -4     8
            6   Man        -4     3
            7   Xyl        -4    -2
            8   GlcNAc    -12     3
            9   LFuc      -20     8
            10  Gal       -20    -2
EDGE        9
            1     2:b1    1:4  
            2     3:a1    1:3  
            3     4:b1    2:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    4:2  
            7     8:b1    6:2  
            8     9:a1    8:6  
            9    10:b1    8:4  
///

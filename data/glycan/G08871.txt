ENTRY       G08871                      Glycan
NODE        4
            1   GlcNAc     10     2
            2   Man         0     2
            3   P          -4    -2
            4   Man       -10     2
EDGE        3
            1     2:a1    1:4  
            2     3       2:2  
            3     4:a1    2:6  
///

ENTRY       G04042                      Glycan
NODE        16
            1   GlcNAc      0     0
            2   LFuc      -10     5
            3   GlcNAc    -10    -5
            4   Man       -20    -5
            5   Man       -30     5
            6   Man       -30   -15
            7   GlcNAc    -40    10
            8   GlcNAc    -40     0
            9   GlcNAc    -40   -10
            10  GlcNAc    -40   -20
            11  Gal       -50     0
            12  Gal       -50   -10
            13  Gal       -50   -20
            14  Neu5Ac    -60     0
            15  Neu5Ac    -60   -10
            16  Neu5Ac    -60   -20
EDGE        15
            1     2:a1    1:6  
            2     3:b1    1:4  
            3     4:b1    3:4  
            4     5:a1    4:6  
            5     6:a1    4:3  
            6     7:b1    5:6  
            7     8:b1    5:2  
            8     9:b1    6:4  
            9    10:b1    6:2  
            10   11:b1    8:3  
            11   12:b1    9:3  
            12   13:b1   10:3  
            13   14:a2   11:3  
            14   15:a2   12:3  
            15   16:a2   13:3  
///

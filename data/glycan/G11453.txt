ENTRY       G11453                      Glycan
NODE        13
            1   Asn        27     3
            2   GlcNAc     20     3
            3   LFuc       12     8
            4   GlcNAc     12    -2
            5   Man         4    -2
            6   Man        -3     3
            7   Man        -3    -7
            8   GlcNAc    -11     3
            9   GlcNAc    -11    -7
            10  GalNAc    -19     3
            11  GalNAc    -19    -7
            12  Neu5Ac    -28     3
            13  Neu5Ac    -28    -7
EDGE        12
            1     2:b1    1    
            2     3:a1    2:6  
            3     4:b1    2:4  
            4     5:b1    4:4  
            5     6:a1    5:6  
            6     7:a1    5:3  
            7     8:b1    6:2  
            8     9:b1    7:2  
            9    10:b1    8:4  
            10   11:b1    9:4  
            11   12:a2   10:6  
            12   13:a2   11:6  
///

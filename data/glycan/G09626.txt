ENTRY       G09626                      Glycan
NODE        3
            1   D/LGlcNAc   6.2  -2.5
            2   S        -2.4   1.8
            3   Gal      -5.8  -2.5
EDGE        2
            1     2       1:6  
            2     3:b1    1:4  
///

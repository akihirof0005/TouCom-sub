ENTRY       G07874                      Glycan
NODE        4
            1   Glc         0     0
            2   GlcNAc    -10     0
            3   Glc       -20     0
            4   GlcNAc    -30     0
EDGE        3
            1     2:b1    1:4  
            2     3:b1    2:3  
            3     4:b1    3:4  
///

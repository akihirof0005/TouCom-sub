ENTRY       G10483                      Glycan
NODE        3
            1   *           8     0
            2   GlcNAc      0     0
            3   *          -9     0
EDGE        2
            1     2:b1    1    
            2     3       2:4  
BRACKET     1  -4.0   2.0  -4.0  -2.0
            1   5.0  -2.0   5.0   2.0
            1 n
///

ENTRY       G07692                      Glycan
NODE        8
            1   GlcN       11     1
            2   S          11    -3
            3   S           7     4
            4   S           7    -2
            5   GlcA        1     1
            6   GlcN       -8     1
            7   S         -12     4
            8   S         -12    -2
EDGE        7
            1     2       1:2  
            2     3       1:6  
            3     4       1:3  
            4     5:b1    1:4  
            5     6:a1    5:4  
            6     7       6:6  
            7     8       6:2  
///

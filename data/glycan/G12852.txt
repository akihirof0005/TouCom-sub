ENTRY       G12852                      Glycan
NODE        7
            1   *          15     0
            2   Glc         7     0
            3   Glc         0     4
            4   Man6Ac     -1    -4
            5   *          -8     4
            6   GlcA       -9    -4
            7   Man       -16    -4
EDGE        6
            1     2:b1    1    
            2     3:b1    2:4  
            3     4:a1    2:3  
            4     5       3:4  
            5     6:b1    4:2  
            6     7:b1    6:4  
BRACKET     1  -4.0   6.0  -4.0   3.0
            1  11.0  -1.0  11.0   2.0
            1 n
///

require "json"
require 'net/http'
require 'uri'

json = File.read('./snfgApi.json')

hash = JSON.parse(json)
cids = ""
name = {}
hash[0].each{|k,v|
  if(v)
    api1 = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/" + k + "/synonyms/JSON"
    uri = URI.parse(api1)
    json = Net::HTTP.get(uri)
    result = JSON.parse(json)
    cid = result["InformationList"]["Information"][0]["CID"]
    cids = cid.to_s + "," + cids
    name[cid] = k
  end
}
cids.chop

api2 = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/"+ cids + "/property/MolecularFormula,Charge,XLogP,TPSA,MonoisotopicMass/JSON"
uri = URI.parse(api2)
json = Net::HTTP.get(uri)
result = JSON.parse(json)

index={}
itemes = result["PropertyTable"]["Properties"]
itemes.each{|v|

if hash[1][name[v["CID"]]].match(/^\[\]\[(.+)\]{}$/)
  name_a = $1
end
if hash[2][name[v["CID"]]].match(/^\[\]\[(.+)\]{}$/)
  name_b = $1
end
index[name_a] = v
index[name_b] = v
}
json = JSON.pretty_generate(index)

File.write('./index-pubchem.json',json)

/**
 * 
 */
package kcam;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * @author aoki
 *
 */
public class KCaMTest {

	KCaM kcam;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	/**
	 * Test method for {@link kcam.KCaM#loop(java.lang.String[])}.
	 */
	@Test
	public void testSample() throws Exception {
		thrown.expect(NullPointerException.class);
		KCaM_param gl1 = null;
		KCaM_param gl2 = null;

		kcam.run(gl1, gl2); // probably fail.
	}

}

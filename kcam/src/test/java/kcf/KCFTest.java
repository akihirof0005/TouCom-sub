package kcf;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class KCFTest {

	@Test
	public void test() {
		String kcfdata = "ENTRY       G00005                      Glycan\n" + 
				"NODE        6\n" + 
				"            1   PP-Dol     15     1\n" + 
				"            2   GlcNAc      8     1\n" + 
				"            3   GlcNAc      0     1\n" + 
				"            4   Man        -9     1\n" + 
				"            5   Man       -16     7\n" + 
				"            6   Man       -16    -6\n" + 
				"EDGE        5\n" + 
				"            1     2:a1    1    \n" + 
				"            2     3:b1    2:4  \n" + 
				"            3     4:b1    3:4  \n" + 
				"            4     5:a1    4:6  \n" + 
				"            5     6:a1    4:3  \n" + 
				"///";
		
		KCF kcf1 = new KCF(kcfdata);
		KCF kcf2 = new KCF(kcfdata);
		System.out.println(kcf1.toString());
		System.out.println(kcf2.toString());
		Assert.assertTrue(kcf1.equals(kcf2));
	}
	@Test
	public void test2() {
		String kcfdata1 = "ENTRY     XYZ          Glycan\n" + 
				"NODE      5\n" + 
				"  1     GlcNAc     15.0     7.0\n" + 
				"  2     GlcNAc      8.0     7.0\n" + 
				"  3     Man         1.0     7.0\n" + 
				"  4     Man        -6.0    12.0\n" + 
				"  5     Man        -6.0     2.0\n" + 
				"EDGE      4\n" + 
				"  1     2:b1       1:4\n" + 
				"  2     3:b1       2:4\n" + 
				"  3     5:a1       3:3\n" + 
				"  4     4:a1       3:6\n" + 
				"///";
		String kcfdata2 = "ENTRY     XYZ          Glycan\n" + 
				"NODE      5\n" + 
				"  1     GlcNAc     10.0     7.0\n" + 
				"  2     GlcNAc      8.0     7.0\n" + 
				"  3     Man         1.0     7.0\n" + 
				"  4     Man        -5.0    12.0\n" + 
				"  5     Man        -6.0     3.0\n" + 
				"EDGE      4\n" + 
				"  1     5:a1       3:3\n" + 
				"  2     4:a1       3:6\n" + 
				"  3     2:b1       1:4\n" + 
				"  4     3:b1       2:4\n" + 
				"///";
		KCF kcf1 = new KCF(kcfdata1);
		KCF kcf2 = new KCF(kcfdata2);
		System.out.println(kcf1.toKCFString());
		System.out.println(kcf2.toKCFString());
		Assert.assertTrue(kcf1.equals(kcf2));
	}
}

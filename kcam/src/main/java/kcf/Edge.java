package kcf;

public class Edge extends Object {
	public int number;

	public int parentNum;

	public int childNum;

	public String name;

	public Edge(int number, int parentNum, int childNum, String name) {
		this.number = number;
		this.parentNum = parentNum;
		this.childNum = childNum;
		this.name = name;
	}

	public int getChildNum() {
		return childNum;
	}

	public void setChildNum(int childNum) {
		this.childNum = childNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getParentNum() {
		return parentNum;
	}

	public void setParentNum(int parentNum) {
		this.parentNum = parentNum;
	}

	public String toString() {
		String result = childNum + " " + name + " " + parentNum;
		return result;
	}

	public String getString() {
		String[] anomeric = name.split("-");
		String result = childNum + ":" + anomeric[0] + "    " + parentNum;
		if (anomeric.length == 2) {
			result += ":" + anomeric[1];
		}
		return result;
	}
	

	
}

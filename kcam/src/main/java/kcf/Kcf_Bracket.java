package kcf;

import java.util.Vector;

public class Kcf_Bracket extends Object {

	double x1_1,x2_1,y2_1,y1_1;
	double x1_1_u, y1_1_u, x2_1_d, y2_1_d;

	double x1_2,y1_2,y2_2,x2_2;
	double x1_2_u, y1_2_u, x2_2_d, y2_2_d;
	
	public double x = -1;

	public double y = -1;

	public int num;

	public String repeat_word = "";
	
	public Vector open = new Vector();
	public Vector close = new Vector();

	public Kcf_Bracket(int number) {
		num = number;
	}

	public void setXY_open(String tx1, String ty1, String tx2, String ty2) {
		x1_1 = Double.parseDouble(tx1);
		y1_1 = Double.parseDouble(ty1);
		x2_1 = Double.parseDouble(tx2);
		y2_1 = Double.parseDouble(ty2);
		open.add(Double.toString(x1_1));
		open.add(Double.toString(y1_1));
		open.add(Double.toString(x2_1));
		open.add(Double.toString(y2_1));
	}	
	public void setopen(Vector openVector){
		open = openVector;
	}
	public void setXY_open_up(double tx1, double ty1, double tx2,double ty2){
		x1_1 = tx1;
		y1_1 = ty1;
		x1_1_u = tx2;
		y1_1_u = ty2;
	}
	public void setXY_open_bottom(double tx1, double ty1, double tx2,double ty2){
		x2_1 = tx1;
		y2_1 = ty1;
		x2_1_d = tx2;
		y2_1_d = ty2;
	}
	public void setXY_close_up(double tx1, double ty1, double tx2,double ty2){
		x1_2 = tx1;
		y1_2 = ty1;
		x1_2_u = tx2;
		y1_2_u = ty2;
	}
	public void setXY_close_bottom(double tx1, double ty1, double tx2,double ty2){
		x2_2 = tx1;
		y2_2 = ty1;
		x2_2_d = tx2;
		y2_2_d = ty2;
	}
	
	public void setclose(Vector closeVector){
		close = closeVector;
	}

	public void setXY_close(String tx1, String ty1, String tx2, String ty2) {
		x1_2 = Double.parseDouble(tx1);
		y1_2 = Double.parseDouble(ty1);
		x2_2 = Double.parseDouble(tx2);
		y2_2 = Double.parseDouble(ty2);
		close.add(Double.toString(x1_2));
		close.add(Double.toString(y1_2));
		close.add(Double.toString(x2_2));
		close.add(Double.toString(y2_2));
	}
	public void setXY_close_num(double tx1, double ty1, double tx2, double ty2) {
		x1_2 = tx1;
		y1_2 = ty1;
		x2_2 = tx2;
		y2_2 = ty2;
	}


	
	public void setrepeatword (String re_word){
		repeat_word = re_word;
	}
	public String getrepeatword (){
		return repeat_word;
	}
	public Vector getXY_open(){
		return open;
	}
	public Vector getXY_close(){
		return close;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getRepeat_word() {
		return repeat_word;
	}

	public void setRepeat_word(String repeat_word) {
		this.repeat_word = repeat_word;
	}

	public double getX1_1() {
		return x1_1;
	}

	public double getX1_1_u() {
		return x1_1_u;
	}

	public double getX1_2() {
		return x1_2;
	}

	public double getX1_2_u() {
		return x1_2_u;
	}

	public double getX2_1() {
		return x2_1;
	}

	public double getX2_1_d() {
		return x2_1_d;
	}

	public double getX2_2() {
		return x2_2;
	}

	public double getX2_2_d() {
		return x2_2_d;
	}

	public double getY1_1() {
		return y1_1;
	}

	public double getY1_1_u() {
		return y1_1_u;
	}

	public double getY1_2() {
		return y1_2;
	}

	public double getY1_2_u() {
		return y1_2_u;
	}

	public double getY2_1() {
		return y2_1;
	}

	public double getY2_1_d() {
		return y2_1_d;
	}

	public double getY2_2() {
		return y2_2;
	}

	public double getY2_2_d() {
		return y2_2_d;
	}

}
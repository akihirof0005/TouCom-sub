package kcf;

import java.util.*;

public class Kcf_Entry {
	String entry, class_name;

	boolean isRight;

	Vector nodes;

	Vector edges;

	Vector bracket;

	Vector tm_bracket = new Vector();;

	String kcf_str;

	HashMap hNodesLy;

	HashMap hNodes;

	HashMap hEdges;

	HashMap hEdgesLy;

	HashMap hEdgesPa;

	HashMap hEdgesCh;

	HashMap nodeMap, edgeMap;

	HashMap hlink , hlinkLy;

	HashMap hlinkpc, hlinkcp;
	
	Vector roots;

	int root_num;

	Vector ok_node;
	
	String rootnode;
	
	long start, stop, diff;
	int count = 0;

	public HashMap getHEdges() {
		return hEdges;
	}

	public void setHEdges(HashMap edges) {
		hEdges = edges;
	}

	public HashMap getHEdgesCh() {
		return hEdgesCh;
	}

	public void setHEdgesCh(HashMap edgesCh) {
		hEdgesCh = edgesCh;
	}

	public HashMap getHEdgesLy() {
		return hEdgesLy;
	}

	public void setHEdgesLy(HashMap edgesLy) {
		hEdgesLy = edgesLy;
	}

	public HashMap getHEdgesPa() {
		return hEdgesPa;
	}

	public void setHEdgesPa(HashMap edgesPa) {
		hEdgesPa = edgesPa;
	}

	public HashMap getHNodes() {
		return hNodes;
	}

	public void setHNodes(HashMap nodes) {
		hNodes = nodes;
	}

	public HashMap getHNodesLy() {
		return hNodesLy;
	}

	public void setHNodesLy(HashMap nodesLy) {
		hNodesLy = nodesLy;
	}

	public void setEdges(Vector edges) {
		this.edges = edges;
	}

	public void setNodes(Vector nodes) {
		this.nodes = nodes;
	}

	public Kcf_Entry(String data, String e) {
		initialize(data, e);
	}
//	public void time(String word) {
//		count++;
//		stop = System.currentTimeMillis() - start  ;
//		diff = stop - diff;
//		System.out.println(word+"KCFM "+count +" : "+ stop + "  " + diff);
//		diff = stop;
//	}
	public void initialize(String data, String e) {
		start = System.currentTimeMillis();
		diff = 0;
	//	time("");
		isRight = true;
		entry = e;
		nodes = new Vector();
		edges = new Vector();
		kcf_str = data;
		class_name = new String();
		pri_getNodesAndEdges();
	//	time("");
		if (tm_bracket != null) {
			bracket = new Vector();
			if(tm_bracket.size()%3 != 0){
				System.out.println("ERROR: BRACKET format is bad "+entry);
				System.exit(0);
			}
			for (int i = 0; i < tm_bracket.size(); i++) {
				Kcf_Bracket bk1 = (Kcf_Bracket) tm_bracket.elementAt(i);
				Kcf_Bracket bk2 = (Kcf_Bracket) tm_bracket.elementAt(i + 1);
				Kcf_Bracket bk3 = (Kcf_Bracket) tm_bracket.elementAt(i + 2);
				Kcf_Bracket bk = new Kcf_Bracket(bk1.num);
				bk.setopen(bk1.getXY_open());
				bk.setclose(bk2.getXY_open());
				bk.setRepeat_word(bk3.getrepeatword());
				i = i + 2;
				bracket.add(bk);
			}
		}
	//	time(" ");
		ok_node = new Vector();
		if (root_num != -1)
			createNode_Edge(0, root_num);
		if (root_num == -1 || nodeMap.size() - nodes.size() != 0) {
			isRight = false;
			System.out.println("ERROR in K_Entry initialize: bad format:"
					+ entry+"\n"+data);
			return;
		}
		if(nodes.size() -1 != edges.size()){
			System.out.println("ERROR in K_Entry initialize: bad format:"
					+ entry+"\n"+data);
			return;
		}
		// hash
		hNodesLy = new HashMap();
		hNodes = new HashMap();
		hEdgesLy = new HashMap();
		hEdges = new HashMap();
		hEdgesPa = new HashMap();
		hEdgesCh = new HashMap();
		hlink = new HashMap();
		hlinkpc = new HashMap();
		hlinkcp = new HashMap();
		hlinkLy = new HashMap();
		roots = new Vector();
	//	time("KKK ");
		createHash();
	//	time("");
		createlink();
	//	time("");
	}

	boolean createlink() {
		
		
		
		for (int i = 0; i < edges.size(); i++) {// edge
			
			Kcf_Edge e = (Kcf_Edge) edges.elementAt(i);
			Node pnode = (Node)hNodes.get(e.getParentNum());
			Node cnode = (Node)hNodes.get(e.getChildNum());
			String pname = pnode.getName().toLowerCase();
			String cname = cnode.getName().toLowerCase();
			
			String tmlinkst =  cname +"@" +e.getName().toLowerCase()+ "@" + pname ;
			hlink.put(e.getNumber(), tmlinkst);
			hlinkLy.put(e.getNumber() , e.getLayer());
			
			if(e.getParentNum() == root_num){
				roots.add(e.getNumber());
			}
			
			Vector tmcnums = (Vector) hEdgesPa.get(e.getChildNum());
			if (tmcnums == null)
				tmcnums = new Vector();
			for (int j = 0; j < tmcnums.size(); j++) {
				Edge e2 = (Edge) tmcnums.elementAt(j);
				Vector v = (Vector) hlinkpc.get(e.getNumber());
				if (v == null)
					v = new Vector();
				v.add(e2.getNumber());
				hlinkpc.put(e.getNumber(), v);
				hlinkcp.put(e2.getNumber(), e.getNumber());
			}
		}
		return true;
	}
	
	boolean createlink_NodeEdge() {
		
		for (int i = 0; i < edges.size(); i++) {// edge
			Edge e = (Edge) edges.elementAt(i);
			Node pnode = (Node)hNodes.get(e.getParentNum());
			Node cnode = (Node)hNodes.get(e.getChildNum());
			String pname = pnode.getName().toLowerCase();
			String cname = cnode.getName().toLowerCase();
			
			String tmlinkst =  cname +"@" +e.getName().toLowerCase();
			hlink.put(e.getNumber(), tmlinkst);

			if(e.getParentNum() == root_num){
				roots.add(e.getNumber());
				hlink.put(0, tmlinkst);
			}
			
			Vector tmcnums = (Vector) hEdgesPa.get(e.getChildNum());
			if (tmcnums == null)
				tmcnums = new Vector();
			for (int j = 0; j < tmcnums.size(); j++) {
				Edge e2 = (Edge) tmcnums.elementAt(j);
				Vector v = (Vector) hlinkpc.get(e.getNumber());
				if (v == null)
					v = new Vector();
				v.add(e2.getNumber());
				hlinkpc.put(e.getNumber(), v);
				hlinkcp.put(e2.getNumber(), e.getNumber());
			}
		}
		return true;
	}

	void createHash() {
		for (int i = 0; i < nodes.size(); i++) {
			Kcf_Node n = (Kcf_Node) nodes.elementAt(i);
			hNodes.put(Integer.valueOf(n.getNum()), n);
			Vector v = (Vector) hNodesLy.get(Integer.valueOf(n
					.getLayer()));
			if (v == null)
				v = new Vector();
			v.add(n);
			hNodesLy.put(Integer.valueOf(n.getLayer()), v);
		}
		for (int i = 0; i < edges.size(); i++) {
			Kcf_Edge e = (Kcf_Edge) edges.elementAt(i);
			hEdges.put(Integer.valueOf(e.getNumber()), e);
			Vector v = (Vector) hEdgesLy.get(Integer.valueOf(e
					.getLayer()));
			if (v == null)
				v = new Vector();
			v.add(e);
			hEdgesLy.put(Integer.valueOf(e.getLayer()), v);
			v = (Vector) hEdgesPa.get(Integer.valueOf(e
					.getParentNum()));
			if (v == null)
				v = new Vector();
			v.add(e);
			hEdgesPa
					.put(Integer.valueOf(e.getParentNum()), v);
			hEdgesCh.put(Integer.valueOf(e.getChildNum()), e);
		}
	}

	public void update() {
		initialize(reCreateKCF(), entry);
	}

	public void createNode_Edge(int layer, int parent) {
		String[] node_lines = ((String) nodeMap.get(Integer.valueOf(parent))).split(" ");
		int last_index = node_lines.length-1;
		double y_po = Double.parseDouble(node_lines[last_index]);
		double x_po = Double.parseDouble(node_lines[last_index-1]);
		
		String n_name = node_lines[0];
		for(int i=0;i<node_lines.length-3;i++){
			String parts = node_lines[1+i];
			n_name += " " + parts;
		}
		
		nodes.add(new Kcf_Node(layer, parent, n_name, x_po, y_po));
		ok_node.add(Integer.valueOf(parent));
		Vector myedges = (Vector) edgeMap.get(Integer.valueOf(parent));
		// for (int i=0; i<edges.size(); i++) {
		if (myedges != null) {
			Iterator it = myedges.iterator();
			while (it.hasNext()) {
				String edge = (String) it.next();
				int number = Integer.parseInt(edge.split(" ")[0]);
				int child = Integer.parseInt(edge.split(" ")[1]);
				String name = edge.split(" ")[2];
				if (ok_node.contains(Integer.valueOf(child)) == false) {
					edges.add(new Kcf_Edge(layer, number, parent, child, name));
					createNode_Edge(layer + 1, child);
				}
			}
		}
	}

	public Vector delNode(Vector arr) {
		Vector del_result = new Vector();
		Vector del_node = new Vector();
		Vector del_edge = new Vector();
		int del_num, node_num, del_edge_num;
		for (int i = 0; i < nodes.size(); i++) {
			Kcf_Node node = (Kcf_Node) nodes.elementAt(i);
			if (arr.contains(node) == false) {
				del_result.add("del::" + entry + "::" + node.getName());
				del_node.add(node);
				del_num = node.getNum();
				HashMap node_change = new HashMap();
				for (int j = 0; j < nodes.size(); j++) {
					Kcf_Node in_node = (Kcf_Node) nodes.elementAt(j);
					node_num = in_node.getNum();
					if (node_num > del_num) {
						node_change.put(Integer.valueOf(node_num), Integer.valueOf(node_num - 1));
						in_node.setNum(node_num - 1);
					}
				}
				for (int j = 0; j < edges.size(); j++) {
					Kcf_Edge edge = (Kcf_Edge) edges.elementAt(j);
					if (edge.getParentNum() == del_num
							|| edge.getChildNum() == del_num) {
						if (del_edge.contains(edge) == false) {
							del_edge.add(edge);
							del_edge_num = edge.getNumber();
							for (int k = 0; k < edges.size(); k++) {
								Kcf_Edge in_edge = (Kcf_Edge) edges
										.elementAt(k);
								if (in_edge.getNumber() > del_edge_num) {
									in_edge.setNumber(in_edge.getNumber() - 1);
								}
							}
						}
					}
				}
				for (int j = 0; j < edges.size(); j++) {
					Kcf_Edge edge = (Kcf_Edge) edges.elementAt(j);
					if (node_change.containsKey(Integer.valueOf(edge.getParentNum())))
						edge.setParentNum(((Integer) node_change
								.get(Integer.valueOf(edge
										.getParentNum()))).intValue());
					if (node_change.containsKey(Integer.valueOf(edge.getChildNum())))
						edge
								.setChildNum(((Integer) node_change.get(Integer
										.valueOf(edge
												.getChildNum()))).intValue());

				}
			}
		}
		nodes.removeAll(del_node);
		edges.removeAll(del_edge);
		update();
		return del_result;
	}

	public String reCreateKCF() {
		HashMap temp_arr = new HashMap();
		String kcfStr = Str.ljust("ENTRY", 12) + Str.ljust(entry, 18)
				+ "Glycan\n";

		for (int i = 0; i < nodes.size(); i++) {
			Kcf_Node node = (Kcf_Node) nodes.elementAt(i);
			temp_arr
					.put(Integer.valueOf(node.getNum()), node);
		}
		kcfStr += Str.ljust("NODE", 12) + nodes.size() + "\n";
		for (int i = 0; i < nodes.size(); i++) {
			Kcf_Node node = (Kcf_Node) temp_arr.get(Integer.valueOf(i + 1));
			kcfStr += node.createKCF();
		}
		temp_arr.clear();
		for (int i = 0; i < edges.size(); i++) {
			Kcf_Edge edge = (Kcf_Edge) edges.elementAt(i);
			temp_arr.put(Integer.valueOf(edge.getNumber()),
					edge);
		}
		kcfStr += Str.ljust("EDGE", 12) + edges.size() + "\n";
		for (int i = 0; i < edges.size(); i++) {
			Kcf_Edge edge = (Kcf_Edge) temp_arr.get(Integer.valueOf(i + 1));
			if (edge.createKCF() != null) {
				kcfStr += edge.createKCF();
			} else {
				System.out.println("error : not tree");
				return "error : not tree";
			}
		}
		kcfStr += "///";
		return kcfStr;
	}

	public String Linklist_to_KCF(Vector linklist) {
		Vector edge2 = new Vector();
		Vector node2 = new Vector();
		
		HashMap hnodeid = new HashMap();
		for (int i = 0; i < linklist.size(); i = i + 2) {
			int id =Integer.parseInt(linklist.elementAt(i).toString());
			Edge e = (Edge)hEdges.get(id);
			edge2.add(e);
			
	
			Node n = (Node) hNodes.get(e.getParentNum());
			Node n2 = (Node) hNodes.get(e.getChildNum());
			if (!node2.contains(n)) {
				node2.add(n);
				hnodeid.put(e.getParentNum(),node2.size());
				String kcf = Str.rjust(" ", 12)
				+ Str.ljust(new Integer(i).toString(), 4)
				+ Str.ljust(n.getName(), 7) + " ";
		kcf += Str.rjust(new Double(n.x).toString(), 5) + " "
				+ Str.rjust(new Double(n.y).toString(), 5) + "\n";
	//	System.out.println(kcf);

			}
			if (!node2.contains(n2)){
				node2.add(n2);
				hnodeid.put(e.getChildNum(),node2.size());
				String kcf = Str.rjust(" ", 12)
				+ Str.ljust(new Integer(i).toString(), 4)
				+ Str.ljust(n2.getName(), 7) + " ";
		kcf += Str.rjust(new Double(n2.x).toString(), 5) + " "
				+ Str.rjust(new Double(n2.y).toString(), 5) + "\n";
	//	System.out.println(kcf);
			}
		}
		
		if(node2.size() != edge2.size()+1 ){
			System.out.println("error node or edge size is bad");
			return "error node or edge size is bad";
		}
		
		String kcf = Str.ljust("ENTRY", 12) + Str.ljust(entry, 18) + "Glycan\n";

		kcf += Str.ljust("NODE", 12) + node2.size() + "\n";

		int num = 1;
		for (int i = 0; i < node2.size(); i++) {
			Kcf_Node node = (Kcf_Node)node2.elementAt(i);
				kcf += Str.rjust(" ", 12)
						+ Str.ljust(new Integer(num).toString(), 4)
						+ Str.ljust(node.getName(), 7) + " ";
				kcf += Str.rjust(new Double(node.x).toString(), 5) + " "
						+ Str.rjust(new Double(node.y).toString(), 5) + "\n";
				num++;
			
		}

		kcf += Str.ljust("EDGE", 12) + edge2.size() + "\n";

		num =1;
		for (int i = 0; i < edge2.size(); i++) {
				Kcf_Edge edge = (Kcf_Edge) edge2.elementAt(i);
				int pnum = Integer.parseInt(hnodeid.get(edge.getParentNum()).toString());
				int cnum = Integer.parseInt(hnodeid.get(edge.getChildNum()).toString());
				if (edge.createKCFforlink(pnum,cnum,num) != null) {
					kcf += edge.createKCFforlink(pnum,cnum,num);
					num++;
				} else {
					System.out.println("error : not tree");
					return "error : not tree";
				}
		}
		kcf += "///";
		return kcf;
	}

	public Vector getNodes() {
		return nodes;
	}

	public Vector getEdges() {
		return edges;
	}

	public String getEntry() {
		return entry;
	}

	public void setEntry(String entry) {
		this.entry = entry;
	}

	public boolean isRight() {
		return isRight;
	}

	public void setRight(boolean isRight) {
		this.isRight = isRight;
	}

	public String getKcf_str() {
		return kcf_str;
	}

	public void setKcf_str(String kcf_str) {
		this.kcf_str = kcf_str;
	}

	private void pri_getNodesAndEdges() {
		int mode = 0;
		root_num = -1;
		double root_x = -10000;
		nodeMap = new HashMap();
		edgeMap = new HashMap();
		String[] lines = this.kcf_str.split("\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			if (line.startsWith("NODE")) {
				mode = 1;
				continue;
			} else if (line.startsWith("EDGE")) {
				mode = 2;
				continue;
			} else if (line.startsWith("BRACKET")) {
				mode = 3;
				// continue;
			} else if (line.startsWith("ENTRY")) {
				line = Str.cleanDelim(line);
				String[] temp_line = line.split(" ");
				if(temp_line.length == 3){
					entry = temp_line[1];
				}
				continue;
			} else if (line.startsWith("///")) {
				mode = 0;
				continue;
			} else if (line.startsWith("CLASS")) {
				line = Str.cleanDelim(line);
				String[] temp_line = line.split(" ");
				class_name = temp_line[temp_line.length - 1];
				continue;
			}

			switch (mode) {
			case 1: {
				line = Str.cleanDelim(line);
				String[] temp_line = line.split(" ");
				int num = Integer.parseInt(temp_line[0]);
				
				int y_index = temp_line.length -1;
				String y_point = temp_line[y_index];
				
				while(!this.chkNumeric(y_point)){
					y_index--;
					y_point = temp_line[y_index];
					
					if(y_index < 0){
						break;
					}
				}
				
				double x_point = Double.parseDouble(temp_line[y_index-1]);
				String nodeName = temp_line[1];
				if(y_index > 3){
					for(int j=0;j<y_index-3;j++){
						String parts = temp_line[2+j];
						nodeName += " " + parts;
					}
				}
				
				if (root_x < x_point) {
					root_x = x_point;
					root_num = num;
					rootnode = nodeName;
				}
				/*
				if(temp_line.length > 4){
					System.out.println(Integer.valueOf(num) + " " +
						nodeName + " " + x_point + " " + y_point);
				}*/
				
				nodeMap.put(Integer.valueOf(num),
						nodeName + " " + x_point + " " + y_point);
				break;
			}
			case 2: {
				line = Str.cleanDelim(line);
				String[] temp_line = line.split(" ");
				String number = temp_line[0];
				int num_l = -1;
				if (temp_line.length != 1) {
					num_l = Integer.valueOf(temp_line[1].split(":")[0])
							.intValue();
				} else {
					System.out.println(this.entry + ": line is nothing");
					return;
				}
				String name_l = "";
				if (temp_line[1].split(":").length > 1)
					name_l = temp_line[1].split(":")[1];
				int num_r = Integer.valueOf(temp_line[2].split(":")[0])
						.intValue();
				String name_r = "";
				if (temp_line[2].split(":").length > 1)
					name_r = temp_line[2].split(":")[1];
				String str_l = number + " " + num_r + " "
				// + ((String) nodeMap.get(Integer.valueOf(num_r)))
						// .split(" ")[0]
						// + Str.OPEN_P
						+ name_r + "-" + name_l;
				// + Str.CLOSE_P
				// + ((String) nodeMap.get(Integer.valueOf(num_l)))
				// .split(" ")[0];
				String str_r = number + " " + num_l + " "
				// + ((String) nodeMap.get(Integer.valueOf(num_l)))
						// .split(" ")[0]
						// + Str.OPEN_P
						+ name_l + "-" + name_r;
				// + Str.CLOSE_P
				// + ((String) nodeMap.get(Integer.valueOf(num_r)))
				// .split(" ")[0];
				Vector v = (Vector) edgeMap.get(Integer.valueOf(num_l));
				if (v == null)
					v = new Vector();
				v.add(str_l);
				edgeMap.put(Integer.valueOf(num_l), v);
				v = (Vector) edgeMap.get(Integer.valueOf(num_r));
				if (v == null)
					v = new Vector();
				v.add(str_r);
				edgeMap.put(Integer.valueOf(num_r), v);
				break;
			}
			case 3: {
				line = Str.cleanDelim(line);
				String[] temp_line = line.split(" ");

				if (temp_line.length == 6) {
					int x = Integer.parseInt(temp_line[1]);
					Kcf_Bracket bk = new Kcf_Bracket(x);
					bk.setXY_open(temp_line[2], temp_line[3], temp_line[4],
							temp_line[5]);
					tm_bracket.add(bk);
				} else if (temp_line.length == 5) {
					int x = Integer.parseInt(temp_line[0]);
					Kcf_Bracket bk = new Kcf_Bracket(x);
					bk.setXY_open(temp_line[1], temp_line[2], temp_line[3],
							temp_line[4]);
					tm_bracket.add(bk);
				} else if (temp_line.length == 2) {
					int x = Integer.parseInt(temp_line[0]);
					Kcf_Bracket bk = new Kcf_Bracket(x);
					bk.setrepeatword(temp_line[1]);
					tm_bracket.add(bk);
				}
			}
			}
		}

	}

	public String getClass_name() {
		return class_name;
	}

	public Vector getBracket() {
		return bracket;
	}

	public HashMap getHlink() {
		return hlink;
	}

	public HashMap getHlinkcp() {
		return hlinkcp;
	}

	public HashMap getHlinkpc() {
		return hlinkpc;
	}

	public Vector getRoots() {
		return roots;
	}

	public String getRootnode() {
		return rootnode;
	}

	public void setRootnode(String rootnode) {
		this.rootnode = rootnode;
	}

	public HashMap getHlinkLy() {
		return hlinkLy;
	}
	
	
	private boolean chkNumeric(String str){
		try{
			Double.parseDouble(str);
			return true;
		}catch(NumberFormatException e){
			return false;
		}
	}
	
}
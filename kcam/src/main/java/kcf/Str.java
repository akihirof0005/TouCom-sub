package kcf;

public class Str {
	public static String OPEN_P = "=";

	public static String CLOSE_P = "~";

	/** toString */
	public static String join(String[] str, String delim) {
		String result = "";
		for (int i = 0; i < str.length; i++) {
			result += str[i] + delim;
		}
		return result;
	}

	public static String join(int[][] x, String delim) {
		String result = "";
		for (int i = 0; i < x.length; i++) {
			result += join(x[i], delim);
		}
		return result;
	}

	public static String join(int[] x, String delim) {
		String result = "";
		for (int i = 0; i < x.length; i++) {
			result += String.valueOf(x[i]) + delim;
		}
		return result;
	}

	public static String join(double[] x, String delim) {
		String result = "";
		for (int i = 0; i < x.length; i++) {
			result += String.valueOf(x[i]) + delim;
		}
		return result + "\n";
	}

	public static String join(double[][] x, String delim) {
		String result = "";
		for (int i = 0; i < x.length; i++) {
			result += join(x[i], delim);
		}
		return result;
	}

	public static String join(double[][][] x, String delim) {
		String result = "";
		for (int i = 0; i < x.length; i++) {
			result += "***" + String.valueOf(i) + "***\n";
			result += join(x[i], delim);
		}
		return result;
	}

	/** output */
	public static void pArray(double[] x, String title) {
		System.out.println(title);
		System.out.print(join(x, "  "));
	}

	public static void pArray(double[][] x, String title) {
		System.out.println(title);
		System.out.print(join(x, "  "));
	}

	public static void pArray(double[][][] x, String title) {
		System.out.println(title);
		System.out.print(join(x, "  "));
	}

	// ???
	public static String ljust(String str, int x) {
		while (str.length() < x) {
			str += " ";
		}
		return str;
	}

	// ???
	public static String rjust(String str, int x) {
		while (str.length() < x) {
			str = " " + str;
		}
		return str;
	}

	public static String ljust(int s, int x) {
		String str = new Integer(s).toString();
		while (str.length() < x) {
			str += " ";
		}
		return str;
	}

	// ???
	public static String rjust(int s, int x) {
		String str = new Integer(s).toString();
		while (str.length() < x) {
			str = " " + str;
		}
		return str;
	}

	public static String cleanDelim(String data) {
		data = data.trim();
		while (data.indexOf("  ") != -1)
			data = data.replaceAll("  ", " ");
		return data;
	}

}

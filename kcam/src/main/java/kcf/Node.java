package kcf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Node extends Object {

	public int num;

	public int layer;

	public boolean root;

	public int parent; // ??Root??????????

	public String name;

	public double x = -1;

	public double y = -1;

	private HashMap children = new HashMap();
	private ArrayList<Node> childNodes = new ArrayList<Node>();
	private Edge parentEdge;

	public Edge getParentEdge() {
		return parentEdge;
	}

	public void setParentEdge(Edge parentEdge) {
		this.parentEdge = parentEdge;
	}

	/**
	 * ??????? ????s?
	 */
	
	public Node(int number, int l, int pa, boolean ro) {
		num = number;
		layer = -1;
		parent = pa;
		root = ro;
		parentEdge = null;
	}
	
	public Node(int number, String name, double x, double y) {
		num = number;
		this.name = name;
		layer = -1;
		setXY(x, y);
	}

	public void setXY(double tx, double ty) {
		x = tx;
		y = ty;
	}

	/** ************* CHECK *************** */
	public boolean isRoot() {
		return root;
	}

	/** ****** assist ********* */
	public String getInput() {
		String result = "new Node(" + num + ", ";
		result += root + ");";
		return result;
	}

	@Override
	public String toString() {
		String result = "";
//		result = new Integer(this.num).toString() + ", ";
//		if (parent != -1) {
//			result += this.parent + "pa, ";
//		}
//		if (root) {
//			result += "root, ";
//		}
//		result += "\n";
		result = num + " " + name + " " + x + " " + y;
		return result;
	}

	public void pString() {
		System.out.println(this.toString());
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public void addChild(String edge, String num) {
		children.put(edge, num);
	}
	
	public void addChild(Node c) {
		childNodes.add(c);
	}

	public HashMap getChildren() {
		return children;
	}
	
	public ArrayList<Node> getChildNodes() {
		return childNodes;
	}

	public Object[] calCkeys() {
		Object[] cKeys = children.keySet().toArray();
		Arrays.sort(cKeys);
		return cKeys;
	}

	public boolean hasChildren() {
		return children.size() > 0;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public int numDescendants() {
		int num = childNodes.size();
		for (Node node : childNodes) {
			num += node.numDescendants();
		}
		return num;
	}

	public String getNodeString(int num) {
		String nodestr = "    "+num+"    "+name+"    "+x+"    "+y+"\n";
		for (Node node : childNodes) {
			nodestr += node.getNodeString(++num);
		}
		return nodestr;
	}
	
	public String getNodeList() {
		String nodelist = name + " ";
		for (Node node : childNodes) {
			nodelist += node.getNodeList();
		}
		return nodelist;
	}

	public String getEdgeString(int num) {
		String edgestr = "";
		if (this.parentEdge != null) {
			edgestr = "    "+num+"    "+parentEdge.getString()+"\n";
		}
		for (Node node : childNodes) {
			edgestr += node.getEdgeString(++num);
		}
		return edgestr;
	}
	
	public String getEdgeList() {
		String edgelist = "";
		if (this.parentEdge != null) {
			edgelist = parentEdge.getName() + " ";
		}
		for (Node node : childNodes) {
			edgelist += node.getEdgeList();
		}
		return edgelist;
	}
}

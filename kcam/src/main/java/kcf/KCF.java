package kcf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

public class KCF {

	Node rootNode = null;
	private String entry;
	private String kcf_str;
	private String class_name;
	ArrayList<Kcf_Bracket> bracket = null;
	
	
	KCF(String data) {
		initialize(data);
	}
	
	public void initialize(String data) {
		kcf_str = data;
		ArrayList<Kcf_Bracket> tm_bracket = pri_getNodesAndEdges();
		if (tm_bracket != null && !tm_bracket.isEmpty()) {
			bracket = new ArrayList<Kcf_Bracket>();
			if (tm_bracket.size() % 3 != 0) {
				System.out.println("ERROR: BRACKET format is bad " + entry);
				System.exit(0);
			}
			for (int i = 0; i < tm_bracket.size(); i++) {
				Kcf_Bracket bk1 = (Kcf_Bracket) tm_bracket.get(i);
				Kcf_Bracket bk2 = (Kcf_Bracket) tm_bracket.get(i + 1);
				Kcf_Bracket bk3 = (Kcf_Bracket) tm_bracket.get(i + 2);
				Kcf_Bracket bk = new Kcf_Bracket(bk1.num);
				bk.setopen(bk1.getXY_open());
				bk.setclose(bk2.getXY_open());
				bk.setRepeat_word(bk3.getrepeatword());
				i = i + 2;
				bracket.add(bk);
			}
		}

	}
	
	private ArrayList<Kcf_Bracket> pri_getNodesAndEdges() {
		int mode = 0;
		HashMap <Integer,Node> nodeMap = new HashMap<Integer,Node>();
		HashMap <Integer,Edge> edgeMap = new HashMap<Integer, Edge>();
		ArrayList<Kcf_Bracket> tm_bracket = new ArrayList<Kcf_Bracket> ();
		
		String[] lines = this.kcf_str.split("\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			if (line.startsWith("NODE")) {
				mode = 1;
				continue;
			} else if (line.startsWith("EDGE")) {
				mode = 2;
				continue;
			} else if (line.startsWith("BRACKET")) {
				mode = 3;
				// continue;
			} else if (line.startsWith("ENTRY")) {
				line = Str.cleanDelim(line);
				String[] temp_line = line.split(" ");
				if (temp_line.length == 3) {
					entry = temp_line[1];
				}
				continue;
			} else if (line.startsWith("///")) {
				mode = 0;
				continue;
			} else if (line.startsWith("CLASS")) {
				line = Str.cleanDelim(line);
				String[] temp_line = line.split(" ");
				class_name = temp_line[temp_line.length - 1];
				continue;
			}

			switch (mode) {
			case 1: { //NODE class
				line = Str.cleanDelim(line);
				String[] temp_line = line.split(" ");
				if (temp_line.length == 1) {
					temp_line = line.split("\t");
				}
				int num = Integer.parseInt(temp_line[0]);

				int y_index = temp_line.length - 1;
				String y_point_str = temp_line[y_index];
				double y_point;

				while (!this.chkNumeric(y_point_str)) {
					y_index--;
					y_point_str = temp_line[y_index];

					if (y_index < 0) {
						break;
					}
				}
				
				y_point = Double.parseDouble(y_point_str);

				double x_point = Double.parseDouble(temp_line[y_index - 1]);
				String nodeName = temp_line[1];
				if (y_index > 3) {
					for (int j = 0; j < y_index - 3; j++) {
						String parts = temp_line[2 + j];
						nodeName += " " + parts;
					}
				}
//
//				if (root_x < x_point) {
//					root_x = x_point;
//					root_num = num;
//					rootnode = nodeName;
//				}
				
				Node newNode = new Node(num, nodeName, x_point, y_point);
				
				nodeMap.put(Integer.valueOf(num), newNode);
				break;
			}
			case 2: { //EDGE
				line = Str.cleanDelim(line);
				String[] temp_line = line.split(" ");
				if (temp_line.length == 1) {
					temp_line = line.split("\t");
				}
				int number = Integer.parseInt(temp_line[0]);
				int num_l = -1;
				if (temp_line.length != 1) {
					num_l = Integer.valueOf(temp_line[1].split(":")[0])
							.intValue();
				}
				String anomer_l = "";
				if (temp_line[1].split(":").length > 1)
					anomer_l = temp_line[1].split(":")[1];
				int num_r = Integer.valueOf(temp_line[2].split(":")[0])
						.intValue();
				String anomer_r = "";
				if (temp_line[2].split(":").length > 1)
					anomer_r = temp_line[2].split(":")[1];
				//String str_l = number + " " + num_r + " " + anomer_r + "-" + anomer_l;
				//String str_r = number + " " + num_l + " " + anomer_l + "-" + anomer_r;
				//Vector v = (Vector) edgeMap.get(Integer.valueOf(num_l));
				//if (v == null) v = new Vector();
				//v.add(str_l);
				//edgeMap.put(Integer.valueOf(num_l), v);
				Edge newEdge;
				Node cNode, pNode;
				//v = (Vector) edgeMap.get(Integer.valueOf(num_r));
				//if (v == null) v = new Vector();
				//v.add(str_r);
				//edgeMap.put(Integer.valueOf(num_r), v);
				if (((Node)nodeMap.get(num_l)).x < ((Node)nodeMap.get(num_r)).x) {
					newEdge = new Edge(number, num_r, num_l, anomer_l + "-" + anomer_r);
					cNode = (Node)nodeMap.get(num_l);
					pNode = (Node)nodeMap.get(num_r);
					
				} else {
					newEdge = new Edge(number, num_l, num_r, anomer_r + "-" + anomer_l);
					cNode = (Node)nodeMap.get(num_r);
					pNode = (Node)nodeMap.get(num_l);
				}
				cNode.setParentEdge(newEdge);
				pNode.addChild(cNode);
				edgeMap.put(number, newEdge);
				break;
			}
			case 3: { //BRACKET
				line = Str.cleanDelim(line);
				String[] temp_line = line.split(" ");

				if (temp_line.length == 6) {
					int x = Integer.parseInt(temp_line[1]);
					Kcf_Bracket bk = new Kcf_Bracket(x);
					bk.setXY_open(temp_line[2], temp_line[3], temp_line[4],
							temp_line[5]);
					tm_bracket.add(bk);
				} else if (temp_line.length == 5) {
					int x = Integer.parseInt(temp_line[0]);
					Kcf_Bracket bk = new Kcf_Bracket(x);
					bk.setXY_open(temp_line[1], temp_line[2], temp_line[3],
							temp_line[4]);
					tm_bracket.add(bk);
				} else if (temp_line.length == 2) {
					int x = Integer.parseInt(temp_line[0]);
					Kcf_Bracket bk = new Kcf_Bracket(x);
					bk.setrepeatword(temp_line[1]);
					tm_bracket.add(bk);
				}
			}
			}
		}
		//fix root node
		for (Iterator it = nodeMap.values().iterator(); it.hasNext();) {
			Node n = (Node)it.next();
			if (n.getParentEdge()==null) {
				if (rootNode != null) {
					System.err.println("Multiple root nodes! "+n);
				}
				rootNode = n;
			}
		}
		if (rootNode == null) {
			System.err.println("No root node");
			rootNode = nodeMap.get(1);
		}
		return tm_bracket;
	}


	
	private boolean chkNumeric(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public int numNodes() {
		int num = 0;
		if (rootNode != null) {
			num++;
			num += rootNode.numDescendants();
		}
		return num;
	}
	
	@Override
	public String toString() {
		String str = rootNode.getNodeList() + rootNode.getEdgeList();
		return str;
	}
	public String toKCFString() { // assuming trees only
		String kcfstring = "ENTRY    "+entry+"    Glycan\n";
		kcfstring += "NODE    "+numNodes()+"\n";
		kcfstring += rootNode.getNodeString(1);
		kcfstring += "EDGE    "+(numNodes()-1)+"\n";
		kcfstring += rootNode.getEdgeString(0);
		kcfstring += "///\n";
		return kcfstring;
	}

	
	@Override
	public boolean equals(Object o) {
		if (o.getClass().equals(KCF.class)) {
			KCF other = (KCF)o;
			if (other.numNodes() != this.numNodes()) return false;
			if (other.toString().equals(this.toString())) return true;
		} 
		return false;
	}
}

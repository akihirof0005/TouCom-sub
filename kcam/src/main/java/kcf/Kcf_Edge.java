package kcf;

public class Kcf_Edge extends Edge {
	public int layer;

	public Kcf_Edge(int la, int number, int parentNum, int childNum, String name) {
		super(number, parentNum, childNum, name);
		this.layer = la;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}
	/*	
public String createKCF() {
	//	if (name.indexOf(Str.OPEN_P) == -1) {
			//System.out.println(this.name);
	//		return "";
	//	}
		String bond = name.split(Str.OPEN_P)[1].split(Str.CLOSE_P)[0];
		String ch = "";
		if (bond.split("-").length > 0)
			ch = bond.split("-")[0];
		String pa = "";
		if (bond.split("-").length > 1)
			pa = bond.split("-")[1];
		String result = Str.rjust(" ", 12)
				+ Str.ljust(new Integer(this.number).toString(), 4) + " ";
		result += Str.rjust(new Integer(this.childNum).toString(), 2);
		if (ch.compareTo("") != 0)
			result += ":" + Str.ljust(ch, 4) + " ";
		else
			result += Str.ljust(" ", 6);
		result += Str.rjust(new Integer(this.parentNum).toString(), 2);
		if (pa.compareTo("") != 0)
			result += ":" + pa;
		result += "\n";
		return result;
	}
	*/
	public String createKCF() {
		/*if (name.indexOf(Str.OPEN_P) == -1) {
			System.out.println(this.name);
			return "";
		}
		*/
		String bond = name;//.split(Str.OPEN_P)[1].split(Str.CLOSE_P)[0];
		String ch = "";
		if (bond.split("-").length > 0)
			ch = bond.split("-")[0];
		String pa = "";
		if (bond.split("-").length > 1)
			pa = bond.split("-")[1];
		String result = Str.rjust(" ", 12)
				+ Str.ljust(new Integer(this.number).toString(), 4) + " ";
		result += Str.rjust(new Integer(this.childNum).toString(), 2);
		if (ch.compareTo("") != 0)
			result += ":" + Str.ljust(ch, 4) + " ";
		else
			result += Str.ljust(" ", 6);
		result += Str.rjust(new Integer(this.parentNum).toString(), 2);
		if (pa.compareTo("") != 0)
			result += ":" + pa;
		result += "\n";
		return result;
	}
	
	public String createKCFforlink(int pnum,int cnum,int num2) {
		if (name.indexOf("-") == -1) {
			//System.out.println(this.name);
			return "";
		}
		
		String bond = name;
		String ch = "";
		if (bond.split("-").length > 0)
			ch = bond.split("-")[0];
		String pa = "";
		if (bond.split("-").length > 1)
			pa = bond.split("-")[1];
		String result = Str.rjust(" ", 12)
				+ Str.ljust(new Integer(num2).toString(), 4) + " ";
		result += Str.rjust(new Integer(cnum).toString(), 2);
		if (ch.compareTo("") != 0)
			result += ":" + Str.ljust(ch, 4) + " ";
		else
			result += Str.ljust(" ", 6);
		result += Str.rjust(new Integer(pnum).toString(), 2);
		if (pa.compareTo("") != 0)
			result += ":" + pa;
		result += "\n";
		
		return result;
	}

	public String toString() {
		String result = " LAYER:"
				+ Str.rjust(new Integer(this.layer).toString(), 2) + " NUMBER:"
				+ Str.rjust(number, 2) + " " + name;
		result += " Child:" + this.childNum + " Parent:" + this.parentNum;
		return result;
	}
}

package kcf;

/**
 * <p>
 * NODE?????????????bject
 * </p>
 */
public class Kcf_Node extends Node {

	// ???
	public Kcf_Node(int layer, int number, String name, double x, double y) {
		super(number, layer, -1, false);
		super.setXY(x, y);
		super.setName(name);
	}

	public void setParent(int pa) {
		this.parent = pa;
	}

	public void setRoot(boolean boo) {
		this.root = boo;
	}

	public boolean hasPa() {
		return parent != -1;
	}

	public String createKCF() {
		String result = "";
		result = Str.rjust(" ", 12)
				+ Str.ljust(new Integer(this.num).toString(), 4)
				+ Str.ljust(this.getName(), 7) + " ";
		result += Str.rjust(new Double(this.x).toString(), 5) + " "
				+ Str.rjust(new Double(this.y).toString(), 5) + "\n";
		return result;
	}

	/*
	 * public String toString() { String result = super.toString(); Object[]
	 * cKeys = calCkeys(); for (int i = 0; i < cKeys.length; i++) { result += " " +
	 * cKeys[i] + "\n"; } return result; }
	 */
	public String toString() {
		String result = " LAYER:"
				+ Str.rjust(new Integer(this.layer).toString(), 2) + " NUMBER:"
				+ Str.rjust(new Integer(this.num).toString(), 2) + "  "
				+ this.name;
		return result;
	}

}

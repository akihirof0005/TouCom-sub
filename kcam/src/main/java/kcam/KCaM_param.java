package kcam;

import java.util.HashMap;
import java.util.Vector;

public class KCaM_param {
  int nodenum = -1;
  HashMap hashpc = new HashMap();
  HashMap link = new HashMap();
  String entry = "";
  Vector list = new Vector();
  String rootNode = "";
  HashMap hlinkLy = new HashMap();
  int Layernum = -99999;

  public Vector getList() {
    return list;
  }

  public void setList(Vector list) {
    this.list = list;
  }

  public KCaM_param(String getentry) {
    entry = getentry;
  }

  public int getKCFsize() {
    return nodenum;
  }

  public void setKCFsize(int nodenum) {
    this.nodenum = nodenum;
  }

  public HashMap getHashpc() {
    return hashpc;
  }

  public void setHashpc(HashMap hashpc) {
    this.hashpc = hashpc;
  }

  public HashMap getLink() {
    return link;
  }

  public void setLink(HashMap link) {
    this.link = link;
  }

  public String getEntry() {
    return entry;
  }

  public String getRootNode() {
    return rootNode;
  }

  public void setRootNode(String rootNode) {
    this.rootNode = rootNode;
  }

  public HashMap getHlinkLy() {
    return hlinkLy;
  }

  public void setHlinkLy(HashMap hlinkLy) {
    this.hlinkLy = hlinkLy;
  }

  public int getLayernum() {
    return Layernum;
  }

  public void setLayernum(int layernum) {
    Layernum = layernum;
  }

}

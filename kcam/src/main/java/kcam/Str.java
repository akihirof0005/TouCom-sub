package kcam;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;

import kcf.Kcf_Entry;

import com.mysql.jdbc.Statement;

public class Str {
  public static Connection sqlload(Connection conn) throws Exception {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager
					.getConnection("jdbc:mysql://127.0.0.1/rings?user=root&password=t@k@k0");
		} catch (SQLException ex) {
		}
		return conn;
	}

  public static HashMap CreateBondMapping(HashMap hm_sugar, Connection conn) {
    Statement stmt = null;
    int code_id = 0;
    int name_id = 0;
    HashMap hm_bond = new HashMap();

    try {
      stmt = (Statement) conn.createStatement();

      String sql = "SELECT * from Glyco_bond";
      ResultSet rs = stmt.executeQuery(sql);
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        hm_bond.put(Integer.toString(id), name);
      }

      sql = "SELECT * from Bond_Mapping";
      rs = stmt.executeQuery(sql);

      while (rs.next()) {
        code_id = rs.getInt("FK_code");
        name_id = rs.getInt("FK_name");
        String link1 = "";
        String link2 = "";

        link1 = hm_bond.get(Integer.toString(code_id)).toString();
        link1 = link1.replaceAll(":", "@");
        link2 = hm_bond.get(Integer.toString(name_id)).toString();
        link2 = link2.replaceAll(":", "@");

        hm_sugar.put(link2.toLowerCase(), link1.toLowerCase());
      }
      stmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return hm_sugar;
  }

  public static HashMap inithash(HashMap hm_sugar, Connection conn) {
    Statement stmt = null;
    HashMap hm_node = new HashMap();
    try {
      stmt = (Statement) conn.createStatement();

      String sql = "SELECT * from Node";
      ResultSet rs = stmt.executeQuery(sql);
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        hm_node.put(Integer.toString(id), name.toLowerCase());
      }

      sql = "SELECT * from Sugar_Mapping2";
      rs = stmt.executeQuery(sql);

      while (rs.next()) {
        int code_id = rs.getInt("code_FK_Node");
        int name_id = rs.getInt("name_FK_Node");

        String node1 = hm_node.get(Integer.toString(code_id)).toString();
        String node2 = hm_node.get(Integer.toString(name_id)).toString();

        hm_sugar.put(node2, node1);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return hm_sugar;
  }

  public static HashMap GetMatrixData(HashMap hm_matrix, Connection conn, String Matrix_name) {
    File query = new File(Matrix_name);
    if (query.exists()) { // file mode
      try {

        BufferedReader br = new BufferedReader(new FileReader(Matrix_name));
        String buffer = "";

        while ((buffer = br.readLine()) != null) {
          String[] pairs = buffer.split("\t");
          String bond1 = pairs[0].split("=")[0].replaceAll(":", "@");
          String bond2 = pairs[0].split("=")[1].replaceAll(":", "@");
          String value = pairs[1];

          String key = bond1 + ":" + bond2;
          String key2 = bond2 + ":" + bond1;
          hm_matrix.put(key.toLowerCase(), value);
          hm_matrix.put(key2.toLowerCase(), value);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    } else {
      if (conn != null) {
        hm_matrix = GetMatrixDatafromDB(hm_matrix, conn, Matrix_name);
      } else {
      }
    }
    return hm_matrix;
  }

  public static HashMap GetMatrixDatafromDB(HashMap hm_matrix, Connection conn, String Matrix_name) {
    Statement stmt = null;
    try {
      stmt = (Statement) conn.createStatement();
      String sql = "SELECT id from Score_Matrix where name = '" + Matrix_name + "'";
      ResultSet rs;

      rs = stmt.executeQuery(sql);

      int id = -1;
      if (rs.next()) {
        id = rs.getInt("id");
      } else {
        return hm_matrix;
      }
      sql = "SELECT * from Matrix_contents where FK_name = " + id;
      rs = stmt.executeQuery(sql);
      while (rs.next()) {
        int FK_edge1 = rs.getInt("FK_edge1");
        int FK_edge2 = rs.getInt("FK_edge2");
        int value = rs.getInt("value");

        Statement stmt2 = (Statement) conn.createStatement();
        sql = "SELECT name from Glyco_bond where id = " + FK_edge1;
        ResultSet rs2 = stmt2.executeQuery(sql);
        String name1 = null;
        String name2 = null;
        if (rs2.next()) {
          name1 = rs2.getString("name");
        }

        sql = "SELECT name from Glyco_bond where id = " + FK_edge2;
        rs2 = stmt2.executeQuery(sql);
        if (rs2.next()) {
          name2 = rs2.getString("name");
        }
        name1 = name1.replaceAll(":", "@");
        name2 = name2.replaceAll(":", "@");
        String key = name1 + ":" + name2;
        String key2 = name2 + ":" + name1;
        hm_matrix.put(key.toLowerCase(), Integer.toString(value));
        hm_matrix.put(key2.toLowerCase(), Integer.toString(value));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return hm_matrix;
  }

  public static HashMap GetMatrixDatafromDB2(HashMap hm_matrixnode, Connection conn, String EorN, String Matrix_name) {
    Statement stmt = null;
    int e_id1 = -1;
    int e_id2 = -1;
    String node_name1 = "";
    String node_name2 = "";
    float value = -1;

    try {
      stmt = (Statement) conn.createStatement();
      String sql = "SELECT id from Score_Matrix2 where name = '" + Matrix_name + "'";
      ResultSet rs;

      rs = stmt.executeQuery(sql);

      int id = -1;
      if (rs.next()) {
        id = rs.getInt("id");
      } else {
        return hm_matrixnode;
      }

      stmt = (Statement) conn.createStatement();
      sql = "SELECT * from " + EorN + "_Matrix where FK_name = " + id;
      rs = stmt.executeQuery(sql);

      while (rs.next()) {
        e_id1 = rs.getInt("FK_" + EorN + "_id1");
        e_id2 = rs.getInt("FK_" + EorN + "_id2");
        value = rs.getFloat("Value");

        Statement stmt2 = (Statement) conn.createStatement();
        sql = "SELECT name from " + EorN + " where id =" + e_id1;
        ResultSet rs2 = stmt2.executeQuery(sql);
        if (rs2.next()) {
          node_name1 = rs2.getString("name").toLowerCase();
        }
        sql = "SELECT name from " + EorN + " where id =" + e_id2;
        ResultSet rs3 = stmt2.executeQuery(sql);
        if (rs3.next()) {
          node_name2 = rs3.getString("name").toLowerCase();
        }
        hm_matrixnode.put(node_name1 + " " + node_name2, value);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return hm_matrixnode;
  }

  public static KCaM_param KCF_processing(String KCF, int netag) {
    Kcf_Entry kcf = new Kcf_Entry(KCF, "");
    if ((kcf.getEdges().size()) != kcf.getNodes().size() - 1) {
      System.out.println("ERROR: KCF format is bad!!");
      System.exit(0);
    }
    String entry = kcf.getEntry();

    int kcfsize = kcf.getNodes().size();

    Vector q = new Vector();
    HashMap hashpc = kcf.getHlinkpc();

    HashMap link = kcf.getHlink();
    String rootnodename = "";
    for (int i = 0; i < kcf.getRoots().size(); i++) {
      int rootnode = Integer.parseInt(kcf.getRoots().elementAt(i).toString());
      Vector roots = (Vector) hashpc.get(0);
      if (roots == null) {
        roots = new Vector();
      }
      if (netag == 1) {
        if (roots.size() != 0 || rootnodename.equals("")) {
          String rootlink = link.get(rootnode).toString();
          rootnodename = rootlink.split("@")[2];
        }
      }
      roots.add(rootnode);
      hashpc.put(0, roots);
    }
    BFS_sort(0, hashpc, q);

    link.put(0, rootnodename);
    int layernum = kcf.getHEdgesLy().size();
    HashMap hlinkLy = kcf.getHlinkLy();
    hlinkLy.put(0, 0);

    KCaM_param gl = new KCaM_param(entry);

    gl.setHashpc(hashpc);
    gl.setLink(link);
    gl.setKCFsize(kcfsize);
    gl.setList(q);
    gl.setRootNode(kcf.getRootnode());
    gl.setHlinkLy(hlinkLy);
    gl.setLayernum(layernum);
    return gl;
  }

  public static boolean BFS_sort(int linkid, HashMap hashlinkpc, Vector q) {
    Vector tm = (Vector) hashlinkpc.get(linkid);
    if (tm == null) {
      tm = new Vector();
    }
    for (int i = 0; i < tm.size(); i++) {
      BFS_sort(Integer.parseInt(tm.elementAt(i).toString()), hashlinkpc, q);
    }
    q.add(linkid);
    return true;
  }

  public static String remove_spaces(String buffer) {
    while (buffer.indexOf("  ") != -1) {
      buffer = buffer.replaceAll("  ", " ");
    }
    return buffer.trim();
  }
}

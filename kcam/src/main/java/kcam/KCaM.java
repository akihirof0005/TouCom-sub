package kcam;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

import com.mysql.jdbc.Connection;

public class KCaM {
  //arr:matrinfile
  static HashMap<String,HashMap<String,String>> arr = new HashMap<String,HashMap<String,String>> ();

	String entry = "";
	HashMap result = new HashMap();
	HashMap maxiMumpair = new HashMap();
	Vector scores = new Vector();
	int MAXSCORE = -999999;
	int minuscore = 0; // global
	// exactの際に、最大スコアを得るときに、既にアライメントされているペアのスコアを引くために使用する変数
	HashMap bondMapping = new HashMap();
	HashMap SugarMap = new HashMap();
	HashMap hmMatrix = new HashMap(); // use db from "Score_Matrix"
	HashMap hmMatrixNode = new HashMap(); // use DB from "Score_Matrix2"
	HashMap hmMatrixEdge = new HashMap(); // 〃
	String simtag = "1"; // similarity score = 0 | normal score = 1
	String programtag = ""; // global local allbyall onebyall などを区別するためのタグ
	int allbyall = 0; // 0 = 1 vs all , 1 = all vs all
	int netag = 1; // NODE-EDGE version = 1 link(NODE-EDGE-NODE) verison = 0
	int advanced = 0; // advanced optionを使用するかしないかを判断するタグ on=1 off=0
	int KEGG = 1; // KEGGバージョンの引数設定は１ その他は0を入れる

	// //// Option parameters //////
	int anomerscore = 0;//-10
	int childhyd = 10;
	int parenthyd = 10 ;//+0
	int queryfrom = 80;//+45
	int queryto = 0;//-35
	int dbfrom = 80;//+45
	int dbto = 0;//-35
	int mismatch_glycan = 0;
	int mismatch_anomer = 0;
	int q_minusscore = 0;
	int d_minusscore = 0;
	String targetDB = "All"; // 比較対象のdatabaseを入力 ex: KEGG or
	static final boolean DEBUG = false;

	// glycosciences.deなど
	// /////////////////////////////////

	public KCaM() {
		super();
	}

	public static void main(String[] args) {
    arr = readSimilarity();
		KCaM kcam = new KCaM();
		String[] tmp = null;
		if(args.length <= 0){
			kcam.printUsage();
		}else if(args[0].equals("db")){
			kcam.setKEGG(0);
			tmp = new String[args.length-1];
			for(int i=0;i<args.length-1;i++){
				tmp[i] = args[i+1];
			}
		}else{
			tmp = args;
		}
		kcam.loop(tmp);
	}

	public void setKEGG(int kegg) {
		this.KEGG = kegg;
	}

	/**
	 * @param args
	 */
	public void loop(String[] args) {
		// start = System.currentTimeMillis();
		if (args.length > 0) {
			programtag = args[0];
		} else {
			printUsage();
		}

		String qglycan = ""; // query glycan file name
		String dbglycan = ""; // db glycan file nema
		String outputfname = ""; // outputfilename
		String matrixname = "none"; // score matrix name

		String[] programs = programtag.split("-"); // netag check node-edge or
		// node-edge-node
		if (programs.length > 1 && programs.length < 4) {
			if (programs[1].equals("0")) {
				netag = 0;
				programtag = programs[0];
			} else if (programs[1].equals("1")) {
				programtag = programs[0];
			}
			if (programs.length == 3) {
				if (programs[2].equals("GlycomeDB")) {
					targetDB = "GlycomeDB";
				} else if (programs[2].equals("RINGS")) {
					targetDB = "RINGS";
				}
			}
		}
		if (KEGG == 1) {
			if (!programtag.equals("1") && !programtag.equals("2")) {
				System.out.println("Please input program number.");
				System.exit(0);
			}

			if (args.length > 3 && args.length < 14) {
				qglycan = args[1];
				dbglycan = args[2];
				simtag = args[3];

				if (args.length > 4) {
					if (args.length == 13) {
						try {
							anomerscore = Integer.parseInt(args[4]);
							childhyd = Integer.parseInt(args[5]);
							parenthyd = Integer.parseInt(args[6]);
							queryfrom = Integer.parseInt(args[7]);
							queryto = Integer.parseInt(args[8]);
							dbfrom = Integer.parseInt(args[9]);
							dbto = Integer.parseInt(args[10]);
							mismatch_glycan = Integer.parseInt(args[11]);
							mismatch_anomer = Integer.parseInt(args[12]);
							if (mismatch_anomer == 1) {
								mismatch_anomer = mismatch_glycan;
							}
							advanced = 1;
						} catch (NumberFormatException e) {
							System.out.println("Please input numbers for advanced option.");
						}
					} else {
						System.out.println("Advanced option error!");
						System.exit(0);
					}
				}
			} else {
				System.out.println("Please input arguments");// kaizenten
				System.exit(0);
			}
		} else { // if KEGG != 1
			if (programtag.equals("1") || programtag.equals("2")) {
			} else if (programtag.equals("3") || programtag.equals("4")) {
				allbyall = 1;
			} else {
				System.out.println("Please input program number.");
				System.exit(0);
			}
			if (args.length > 3 && args.length < 16) {
				qglycan = args[1];
				dbglycan = args[2];
				if (allbyall == 1) {
					if (args[2].equals("db")) {
						dbglycan = "db";
					} else if (!args[2].equals("none")) {
						System.out.println("Please input \"db\" or \"none\".");
						System.exit(0);
					}
				}

				if (args[3].equals("0") || args[3].equals("1")) {
					simtag = args[3];
				} else {
					System.out.println("similarity tag error.");
					System.exit(0);
				}
				if (args.length > 4) {
					if (!args[4].equals("none")) {
						outputfname = args[4];
					}
				}
				if (args.length > 5) {
					matrixname = args[5];
				}
				if (args.length > 6) {
					if (args.length == 15) {
						try {
							anomerscore = Integer.parseInt(args[6]);
							childhyd = Integer.parseInt(args[7]);
							parenthyd = Integer.parseInt(args[8]);
							queryfrom = Integer.parseInt(args[9]);
							queryto = Integer.parseInt(args[10]);
							dbfrom = Integer.parseInt(args[11]);
							dbto = Integer.parseInt(args[12]);
							mismatch_glycan = Integer.parseInt(args[13]);
							mismatch_anomer = Integer.parseInt(args[14]);
							if (mismatch_anomer == 1) {
								mismatch_anomer = mismatch_glycan;
							}
							// simtag = "1";
							advanced = 1;
						} catch (NumberFormatException e) {
							System.out
									.println("Please input numbers for advanced option.");
						}
					} else {
						System.out.println("Advanced option error.");
						System.exit(0);
					}
				}
			} else {
				System.out.println("Please input arguments");// kaizenten
				System.exit(0);
			}
		}

		/*
		 * Read query file section
		 * 
		 */
		try {
			ArrayList kcfs = new ArrayList();// ??KCF??????????????????
			ArrayList names = new ArrayList();// ??KCF?????(KCF?????)
			ArrayList edgenums = new ArrayList();
			Connection conn = null;
			String buffer = "";

			if (KEGG == 0) {
				conn = (Connection) Str.sqlload(conn);
				if (conn != null) {
					bondMapping = Str.CreateBondMapping(bondMapping, conn);
					SugarMap = Str.inithash(SugarMap, conn);
				}
				if (!matrixname.equals("none")) {// noneはmatrixを使用しない
					hmMatrix = Str.GetMatrixData(hmMatrix, conn, matrixname);
					if (hmMatrix.size() == 0 && conn != null) {
						hmMatrixNode = Str.GetMatrixDatafromDB2(hmMatrixNode,
								conn, "Node", matrixname);
						hmMatrixEdge = Str.GetMatrixDatafromDB2(hmMatrixEdge,
								conn, "Edge", matrixname);
						netag = 1; //node-edgeに強制的に変更
						if (hmMatrixNode.size() == 0
								&& hmMatrixEdge.size() == 0) {
							System.out
									.println("ERROR: Other matrix name please.");
							System.exit(0);
						}
					}
				}

			}

			File query = new File(qglycan);
			if (query.exists()) { // file version
				// System.out.println("file mode");
				BufferedReader br1 = new BufferedReader(new FileReader(qglycan));
				String KCF = "";
				int mode = 0;
				while ((buffer = br1.readLine()) != null) {
					if (buffer.startsWith("///")) {
						if (mode != 2) {
							System.out.println("KCF error");
							System.exit(0);
						}
						KCF += buffer;
						kcfs.add(KCF);
						//if (allbyall == 0) { // If 1 vs all
						//	break;
						//}
						mode = 0;
						KCF = "";
					} else if (buffer.startsWith("ENTRY")) {// ??????????????????????????????????????
						if (mode != 0) {
							System.out.println("KCF error");
							System.exit(0);
						}
						buffer = Str.remove_spaces(buffer);
						String[] entryline = buffer.split(" ");
						names.add(entryline[1]);
						mode = 1;
						KCF += buffer + "\n";
					} else if (buffer.startsWith("EDGE")) {// ??????????????????????????????????????
						if (mode != 3) {
							System.out.println("KCF error");
							System.exit(0);
						}
						buffer = Str.remove_spaces(buffer);
						String[] edgeline = buffer.split(" ");
						edgenums.add(edgeline[1]);
						mode = 2;
						KCF += buffer + "\n";
					} else if (buffer.startsWith("NODE")) {// ??????????????????????????????????????
						if (mode != 1) {
							System.out.println("KCF error");
							System.exit(0);
						}
						buffer = Str.remove_spaces(buffer);
						mode = 3;
						KCF += buffer + "\n";
					} else if (buffer.length() != 0 && mode != 1) {// ??????????????????????????????????????
						KCF += buffer + "\n";
					}
				}
			} else if (KEGG == 0) { // database version
				if (conn == null) {
					try {
						conn = (Connection) Str.sqlload(conn);

					} catch (RuntimeException e) {
						// TODO 自動生成された catch ブロック
						System.out.println("Can not connect database.");
						System.out.println("Or file:\"" + qglycan
								+ "\" is not found.");
						e.printStackTrace();
					}
				}
				// System.out.println("database mode");

				Statement stmt = conn.createStatement();
				String sql = "SELECT name,KCF,numEdges from Glycan";
				if (allbyall == 0) {
					sql += " where name = '" + qglycan + "'";
				} else if (allbyall == 1) {
					sql += " where name like '" + qglycan + "%'";
				} else {
					System.out.println("SQL error!");
					System.exit(0);
				}
				if (!targetDB.equals("All")) {
					if (sql.contains("where")) {
						sql += " AND ";
					} else {
						sql += " where ";
					}
					if (targetDB.equals("GlycomeDB")) {
						sql += "name like 'G%'";
					} else if (targetDB.equals("RINGS")) {
						sql += "name like 'RINGS%'";
					} else {
						System.out.println("target DataBase error.");
						System.exit(0);
					}
				}
				if (sql.contains("where")) {
					sql += " AND KCF is not null";
				} else {
					sql += " where KCF is not null";
				}

				// System.out.println(sql);
				ResultSet rs = stmt.executeQuery(sql);
				while (rs.next()) {
					String KCF = rs.getString("KCF");
					String name = rs.getString("name");
					int edgenum = rs.getInt("numEdges");

					kcfs.add(KCF);
					names.add(name);
					edgenums.add(edgenum);
					if (allbyall == 0) { // If 1 vs all
						break;
					}
				}
				if (kcfs.size() == 0) {
					System.out.println("Can not get any data from the database.");
					System.out
							.println("Please input other query file or glycan name.");
					System.exit(0);
				}
			} else {
				System.out.println("ERROR: " + query + " file is not found.");
				System.exit(0);
			}
			/*
			 * read dbfile section
			 *
			 */
			PrintWriter pw = null;
			if (!outputfname.equals("")) {
				pw = new PrintWriter(new BufferedWriter(new FileWriter(
						outputfname)));

			}
			if (allbyall == 0) {
				doOneByAll(dbglycan, outputfname, kcfs, conn, pw);

			} else if (allbyall == 1) {
				doAllByAll(outputfname, kcfs, names, edgenums, pw);
			}
			if (!outputfname.equals("")) {
				pw.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	private void printUsage() {
		System.out.println("Please input program number.");
		if (KEGG == 1) {
			System.out.println("Usage: <program number> <query filename> <candidate filename> <similarity tag>");
		} else {
			System.out.println("Usage: <program number> <query filename> <candidate filename> <similarity tag> <output filename(option)> <score matrix name(option)>");
		}
		System.out.println("advanced option: <child's hydroxyl group> <anomer> <parent's hydroxyl group(leaf)> <parent's hydroxyl group(root)> <candidate structure weight range(leaf)> <candidate structure weight range(root)> <penalty for mismatched monosaccharide> <penalize for missing hydroxyl groups or anomer>");
		if (KEGG == 0) {
			System.out.println("  Similarity tag: 0 = similarity score  1 = matched score");
			System.out.println("  Program number: 1 = one KCF vs KCFs (local exact match)");
			System.out.println("                  2 = one KCF vs KCFs (global exact match)");
			System.out.println("                  3 = KCFs vs KCFs (local exact match)");
			System.out.println("                  4 = KCFs vs KCFs (global exact match)");
			System.out.println("                  In adition, if program number = 1-0, program runs the node-edge version.");
			System.out.println("                  Otherwise, if program number = 1-1, program runs the node-edge-node version.");
			System.out.println("EX: 1 queryKCF.txt glycans.txt 1 resultKCaM.txt none 15 10 25 25 25 25 25 0 0");
		} else {
			System.out.println("  Similarity tag: 0 = similarity score  1 = matched score");
			System.out.println("Program number options: 1 = one KCF vs KCFs (local exact match)");
			System.out.println("                        2 = one KCF vs KCFs (global exact match)");
		}
		System.exit(0);
	}

	private void doOneByAll(String dbglycan, String outputfname, ArrayList kcfs, Connection conn, PrintWriter pw) throws FileNotFoundException, IOException, Exception, SQLException {
		String buffer;
		File dbfile = new File(dbglycan);
		for (int i = 0; i < kcfs.size(); i++) {
			String KCF1 = kcfs.get(i).toString();
			KCaM_param gl1 = Str.KCF_processing(KCF1, netag);

			if (dbfile.exists()) { // file version
				BufferedReader brDB = new BufferedReader(
						new FileReader(dbglycan));
				// System.out.println("file mode");

				String KCFDB = "";
				while ((buffer = brDB.readLine()) != null) {
					if (buffer.startsWith("///")) {
						KCFDB += buffer;
						KCaM_param gl2 = Str.KCF_processing(KCFDB,
								netag);
						if (gl1 != null) {
							if (gl2 != null) {
								run(gl1, gl2);// KCaM??????
							} else {
								continue;// NODE???????????????????
							}
						} else {
							continue;// NODE???????????????????
						}
						KCFDB = "";
					} else if (buffer.length() != 0) {// ????????????????????????????????????????????????
						KCFDB += buffer + "\n";
					}
				}
			} else if (dbglycan.equals("db")  && KEGG == 0 ) { // DB
				// version
				// System.out.println("database mode");
				if (conn == null) {
					conn = (Connection) Str.sqlload(conn);
					if (conn == null) {
						System.out.println("Connection error.");
						System.exit(0);
					}
				}
				Statement stmt = conn.createStatement();
				String sql = "SELECT KCF from Glycan";
				if (!targetDB.equals("All")) {
					if (sql.contains("where")) {
						sql += " AND ";
					} else {
						sql += " where ";
					}
					if (targetDB.equals("GlycomeDB")) {
						sql += "name like 'G%'";
					} else if (targetDB.equals("RINGS")) {
						sql += "name like 'RINGS%'";
					} else {
						System.out.println("target DataBase error.");
						System.exit(0);
					}
				}
				if (sql.contains("where")) {
					sql += " AND KCF is not null";
				} else {
					sql += " where KCF is not null";
				}

				ResultSet rs = stmt.executeQuery(sql);
				// int count =1;

				while (rs.next()) {
					String KCF2 = rs.getString("KCF");
					// System.out.println(count);

					// System.out.println(KCF2);
					// count++;
					KCaM_param gl2=null;
					try {
						gl2 = Str.KCF_processing(KCF2, netag);
					} catch (RuntimeException e) {
						// TODO 自動生成された catch ブロック
						System.err.println("Error processing KCF2 "+KCF2);
						e.printStackTrace();
					}
					if (gl1 != null) {
						if (gl2 != null) {
							run(gl1, gl2);// KCaM??????
							gl2 = null;
						} else {
							continue;// NODE???????????????????
						}
					} else {
						continue;// NODE???????????????????
					}
				}
				rs.close();
				stmt.close();
				if (conn != null) {
					conn.close();
				}
			} else {
				System.out.println(dbglycan + " is not found.");
				System.exit(0);
			}

			output(outputfname, " ", pw, gl1.entry);
			scores = new Vector();
			result = new HashMap();
			maxiMumpair = new HashMap();
		}
	}
	public ArrayList<String> searchOneByAll(String glycan, String outputfname, ArrayList kcfs) {
		ArrayList<String> mathchedStructures = new ArrayList<String>();
		KCaM_param query = Str.KCF_processing(glycan, netag);
		for (int i = 0; i < kcfs.size(); i++) {
			//scores = new Vector();
			scores.clear();
			result = new HashMap();
			maxiMumpair = new HashMap();
			
			String KCF1 = kcfs.get(i).toString();
			KCaM_param gl1 = Str.KCF_processing(KCF1, netag);
			
			if (query != null && gl1 != null) {
				run(query, gl1);// KCaM
			} else {
				System.err.println("Error with input KCF data");
			}
			
			
			float tmpscore = (Float) this.scores.get(0);
			int matchScore = gl1.nodenum;
			matchScore = matchScore*100;
			if (tmpscore ==  200){
				mathchedStructures.add(this.entry.toString());
			}
			

		}
		return mathchedStructures;
	}

	private void doAllByAll(String outputfname, ArrayList kcfs, ArrayList names, ArrayList edgenums, PrintWriter pw) {
		for (int i = 0; i < kcfs.size(); i++) {
			String KCF1 = kcfs.get(i).toString();
			String name1 = names.get(i).toString();
			float edgenum1 = Float.parseFloat(edgenums.get(i)
					.toString());
			KCaM_param gl1= null;
			try {
				gl1 = Str.KCF_processing(KCF1, netag);
			} catch (Exception e) {
				System.err.println("Caught KCF1 processing exception for "+KCF1);
			}					
			for (int j = i + 1; j < kcfs.size(); j++) {
				String KCF2 = kcfs.get(j).toString();
				String name2 = names.get(j).toString();
				float edgenum2 = Float.parseFloat(edgenums.get(j)
						.toString());
				// System.out.println("Start");
				// time();
				KCaM_param gl2 = null;
				try {
					gl2 = Str.KCF_processing(KCF2, netag);
				} catch (Exception e) {
					System.err.println("Caught KCF2 processing exception for "+KCF1);
				}						
				if (gl1 != null) {
					if (gl2 != null) {
						run(gl1, gl2);// KCaM??????
					} else {
						continue;// NODE???????????????????
					}
				} else {
					continue;// NODE???????????????????
				}
				// time();
				// System.out.println("END");
				output(outputfname, "\t", pw);
				maxiMumpair = new HashMap();
				result = new HashMap();
				// MAXSCORE = -999999;
				scores = new Vector();
			}
		}
	}
	public void output(String outputfname, String spacing, PrintWriter pw ) {
	    output(outputfname, spacing, pw, "");
	}
	
	public void output(String outputfname, String spacing, String entryname) { 
	
		Collections.sort(scores, Collections.reverseOrder());
		if(!entryname.equals("")){
			entryname = entryname +" ";
		}
		for (int i = 0; i < scores.size(); i++) {
			float score = Float.parseFloat(scores.elementAt(i).toString());
			Vector value = (Vector) result.get(score);
			Collections.sort(value);// GlycanID
			
			if (score != 0 || allbyall == 1) {
				for (int j = 0; j < value.size(); j++) {
					String entrynum = value.elementAt(j).toString();
					BigDecimal bi = new BigDecimal(score);
					if (!outputfname.equals("")) {
						
						System.out.println(entryname
								+ entrynum
								+ spacing
								+ bi.setScale(1, BigDecimal.ROUND_HALF_UP)
										.floatValue());
						
					}					
					Vector pair = (Vector) maxiMumpair.get(entrynum);
					for (int k = 0; k < pair.size(); k = k + 2) {
						int pairedge1 = Integer.parseInt(pair.elementAt(k)
								.toString());
						int pairedge2 = Integer.parseInt(pair.elementAt(k + 1)
								.toString());
						if (pairedge1 != 0 || netag == 1) {
							if (pairedge2 != 0 || netag == 1) {
								if (!outputfname.equals("")) {
									
									System.out.print(spacing + pairedge1
											+ spacing + pairedge2);
								}
							}
						}

					}
					if (!outputfname.equals("")) {
						System.out.println();
						
					}
				}

			}

		}

	}

	/**
	 * KCaMの結果を標準出力する、またはファイルに書き込む
	 * 
	 * @param outputfname
	 *            出力されるファイル名
	 * @param spacing
	 *            スペース文字(スペースかタブにのどちらかにするために使用)
	 * @param pw
	 *            PrintWriter 書き込み変数
	 */

	public void output(String outputfname, String spacing, PrintWriter pw ,String entryname) {
		//TODO: public void output(String outputfname, String spacing, String entryname) {
		//から呼ぶようにする
		Collections.sort(scores, Collections.reverseOrder());
		if(!entryname.equals("")){
			entryname = entryname +" ";
		}
		for (int i = 0; i < scores.size(); i++) {
			float score = Float.parseFloat(scores.elementAt(i).toString());
			Vector value = (Vector) result.get(score);
			Collections.sort(value);// GlycanID
			if (score != 0 || allbyall == 1) {
				for (int j = 0; j < value.size(); j++) {
					String entrynum = value.elementAt(j).toString();
					BigDecimal bi = new BigDecimal(score);
					if (!outputfname.equals("")) {
						pw.print(entryname+entrynum
								+ spacing
								+ bi.setScale(1, BigDecimal.ROUND_HALF_UP)
										.floatValue());
					}
					//if (DEBUG) {
						System.out.print(entryname
								+ entrynum
								+ spacing
								+ bi.setScale(1, BigDecimal.ROUND_HALF_UP)
										.floatValue());
					//}					
					Vector pair = (Vector) maxiMumpair.get(entrynum);
					for (int k = 0; k < pair.size(); k = k + 2) {
						int pairedge1 = Integer.parseInt(pair.elementAt(k)
								.toString());
						int pairedge2 = Integer.parseInt(pair.elementAt(k + 1)
								.toString());
						if (pairedge1 != 0 || netag == 1) {
							if (pairedge2 != 0 || netag == 1) {
								if (!outputfname.equals("")) {
									pw.print(spacing + pairedge1 + spacing
											+ pairedge2);
								}
								//if (DEBUG) {
									System.out.print(spacing + pairedge1
											+ spacing + pairedge2);
								//}
							}
						}

					}
					if (!outputfname.equals("")) {
						pw.println();
					}
					/*if (DEBUG)*/ System.out.println();
				}

			}

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean run(KCaM_param gl1, KCaM_param gl2) { // link version
		// (node-edge-node)
		entry = gl2.getEntry();
		int kcf1size = gl1.getKCFsize();
		int kcf2size = gl2.getKCFsize();
		HashMap qlink = gl1.getLink();
		HashMap dlink = gl2.getLink();
		Vector q = gl1.getList();
		Vector d = gl2.getList();
		HashMap qhashpc = gl1.getHashpc();
		HashMap dhashpc = gl2.getHashpc();
		HashMap qhlinkLy = gl1.getHlinkLy();
		HashMap dhlinkLy = gl2.getHlinkLy();
		int qLayernum = gl1.getLayernum();
		int dLayernum = gl2.getLayernum();

		int[][] matrix = new int[kcf1size][kcf2size];
		Vector[][] sons = new Vector[kcf1size][kcf2size];
		int MaxX = -1;
		int MaxY = -1;
		int Maxscore = -999999;

		float qmaximumscore = 0; // similarity score wo calculate surutameni
		// siyou
		float dmaximumscore = 0; // same

		int qnum = queryto - queryfrom;
		if (qnum != 0) {
			q_minusscore = qnum / qLayernum;
		}
		int dnum = dbto - dbfrom;
		if (dnum != 0) {
			d_minusscore = dnum / dLayernum;
		}

		for (int i = 0; i < kcf1size; i++) {
			String link1 = qlink.get(q.elementAt(i)).toString().toLowerCase();
			Integer qi = (Integer)q.elementAt(i);
			int qLayer = Integer.parseInt(qhlinkLy.get(qi).toString());
			if (DEBUG) System.out.println("1Link "+i+" is "+link1+" and qLayer is "+qLayer);

			if (link1.equals("") || !link1.contains("@")) {
				// } else if (bondMapping.get(link1) != null) {
				// link1 = bondMapping.get(link1).toString();
			} else if (KEGG == 0){
				link1 = bondmapping(link1);
			}

			if (netag == 1) {
				if (link1.contains("@")) {
					String[] links1 = link1.split("@");
					if (links1.length == 3) {
						link1 = links1[0] + "@" + links1[1];
						// System.out.println("link1: " + link1);
					} else {
						System.out.println("link error: " + link1);
						System.exit(0);
					}
				}
			}
			if (simtag.equals("0") && advanced == 1) { // if advenced option
				// used, kouzouno
				// saidaiti wo erunoni
				// siyousuru
				int qscore = queryto - q_minusscore * qLayer;
				int dscore = dbto - d_minusscore * qLayer;
				qmaximumscore += qscore + dscore + anomerscore + childhyd + parenthyd;
			}
			for (int j = 0; j < kcf2size; j++) {
				Integer dj = (Integer)d.elementAt(j);
				String link2 = dlink.get(dj).toString().toLowerCase();
				int dLayer = Integer.parseInt(dhlinkLy.get(dj).toString());
				if (DEBUG) System.out.println("2Link "+j+" is "+link2+" and dLayer is "+dLayer);

				if (link2.equals("") || !link2.contains("@")) {
					// } else if (bondMapping.get(link2) != null) {
					// link2 = bondMapping.get(link2).toString();
				} else if (KEGG == 0) {
					link2 = bondmapping(link2);
				}
				int matrixscore = 0;
				// if (hmMatrix.containsKey(key)) {
				// matrixscore = Integer
				// .parseInt(hmMatrix.get(key).toString());
				// } else
				if (simtag.equals("0") && advanced == 1 && i == 1) { // if
					// advenced option used, kouzouno saidaiti wo erunoni siyousuru
					int qscore = queryto - q_minusscore * dLayer;
					int dscore = dbto - d_minusscore * dLayer;
					dmaximumscore += qscore + dscore + anomerscore + childhyd + parenthyd;
				}
				if (netag == 1) {
					if (link2.contains("@")) {
						String[] links2 = link2.split("@");
						if (links2.length == 3) {
							link2 = links2[0] + "@" + links2[1];
						} else {
							System.out.println("link error: " + link2);
							System.out.println("1: " + link1 + "2: " + link2);
							System.exit(0);
						}
					}
				}
				if (link1.equals("") || link2.equals("")) {
				} else if (hmMatrix.size() > 0) { // score matrix version no baai
					String key = link1 + ":" + link2;
					if (hmMatrix.containsKey(key)) {
						matrixscore = Integer.parseInt(hmMatrix.get(key).toString());
					}
				} else if (hmMatrixNode.size() > 0 && hmMatrixEdge.size() > 0) {
					if (netag == 1) {
						// System.out.print(link1 +"vs"+ link2+ " = ");
						matrixscore = getmatrixscore(link1, link2);
						// System.out.println(matrixscore);
					}
				} else { // If matrix is nothing
					if (netag == 1 && !link1.contains("@")	|| (!link2.contains("@"))) {
						if (DEBUG) System.out.println("1: "+link1 + " 2: "+link2);
						if (link1.equals(link2)) {
							int qscore = queryto - q_minusscore * qLayer;
							int dscore = dbto - d_minusscore * dLayer;
							matrixscore = qscore + dscore + anomerscore	+ childhyd + parenthyd;
							// matrixscore = 100; // ex: Asn vs Asn
							// System.out.println(100);
						} else {
							String[] link1_1 = link1.split("@"); // one node// ?????
							if (link1_1.length == 2) {
								if (link1_1[0].equals(link2)) {
									int qscore = queryto - q_minusscore	* qLayer;
									int dscore = dbto - d_minusscore * dLayer;
									matrixscore = qscore + dscore;
									// matrixscore = 35; // ex: glc@b1-4 vs glc
								} else {
									String[] link2_1 = link2.split("@");
									if (link1.equals(link2_1[0])) {
										// matrixscore = 35; // ex: glc vs
										// glc@b1-4
										int qscore = queryto - q_minusscore * qLayer;
										int dscore = dbto - d_minusscore * dLayer;
										matrixscore = qscore + dscore;
									}
								}
							}
						}
					} else if (link1.equals(link2)) {
						int qscore = queryto - q_minusscore * qLayer;
						int dscore = dbto - d_minusscore * dLayer;
						matrixscore = qscore + dscore + anomerscore + childhyd	+ parenthyd;
						// matrixscore = 100;
					} else {
						// matrixscore = getscore(link1, link2);
						//matrixscore = getscore(link1, link2, qLayer, dLayer); // add
						matrixscore = getscore2(link1, link2, qLayer, dLayer); // add
						// weight
						// range
					}
				}
				if (DEBUG) System.out.println(qi+ ", "+dj+" Matrix score = "+matrixscore);
				matrix[qi.intValue()][dj.intValue()] = matrixscore;

				Vector maxX = new Vector();
				int score = 0;
				Vector re = new Vector();
				re.add(0, score);

				re = (Vector) getmap((Vector) qhashpc.get(qi),
						(Vector) dhashpc.get(dj), maxX, re, matrix);

				if (re.size() > 1) {
					score = Integer.parseInt(re.elementAt(0).toString());
					maxX = (Vector) re.elementAt(1);
				}
				if (Maxscore < matrixscore + score) {
					Maxscore = matrixscore + score;

					MaxX = qi.intValue();
					MaxY = dj.intValue();
				}
				
				
				matrix[qi.intValue()][dj.intValue()] += score;
				sons[qi.intValue()][dj.intValue()] = maxX;
				if (DEBUG) {
					System.out.println(qi + ", " + dj + " Matrix score = "
							+ matrix[qi.intValue()][dj.intValue()]);
					System.out.println(sons[qi.intValue()][dj.intValue()]
							.toString());
				}				
			}
		}
		MAXSCORE = Maxscore;
		Vector pairs = new Vector();
		if (MAXSCORE != 0) {
			pairs = getpairing(matrix, sons, MaxX, MaxY);

			if (programtag.equals("2") || programtag.equals("4")) { // global
				// exact
				// match の場合
				pairs = getAllPairs(matrix, sons, kcf1size, kcf2size, pairs, q, d); // この中でglobal検索が再帰的に行われる
			}
		}
		// scoring section
		float score = 0;
		int minusnum = 1;
		if (netag == 1) {
			minusnum = 0;
		}
		float qglycanmaxscore = 0;
		float dglycanmaxscore = 0;
		if (advanced == 1) { // advenced option wo siyousite nodono weight wo
			// tukerutoki
			qglycanmaxscore = qmaximumscore / 100;
			dglycanmaxscore = dmaximumscore / 100;
		} else {
			qglycanmaxscore = kcf1size - minusnum;
			dglycanmaxscore = kcf2size - minusnum;
		}
		// similarity ???????????????? simscore = ????(?????? ??????)????
		if (simtag.equals("0")) {
			score = (float) MAXSCORE / ((float) Math.max(qglycanmaxscore, dglycanmaxscore));
		} else if (simtag.equals("1")) {
			score = (int) MAXSCORE;
		} else {
			System.out.println("Similarity tag number error.");
			System.exit(0);
		}
		Vector Gnums = null;
		if (result.containsKey(score)) {
			Gnums = (Vector) result.get(score);
		} else {
			Gnums = new Vector();
			scores.add(score);
		}
		String entries = "";
		if (allbyall == 0) {
			entries = entry;
		} else if (allbyall == 1) {
			entries = gl1.getEntry() + "\t" + entry;
		}
		Gnums.add(entries);
		result.put(score, Gnums);
		maxiMumpair.put(entries, pairs);

		return true;
	}

	public Vector getAllPairs(int[][] matrix, Vector[][] sons, int qKCFsize,
			int dKCFsize, Vector pairs, Vector d, Vector q) { // For global
		// pairを分解する
		Vector qpairs = new Vector(); // query側の一致したエッジ番号のリスト
		Vector dpairs = new Vector(); // ターゲット側の一致したエッジ番号のリスト
		for (int k = 0; k < pairs.size(); k = k + 2) {
			// System.out.print(pairs.elementAt(k) + " ");
			qpairs.add(Integer.parseInt(pairs.elementAt(k).toString()));
			dpairs.add(Integer.parseInt(pairs.elementAt(k + 1).toString()));
		}

		// 既に一致したクエリー側の一致したエッジ番号の列を0以下にする
		// 同じ様にターゲット側も行う。最終的にクエリーとターゲット側共に交わらない位置の最大スコアとその位置を取得する。
		int Maxscore = -99999; // pairで一致しない部分での最大スコアを入れる変数
		int MaxX = -1, MaxY = -1; // 最大スコアのXY座標の変数
		for (int m = 0; m < qKCFsize; m++) {
			if (qpairs.contains(m)) { // クエリー側が一致する列
				for (int n = 0; n < dKCFsize; n++) { // その列を全てマイナス以下にする
					if (matrix[m][n] > 0) { // まだマイナスにされていない部分を全てマイナスにする
						matrix[m][n] = -matrix[m][n];
						sons[m][n] = new Vector();
					}
					// System.out.print(matrix[m][n] + "\t");
				}
			} else {
				for (int n = 0; n < dKCFsize; n++) {
					if (dpairs.contains(n)) {// ターゲット側が一致する列
						if (matrix[m][n] > 0) { // まだマイナスにされていない部分を全てマイナスにする
							matrix[m][n] = -matrix[m][n]; // その部分をマイナスにする
							sons[m][n] = new Vector();
						}
					} else { // pairが両方共に一致しない部分
						if (Maxscore < matrix[m][n]) { // 最大スコアとXYを得る
							Maxscore = matrix[m][n];
							MaxX = m;
							MaxY = n;
						}
					}

				}
			}
		}

		if (Maxscore > 0) { // 最大スコアがあるとき

			Vector getpairs = getpairing(matrix, sons, MaxX, MaxY);// ペアを得る
			if (getpairs.size() > 0) { // ペアが見つかったとき

				if (minuscore > 0) {
					Maxscore = Maxscore - minuscore; // 既にアライメントされている分のスコアを引く
					minuscore = 0;
				}
				MAXSCORE = MAXSCORE + Maxscore;// add max score

				pairs.addAll(getpairs);
				pairs = getAllPairs(matrix, sons, qKCFsize, dKCFsize, pairs, q,	d);
			}
		}
		return pairs;
	}


	public Vector getmap(Vector qV, Vector dV, Vector X, Vector re, int[][] matrix) {
		Vector X2 = new Vector();
		Vector Y2 = new Vector();

		if (qV == null)
			qV = new Vector();
		if (dV == null)
			dV = new Vector();
		Vector Xclone = (Vector) X.clone();
		for (int i = 0; i < qV.size(); i++) {
			for (int j = 0; j < dV.size(); j++) {
				X = (Vector) Xclone.clone();
				X.add(qV.elementAt(i));
				X.add(dV.elementAt(j));
				X2 = (Vector) qV.clone();
				X2.remove(qV.elementAt(i));
				Y2 = (Vector) dV.clone();
				Y2.remove(dV.elementAt(j));
				re = (Vector) getmap(X2, Y2, X, re, matrix);
			}
		}

		int tmscore = 0;
		for (int i = 0; i < X.size(); i = i + 2) {
			tmscore += matrix[Integer.parseInt(X.elementAt(i).toString())][Integer.parseInt(X.elementAt(i + 1).toString())];
		}
		if (X.size() > 0 && Integer.parseInt(re.elementAt(0).toString()) < tmscore) {
			re.clear();
			re.add(0, Integer.toString(tmscore));
			re.add(1, X.clone());
		}
		X.clear();

		return re;
	}

	public Vector getpairing(int[][] matrix, Vector[][] sons, int MaxX, int MaxY) {
		int X = MaxX, Y = MaxY;

		if (X < 0) { // MaxXとMaxYが０以下のときは座標としておかしいためエラー
			System.out.println("error: matrix error " + entry);
			return null;
		}
		Vector parents = new Vector(); // 子どもを探すために親の座標だけが入った配列を作成

		parents.add(X);
		parents.add(Y);
		Vector pairs = new Vector(); // 最終的に出力されるペアを入れる変数
		pairs.add(X);
		pairs.add(Y);
		pairs = getsons(matrix, parents, pairs, sons); // 入力されたペアと関連のある子のペアを探す

		return pairs;
	}

	public Vector getsons(int[][] matrix, Vector parents, Vector pairs,
			Vector[][] sons) {
		Vector p = new Vector();
		for (int i = 0; i < parents.size(); i = i + 2) {
			int x = Integer.parseInt(parents.elementAt(i).toString());
			int y = Integer.parseInt(parents.elementAt(i + 1).toString());
			p = sons[x][y];

			for (int j = 0; j < p.size(); j = j + 2) {
				if (matrix[Integer.parseInt(p.elementAt(j).toString())][Integer.parseInt(p.elementAt(j + 1).toString())] > 0) {// use
					// global
					// exact
					// match
					// if
					// -99999
					// => no
					// add
					pairs.add(p.elementAt(j));
					pairs.add(p.elementAt(j + 1));
				} else { // 既にアライメントされているペアのスコアを引く。これにより最大スコアが大きくならないようにする。
					minuscore += -matrix[Integer.parseInt(p.elementAt(j)
							.toString())][Integer.parseInt(p.elementAt(j + 1)
							.toString())];
					// System.out.println(minuscore);
				}
			}
			// if(p.size() == 0){
			// leafchildren.add(pairs.elementAt(pairs.size()-2*i-2));
			// leafchildren.add(pairs.elementAt(pairs.size()-2*i-1));
			// }
			getsons(matrix, p, pairs, sons);
		}

		return pairs;
	}

	public int getmatrixscore(String link1, String link2) {
		int score = 0;
		String[] slink1 = link1.split("@");
		String link1node1 = slink1[0];

		String linkedge1 = "";
		if (slink1.length > 1) {
			if (slink1[1].startsWith("-")) {
				slink1[1] = slink1[1].replaceAll("-", "");
			}
			linkedge1 = slink1[1];
		}
		String[] slink2 = link2.split("@");
		String link2node1 = slink2[0];
		String linkedge2 = "";
		if (slink2.length > 1) {
			if (slink2[1].startsWith("-")) {
				slink2[1] = slink2[1].replaceAll("-", "");
			}
			linkedge2 = slink2[1];
		}
    //apiを使ってリプレース
		if (hmMatrixNode.containsKey(link1node1 + " " + link2node1)) {
			float score1 = Float.parseFloat(hmMatrixNode.get(link1node1 + " " + link2node1).toString());
			float score2 = 0;
			if (hmMatrixEdge.containsKey(linkedge1 + " " + linkedge2)) {
				score2 = Float.parseFloat(hmMatrixEdge.get(linkedge1 + " " + linkedge2).toString());
				score = (int) (score1 * score2 * 100);
				// System.out.print(score1 +" * " +score2+ " * 100");
				// System.out.println(" = "+score );
			} else if (linkedge1.equals("") && linkedge2.equals("")) {
				score = (int) (score1 * 100);
				// }else{
				// System.out.println(link1node1 + " " + link2node1 +"=
				// "+score1);
				// System.out.println(linkedge1 + " " + linkedge2 +"= "+score2);
			}
		}
		return score;
	}

  public HashMap<String,String> breakDown(String link){
    String[] slink = link.split("@");
		String linkNode = slink[0];
		String elink = slink[1];
    if (elink.startsWith("a1") || elink.startsWith("a2") ){
      linkNode = "a-" + linkNode;
		  elink = elink.substring(3, elink.length());
    }else if (elink.startsWith("b1") || elink.startsWith("a2")){
      linkNode = "b-" + linkNode;
		  elink = elink.substring(3, elink.length());
    }else{ }
    HashMap<String,String> ret = new HashMap<String,String>();
    ret.put("node",linkNode);
    ret.put("edge",elink);
    return ret;
  }

  static public HashMap<String,HashMap<String,String>>  readSimilarity(){

    String value = "0";
    HashMap<String,HashMap<String,String>> ret = new HashMap<String,HashMap<String,String>>();
    try {
      File f = new File("/home/skit/src/gitlab/akihirof0005/paper02/kcam/similarity.txt");
      BufferedReader br = new BufferedReader(new FileReader(f));
      String line;
      while ((line = br.readLine()) != null) {
        String[] data = line.split(",", 0);
        String str1 = data[0].toLowerCase();
        String str2 = data[1].toLowerCase();


        if (ret.containsKey(str1)){
        HashMap<String,String> m = ret.get(str1);
        m.put(str2 , data[2]);
        }else{
        ret.put(str1 , new HashMap<String,String>());
        HashMap<String,String> m = ret.get(str1);
        m.put(str2 , data[2]);
        }
        if (ret.containsKey(str2)){
        HashMap<String,String> m = ret.get(str2);
        ret.get(str2).put(str1 , data[2]);
        m.put(str1 , data[2]);
        }else{
        ret.put(str2 , new HashMap<String,String>());
        HashMap<String,String> m = ret.get(str2);
        ret.get(str2).put(str1 , data[2]);
        m.put(str1 , data[2]);
        }
      }
      br.close();
    } catch (IOException e) {
      System.out.println(e);
    }
    return ret;
  }

  public float getSimilarity(String node1,String node2){
        node1 = node1.replace("lfuc","fuc");
        node1 = node1.replace("lido","ido");
        node1 = node1.replace("galf","gal");
        node1 = node1.replace("araf","ara");
        
        node2 = node2.replace("lfuc","fuc");
        node2 = node2.replace("lido","ido");
        node2 = node2.replace("galf","gal");
        node2 = node2.replace("araf","ara");
//System.out.println(node1 + " " + node2);
    String value = arr.get(node1).get(node2);
    if (value == null){
      value = "0";
      if(node1.equals(node2)){
        value = "1";
      }//else{
       //System.err.println(node1 + " " + node2);
       //}
    }
    return Float.parseFloat(value) * 2;
  }

	public int getscore2(String str1, String str2, int qLayer, int dLayer) {
		float glycanscore = 0;
		int edgescore = 0;

    HashMap<String,String> link1 = breakDown(str1);
    HashMap<String,String> link2 = breakDown(str2);

			float qscore = queryto - q_minusscore * qLayer;
			float dscore = dbto - d_minusscore * dLayer;
			glycanscore += (qscore  + dscore)/2  ;
      glycanscore = (float) glycanscore *  (float) getSimilarity( link1.get("node") , link2.get("node") );
     if(link1.get("edge").equals(link2.get("edge"))){
       edgescore += parenthyd;
     }//else{
      //  System.err.println(link1.get("edge") + " " + link2.get("edge"));
      //}

		int score = (int) glycanscore + edgescore;
		return score;
	}

	public int getscore(String link1, String link2, int qLayer, int dLayer) {
		float glycanscore = 0;
		int edgescore = 0;
		String[] slink1 = link1.split("@");
		String link1node1 = slink1[0];
		String link1node2 = "";
		if (slink1.length > 2) {
			link1node2 = slink1[2];
		}
		String elink1 = slink1[1];
		if (elink1.startsWith("-")) {
			elink1 = elink1.replaceAll("-", "");
		}
		String[] link1edges = elink1.split("-");
		String[] slink2 = link2.split("@");

		String link2node1 = slink2[0];
		String link2node2 = "";
		if (slink2.length > 2) {
			link2node2 = slink2[2];
		}
		String elink2 = slink2[1];
		if (elink2.startsWith("-")) {
			elink2 = elink2.replaceAll("-", "");
		}
		String[] link2edges = elink2.split("-");

		if (link1node1.equals(link2node1)) {
			float qscore = queryto - q_minusscore * qLayer;
			float dscore = dbto - d_minusscore * dLayer;
			glycanscore += qscore / 2 + dscore / 2;
			// score += 35;
		}
		if (link1node2.equals(link2node2)) {
			float qscore = queryto - q_minusscore * qLayer;
			float dscore = dbto - d_minusscore * dLayer;
			glycanscore += qscore / 2 + dscore / 2;
			// score += 35;
		}

		// //////////////////////////
		if (glycanscore == 0 && advanced == 1) { // if mismatch option ga
			// setteisareteitara
			glycanscore -= mismatch_glycan;
		}
		// ////////////////////////////////

		if (link1edges.length == 0 && link2edges.length == 0) {
			// score += 30;
			edgescore += anomerscore + parenthyd + childhyd;
		} else if (link1edges.length == 1 && link2edges.length == 1) {
      if (slink1[1].startsWith("-") && slink2[1].startsWith("-")) {
				if (link1edges[0].equals(link2edges[0])) {
					// score += 10;
					edgescore += parenthyd; // If edge info is parent only ex:
					// -4 vs
					// -3
				}
			} else if (slink1[1].endsWith("-") && slink2[1].endsWith("-")) {
				edgescore = edgeleftscore(edgescore, link1edges, link2edges); // ex:
				// b1-
				// vs a1
			}
		} else if (link1edges.length == 2 && link2edges.length == 2) {
			edgescore = edgeleftscore(edgescore, link1edges, link2edges); // ex:
			// b1-4
			// vs a1-6
			if (link1edges[1].equals(link2edges[1])) {
				// score += 10;
				edgescore += parenthyd;
			}
		} else if (link1edges.length == 0 || link2edges.length == 0) {

		} else {
			String[] smaller;
			String[] larger;
			String smalledge = "";
			if (link1edges.length > link2edges.length) {
				//if (slink2[1].indexOf("-") == slink2[1].length() - 1) {
				//}
				smaller = link2edges;
				larger = link1edges;
				smalledge = slink2[1];
			} else {
				smaller = link1edges;
				larger = link2edges;
				smalledge = slink1[1];
			}
			if (smalledge.startsWith("-")) {
				if (larger[1].indexOf(smaller[0]) != -1) {
					edgescore += parenthyd;
				}
			} else {
				edgescore = edgeleftscore(edgescore, link1edges, link2edges);
			}
			// for (int i = 0; i < 2; i++) {
			// if (larger[i].indexOf(smaller[0]) != -1) {
			// score += 10;
			// break;
			// }
			// }
		}
		// /////////////////////////////////////////////////
		if (edgescore == 0 && advanced == 1) { // if mismatch option ga
			// setteisareteitara
			edgescore -= mismatch_anomer;
		}
		int score = (int) glycanscore + edgescore;
		// ////////////////////////////////////////////////
		return score;
	}

	private int edgeleftscore(int score, String[] link1edges,
			String[] link2edges) {
		if (link1edges[0].length() == link2edges[0].length()) {
			if (link1edges[0].length() == 1 && link1edges[0].charAt(0) == link2edges[0].charAt(0)) {
				if (link1edges[0].equals("a") || link1edges[0].equals("b")) {
					score += anomerscore; // (a)-4 vs (a)-3
				} else {
					score += childhyd; // (1)-4 vs (1)-3
				}
			}
			if (link1edges[0].length() == 2 && link1edges[0].charAt(0) == link2edges[0].charAt(0)) {
				score += anomerscore; // (b)1-4 v (a)1-3
			}
			if (link1edges[0].length() == 2 && link1edges[0].charAt(1) == link2edges[0].charAt(1)) {
				// score += 10;
				score += childhyd; // b(1)-4 v a(1)-3
			}
		} else if (link1edges[0].length() > 0 && link2edges[0].length() > 0) {
			String smaller = "";
			String larger = "";
			if (link1edges[0].length() > link2edges[0].length()) {
				smaller = link2edges[0];
				larger = link1edges[0];
			} else {
				smaller = link1edges[0];
				larger = link2edges[0];
			}
			if (larger.indexOf(smaller) != -1) {
				if (smaller.equals("a") || smaller.equals("b")) {
					score += anomerscore;
				} else {
					score += childhyd;
				}
			}
		}
		return score;
	}

	public String bondmapping(String bondname) {
		String[] bond = bondname.split("@");
		String node1 = "", node2 = "", edge = "";
		if (SugarMap.containsKey(bond[0])) {
			node1 = SugarMap.get(bond[0]).toString();
		} else {
			node1 = bond[0];
		}
		edge = bond[1];
		if (SugarMap.containsKey(bond[2])) {
			node2 = SugarMap.get(bond[2]).toString();
		} else {
			node2 = bond[2];
		}
		if (node2.length() == 0) {
			System.out.println(bondname);
			System.exit(-1);
		}
		String createdbond = node1 + "@" + edge + "@" + node2;

		return createdbond;
	}
}

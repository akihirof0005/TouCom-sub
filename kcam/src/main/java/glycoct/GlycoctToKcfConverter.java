package glycoct;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import kcam.KCaM;

public class GlycoctToKcfConverter {
	public static void main(String query, ArrayList<String> glycoctArray, ArrayList<String> glycanID) {
		// TODO 自動生成されたメソッド・スタブ
		ArrayList<String> kcfArray = new ArrayList<String>();
		ArrayList<String> mathcedStructure = new ArrayList<String>();
		String $strKCF;
		
		
		$strKCF= convert(query);
		System.out.print($strKCF);
		
		for  ( int i = 0; i < glycoctArray.size(); i++){
			kcfArray.add(convert((String) glycoctArray.get(i)));
			String tmpstr = (String) kcfArray.get(i);
			String tmpID = (String) glycanID.get(i);
			String replacedStr = tmpstr.replaceAll("CT-1", tmpID);
			kcfArray.set(i, replacedStr);
		}
	
		KCaM kcam = new KCaM();
		mathcedStructure = kcam.searchOneByAll($strKCF,"", kcfArray);
		for  ( int i = 0; i < mathcedStructure.size(); i++){
			System.out.println(mathcedStructure.get(i));
		}
	}

	public static String convert(String str){
		String filepass = "./GlycoctToKcf2.pm";
		String language = "perl"; 
		String[] cmd = {language,filepass,str};
		String resultStructure = "";
	 try {
	        Runtime runtime = Runtime.getRuntime();
	        Process p = runtime.exec(cmd);
	        InputStream is = p.getInputStream(); 
	        InputStreamReader isr = new InputStreamReader(is);
	        BufferedReader br = new BufferedReader(isr);
	          
			String result;
			while ((result = br.readLine()) != null) {		
				resultStructure = resultStructure + result + "\n";	
			}
	      } catch (IOException ex) {
	        ex.printStackTrace();
	      }
	 return resultStructure;
	}
}

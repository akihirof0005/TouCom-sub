package glycoct;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import kcam.KCaM;

public class GlycoctToKcfConverter2 {

	public static void main (String args[]){
		ArrayList<String> glycoctArray = new ArrayList<String>();
		ArrayList<String> kcfArray = new ArrayList<String>();
		ArrayList<String> glycanID = new ArrayList<String>();
		ArrayList<String> mathcedStructure = new ArrayList<String>();
		
		String str1="RES\n"
				+ "1b:x-dglc-HEX-1:5\n"
				+ "2s:n-acetyl\n"
				+ "3b:b-dgal-HEX-1:5\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:1o(4+1)3d";
		String str2="RES\n"
				+ "1b:a-lglc-HEX-1:5\n"
				+ "2s:n-acetyl\n"
				+ "3b:b-dgal-HEX-1:5\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:1o(4+1)3d";
		String str3="RES\n"
				+ "1b:x-dglc-HEX-1:5\n"
				+ "2s:n-acetyl\n"
				+ "3b:b-dgal-HEX-1:5\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:1o(4+1)3d";
		String str4="RES\n"
				+ "1b:a-dglc-HEX-1:5\n"
				+ "2s:n-acetyl\n"
				+ "3b:b-dgal-HEX-1:5\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:1o(4+1)3d";
		String str5="RES\n"
				+ "1b:x-dglc-HEX-1:5\n"
				+ "2s:n-acetyl\n"
				+ "3b:b-dgal-HEX-1:5\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:1o(4+1)3d";
		String $strKCF;
		
		$strKCF= convert(str1);
		//System.out.print($strKCF);
		
		glycoctArray.add(str2);
		glycoctArray.add(str3);
		glycoctArray.add(str4);
		glycoctArray.add(str5);
		
		glycanID.add("G00322");
		glycanID.add("G00323");
		glycanID.add("G00324");
		glycanID.add("G00325");
	
		for  ( int i = 0; i < glycoctArray.size(); i++){
			kcfArray.add(convert((String) glycoctArray.get(i)));
			String tmpstr = (String) kcfArray.get(i);
			String tmpID = (String) glycanID.get(i);
			String replacedStr = tmpstr.replaceAll("CT-1", tmpID);
			kcfArray.set(i, replacedStr);
			//System.out.print(kcfArray.get(i));
		}
	
		KCaM kcam = new KCaM();
		mathcedStructure = kcam.searchOneByAll($strKCF,"", kcfArray);
		for  ( int i = 0; i < mathcedStructure.size(); i++){
			System.out.println(mathcedStructure.get(i));
		}
	}

	public static String convert(String str){
		String filepass = "./GlycoctToKcf2.pl";
		String language = "perl"; 
		String[] cmd = {language,filepass,str};
		String resultStructure = "";
		
	 try {
	        Runtime runtime = Runtime.getRuntime();
	        Process p = runtime.exec(cmd);
	        InputStream is = p.getInputStream(); 
	        InputStreamReader isr = new InputStreamReader(is);
	        BufferedReader br = new BufferedReader(isr);
	          
			String result;
			while ((result = br.readLine()) != null) {			
				resultStructure = resultStructure + result + "\n";	
			}
	      } catch (IOException ex) {
	        ex.printStackTrace();
	      }
	 return resultStructure;
	}
}
	




